@echo off

set output=%~dp0..\output\build

echo Copying Solution
copy %~dp0SolutionTemplate.sln %output%\ProjectNameToken.sln

echo Copying packages
md %output%\packages
copy %~dp0packages\repositories.config %output%\packages\repositories.config

md %output%\packages\BinaryDependencies
xcopy %~dp0packages\BinaryDependencies\*.* %output%\packages\BinaryDependencies\*.* /I/Y/S/R/Q

md %output%\Data\cms\SiteGlobalFiles

echo Copying project files
xcopy %~dp0*.* %output%\*.* /I/Y/S/R/Q /EXCLUDE:package.exclude
