﻿using System;
using System.Configuration;
using System.Linq.Expressions;
using System.Web.Mvc;
using Creuna.Common.Extensions;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;
using ProjectNameToken.ContentModels.Pages;
using Creuna.Common.EPiServer.Extensions;
using ProjectNameToken.ContentModels.Pages.Base;
using StructureMap;

namespace ProjectNameToken.Orchestrators.Extensions.Mvc
{
    public partial class CmsHelper : Base.HelperBase, ICmsConfiguration
    {
        [DefaultConstructor]
        public CmsHelper()
        {}

        public CmsHelper(ViewContext viewContext)
            : base(viewContext)
        { }

        public virtual FrontPage FrontPage
        {
            get { return ContentReference.StartPage.GetContent<FrontPage>(); }
        }

        public virtual PageData CurrentPage()
        {
            var router = ServiceLocator.Current.GetInstance<PageRouteHelper>();
            var result = router.IfNotNull(x => x.Page);
            return result;
        }

        public virtual TPage CurrentPageAs<TPage>()
            where TPage : class
        {
            var currentPage = CurrentPage();
            var result = currentPage as TPage;
            return result;
        }

        internal virtual TPage GetRequiredFrontPageSetting<TPage>(Func<FrontPage, PageReference> getter, string propertyName)
            where TPage : PageData
        {
            var frontPage = FrontPage;
            var result = frontPage.GetRequiredSetting<TPage>(getter(frontPage), propertyName);
            return result;
        }

        internal virtual TPage GetRequiredFrontPageSetting<TPage>(Expression<Func<FrontPage, PageReference>> memberAccessExpression)
            where TPage : PageData
        {
            var getter = memberAccessExpression.Compile();
            var propertyName = GetParameterNameFromExpression(memberAccessExpression);
            return GetRequiredFrontPageSetting<TPage>(getter, propertyName);
        }

        internal virtual string GetParameterNameFromExpression<TObject, TParameter>(
            Expression<Func<TObject, TParameter>> memberAccessExpression)
        {
            if (memberAccessExpression == null)
            {
                throw new ArgumentNullException("memberAccessExpression");
            }

            var memberExpression = memberAccessExpression.Body as MemberExpression;
            if (memberExpression == null)
            {
                throw new ArgumentException("Expression is not a member access expression", "memberAccessExpression");
            }

            return memberExpression.Member.Name;
        }

        internal virtual int GetFrontPageIntSetting(Func<FrontPage, int?> getter, string appSettingKey, int @default)
        {
            var appSetting = ConfigurationManager.AppSettings[appSettingKey];
            var fallback = appSetting != null ? Convert.ToInt32(appSetting) : @default;

            var result = GetFrontPageSetting(getter, fallback);
            return result;
        }

        internal virtual string GetFrontPageStringSetting(Func<FrontPage, string> getter, string appSettingKey, string @default)
        {
            var appSetting = ConfigurationManager.AppSettings[appSettingKey];
            var fallback = appSetting ?? @default;

            var result = GetFrontPageSetting(getter, fallback);
            return result;
        }

        internal virtual TValue GetFrontPageSetting<TValue>(Func<FrontPage, TValue> getter, TValue @default = null)
            where TValue : class
        {
            var frontPage = FrontPage;
            var result = getter(frontPage) ?? @default;
            return result;
        }

        internal virtual TValue GetFrontPageSetting<TValue>(Func<FrontPage, TValue?> getter, TValue @default)
            where TValue : struct
        {
            var frontPage = FrontPage;
            var result = getter(frontPage).GetValueOrDefault(@default);
            return result;
        }
    }
}
