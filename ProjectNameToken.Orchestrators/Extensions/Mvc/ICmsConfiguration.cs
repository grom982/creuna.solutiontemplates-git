﻿using ProjectNameToken.ContentModels.Pages;

namespace ProjectNameToken.Orchestrators.Extensions.Mvc
{
    public interface ICmsConfiguration
    {
        FrontPage FrontPage { get; }
    }
}