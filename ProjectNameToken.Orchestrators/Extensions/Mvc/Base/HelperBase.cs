﻿using System.Web.Mvc;

namespace ProjectNameToken.Orchestrators.Extensions.Mvc.Base
{
    public partial class HelperBase
    {
        public HelperBase()
        {}

        private readonly ViewContext _viewContext;
        public HelperBase(ViewContext viewContext)
        {
            _viewContext = viewContext;
        }

        public virtual ViewContext ViewContext
        {
            get { return _viewContext; }
        }
    }
}
