﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectNameToken.Orchestrators.Extensions.Mvc
{
    public static class ViewDataExtensions
    {
        public static ViewDataDictionary ToViewDataDictionary(this object data)
        {
            var values = new RouteValueDictionary(data);

            var result = new ViewDataDictionary();

            values.ToList().ForEach(result.Add);

            return result;
        }
    }
}