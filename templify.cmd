@echo off
set Templify=C:\Program Files (x86)\endjin\Templify\Framework\TemplifyCmd.exe

rem set output=%~dp0
set version=1.0.1-rc5

set cmd="%Templify%" -m c -p "build" -n "Creuna.Templates.EPiServer7" -t "ProjectNameToken=__NAME__" -a "Creuna Norge (Kharkiv Office)" -v "%version%" -r "."

echo %cmd% 
call %cmd%
