﻿using EPiServer.Core;
using ProjectNameToken.Orchestrators.Extensions.Mvc;
using StructureMap.Attributes;

namespace ProjectNameToken.Controllers.Base
{
    public partial class PageController<TPage> : EPiServer.Web.Mvc.PageController<TPage>
        where TPage : PageData
    {
        [SetterProperty]
        public virtual CmsHelper Cms { get; set; }
    }
}