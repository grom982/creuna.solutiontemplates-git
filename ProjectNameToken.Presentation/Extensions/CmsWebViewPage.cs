﻿using System.Web.Mvc;
using ProjectNameToken.Orchestrators.Extensions.Mvc;

namespace ProjectNameToken.Extensions
{
    public abstract class CmsViewPage<TModel> : WebViewPage<TModel>
    {
        public override void InitHelpers()
        {
            base.InitHelpers();

            Cms = new CmsHelper(ViewContext);
        }

        public virtual CmsHelper Cms { get; private set; }
    }
}