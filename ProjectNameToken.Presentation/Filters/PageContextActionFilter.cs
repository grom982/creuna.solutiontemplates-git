﻿using System.Web.Mvc;
using ProjectNameToken.Common;
using ProjectNameToken.Common.Contracts;
using ProjectNameToken.ContentModels.Pages;
using ProjectNameToken.ViewModels.Pages;

namespace $safeprojectname$.Filters
{
    /// <summary>
    /// Intercepts controller methods at the end of execution - use this to modify all view results. 
    /// Can be used to insert master page data for instance. 
    /// </summary>
    public class PageContextActionFilter : IResultFilter
    {
        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model;

            var model = viewModel as IPageViewModel<PageDataBase>;
            if (model != null)
            {
                // Fill any layout/master data properties here. 

                model.SiteSettings = Locate<ISiteSettings>.Instance;
            }
        }

        public void OnResultExecuted(ResultExecutedContext filterContext) { }
    }
}