﻿using System.Web.Mvc;

namespace ProjectNameToken
{
    public class Global : EPiServer.Global
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
        }
    }
}