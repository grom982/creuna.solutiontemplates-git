using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Mvc;
using Creuna.IoC.StructureMap.AutoScan;
using Creuna.IoC.StructureMap.AutoScan.Configuration;
using StructureMap;
using WebActivatorEx;
using ProjectNameToken.App_Start;
using ProjectNameToken.DependencyResolution;

[assembly: PreApplicationStartMethod(typeof(AutoScan), "OnBeforeAppStart")]
[assembly: PostApplicationStartMethod(typeof(AutoScan), "OnAfterAppStart")]

namespace ProjectNameToken.App_Start
{
    public class AutoScan
    {
        public static void OnBeforeAppStart()
        {
            var scanner = new AutoScan();
            scanner.ScanAndConfigure();
        }

        public static void OnAfterAppStart()
        {
        }
		
        private readonly AutoScanSection _configuration;
        private IEnumerable<Regex> _includes;
        private IEnumerable<Regex> _excludes;

        public AutoScan()
        {
            _configuration = Give.Me.Configuration();
        }

        public virtual AutoScanSection Configuration
        {
            get { return _configuration; }
        }

        internal virtual bool IsMatch(string assemblyName, IEnumerable<Regex> patterns)
        {
            if (string.IsNullOrEmpty(assemblyName))
            {
                return false;
            }

            return patterns.Any(x => x.IsMatch(assemblyName));
        }

        internal virtual bool ScanAssemblyPredicate(Assembly assembly)
        {
            bool isIncluded = IsMatch(assembly.FullName, _includes);
            bool isExcluded = IsMatch(assembly.FullName, _excludes);

            return isIncluded && (!isExcluded);
        }

        public virtual void ScanAndConfigure()
        {
            _includes = Configuration.Includes.Select(x => new Regex(x, RegexOptions.IgnoreCase));
            _excludes = Configuration.Excludes.Select(x => new Regex(x, RegexOptions.IgnoreCase));

            ObjectFactory.Initialize(x => x.Scan(scanner =>
            {
                scanner.AssembliesFromApplicationBaseDirectory(ScanAssemblyPredicate);
                scanner.WithDefaultConventions();
                scanner.LookForRegistries();
            }));

            var container = ObjectFactory.Container;
//#if DEBUG
//            // doesn't work for Http related instance scopes, commenting out
//            ObjectFactory.AssertConfigurationIsValid();
//            System.Diagnostics.Trace.WriteLine(container.WhatDoIHave());
//#endif
            DependencyResolver.SetResolver(new StructureMapDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new StructureMapDependencyResolver(container);
        }		
    }
}
