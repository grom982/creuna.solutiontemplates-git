/****** Object:  Role [aspnet_Membership_BasicAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE ROLE [aspnet_Membership_BasicAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Membership_ReportingAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE ROLE [aspnet_Membership_ReportingAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Membership_FullAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE ROLE [aspnet_Membership_FullAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Personalization_BasicAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE ROLE [aspnet_Personalization_BasicAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Personalization_ReportingAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE ROLE [aspnet_Personalization_ReportingAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Personalization_FullAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE ROLE [aspnet_Personalization_FullAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Profile_BasicAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE ROLE [aspnet_Profile_BasicAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Profile_ReportingAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE ROLE [aspnet_Profile_ReportingAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Profile_FullAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE ROLE [aspnet_Profile_FullAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Roles_BasicAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE ROLE [aspnet_Roles_BasicAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Roles_ReportingAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE ROLE [aspnet_Roles_ReportingAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_Roles_FullAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE ROLE [aspnet_Roles_FullAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [aspnet_WebEvent_FullAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE ROLE [aspnet_WebEvent_FullAccess] AUTHORIZATION [dbo]
GO
/****** Object:  Role [state_persistence_users]    Script Date: 12/06/2012 12:38:55 ******/
CREATE ROLE [state_persistence_users] AUTHORIZATION [dbo]
GO
/****** Object:  Schema [state_persistence_users]    Script Date: 12/06/2012 12:38:55 ******/
CREATE SCHEMA [state_persistence_users] AUTHORIZATION [state_persistence_users]
GO
/****** Object:  Schema [aspnet_WebEvent_FullAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE SCHEMA [aspnet_WebEvent_FullAccess] AUTHORIZATION [aspnet_WebEvent_FullAccess]
GO
/****** Object:  Schema [aspnet_Roles_ReportingAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE SCHEMA [aspnet_Roles_ReportingAccess] AUTHORIZATION [aspnet_Roles_ReportingAccess]
GO
/****** Object:  Schema [aspnet_Roles_FullAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE SCHEMA [aspnet_Roles_FullAccess] AUTHORIZATION [aspnet_Roles_FullAccess]
GO
/****** Object:  Schema [aspnet_Roles_BasicAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE SCHEMA [aspnet_Roles_BasicAccess] AUTHORIZATION [aspnet_Roles_BasicAccess]
GO
/****** Object:  Schema [aspnet_Profile_ReportingAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE SCHEMA [aspnet_Profile_ReportingAccess] AUTHORIZATION [aspnet_Profile_ReportingAccess]
GO
/****** Object:  Schema [aspnet_Profile_FullAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE SCHEMA [aspnet_Profile_FullAccess] AUTHORIZATION [aspnet_Profile_FullAccess]
GO
/****** Object:  Schema [aspnet_Profile_BasicAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE SCHEMA [aspnet_Profile_BasicAccess] AUTHORIZATION [aspnet_Profile_BasicAccess]
GO
/****** Object:  Schema [aspnet_Personalization_ReportingAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE SCHEMA [aspnet_Personalization_ReportingAccess] AUTHORIZATION [aspnet_Personalization_ReportingAccess]
GO
/****** Object:  Schema [aspnet_Personalization_FullAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE SCHEMA [aspnet_Personalization_FullAccess] AUTHORIZATION [aspnet_Personalization_FullAccess]
GO
/****** Object:  Schema [aspnet_Personalization_BasicAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE SCHEMA [aspnet_Personalization_BasicAccess] AUTHORIZATION [aspnet_Personalization_BasicAccess]
GO
/****** Object:  Schema [aspnet_Membership_ReportingAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE SCHEMA [aspnet_Membership_ReportingAccess] AUTHORIZATION [aspnet_Membership_ReportingAccess]
GO
/****** Object:  Schema [aspnet_Membership_FullAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE SCHEMA [aspnet_Membership_FullAccess] AUTHORIZATION [aspnet_Membership_FullAccess]
GO
/****** Object:  Schema [aspnet_Membership_BasicAccess]    Script Date: 12/06/2012 12:38:55 ******/
CREATE SCHEMA [aspnet_Membership_BasicAccess] AUTHORIZATION [aspnet_Membership_BasicAccess]
GO
/****** Object:  Table [dbo].[tblWindowsUser]    Script Date: 12/06/2012 12:38:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblWindowsUser](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](255) NOT NULL,
	[LoweredUserName] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_tblWindowsUser] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblWindowsUser_Unique] ON [dbo].[tblWindowsUser] 
(
	[LoweredUserName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblWindowsUser] ON
INSERT [dbo].[tblWindowsUser] ([pkID], [UserName], [LoweredUserName]) VALUES (1, N'vladimir.levchuk', N'vladimir.levchuk')
SET IDENTITY_INSERT [dbo].[tblWindowsUser] OFF
/****** Object:  Table [dbo].[tblAccessType]    Script Date: 12/06/2012 12:38:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblAccessType](
	[pkID] [int] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_tblAccessType] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[tblAccessType] ([pkID], [Name]) VALUES (1, N'Read')
INSERT [dbo].[tblAccessType] ([pkID], [Name]) VALUES (2, N'Create')
INSERT [dbo].[tblAccessType] ([pkID], [Name]) VALUES (4, N'Edit')
INSERT [dbo].[tblAccessType] ([pkID], [Name]) VALUES (8, N'Delete')
INSERT [dbo].[tblAccessType] ([pkID], [Name]) VALUES (16, N'Publish')
INSERT [dbo].[tblAccessType] ([pkID], [Name]) VALUES (32, N'Administer')
/****** Object:  StoredProcedure [dbo].[sp_FxDatabaseVersion]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_FxDatabaseVersion]
AS
	RETURN 6207
GO
/****** Object:  StoredProcedure [dbo].[sp_DatabaseVersion]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_DatabaseVersion]
AS
	RETURN 7003
GO
/****** Object:  Table [dbo].[tblSiteConfig]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSiteConfig](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[SiteID] [varchar](250) NOT NULL,
	[PropertyName] [varchar](250) NOT NULL,
	[PropertyValue] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_tblSiteConfig] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_tblSiteConfig] ON [dbo].[tblSiteConfig] 
(
	[SiteID] ASC,
	[PropertyName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblSiteConfig] ON
INSERT [dbo].[tblSiteConfig] ([pkID], [SiteID], [PropertyName], [PropertyValue]) VALUES (1, N'Spekter7', N'UniqueId', N'71197c0d-1bec-4d14-8221-8b7a85c7b1fc')
INSERT [dbo].[tblSiteConfig] ([pkID], [SiteID], [PropertyName], [PropertyValue]) VALUES (2, N'Spekter7', N'WebConfig', N'<configuration><configSections><section name="workflowRuntime" type="System.Workflow.Runtime.Configuration.WorkflowRuntimeSection, System.Workflow.Runtime, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" /><section name="episerver" type="EPiServer.Configuration.EPiServerSection, EPiServer.Configuration, Version=7.0.586.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7" restartOnExternalChanges="true" /><section name="episerver.baseLibrary" allowDefinition="MachineToApplication" allowLocation="false" type="EPiServer.BaseLibrary.ConfigurationHandler,EPiServer.BaseLibrary" /><sectionGroup name="episerverModules"></sectionGroup><section name="episerver.shell" type="EPiServer.Shell.Configuration.EPiServerShellSection, EPiServer.Shell" /><section name="episerver.framework" type="EPiServer.Framework.Configuration.EPiServerFrameworkSection, EPiServer.Framework" restartOnExternalChanges="true" /><section name="episerver.search" type="EPiServer.Search.Configuration.SearchSection, EPiServer.ApplicationModules" /><section name="episerver.packaging" type="EPiServer.Packaging.Configuration.EPiServerPackagingSection, EPiServer.Packaging" /><section name="episerver.search.indexingservice" type="EPiServer.Search.IndexingService.Configuration.IndexingServiceSection, EPiServer.Search.IndexingService" /></configSections><episerver xmlns="http://EPiServer.Configuration.EPiServerSection"><workflowSettings><workflowHost type="EPiServer.WorkflowFoundation.AspNetWorkflowManager,EPiServer.WorkflowFoundation" /><definitions><!-- definition:           Workflow definitions that should be predefined, that is if no definition with
                                     specified type exists it will be created--><!-- 
              <definition name="Sequential Approval"    description="A sequential approval workflow for pages"          type="EPiServer.WorkflowFoundation.Workflows.SequentialApproval,EPiServer.WorkflowFoundation"/>
              <definition name="Parallel Approval"      description="A paralell approval workflow for pages"            type="EPiServer.WorkflowFoundation.Workflows.ParallelApproval,EPiServer.WorkflowFoundation"/>
              <definition name="Request for feedback"   description="Assigns request for feedback tasks to users/roles" type="EPiServer.WorkflowFoundation.Workflows.RequestForFeedback,EPiServer.WorkflowFoundation"/>
              <definition name="Ready for translation"  description="Assigns translation tasks to users/roles"          type="EPiServer.WorkflowFoundation.Workflows.ReadyForTranslation,EPiServer.WorkflowFoundation"/>
        --></definitions><externalServices><!-- externalService:      Custom services that is to be registered with workflow runtime--><externalService type="EPiServer.WorkflowFoundation.Workflows.ApprovalService,EPiServer.WorkflowFoundation" /><externalService type="EPiServer.WorkflowFoundation.Workflows.ReadyForTranslationService,EPiServer.WorkflowFoundation" /><externalService type="EPiServer.WorkflowFoundation.Workflows.RequestForFeedbackService,EPiServer.WorkflowFoundation" /></externalServices><references><!-- reference:            References for xoml based workflows, used at compiling of xoml based workflows--><!-- reference path="C:\Inetpub\wwwroot\mysiste\bin\customdependency.dll" /--></references></workflowSettings><sites><site description="Spekter7" siteId="Spekter7"><siteSettings httpCacheability="Public" httpCacheVaryByCustom="path" httpCacheVaryByParams="id,epslanguage" pageFolderVirtualPathProvider="SitePageFiles" pageRootId="1" pageStartId="1" pageValidateTemplate="false" pageWastebasketId="2" globalBlockFolderId="3" siteBlockFolderId="3" uiShowGlobalizationUserInterface="true" urlRebaseKind="ToRootRelative" siteDisplayName="Spekter7" siteUrl="http://localhost:9009" uiUrl="~/EPiServer/CMS/" utilUrl="~/util/" /></site></sites></episerver><episerver.baseLibrary><classFactories><add type="EPiServer.Implementation.DefaultBaseLibraryFactory, EPiServer.Implementation" id="ContentChannelFactory"><register type="EPiServer.ContentChannels.ContentChannelAdministration, EPiServer" mappedType="EPiServer.ContentChannels.ObjectStoreContentChannelAdministration, EPiServer" /><register type="EPiServer.BaseLibrary.IContentChannelHandler, EPiServer" mappedType="EPiServer.ContentChannels.ContentChannelHandler, EPiServer" /></add><add type="EPiServer.Core.PropertyControlClassFactory, EPiServer" id="PropertyControlFactory"></add></classFactories></episerver.baseLibrary><workflowRuntime EnablePerformanceCounters="false"><Services><add type="System.Workflow.Runtime.Hosting.DefaultWorkflowSchedulerService, System.Workflow.Runtime, Version=3.0.00000.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" maxSimultaneousWorkflows="5" /><add type="System.Workflow.Runtime.Hosting.SharedConnectionWorkflowCommitWorkBatchService, System.Workflow.Runtime, Version=3.0.00000.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" /><add type="System.Workflow.Runtime.Hosting.SqlWorkflowPersistenceService, System.Workflow.Runtime, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" UnloadOnIdle="true" /></Services></workflowRuntime><runtime><assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1"><probing privatePath="modulesbin" /><dependentAssembly><assemblyIdentity name="EPiServer.ApplicationModules" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="1.0.0.0-7.0.859.1" newVersion="7.0.859.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.Data.Cache" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="1.0.0.0-7.0.859.1" newVersion="7.0.859.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.Data" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="1.0.0.0-7.0.859.1" newVersion="7.0.859.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.Events" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="1.0.0.0-7.0.859.1" newVersion="7.0.859.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.Framework" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="1.0.0.0-7.0.859.1" newVersion="7.0.859.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.Licensing" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="1.0.0.0-7.0.859.1" newVersion="7.0.859.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.Shell" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="1.0.0.0-7.0.859.1" newVersion="7.0.859.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35" culture="neutral" /><bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="4.0.0.0" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.BaseLibrary" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="5.2.375.0-7.65535.65535.65535" newVersion="7.0.586.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.Blog" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="5.2.375.0-7.65535.65535.65535" newVersion="7.0.586.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.Configuration" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="5.2.375.0-7.65535.65535.65535" newVersion="7.0.586.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="5.2.375.0-7.65535.65535.65535" newVersion="7.0.586.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.Enterprise" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="5.2.375.0-7.65535.65535.65535" newVersion="7.0.586.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.ImageLibrary" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="5.2.375.0-7.65535.65535.65535" newVersion="7.0.586.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.Implementation" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="5.2.375.0-7.65535.65535.65535" newVersion="7.0.586.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.LinkAnalyzer" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="5.2.375.0-7.65535.65535.65535" newVersion="7.0.586.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.Scheduler" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="5.2.375.0-7.65535.65535.65535" newVersion="7.0.586.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.Scheduler.WKTL" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="5.2.375.0-7.65535.65535.65535" newVersion="5.1.422.4" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.UI" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="5.2.375.0-7.65535.65535.65535" newVersion="7.0.586.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.Web.WebControls" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="5.2.375.0-7.65535.65535.65535" newVersion="7.0.586.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.WebDav" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="5.2.375.0-7.65535.65535.65535" newVersion="7.0.586.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.WebParts" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="5.2.375.0-7.65535.65535.65535" newVersion="7.0.586.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.WorkflowFoundation" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="5.2.375.0-7.65535.65535.65535" newVersion="7.0.586.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.XForms" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="5.2.375.0-7.65535.65535.65535" newVersion="7.0.586.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.XmlRpc" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="5.2.375.0-7.65535.65535.65535" newVersion="7.0.586.1" /></dependentAssembly><dependentAssembly><assemblyIdentity name="Castle.Core" publicKeyToken="407dd0808d44fbdc" culture="neutral" /><bindingRedirect oldVersion="3.0.0.0-3.65535.65535.65535" newVersion="3.0.0.0" /></dependentAssembly><dependentAssembly><assemblyIdentity name="Castle.Windsor" publicKeyToken="407dd0808d44fbdc" culture="neutral" /><bindingRedirect oldVersion="3.0.0.0-3.65535.65535.65535" newVersion="3.0.0.0" /></dependentAssembly><dependentAssembly><assemblyIdentity name="log4net" publicKeyToken="1b44e1d426115821" culture="neutral" /><bindingRedirect oldVersion="1.0.0.0-1.65535.65535.65535" newVersion="1.2.10.0" /></dependentAssembly><dependentAssembly><assemblyIdentity name="EPiServer.Search.IndexingService" publicKeyToken="8fe83dea738b45b7" culture="neutral" /><bindingRedirect oldVersion="1.0.517.262-6.65535.65535.65535" newVersion="7.0.1764.1" /></dependentAssembly></assemblyBinding></runtime><!-- Connection string info is stored in a separate file --><connectionStrings><clear /><add name="EPiServerDB" connectionString="Data Source=.\SQLEXPRESS;Initial Catalog=dbSpekter7;Integrated Security=False;User ID=dbUserdbSpekter7;Password=-07A73t718;MultipleActiveResultSets=True;Connect Timeout=10" providerName="System.Data.SqlClient" /></connectionStrings><system.web><roleManager configSource="configs\roleManager.config" /><membership configSource="configs\membership.config" /><profile enabled="true" defaultProvider="SqlProfile" automaticSaveEnabled="true"><properties><add name="Address" type="System.String" /><add name="ZipCode" type="System.String" /><add name="Locality" type="System.String" /><add name="Email" type="System.String" /><add name="FirstName" type="System.String" /><add name="LastName" type="System.String" /><add name="Language" type="System.String" /><add name="Country" type="System.String" /><add name="Company" type="System.String" /><add name="Title" type="System.String" /><add name="SubscriptionInfo" type="EPiServer.Personalization.SubscriptionInfo, EPiServer" /><add name="CustomExplorerTreePanel" type="System.String" /><add name="FileManagerFavourites" type="System.Collections.Generic.List`1[System.String]" /><add name="EditTreeSettings" type="EPiServer.Personalization.GuiSettings, EPiServer" /><add name="ClientToolsActivationKey" type="System.String" /><add name="FrameworkName" type="System.String" /></properties><providers><clear /><add name="SqlProfile" type="System.Web.Profile.SqlProfileProvider, System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" connectionStringName="EPiServerDB" applicationName="EPiServerSample" /></providers></profile><compilation configSource="configs\compilationConfig.config" /><customErrors mode="RemoteOnly" /><authentication mode="Forms"><forms name=".EPiServerLogin" loginUrl="Util/login.aspx" timeout="120" defaultUrl="~/" /></authentication><globalization culture="en-US" uiCulture="en" requestEncoding="utf-8" responseEncoding="utf-8" resourceProviderFactoryType="EPiServer.Framework.Localization.LocalizationServiceResourceProviderFactory, EPiServer.Framework" /><httpRuntime requestValidationMode="2.0" /><pages validateRequest="false" enableEventValidation="true" pageParserFilterType="System.Web.Mvc.ViewTypeParserFilter, System.Web.Mvc, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"><controls><add tagPrefix="EPiServer" namespace="EPiServer.Web.WebControls" assembly="EPiServer" /><add tagPrefix="EPiServer" namespace="EPiServer.Web.WebControls" assembly="EPiServer.Web.WebControls" /><add tagPrefix="XForms" namespace="EPiServer.XForms.WebControls" assembly="EPiServer.XForms" /><add tagPrefix="EPiServer" namespace="EPiServer.Framework.Web.WebControls" assembly="EPiServer.Framework" /></controls><namespaces><add namespace="System.Web.Mvc" /><add namespace="System.Web.Mvc.Ajax" /><add namespace="System.Web.Mvc.Html" /><add namespace="System.Web.Routing" /><add namespace="System.Linq" /><add namespace="System.Collections.Generic" /><add namespace="EPiServer.Shell.Web.Mvc.Html" /><add namespace="System.Web.Helpers" /><add namespace="System.Web.WebPages" /><add namespace="EPiServer.Framework.Web.Mvc.Html" /><add namespace="EPiServer.Web.Mvc.Html" /></namespaces></pages><caching><outputCacheSettings><outputCacheProfiles><add name="ClientResourceCache" enabled="true" duration="3600" varyByParam="*" varyByContentEncoding="gzip;deflate" /></outputCacheProfiles></outputCacheSettings></caching></system.web><system.webServer><modules runAllManagedModulesForAllRequests="true"><add name="InitializationModule" type="EPiServer.Framework.Initialization.InitializationModule, EPiServer.Framework" preCondition="managedHandler" /><add name="UrlRewriteModule" type="EPiServer.Web.RoutingUrlRewriteModule, EPiServer" preCondition="managedHandler" /><add name="ShellRoutingModule" type="EPiServer.Shell.Web.Routing.ShellRoutingModule, EPiServer.Shell" /><remove name="WebDAVModule" /></modules><handlers><add name="UrlRoutingHandler" preCondition="integratedMode" verb="*" path="UrlRouting.axd" type="System.Web.HttpForbiddenHandler, System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" /></handlers><staticContent><clientCache cacheControlMode="UseMaxAge" cacheControlMaxAge="1.00:00:00"></clientCache></staticContent><caching><profiles><add extension=".gif" policy="DontCache" kernelCachePolicy="CacheUntilChange" duration="0.00:01:00" location="Any" /><add extension=".png" policy="DontCache" kernelCachePolicy="CacheUntilChange" duration="0.00:01:00" location="Any" /><add extension=".js" policy="DontCache" kernelCachePolicy="CacheUntilChange" duration="0.00:01:00" location="Any" /><add extension=".css" policy="DontCache" kernelCachePolicy="CacheUntilChange" duration="0.00:01:00" location="Any" /><add extension=".jpg" policy="DontCache" kernelCachePolicy="CacheUntilChange" duration="0.00:01:00" location="Any" /><add extension=".jpeg" policy="DontCache" kernelCachePolicy="CacheUntilChange" duration="0.00:01:00" location="Any" /></profiles></caching></system.webServer><location path="EPiServer"><system.web><httpRuntime maxRequestLength="1000000" requestValidationMode="2.0" /><pages enableEventValidation="true" enableViewState="true" enableSessionState="true" enableViewStateMac="true"><controls><add tagPrefix="EPiServerUI" namespace="EPiServer.UI.WebControls" assembly="EPiServer.UI" /><add tagPrefix="EPiServerScript" namespace="EPiServer.ClientScript.WebControls" assembly="EPiServer" /><add tagPrefix="EPiServerScript" namespace="EPiServer.UI.ClientScript.WebControls" assembly="EPiServer.UI" /></controls></pages><globalization requestEncoding="utf-8" responseEncoding="utf-8" /><authorization><allow roles="WebEditors, WebAdmins, Administrators" /><deny users="*" /></authorization></system.web><system.webServer><handlers><clear /><!-- This section is copied from applicationhost.config --><add name="AssemblyResourceLoader-Integrated-4.0" path="WebResource.axd" verb="GET,DEBUG" type="System.Web.Handlers.AssemblyResourceLoader" preCondition="integratedMode,runtimeVersionv4.0" /><add name="PageHandlerFactory-Integrated-4.0" path="*.aspx" verb="GET,HEAD,POST,DEBUG" type="System.Web.UI.PageHandlerFactory" preCondition="integratedMode,runtimeVersionv4.0" /><add name="SimpleHandlerFactory-Integrated-4.0" path="*.ashx" verb="GET,HEAD,POST,DEBUG" type="System.Web.UI.SimpleHandlerFactory" preCondition="integratedMode,runtimeVersionv4.0" /><add name="WebServiceHandlerFactory-Integrated-4.0" path="*.asmx" verb="GET,HEAD,POST,DEBUG" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" preCondition="integratedMode,runtimeVersionv4.0" /><add name="svc-Integrated-4.0" path="*.svc" verb="*" type="System.ServiceModel.Activation.ServiceHttpHandlerFactory, System.ServiceModel.Activation, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" preCondition="integratedMode,runtimeVersionv4.0" /><add name="wildcard" path="*" verb="*" type="EPiServer.Web.StaticFileHandler, EPiServer.Framework" /></handlers></system.webServer></location><location path="EPiServer/CMS/admin"><system.web><authorization><allow roles="WebAdmins, Administrators" /><deny users="*" /></authorization></system.web></location><location path="WebServices"><system.web><httpRuntime maxRequestLength="1000000" /><authorization><allow roles="WebServices,Administrators" /><deny users="*" /></authorization></system.web><system.webServer><handlers><clear /><add name="AssemblyResourceLoader-Integrated-4.0" path="WebResource.axd" verb="GET,DEBUG" type="System.Web.Handlers.AssemblyResourceLoader" preCondition="integratedMode,runtimeVersionv4.0" /><add name="WebServiceHandlerFactory-Integrated-4.0" path="*.asmx" verb="GET,HEAD,POST,DEBUG" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" preCondition="integratedMode,runtimeVersionv4.0" /><add name="svc-Integrated-4.0" path="*.svc" verb="*" type="System.ServiceModel.Activation.ServiceHttpHandlerFactory, System.ServiceModel.Activation, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" preCondition="integratedMode,runtimeVersionv4.0" /><add name="wildcard" path="*" verb="*" type="EPiServer.Web.StaticFileHandler, EPiServer.Framework" /></handlers></system.webServer></location><location path="util"><system.web><pages enableEventValidation="true"><controls><add tagPrefix="EPiServerUI" namespace="EPiServer.UI.WebControls" assembly="EPiServer.UI" /><add tagPrefix="EPiServerScript" namespace="EPiServer.ClientScript.WebControls" assembly="EPiServer" /></controls></pages><globalization requestEncoding="utf-8" responseEncoding="utf-8" /></system.web><system.webServer><handlers><clear /><add name="AssemblyResourceLoader-Integrated-4.0" path="WebResource.axd" verb="GET,DEBUG" type="System.Web.Handlers.AssemblyResourceLoader" preCondition="integratedMode,runtimeVersionv4.0" /><add name="PageHandlerFactory-Integrated-4.0" path="*.aspx" verb="GET,HEAD,POST,DEBUG" type="System.Web.UI.PageHandlerFactory" preCondition="integratedMode,runtimeVersionv4.0" /><add name="SimpleHandlerFactory-Integrated-4.0" path="*.ashx" verb="GET,HEAD,POST,DEBUG" type="System.Web.UI.SimpleHandlerFactory" preCondition="integratedMode,runtimeVersionv4.0" /><add name="WebServiceHandlerFactory-Integrated-4.0" path="*.asmx" verb="GET,HEAD,POST,DEBUG" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" preCondition="integratedMode,runtimeVersionv4.0" /><add name="wildcard" path="*" verb="*" type="EPiServer.Web.StaticFileHandler, EPiServer.Framework" /></handlers></system.webServer></location><episerver.framework><scanAssembly forceBinFolderScan="true" /><siteHostMapping><siteHosts siteId="Spekter7"><clear /><add name="localhost:9009" language="no" /><add name="spekter7.local" language="no" /><add name="*" /></siteHosts></siteHostMapping><licensing licenseFilePath="Licenses/License.config" /><automaticSiteMapping><add key="/LM/W3SVC/32/ROOT:PC-LEV" siteId="Spekter7" /></automaticSiteMapping><virtualPathProviders><clear /><add virtualPath="~/EPiServer/" physicalPath="[appDataPath]\Modules" name="ProtectedAddons" type="EPiServer.Web.Hosting.VirtualPathNonUnifiedProvider, EPiServer.Framework" /><add showInFileManager="true" virtualName="Page Files" virtualPath="~/PageFiles/" bypassAccessCheck="false" indexingServiceCatalog="Web" useRouting="true" customFileSummary="~/FileSummary.config" name="SitePageFiles" type="EPiServer.Web.Hosting.VirtualPathVersioningProvider, EPiServer" /><add showInFileManager="true" virtualName="Global Files" virtualPath="~/Global/" bypassAccessCheck="false" indexingServiceCatalog="Web" useRouting="true" customFileSummary="~/FileSummary.config" name="SiteGlobalFiles" type="EPiServer.Web.Hosting.VirtualPathVersioningProvider, EPiServer" /><add showInFileManager="true" virtualName="Documents" virtualPath="~/Documents/" bypassAccessCheck="false" maxVersions="5" useRouting="true" customFileSummary="~/FileSummary.config" name="SiteDocuments" type="EPiServer.Web.Hosting.VirtualPathVersioningProvider, EPiServer" /><add virtualPath="~/App_Themes/Default/" physicalPath="C:\Program Files (x86)\EPiServer\CMS\7.0.586.1\application\App_Themes\Default" useRouting="true" name="App_Themes_Default" type="EPiServer.Web.Hosting.VirtualPathNonUnifiedProvider, EPiServer.Framework" /><add virtualPath="~/EPiServer/CMS/" physicalPath="C:\Program Files (x86)\EPiServer\CMS\7.0.586.1\application\UI\CMS" name="UI" type="EPiServer.Web.Hosting.VirtualPathNonUnifiedProvider, EPiServer.Framework" /><add virtualPath="~/Util/" physicalPath="C:\Program Files (x86)\EPiServer\CMS\7.0.586.1\application\util" name="UtilFiles" type="EPiServer.Web.Hosting.VirtualPathNonUnifiedProvider, EPiServer.Framework" /><add virtualPath="~/WebServices/" physicalPath="C:\Program Files (x86)\EPiServer\CMS\7.0.586.1\application\webservices" name="WebServiceFiles" type="EPiServer.Web.Hosting.VirtualPathNonUnifiedProvider, EPiServer.Framework" /></virtualPathProviders><virtualRoles replacePrincipal="true"><providers><add name="Administrators" type="EPiServer.Security.WindowsAdministratorsRole, EPiServer.Framework" /><add name="Everyone" type="EPiServer.Security.EveryoneRole, EPiServer.Framework" /><add name="Authenticated" type="EPiServer.Security.AuthenticatedRole, EPiServer.Framework" /><add name="Anonymous" type="EPiServer.Security.AnonymousRole, EPiServer.Framework" /><add roles="WebAdmins, Administrators" mode="Any" name="PackagingAdmins" type="EPiServer.Security.MappedRole, EPiServer.Framework" /><add roles="WebAdmins, Administrators" mode="Any" name="CmsAdmins" type="EPiServer.Security.MappedRole, EPiServer.Framework" /><add roles="WebEditors" mode="Any" name="CmsEditors" type="EPiServer.Security.MappedRole, EPiServer.Framework" /><add name="Creator" type="EPiServer.Security.CreatorRole, EPiServer" /></providers></virtualRoles><geolocation defaultProvider="maxmind"><providers><add databaseFileName="C:\Program Files (x86)\EPiServer\Framework\7.0.859.1\Geolocation\GeoLiteCity.dat" name="maxmind" type="EPiServer.Personalization.Providers.MaxMind.GeolocationProvider, EPiServer.ApplicationModules" /></providers></geolocation><appData basePath="..\Data\cms" /></episerver.framework><episerver.shell><publicModules rootPath="~/modules/" autoDiscovery="Modules" /><protectedModules rootPath="~/EPiServer/"></protectedModules></episerver.shell><system.serviceModel><extensions><bindingElementExtensions><add name="udpTransportCustom" type="Microsoft.ServiceModel.Samples.UdpTransportElement, EPiServer.Events" /></bindingElementExtensions></extensions><services><service name="EPiServer.Events.Remote.EventReplication"><!-- Uncomment this endpoint and the "RemoteEventServiceClientEndPoint" to enable remote events
            <endpoint name="RemoteEventServiceEndPoint"
                    contract="EPiServer.Events.ServiceModel.IEventReplication"
                    binding="customBinding"
                    bindingConfiguration="RemoteEventsBinding"
                    address="soap.udp://239.255.255.19:5000/RemoteEventService" />--></service></services><client><!-- Uncomment this endpoint and the "RemoteEventServiceEndPoint" to enable remote events
        <endpoint name="RemoteEventServiceClientEndPoint"
           address="soap.udp://239.255.255.19:5000/RemoteEventService"
           binding="customBinding"
           bindingConfiguration="RemoteEventsBinding"
           contract="EPiServer.Events.ServiceModel.IEventReplication" />--></client><behaviors><serviceBehaviors><behavior name="DebugServiceBehaviour"><serviceDebug includeExceptionDetailInFaults="true" /></behavior></serviceBehaviors></behaviors><bindings><customBinding><binding name="RemoteEventsBinding"><binaryMessageEncoding /><udpTransportCustom multicast="True" /></binding></customBinding><webHttpBinding><binding name="IndexingServiceCustomBinding" maxBufferPoolSize="1073741824" maxReceivedMessageSize="2147483647" maxBufferSize="2147483647"><readerQuotas maxStringContentLength="10000000" /></binding></webHttpBinding></bindings></system.serviceModel><episerver.search active="true"><namedIndexingServices defaultService="serviceName"><services><!--<add name="{serviceName}" baseUri="{indexingServiceBaseUri}" accessKey="{accessKey}"/>--><add name="serviceName" baseUri="http://localhost:9002/IndexingService/IndexingService.svc" accessKey="local" /></services></namedIndexingServices><searchResultFilter defaultInclude="true"><providers /></searchResultFilter></episerver.search><location path="modulesbin"><system.web><authorization><deny users="*" /></authorization></system.web></location><episerver.packaging protectedVirtualPath="~/EPiServer/" /><episerver.search.indexingservice><clients><!--add name="example" description="example" ipAddress="127.0.0.1/8,192.168.0.0/24" ip6Address="" allowLocal="true|false" readonly="true|false" /--><add name="local" description="local" allowLocal="true" readonly="false" /></clients><namedIndexes defaultIndex="default"><indexes><add name="default" directoryPath="[appDataPath]\Index" readonly="false" /></indexes></namedIndexes></episerver.search.indexingservice><location path="IndexingService/IndexingService.svc"><system.web><httpRuntime maxQueryStringLength="65536" /></system.web><system.webServer><security><requestFiltering><requestLimits maxQueryString="65536" /></requestFiltering></security></system.webServer></location><appSettings><add key="webpages:Version" value="2.0.0.0" /><add key="webpages:Enabled" value="false" /><!-- Google Analytics tracking --><add key="google.analytics.account" value="UA-28713594-1" /><add key="google.analytics.domain" value=".local.spekter.no" /><add key="ShareThisPublisherId" value="11111111-1111-1111-1111-111111111111" /></appSettings><!-- SMTP Settings for ASP.NET 2.0 Mail classes. --><system.net><mailSettings><smtp configSource="configs\smtp.config" /></mailSettings></system.net></configuration>')
SET IDENTITY_INSERT [dbo].[tblSiteConfig] OFF
/****** Object:  Table [dbo].[tblWindowsGroup]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblWindowsGroup](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [nvarchar](255) NOT NULL,
	[LoweredGroupName] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_tblWindowsGroup] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblWindowsGroup_Unique] ON [dbo].[tblWindowsGroup] 
(
	[LoweredGroupName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblWindowsGroup] ON
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (1, N'CO\Domain Users', N'co\domain users')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (2, N'Everyone', N'everyone')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (3, N'Administrators', N'administrators')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (4, N'Users', N'users')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (5, N'NT AUTHORITY\NETWORK', N'nt authority\network')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (6, N'NT AUTHORITY\Authenticated Users', N'nt authority\authenticated users')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (7, N'NT AUTHORITY\This Organization', N'nt authority\this organization')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (8, N'CO\sto-pub-creuna-admins', N'co\sto-pub-creuna-admins')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (9, N'CO\sto-stos-sse', N'co\sto-stos-sse')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (10, N'CO\sto-creuna-files', N'co\sto-creuna-files')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (11, N'CO\creuna-wlan', N'co\creuna-wlan')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (12, N'CO\sto-pub-sandbox-admins', N'co\sto-pub-sandbox-admins')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (13, N'CO\sto-stos-sse-preprod', N'co\sto-stos-sse-preprod')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (14, N'CO\sto-tork-ptyp-users', N'co\sto-tork-ptyp-users')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (15, N'CO\sto-stos-preprodplatsadmin', N'co\sto-stos-preprodplatsadmin')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (16, N'CO\sto-ledn-users', N'co\sto-ledn-users')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (17, N'CO\sto-clr3-users', N'co\sto-clr3-users')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (18, N'CO\sto-cpl-templatesite', N'co\sto-cpl-templatesite')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (19, N'CO\sto-stos-ksl', N'co\sto-stos-ksl')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (20, N'CO\sto-sandbox-files-users', N'co\sto-sandbox-files-users')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (21, N'CO\sto-rcpt-users', N'co\sto-rcpt-users')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (22, N'CO\sto-creuna-ptyp-admins', N'co\sto-creuna-ptyp-admins')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (23, N'CO\sto-sandbox-ptyp-admins', N'co\sto-sandbox-ptyp-admins')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (24, N'CO\sto-logon-users', N'co\sto-logon-users')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (25, N'CO\sto-sandbox-ptyp-users', N'co\sto-sandbox-ptyp-users')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (26, N'CO\sto-clr2-users', N'co\sto-clr2-users')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (27, N'CO\OSL-Creuna Employees', N'co\osl-creuna employees')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (28, N'CO\creuna-all', N'co\creuna-all')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (29, N'CO\kha-no-team', N'co\kha-no-team')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (30, N'CO\osl-vm development users - heavy duty', N'co\osl-vm development users - heavy duty')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (31, N'CO\Project-group', N'co\project-group')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (32, N'CO\dk-Secure Access', N'co\dk-secure access')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (33, N'CO\osl-Nice Support', N'co\osl-nice support')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (34, N'CO\osl-Intranett Users', N'co\osl-intranett users')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (35, N'CO\kha-employees', N'co\kha-employees')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (36, N'CO\osl-sslvpn', N'co\osl-sslvpn')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (37, N'CO\osl-vm development users', N'co\osl-vm development users')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (38, N'CO\Software Office', N'co\software office')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (39, N'CO\CERTSVC_DCOM_ACCESS', N'co\certsvc_dcom_access')
INSERT [dbo].[tblWindowsGroup] ([pkID], [GroupName], [LoweredGroupName]) VALUES (40, N'NT AUTHORITY\NTLM Authentication', N'nt authority\ntlm authentication')
SET IDENTITY_INSERT [dbo].[tblWindowsGroup] OFF
/****** Object:  Table [dbo].[tblUserPermission]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUserPermission](
	[Name] [nvarchar](255) NOT NULL,
	[IsRole] [int] NOT NULL,
	[Permission] [int] NOT NULL,
 CONSTRAINT [PK_tblUserPermission] PRIMARY KEY CLUSTERED 
(
	[Name] ASC,
	[IsRole] ASC,
	[Permission] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[tblUserPermission] ([Name], [IsRole], [Permission]) VALUES (N'Administrators', 1, 2)
/****** Object:  Table [dbo].[tblUniqueSequence]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUniqueSequence](
	[Name] [nvarchar](255) NOT NULL,
	[LastValue] [int] NOT NULL,
 CONSTRAINT [PK_tblUniqueSequence] PRIMARY KEY CLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblPlugIn]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPlugIn](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[AssemblyName] [nvarchar](255) NOT NULL,
	[TypeName] [nvarchar](255) NOT NULL,
	[Settings] [nvarchar](max) NULL,
	[Saved] [datetime] NOT NULL,
	[Created] [datetime] NOT NULL,
	[Enabled] [bit] NOT NULL,
 CONSTRAINT [PK_tblPlugIn] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblPlugIn] ON
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (1, N'EPiServer', N'EPiServer.DynamicContent.PlugIn.DynamicPageProperty', NULL, CAST(0x0000A11801553E38 AS DateTime), CAST(0x0000A11801553E38 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (2, N'EPiServer', N'EPiServer.SpecializedProperties.PropertyContentArea', NULL, CAST(0x0000A11801553E3D AS DateTime), CAST(0x0000A11801553E3D AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (3, N'EPiServer', N'EPiServer.SpecializedProperties.PropertyDropDownList', NULL, CAST(0x0000A11801553E3D AS DateTime), CAST(0x0000A11801553E3D AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (4, N'EPiServer', N'EPiServer.SpecializedProperties.PropertyCheckBoxList', NULL, CAST(0x0000A11801553E3E AS DateTime), CAST(0x0000A11801553E3E AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (5, N'EPiServer', N'EPiServer.SpecializedProperties.PropertyUrl', NULL, CAST(0x0000A11801553E3E AS DateTime), CAST(0x0000A11801553E3E AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (6, N'EPiServer', N'EPiServer.Core.PropertyXForm', NULL, CAST(0x0000A11801553E3E AS DateTime), CAST(0x0000A11801553E3E AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (7, N'EPiServer', N'EPiServer.SpecializedProperties.PropertyAppSettingsMultiple', NULL, CAST(0x0000A11801553E3E AS DateTime), CAST(0x0000A11801553E3E AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (8, N'EPiServer', N'EPiServer.SpecializedProperties.PropertyAppSettings', NULL, CAST(0x0000A11801553E3F AS DateTime), CAST(0x0000A11801553E3F AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (9, N'EPiServer', N'EPiServer.SpecializedProperties.PropertyDocumentUrl', NULL, CAST(0x0000A11801553E3F AS DateTime), CAST(0x0000A11801553E3F AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (10, N'EPiServer', N'EPiServer.SpecializedProperties.PropertyFileSortOrder', NULL, CAST(0x0000A11801553E40 AS DateTime), CAST(0x0000A11801553E40 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (11, N'EPiServer', N'EPiServer.SpecializedProperties.PropertyFrame', NULL, CAST(0x0000A11801553E40 AS DateTime), CAST(0x0000A11801553E40 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (12, N'EPiServer', N'EPiServer.SpecializedProperties.PropertyImageUrl', NULL, CAST(0x0000A11801553E40 AS DateTime), CAST(0x0000A11801553E40 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (13, N'EPiServer', N'EPiServer.SpecializedProperties.PropertyLanguage', NULL, CAST(0x0000A11801553E40 AS DateTime), CAST(0x0000A11801553E40 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (14, N'EPiServer', N'EPiServer.SpecializedProperties.PropertyXhtmlString', NULL, CAST(0x0000A11801553E41 AS DateTime), CAST(0x0000A11801553E41 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (15, N'EPiServer', N'EPiServer.SpecializedProperties.PropertyLinkCollection', NULL, CAST(0x0000A11801553E41 AS DateTime), CAST(0x0000A11801553E41 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (16, N'EPiServer', N'EPiServer.SpecializedProperties.PropertySelector', NULL, CAST(0x0000A11801553E41 AS DateTime), CAST(0x0000A11801553E41 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (17, N'EPiServer', N'EPiServer.SpecializedProperties.PropertySortOrder', NULL, CAST(0x0000A11801553E41 AS DateTime), CAST(0x0000A11801553E41 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (18, N'EPiServer', N'EPiServer.SpecializedProperties.PropertyVirtualLink', NULL, CAST(0x0000A11801553E46 AS DateTime), CAST(0x0000A11801553E46 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (19, N'EPiServer', N'EPiServer.SpecializedProperties.PropertyWeekDay', NULL, CAST(0x0000A11801553E46 AS DateTime), CAST(0x0000A11801553E46 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (20, N'EPiServer', N'EPiServer.WebParts.Core.PropertyWebPart', NULL, CAST(0x0000A11801553E47 AS DateTime), CAST(0x0000A11801553E47 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (21, N'EPiServer', N'EPiServer.Web.PageExtensions.ShellCommunication', NULL, CAST(0x0000A11801553E8C AS DateTime), CAST(0x0000A11801553E8C AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (22, N'EPiServer', N'EPiServer.Web.PageExtensions.QuickNavigator', NULL, CAST(0x0000A11801553E8C AS DateTime), CAST(0x0000A11801553E8C AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (23, N'EPiServer', N'EPiServer.Web.PageExtensions.AntiForgeryValidation', NULL, CAST(0x0000A11801553E8C AS DateTime), CAST(0x0000A11801553E8C AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (24, N'EPiServer', N'EPiServer.Web.PageExtensions.PageNotFoundRedirect', NULL, CAST(0x0000A11801553E8D AS DateTime), CAST(0x0000A11801553E8D AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (25, N'EPiServer', N'EPiServer.Web.PageExtensions.SetupContentRedirect', NULL, CAST(0x0000A11801553E8E AS DateTime), CAST(0x0000A11801553E8E AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (26, N'EPiServer', N'EPiServer.Web.PageExtensions.PageVisited', NULL, CAST(0x0000A11801553E8E AS DateTime), CAST(0x0000A11801553E8E AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (27, N'EPiServer', N'EPiServer.Web.PageExtensions.ServerTransferBugfix', NULL, CAST(0x0000A11801553E8E AS DateTime), CAST(0x0000A11801553E8E AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (28, N'EPiServer', N'EPiServer.Web.PageExtensions.CrossSiteNavigation', NULL, CAST(0x0000A11801553E8E AS DateTime), CAST(0x0000A11801553E8E AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (29, N'EPiServer', N'EPiServer.Web.PageExtensions.CustomPageLink', NULL, CAST(0x0000A11801553E8F AS DateTime), CAST(0x0000A11801553E8F AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (30, N'EPiServer', N'EPiServer.Web.PageExtensions.ContextMenu', NULL, CAST(0x0000A11801553E8F AS DateTime), CAST(0x0000A11801553E8F AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (31, N'EPiServer', N'EPiServer.Web.PageExtensions.CultureSupport', NULL, CAST(0x0000A11801553E8F AS DateTime), CAST(0x0000A11801553E8F AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (32, N'EPiServer', N'EPiServer.Web.PageExtensions.LoadCurrentPage', NULL, CAST(0x0000A11801553E8F AS DateTime), CAST(0x0000A11801553E8F AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (33, N'EPiServer', N'EPiServer.Web.PageExtensions.PageTranslation', NULL, CAST(0x0000A11801553E8F AS DateTime), CAST(0x0000A11801553E8F AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (34, N'EPiServer', N'EPiServer.Web.PageExtensions.SaveCurrentPage', NULL, CAST(0x0000A11801553E8F AS DateTime), CAST(0x0000A11801553E8F AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (35, N'EPiServer', N'EPiServer.Web.PageExtensions.SiteRedirect', NULL, CAST(0x0000A11801553E90 AS DateTime), CAST(0x0000A11801553E90 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (36, N'EPiServer', N'EPiServer.Web.PageExtensions.ThemeUtility', NULL, CAST(0x0000A11801553E90 AS DateTime), CAST(0x0000A11801553E90 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (37, N'EPiServer.Blog', N'EPiSearch.Blog.MetaWeblog.PageExtension.EditUriHeader', NULL, CAST(0x0000A11801553E90 AS DateTime), CAST(0x0000A11801553E90 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (38, N'EPiServer.UI', N'EPiServer.UI.SaveCurrentContent', NULL, CAST(0x0000A11801553E94 AS DateTime), CAST(0x0000A11801553E94 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (39, N'EPiServer.UI', N'EPiServer.UI.LoadCurrentContent', NULL, CAST(0x0000A11801553E98 AS DateTime), CAST(0x0000A11801553E98 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (40, N'EPiServer.UI', N'EPiServer.WorkflowFoundation.UI.WorkflowStartControl', NULL, CAST(0x0000A119009F6DDF AS DateTime), CAST(0x0000A119009F6DDF AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (41, N'EPiServer.UI', N'EPiServer.UI.Hosting.VirtualPathControl', NULL, CAST(0x0000A119009F6DE0 AS DateTime), CAST(0x0000A119009F6DE0 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (42, N'EPiServer.UI', N'EPiServer.UI.Edit.GlobalizationControl', NULL, CAST(0x0000A119009F6DE0 AS DateTime), CAST(0x0000A119009F6DE0 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (43, N'EPiServer.UI', N'EPiServer.UI.Edit.DraftList', NULL, CAST(0x0000A119009F7039 AS DateTime), CAST(0x0000A119009F7039 AS DateTime), 1)
INSERT [dbo].[tblPlugIn] ([pkID], [AssemblyName], [TypeName], [Settings], [Saved], [Created], [Enabled]) VALUES (44, N'EPiServer.UI', N'EPiServer.UI.Util.PlugIns.EditTask', NULL, CAST(0x0000A119009F703A AS DateTime), CAST(0x0000A119009F703A AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[tblPlugIn] OFF
/****** Object:  Table [dbo].[tblSchema]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSchema](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[SchemaId] [nvarchar](256) NOT NULL,
	[IdType] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_tblSchema] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_tblSchema_SchemaId] UNIQUE NONCLUSTERED 
(
	[SchemaId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblSchema] ON
INSERT [dbo].[tblSchema] ([pkID], [SchemaId], [IdType]) VALUES (1, N'DefaultItem', N'System.Guid')
INSERT [dbo].[tblSchema] ([pkID], [SchemaId], [IdType]) VALUES (2, N'DirectoryItem', N'System.Guid')
SET IDENTITY_INSERT [dbo].[tblSchema] OFF
/****** Object:  Table [dbo].[tblScheduledItem]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblScheduledItem](
	[pkID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Enabled] [bit] NOT NULL,
	[LastExec] [datetime] NULL,
	[LastStatus] [int] NULL,
	[LastText] [nvarchar](2048) NULL,
	[NextExec] [datetime] NULL,
	[DatePart] [nchar](2) NULL,
	[Interval] [int] NULL,
	[MethodName] [nvarchar](100) NOT NULL,
	[fStatic] [bit] NOT NULL,
	[TypeName] [nvarchar](100) NOT NULL,
	[AssemblyName] [nvarchar](100) NOT NULL,
	[InstanceData] [image] NULL,
	[IsRunning] [bit] NOT NULL,
	[CurrentStatusMessage] [nvarchar](2048) NULL,
	[LastPing] [datetime] NULL,
 CONSTRAINT [PK__tblScheduledItem__1940BAED] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[tblScheduledItem] ([pkID], [Name], [Enabled], [LastExec], [LastStatus], [LastText], [NextExec], [DatePart], [Interval], [MethodName], [fStatic], [TypeName], [AssemblyName], [InstanceData], [IsRunning], [CurrentStatusMessage], [LastPing]) VALUES (N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', N'Publish delayed pages', 1, CAST(0x0000A119008033A6 AS DateTime), 0, N'0 contents were published.', CAST(0x0000A11900805B80 AS DateTime), N'mi', 1, N'Execute', 1, N'EPiServer.Util.DelayedPublishJob', N'EPiServer', NULL, 0, NULL, CAST(0x0000A11900A12925 AS DateTime))
INSERT [dbo].[tblScheduledItem] ([pkID], [Name], [Enabled], [LastExec], [LastStatus], [LastText], [NextExec], [DatePart], [Interval], [MethodName], [fStatic], [TypeName], [AssemblyName], [InstanceData], [IsRunning], [CurrentStatusMessage], [LastPing]) VALUES (N'7d8fad39-2682-43b6-b49c-d7c8199d5195', N'EPiServer Subscription Job', 1, CAST(0x0000A119007F2211 AS DateTime), 0, N'1 user profiles were found. 0 subscription e-mails were sent.', CAST(0x0000A119008F9BFC AS DateTime), N'mi', 60, N'Execute', 1, N'EPiServer.Personalization.SubscriptionJob', N'EPiServer', NULL, 0, NULL, CAST(0x0000A11900A01790 AS DateTime))
/****** Object:  Table [dbo].[tblRemoteSite]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRemoteSite](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Url] [nvarchar](255) NOT NULL,
	[IsTrusted] [bit] NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Domain] [nvarchar](50) NULL,
	[AllowUrlLookup] [bit] NOT NULL,
 CONSTRAINT [PK_tblRemoteSite] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_tblRemoteSite] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblPropertyDefinitionType]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPropertyDefinitionType](
	[pkID] [int] NOT NULL,
	[Property] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[TypeName] [nvarchar](255) NULL,
	[AssemblyName] [nvarchar](255) NULL,
	[fkContentTypeGUID] [uniqueidentifier] NULL,
 CONSTRAINT [PK_tblPropertyDefinitionType] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (0, 0, N'Boolean', NULL, NULL, NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (1, 1, N'Number', NULL, NULL, NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (2, 2, N'FloatNumber', NULL, NULL, NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (3, 3, N'PageType', NULL, NULL, NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (4, 4, N'PageReference', NULL, NULL, NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (5, 5, N'Date', NULL, NULL, NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (6, 6, N'String', NULL, NULL, NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (7, 7, N'LongString', NULL, NULL, NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (8, 8, N'Category', NULL, NULL, NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (11, 11, N'ContentReference', NULL, NULL, NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (12, 7, N'ContentArea', N'EPiServer.SpecializedProperties.PropertyContentArea', N'EPiServer', NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (13, 6, N'VirtualLink', N'EPiServer.SpecializedProperties.PropertyVirtualLink', N'EPiServer', NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (14, 1, N'SortOrder', N'EPiServer.SpecializedProperties.PropertySortOrder', N'EPiServer', NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (15, 6, N'Selector', N'EPiServer.SpecializedProperties.PropertySelector', N'EPiServer', NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (16, 10, N'LinkCollection', N'EPiServer.SpecializedProperties.PropertyLinkCollection', N'EPiServer', NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (17, 7, N'XhtmlString', N'EPiServer.SpecializedProperties.PropertyXhtmlString', N'EPiServer', NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (18, 6, N'Language', N'EPiServer.SpecializedProperties.PropertyLanguage', N'EPiServer', NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (19, 6, N'ImageUrl', N'EPiServer.SpecializedProperties.PropertyImageUrl', N'EPiServer', NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (20, 1, N'WeekDay', N'EPiServer.SpecializedProperties.PropertyWeekDay', N'EPiServer', NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (21, 1, N'Frame', N'EPiServer.SpecializedProperties.PropertyFrame', N'EPiServer', NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (22, 6, N'DocumentUrl', N'EPiServer.SpecializedProperties.PropertyDocumentUrl', N'EPiServer', NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (23, 6, N'AppSettings', N'EPiServer.SpecializedProperties.PropertyAppSettings', N'EPiServer', NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (24, 6, N'AppSettingsMultiple', N'EPiServer.SpecializedProperties.PropertyAppSettingsMultiple', N'EPiServer', NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (25, 6, N'XForm', N'EPiServer.Core.PropertyXForm', N'EPiServer', NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (26, 6, N'Url', N'EPiServer.SpecializedProperties.PropertyUrl', N'EPiServer', NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (27, 6, N'CheckBoxList', N'EPiServer.SpecializedProperties.PropertyCheckBoxList', N'EPiServer', NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (28, 6, N'DropDownList', N'EPiServer.SpecializedProperties.PropertyDropDownList', N'EPiServer', NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (29, 1, N'FileSortOrder', N'EPiServer.SpecializedProperties.PropertyFileSortOrder', N'EPiServer', NULL)
INSERT [dbo].[tblPropertyDefinitionType] ([pkID], [Property], [Name], [TypeName], [AssemblyName], [fkContentTypeGUID]) VALUES (30, 6, N'WebPart', N'EPiServer.WebParts.Core.PropertyWebPart', N'EPiServer', NULL)
/****** Object:  Table [dbo].[tblPropertyDefinitionGroup]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPropertyDefinitionGroup](
	[pkID] [int] IDENTITY(100,1) NOT NULL,
	[SystemGroup] [bit] NOT NULL,
	[Access] [int] NOT NULL,
	[GroupVisible] [bit] NOT NULL,
	[GroupOrder] [int] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[DisplayName] [nvarchar](100) NULL,
 CONSTRAINT [PK_tblPropertyDefinitionGroup] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblPropertyDefinitionGroup] ON
INSERT [dbo].[tblPropertyDefinitionGroup] ([pkID], [SystemGroup], [Access], [GroupVisible], [GroupOrder], [Name], [DisplayName]) VALUES (0, 1, 1, 1, 10, N'Information', NULL)
INSERT [dbo].[tblPropertyDefinitionGroup] ([pkID], [SystemGroup], [Access], [GroupVisible], [GroupOrder], [Name], [DisplayName]) VALUES (1, 1, 4, 0, 30, N'Advanced', NULL)
INSERT [dbo].[tblPropertyDefinitionGroup] ([pkID], [SystemGroup], [Access], [GroupVisible], [GroupOrder], [Name], [DisplayName]) VALUES (2, 1, 1, 0, 50, N'Categories', NULL)
INSERT [dbo].[tblPropertyDefinitionGroup] ([pkID], [SystemGroup], [Access], [GroupVisible], [GroupOrder], [Name], [DisplayName]) VALUES (3, 1, 1, 0, 40, N'Shortcut', NULL)
INSERT [dbo].[tblPropertyDefinitionGroup] ([pkID], [SystemGroup], [Access], [GroupVisible], [GroupOrder], [Name], [DisplayName]) VALUES (4, 1, 1, 0, 20, N'Scheduling', NULL)
INSERT [dbo].[tblPropertyDefinitionGroup] ([pkID], [SystemGroup], [Access], [GroupVisible], [GroupOrder], [Name], [DisplayName]) VALUES (5, 1, 1, 0, 60, N'DynamicBlocks', NULL)
SET IDENTITY_INSERT [dbo].[tblPropertyDefinitionGroup] OFF
/****** Object:  Table [dbo].[tblPropertyDefinitionDefault]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPropertyDefinitionDefault](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkPropertyDefinitionID] [int] NOT NULL,
	[Boolean] [bit] NOT NULL,
	[Number] [int] NULL,
	[FloatNumber] [float] NULL,
	[ContentType] [int] NULL,
	[ContentLink] [int] NULL,
	[Date] [datetime] NULL,
	[String] [nvarchar](450) NULL,
	[LongString] [nvarchar](max) NULL,
	[LinkGuid] [uniqueidentifier] NULL,
 CONSTRAINT [PK_tblPropertyDefinitionDefault] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUnifiedPath]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUnifiedPath](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[Path] [nvarchar](4000) NOT NULL,
	[InheritAcl] [bit] NOT NULL,
 CONSTRAINT [PK_tblUnifiedPath] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTree]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTree](
	[fkParentID] [int] NOT NULL,
	[fkChildID] [int] NOT NULL,
	[NestingLevel] [smallint] NOT NULL,
 CONSTRAINT [PK_tblTree] PRIMARY KEY CLUSTERED 
(
	[fkParentID] ASC,
	[fkChildID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblTree_fkChildID] ON [dbo].[tblTree] 
(
	[fkChildID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
INSERT [dbo].[tblTree] ([fkParentID], [fkChildID], [NestingLevel]) VALUES (1, 2, 1)
INSERT [dbo].[tblTree] ([fkParentID], [fkChildID], [NestingLevel]) VALUES (1, 3, 1)
/****** Object:  Table [dbo].[tblContentType]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblContentType](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[ContentTypeGUID] [uniqueidentifier] NOT NULL,
	[Created] [datetime] NOT NULL,
	[DefaultWebFormTemplate] [nvarchar](1024) NULL,
	[DefaultMvcController] [nvarchar](1024) NULL,
	[DefaultMvcPartialView] [nvarchar](255) NULL,
	[Filename] [nvarchar](255) NULL,
	[ModelType] [nvarchar](1024) NULL,
	[Name] [nvarchar](50) NOT NULL,
	[DisplayName] [nvarchar](50) NULL,
	[Description] [nvarchar](255) NULL,
	[IdString] [nvarchar](50) NULL,
	[Available] [bit] NULL,
	[SortOrder] [int] NULL,
	[MetaDataInherit] [int] NOT NULL,
	[MetaDataDefault] [int] NOT NULL,
	[WorkflowEditFields] [bit] NULL,
	[ACL] [nvarchar](max) NULL,
	[ContentType] [int] NOT NULL,
 CONSTRAINT [PK_tblContentType] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblContentType] ON
INSERT [dbo].[tblContentType] ([pkID], [ContentTypeGUID], [Created], [DefaultWebFormTemplate], [DefaultMvcController], [DefaultMvcPartialView], [Filename], [ModelType], [Name], [DisplayName], [Description], [IdString], [Available], [SortOrder], [MetaDataInherit], [MetaDataDefault], [WorkflowEditFields], [ACL], [ContentType]) VALUES (1, N'3fa7d9e7-877b-11d3-827c-00a024cacfcb', CAST(0x00008D3F00000000 AS DateTime), NULL, NULL, NULL, N'~/EPiServer/cms/edit/workspace.aspx', NULL, N'SysRoot', NULL, N'Used as root/welcome page', N'?id=', 0, 10000, 0, 0, 0, NULL, 0)
INSERT [dbo].[tblContentType] ([pkID], [ContentTypeGUID], [Created], [DefaultWebFormTemplate], [DefaultMvcController], [DefaultMvcPartialView], [Filename], [ModelType], [Name], [DisplayName], [Description], [IdString], [Available], [SortOrder], [MetaDataInherit], [MetaDataDefault], [WorkflowEditFields], [ACL], [ContentType]) VALUES (2, N'3fa7d9e8-877b-11d3-827c-00a024cacfcb', CAST(0x00008D3F00000000 AS DateTime), NULL, NULL, NULL, N'~/EPiServer/cms/edit/wastebasketpage.aspx', NULL, N'SysRecycleBin', NULL, N'Used as recycle bin for the website', N'?id=', 0, 10010, 0, 0, 0, NULL, 0)
INSERT [dbo].[tblContentType] ([pkID], [ContentTypeGUID], [Created], [DefaultWebFormTemplate], [DefaultMvcController], [DefaultMvcPartialView], [Filename], [ModelType], [Name], [DisplayName], [Description], [IdString], [Available], [SortOrder], [MetaDataInherit], [MetaDataDefault], [WorkflowEditFields], [ACL], [ContentType]) VALUES (3, N'52f8d1e9-6d87-4db6-a465-41890289fb78', CAST(0x00008D3F00000000 AS DateTime), NULL, NULL, NULL, NULL, N'EPiServer.Core.ContentFolder,EPiServer', N'SysContentFolder', NULL, N'Used as content folder', N'?id=', 0, 10020, 0, 0, 0, NULL, 2)
SET IDENTITY_INSERT [dbo].[tblContentType] OFF
/****** Object:  Table [dbo].[tblLanguageBranch]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLanguageBranch](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[LanguageID] [nchar](17) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[SortIndex] [int] NOT NULL,
	[SystemIconPath] [nvarchar](255) NULL,
	[URLSegment] [nvarchar](255) NULL,
	[ACL] [nvarchar](max) NULL,
	[Enabled] [bit] NOT NULL,
 CONSTRAINT [PK_tblLanguageBranch] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_tblLanguageBranch] UNIQUE NONCLUSTERED 
(
	[LanguageID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblLanguageBranch] ON
INSERT [dbo].[tblLanguageBranch] ([pkID], [LanguageID], [Name], [SortIndex], [SystemIconPath], [URLSegment], [ACL], [Enabled]) VALUES (1, N'en               ', NULL, 10, N'~/app_themes/default/images/flags/en.gif', NULL, NULL, 1)
INSERT [dbo].[tblLanguageBranch] ([pkID], [LanguageID], [Name], [SortIndex], [SystemIconPath], [URLSegment], [ACL], [Enabled]) VALUES (2, N'en-GB            ', NULL, 20, N'~/app_themes/default/images/flags/en-gb.gif', NULL, NULL, 0)
INSERT [dbo].[tblLanguageBranch] ([pkID], [LanguageID], [Name], [SortIndex], [SystemIconPath], [URLSegment], [ACL], [Enabled]) VALUES (3, N'en-NZ            ', NULL, 30, N'~/app_themes/Default/Images/flags/en-NZ.gif', NULL, NULL, 0)
INSERT [dbo].[tblLanguageBranch] ([pkID], [LanguageID], [Name], [SortIndex], [SystemIconPath], [URLSegment], [ACL], [Enabled]) VALUES (4, N'en-ZA            ', NULL, 40, N'~/app_themes/default/images/flags/en-za.gif', NULL, NULL, 0)
INSERT [dbo].[tblLanguageBranch] ([pkID], [LanguageID], [Name], [SortIndex], [SystemIconPath], [URLSegment], [ACL], [Enabled]) VALUES (5, N'de               ', NULL, 50, N'~/app_themes/Default/Images/flags/de.gif', NULL, NULL, 0)
INSERT [dbo].[tblLanguageBranch] ([pkID], [LanguageID], [Name], [SortIndex], [SystemIconPath], [URLSegment], [ACL], [Enabled]) VALUES (6, N'fr               ', NULL, 60, N'~/app_themes/default/images/flags/fr.gif', NULL, NULL, 0)
INSERT [dbo].[tblLanguageBranch] ([pkID], [LanguageID], [Name], [SortIndex], [SystemIconPath], [URLSegment], [ACL], [Enabled]) VALUES (7, N'es               ', NULL, 70, N'~/app_themes/default/images/flags/es.gif', NULL, NULL, 0)
INSERT [dbo].[tblLanguageBranch] ([pkID], [LanguageID], [Name], [SortIndex], [SystemIconPath], [URLSegment], [ACL], [Enabled]) VALUES (8, N'sv               ', NULL, 80, N'~/app_themes/default/images/flags/sv.gif', NULL, NULL, 1)
INSERT [dbo].[tblLanguageBranch] ([pkID], [LanguageID], [Name], [SortIndex], [SystemIconPath], [URLSegment], [ACL], [Enabled]) VALUES (9, N'no               ', NULL, 90, N'~/app_themes/default/images/flags/no.gif', NULL, NULL, 0)
INSERT [dbo].[tblLanguageBranch] ([pkID], [LanguageID], [Name], [SortIndex], [SystemIconPath], [URLSegment], [ACL], [Enabled]) VALUES (10, N'da               ', NULL, 100, N'~/app_themes/default/images/flags/da.gif', NULL, NULL, 0)
INSERT [dbo].[tblLanguageBranch] ([pkID], [LanguageID], [Name], [SortIndex], [SystemIconPath], [URLSegment], [ACL], [Enabled]) VALUES (11, N'fi               ', NULL, 110, N'~/app_themes/default/images/flags/fi.gif', NULL, NULL, 0)
INSERT [dbo].[tblLanguageBranch] ([pkID], [LanguageID], [Name], [SortIndex], [SystemIconPath], [URLSegment], [ACL], [Enabled]) VALUES (12, N'nl               ', NULL, 120, N'~/app_themes/default/images/flags/nl.gif', NULL, NULL, 0)
INSERT [dbo].[tblLanguageBranch] ([pkID], [LanguageID], [Name], [SortIndex], [SystemIconPath], [URLSegment], [ACL], [Enabled]) VALUES (13, N'nl-BE            ', NULL, 130, N'~/app_themes/default/images/flags/nl-be.gif', NULL, NULL, 0)
INSERT [dbo].[tblLanguageBranch] ([pkID], [LanguageID], [Name], [SortIndex], [SystemIconPath], [URLSegment], [ACL], [Enabled]) VALUES (14, N'pt-BR            ', NULL, 140, N'~/app_themes/default/images/flags/pt-br.gif', NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[tblLanguageBranch] OFF
/****** Object:  Table [dbo].[tblItem]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblItem](
	[pkID] [nvarchar](100) NOT NULL,
	[fkSchemaId] [int] NOT NULL,
	[Name] [nvarchar](256) NULL,
	[ItemData] [image] NULL,
 CONSTRAINT [PK_tblItem] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblItem_Name] ON [dbo].[tblItem] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
INSERT [dbo].[tblItem] ([pkID], [fkSchemaId], [Name], [ItemData]) VALUES (N'12f5974d-cd47-4d4c-9977-b912be06a41a', 2, N'Documents', 0x01000000FEFFFFFF1A4400690072006500630074006F00720079004900740065006D0000000000FAFFFFFF4D97F51247CD4C4D9977B912BE06A41A065F0069006400120000001244006F00630075006D0065006E00740073000A5F006E0061006D006500120000000C730079007300740065006D00145F006300720065006100740065006400420079001000000040F21A69789CCF88105F006300720065006100740065006400FFFFFFFF)
INSERT [dbo].[tblItem] ([pkID], [fkSchemaId], [Name], [ItemData]) VALUES (N'490344e2-1661-42f2-826f-4594a1fa3586', 2, N'PageFiles', 0x01000000FEFFFFFF1A4400690072006500630074006F00720079004900740065006D0000000000FAFFFFFFE24403496116F242826F4594A1FA3586065F006900640012000000125000610067006500460069006C00650073000A5F006E0061006D006500120000000C730079007300740065006D00145F0063007200650061007400650064004200790010000000A3A9FB68789CCF88105F006300720065006100740065006400FFFFFFFF)
INSERT [dbo].[tblItem] ([pkID], [fkSchemaId], [Name], [ItemData]) VALUES (N'a8aef1fd-8716-4fb7-85e6-36b41722a22c', 2, N'Global', 0x01000000FEFFFFFF1A4400690072006500630074006F00720079004900740065006D0000000000FAFFFFFFFDF1AEA81687B74F85E636B41722A22C065F0069006400120000000C47006C006F00620061006C000A5F006E0061006D006500120000000C730079007300740065006D00145F0063007200650061007400650064004200790010000000B8B91969789CCF88105F006300720065006100740065006400FFFFFFFF)
INSERT [dbo].[tblItem] ([pkID], [fkSchemaId], [Name], [ItemData]) VALUES (N'd2667d10-5351-4670-8ceb-5e457632eaa9', 1, N'/filesystem', 0x01000000FEFFFFFF16440065006600610075006C0074004900740065006D0000000000FAFFFFFF107D66D2515370468CEB5E457632EAA9065F006900640012000000162F00660069006C006500730079007300740065006D000A5F006E0061006D006500FFFFFFFF)
/****** Object:  Table [dbo].[tblIndexString]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblIndexString](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkSchemaItemId] [int] NOT NULL,
	[fkItemId] [nvarchar](256) NOT NULL,
	[FieldValue] [nvarchar](512) NULL,
 CONSTRAINT [PK_tblIndexString] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Idx_TblIndexString_fkItemId] ON [dbo].[tblIndexString] 
(
	[fkItemId] ASC,
	[fkSchemaItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblIndexInt]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblIndexInt](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkSchemaItemId] [int] NOT NULL,
	[fkItemId] [nvarchar](256) NOT NULL,
	[FieldValue] [int] NULL,
 CONSTRAINT [PK_tblIndexInt] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Idx_TblIndexInt_fkItemId] ON [dbo].[tblIndexInt] 
(
	[fkItemId] ASC,
	[fkSchemaItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblIndexGuid]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblIndexGuid](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkSchemaItemId] [int] NOT NULL,
	[fkItemId] [nvarchar](256) NOT NULL,
	[FieldValue] [uniqueidentifier] NULL,
 CONSTRAINT [PK_tblIndexGuid] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Idx_TblIndexGuid_fkItemId] ON [dbo].[tblIndexGuid] 
(
	[fkItemId] ASC,
	[fkSchemaItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblIndexFloat]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblIndexFloat](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkSchemaItemId] [int] NOT NULL,
	[fkItemId] [nvarchar](256) NOT NULL,
	[FieldValue] [float] NULL,
 CONSTRAINT [PK_tblIndexFloat] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Idx_TblIndexFloat_fkItemId] ON [dbo].[tblIndexFloat] 
(
	[fkItemId] ASC,
	[fkSchemaItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblIndexDecimal]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblIndexDecimal](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkSchemaItemId] [int] NOT NULL,
	[fkItemId] [nvarchar](256) NOT NULL,
	[FieldValue] [decimal](18, 0) NULL,
 CONSTRAINT [PK_tblIndexDecimal] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Idx_TblIndexDecimal_fkItemId] ON [dbo].[tblIndexDecimal] 
(
	[fkItemId] ASC,
	[fkSchemaItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblIndexDateTime]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblIndexDateTime](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkSchemaItemId] [int] NOT NULL,
	[fkItemId] [nvarchar](256) NOT NULL,
	[FieldValue] [datetime] NULL,
 CONSTRAINT [PK_tblIndexDateTime] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Idx_TblIndexDataTime_fkItemId] ON [dbo].[tblIndexDateTime] 
(
	[fkItemId] ASC,
	[fkSchemaItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblIndexBigInt]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblIndexBigInt](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkSchemaItemId] [int] NOT NULL,
	[fkItemId] [nvarchar](256) NOT NULL,
	[FieldValue] [bigint] NULL,
 CONSTRAINT [PK_tblIndexBigInt] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblFrame]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblFrame](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[FrameName] [nvarchar](100) NOT NULL,
	[FrameDescription] [nvarchar](255) NULL,
	[SystemFrame] [bit] NOT NULL,
 CONSTRAINT [PK_tblFrame] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblFrame] ON
INSERT [dbo].[tblFrame] ([pkID], [FrameName], [FrameDescription], [SystemFrame]) VALUES (1, N'target="_blank"', N'Open the link in a new window', 1)
INSERT [dbo].[tblFrame] ([pkID], [FrameName], [FrameDescription], [SystemFrame]) VALUES (2, N'target="_top"', N'Open the link in the whole window', 1)
SET IDENTITY_INSERT [dbo].[tblFrame] OFF
/****** Object:  Table [dbo].[tblEntityType]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEntityType](
	[intID] [int] IDENTITY(1,1) NOT NULL,
	[strName] [varchar](400) NOT NULL,
 CONSTRAINT [PK_tblEntityType] PRIMARY KEY CLUSTERED 
(
	[intID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblEntityGuid]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblEntityGuid](
	[intObjectTypeID] [int] NOT NULL,
	[intObjectID] [int] NOT NULL,
	[unqID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_tblEntityGuid] PRIMARY KEY CLUSTERED 
(
	[intObjectTypeID] ASC,
	[intObjectID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aspnet_Applications]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_Applications](
	[ApplicationName] [nvarchar](256) NOT NULL,
	[LoweredApplicationName] [nvarchar](256) NOT NULL,
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](256) NULL,
PRIMARY KEY NONCLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[LoweredApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [aspnet_Applications_Index] ON [dbo].[aspnet_Applications] 
(
	[LoweredApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
INSERT [dbo].[aspnet_Applications] ([ApplicationName], [LoweredApplicationName], [ApplicationId], [Description]) VALUES (N'EPiServerSample', N'episerversample', N'cd4c8921-a5d2-485f-a0ac-5d2aa7f8bad1', NULL)
/****** Object:  Table [dbo].[InstanceState]    Script Date: 12/06/2012 12:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InstanceState](
	[uidInstanceID] [uniqueidentifier] NOT NULL,
	[state] [image] NULL,
	[status] [int] NULL,
	[unlocked] [int] NULL,
	[blocked] [int] NULL,
	[info] [ntext] NULL,
	[modified] [datetime] NOT NULL,
	[ownerID] [uniqueidentifier] NULL,
	[ownedUntil] [datetime] NULL,
	[nextTimer] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [IX_InstanceState] ON [dbo].[InstanceState] 
(
	[uidInstanceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[ConvertScopeName]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ConvertScopeName]
(
	@ScopeName nvarchar(450),
	@OldDefinitionID int,
	@NewDefinitionID int	
)
RETURNS nvarchar(450)
AS
BEGIN
	DECLARE @ConvertedScopeName nvarchar(450)
	set @ConvertedScopeName = REPLACE(@ScopeName, 
						'.' + CAST(@OldDefinitionID as varchar) + '.', 
						'.'+ CAST(@NewDefinitionID as varchar) +'.')
	RETURN @ConvertedScopeName
END
GO
/****** Object:  Table [dbo].[CompletedScope]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompletedScope](
	[uidInstanceID] [uniqueidentifier] NOT NULL,
	[completedScopeID] [uniqueidentifier] NOT NULL,
	[state] [image] NOT NULL,
	[modified] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_CompletedScope] ON [dbo].[CompletedScope] 
(
	[completedScopeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_CompletedScope_InstanceID] ON [dbo].[CompletedScope] 
(
	[uidInstanceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[BigTableDeleteAll]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BigTableDeleteAll]
@ViewName nvarchar(4000)
AS
BEGIN
	CREATE TABLE #deletes(Id BIGINT, NestLevel INT, ObjectPath varchar(max))
	CREATE INDEX IX_Deletes_Id ON #deletes(Id)
	CREATE INDEX IX_Deletes_Id_NestLevel ON #deletes(Id, NestLevel)
	INSERT INTO #deletes(Id, NestLevel, ObjectPath)
	EXEC ('SELECT [StoreId], 1, ''/'' + CAST([StoreId] AS VARCHAR) + ''/'' FROM ' + @ViewName)
	EXEC ('BigTableDeleteItemInternal 1')
	DROP INDEX IX_Deletes_Id_NestLevel ON #deletes
	DROP INDEX IX_Deletes_Id ON #deletes
	DROP TABLE #deletes
END
GO
/****** Object:  UserDefinedFunction [dbo].[BigTableDateTimeSubtract]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create FUNCTION [dbo].[BigTableDateTimeSubtract]
(
	@DateTime1 DateTime,
	@DateTime2 DateTime
)
RETURNS BigInt
AS
BEGIN
declare @Return BigInt
Select @Return = (Convert(BigInt, 
	DATEDIFF(day, @DateTime1, @DateTime2)) * 86400000) + 
	(DATEDIFF(millisecond, 
		DATEADD(day, 
			DATEDIFF(day, @DateTime1, @DateTime2)
		, @DateTime1)
	, @DateTime2
	)
)
return @Return
END
GO
/****** Object:  Table [dbo].[aspnet_WebEvent_Events]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aspnet_WebEvent_Events](
	[EventId] [char](32) NOT NULL,
	[EventTimeUtc] [datetime] NOT NULL,
	[EventTime] [datetime] NOT NULL,
	[EventType] [nvarchar](256) NOT NULL,
	[EventSequence] [decimal](19, 0) NOT NULL,
	[EventOccurrence] [decimal](19, 0) NOT NULL,
	[EventCode] [int] NOT NULL,
	[EventDetailCode] [int] NOT NULL,
	[Message] [nvarchar](1024) NULL,
	[ApplicationPath] [nvarchar](256) NULL,
	[ApplicationVirtualPath] [nvarchar](256) NULL,
	[MachineName] [nvarchar](256) NOT NULL,
	[RequestUrl] [nvarchar](1024) NULL,
	[ExceptionType] [nvarchar](256) NULL,
	[Details] [ntext] NULL,
PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RestorePermissions]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Setup_RestorePermissions]
    @name   sysname
AS
BEGIN
    DECLARE @object sysname
    DECLARE @protectType char(10)
    DECLARE @action varchar(60)
    DECLARE @grantee sysname
    DECLARE @cmd nvarchar(500)
    DECLARE c1 cursor FORWARD_ONLY FOR
        SELECT Object, ProtectType, [Action], Grantee FROM #aspnet_Permissions where Object = @name

    OPEN c1

    FETCH c1 INTO @object, @protectType, @action, @grantee
    WHILE (@@fetch_status = 0)
    BEGIN
        SET @cmd = @protectType + ' ' + @action + ' on ' + @object + ' TO [' + @grantee + ']'
        EXEC (@cmd)
        FETCH c1 INTO @object, @protectType, @action, @grantee
    END

    CLOSE c1
    DEALLOCATE c1
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RemoveAllRoleMembers]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Setup_RemoveAllRoleMembers]
    @name   sysname
AS
BEGIN
    CREATE TABLE #aspnet_RoleMembers
    (
        Group_name      sysname,
        Group_id        smallint,
        Users_in_group  sysname,
        User_id         smallint
    )

    INSERT INTO #aspnet_RoleMembers
    EXEC sp_helpuser @name

    DECLARE @user_id smallint
    DECLARE @cmd nvarchar(500)
    DECLARE c1 cursor FORWARD_ONLY FOR
        SELECT User_id FROM #aspnet_RoleMembers

    OPEN c1

    FETCH c1 INTO @user_id
    WHILE (@@fetch_status = 0)
    BEGIN
        SET @cmd = 'EXEC sp_droprolemember ' + '''' + @name + ''', ''' + USER_NAME(@user_id) + ''''
        EXEC (@cmd)
        FETCH c1 INTO @user_id
    END

    CLOSE c1
    DEALLOCATE c1
END
GO
/****** Object:  Table [dbo].[aspnet_SchemaVersions]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_SchemaVersions](
	[Feature] [nvarchar](128) NOT NULL,
	[CompatibleSchemaVersion] [nvarchar](128) NOT NULL,
	[IsCurrentVersion] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Feature] ASC,
	[CompatibleSchemaVersion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'common', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'health monitoring', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'membership', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'personalization', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'profile', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'role manager', N'1', 1)
/****** Object:  StoredProcedure [dbo].[netMasterLicenseSet]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netMasterLicenseSet]
	@MasterID			INT,
	@Updated			INT,
	@Updated2			INT,
	@VerificationTicket	INT
AS
BEGIN
	SET XACT_ABORT OFF
	SET NOCOUNT ON;
	UPDATE tblMasterSlave SET Updated = @Updated, Updated2 = @Updated2, VerificationTicket = @VerificationTicket WHERE MasterID = @MasterID
	-- No row to update insert the row
	IF (@@ROWCOUNT = 0)
	BEGIN	
		-- If race condition appears (the insert from the other thread happens after the update returns no rows) catch the exception and continue
		INSERT INTO tblMasterSlave (MasterID, Updated, Updated2, VerificationTicket) VALUES (@MasterID, @Updated, @Updated2, @VerificationTicket)
	IF  @@ERROR = 0 OR @@ERROR = 2627
			RETURN;
		ELSE
			RAISERROR('Unhandled EPiServer exception in stored procedure', 14, 1)	
		END
END
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[netMasterLicenseGet]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netMasterLicenseGet]
	@MasterID			INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT MasterID, Updated, Updated2, VerificationTicket FROM tblMasterSlave WHERE MasterID = @MasterID
END
SET ANSI_NULLS ON
GO
/****** Object:  UserDefinedFunction [dbo].[netDateTimeSubtract]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create FUNCTION [dbo].[netDateTimeSubtract]
(
	@DateTime1 DateTime,
	@DateTime2 DateTime
)
RETURNS BigInt
AS
BEGIN
declare @Return BigInt
Select @Return = (Convert(BigInt, 
	DATEDIFF(day, @DateTime1, @DateTime2)) * 86400000) + 
	(DATEDIFF(millisecond, 
		DATEADD(day, 
			DATEDIFF(day, @DateTime1, @DateTime2)
		, @DateTime1)
	, @DateTime2
	)
)
return @Return
END
GO
/****** Object:  StoredProcedure [dbo].[netReportPublishedPages]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Return a list of pages in a particular branch of the tree published between a start date and a stop date
CREATE PROCEDURE [dbo].[netReportPublishedPages](
	@PageID int,
	@StartDate datetime,
	@StopDate datetime,
	@Language int = -1,
	@ChangedByUserName nvarchar(256) = null,
	@PageSize int,
	@PageNumber int = 0,
	@SortColumn varchar(40) = 'StartPublish',
	@SortDescending bit = 0
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @OrderBy NVARCHAR(MAX)
	SET @OrderBy =
		CASE @SortColumn
			WHEN 'PageName' THEN 'tblPageLanguage.Name'
			WHEN 'StartPublish' THEN 'tblPageLanguage.StartPublish'
			WHEN 'StopPublish' THEN 'tblPageLanguage.StopPublish'
			WHEN 'ChangedBy' THEN 'tblPageLanguage.ChangedByName'
			WHEN 'Saved' THEN 'tblPageLanguage.Saved'
			WHEN 'Language' THEN 'tblLanguageBranch.LanguageID'
			WHEN 'PageTypeName' THEN 'tblPageType.Name'
		END
	IF(@SortDescending = 1)
		SET @OrderBy = @OrderBy + ' DESC'
	DECLARE @sql NVARCHAR(MAX)
	SET @sql = 'WITH PageCTE AS
	(
		SELECT ROW_NUMBER() OVER(ORDER BY ' 
			+ @OrderBy
			+ ') AS rownum,
		tblPageLanguage.fkPageID, tblPageLanguage.PublishedVersion, count(*) over () as totcount
		FROM tblPageLanguage 
		INNER JOIN tblTree ON tblTree.fkChildID=tblPageLanguage.fkPageID 
		INNER JOIN tblPage ON tblPage.pkID=tblPageLanguage.fkPageID 
		INNER JOIN tblPageType ON tblPageType.pkID=tblPage.fkPageTypeID 
		INNER JOIN tblLanguageBranch ON tblLanguageBranch.pkID=tblPageLanguage.fkLanguageBranchID
		WHERE 
		(tblTree.fkParentID=@PageID OR (tblPageLanguage.fkPageID=@PageID AND tblTree.NestingLevel = 1 ))
		AND 
		(@StartDate IS NULL OR tblPageLanguage.StartPublish>@StartDate)
		AND
		(@StopDate IS NULL OR tblPageLanguage.StartPublish<@StopDate)
		AND
		(@Language = -1 OR tblPageLanguage.fkLanguageBranchID = @Language)
		AND
		(@ChangedByUserName IS NULL OR tblPageLanguage.ChangedByName = @ChangedByUserName)
		AND (tblPageLanguage.PublishedVersion IS NOT NULL)
	)
	SELECT PageCTE.fkPageID, PageCTE.PublishedVersion, PageCTE.rownum, totcount
	FROM PageCTE
	WHERE rownum > @PageSize * (@PageNumber)
	AND rownum <= @PageSize * (@PageNumber+1)
	ORDER BY rownum'
	EXEC sp_executesql @sql, N'@PageID int, @StartDate datetime, @StopDate datetime, @Language int, @ChangedByUserName nvarchar(256), @PageSize int, @PageNumber int',
		@PageID = @PageID, 
		@StartDate = @StartDate, 
		@StopDate = @StopDate, 
		@Language = @Language, 
		@ChangedByUserName = @ChangedByUserName, 
		@PageSize = @PageSize, 
		@PageNumber = @PageNumber
END
GO
/****** Object:  StoredProcedure [dbo].[netReportExpiredPages]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Returns a list of pages which will expire between the supplied dates in a particular branch of the tree.
CREATE PROCEDURE [dbo].[netReportExpiredPages](
	@PageID int,
	@StartDate datetime,
	@StopDate datetime,
	@Language int = -1,
	@PageSize int,
	@PageNumber int = 0,
	@SortColumn varchar(40) = 'StopPublish',
	@SortDescending bit = 0,
	@PublishedByName nvarchar(256) = null
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @OrderBy NVARCHAR(MAX)
	SET @OrderBy =
		CASE @SortColumn
			WHEN 'PageName' THEN 'tblPageLanguage.Name'
			WHEN 'StartPublish' THEN 'tblPageLanguage.StartPublish'
			WHEN 'StopPublish' THEN 'tblPageLanguage.StopPublish'
			WHEN 'ChangedBy' THEN 'tblPageLanguage.ChangedByName'
			WHEN 'Saved' THEN 'tblPageLanguage.Saved'
			WHEN 'Language' THEN 'tblLanguageBranch.LanguageID'
			WHEN 'PageTypeName' THEN 'tblPageType.Name'
		END
	IF(@SortDescending = 1)
		SET @OrderBy = @OrderBy + ' DESC'
    DECLARE @sql NVARCHAR(MAX)
	SET @sql = 'WITH PageCTE AS
    (
        SELECT ROW_NUMBER() OVER(ORDER BY ' 
			+ @OrderBy 
			+ ') AS rownum,
        tblPageLanguage.fkPageID, tblPageLanguage.PublishedVersion, count(tblPageLanguage.fkPageID) over () as totcount                        
        FROM tblPageLanguage 
        INNER JOIN tblTree ON tblTree.fkChildID=tblPageLanguage.fkPageID 
        INNER JOIN tblPage ON tblPage.pkID=tblPageLanguage.fkPageID 
        INNER JOIN tblPageType ON tblPageType.pkID=tblPage.fkPageTypeID 
        INNER JOIN tblLanguageBranch ON tblLanguageBranch.pkID=tblPageLanguage.fkLanguageBranchID 
        WHERE 
        (tblTree.fkParentID = @PageID OR (tblPageLanguage.fkPageID = @PageID AND tblTree.NestingLevel = 1))
        AND 
        (@StartDate IS NULL OR tblPageLanguage.StopPublish>@StartDate)
        AND
        (@StopDate IS NULL OR tblPageLanguage.StopPublish<@StopDate)
		AND
		(@Language = -1 OR tblPageLanguage.fkLanguageBranchID = @Language)
		AND
		(LEN(@PublishedByName) = 0 OR tblPageLanguage.ChangedByName = @PublishedByName)
    )
    SELECT PageCTE.fkPageID, PageCTE.PublishedVersion, PageCTE.rownum, totcount
    FROM PageCTE
    WHERE rownum > @PageSize * (@PageNumber)
    AND rownum <= @PageSize * (@PageNumber+1)
    ORDER BY rownum'
    EXEC sp_executesql @sql, N'@PageID int, @StartDate datetime, @StopDate datetime, @Language int, @PublishedByName nvarchar(256), @PageSize int, @PageNumber int',
		@PageID = @PageID, 
		@StartDate = @StartDate, 
		@StopDate = @StopDate, 
		@Language = @Language, 
		@PublishedByName = @PublishedByName, 
		@PageSize = @PageSize, 
		@PageNumber = @PageNumber
END
GO
/****** Object:  StoredProcedure [dbo].[netReportChangedPages]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Return a list of pages in a particular branch of the tree changed between a start date and a stop date
CREATE PROCEDURE [dbo].[netReportChangedPages](
	@PageID int,
	@StartDate datetime,
	@StopDate datetime,
	@Language int = -1,
	@ChangedByUserName nvarchar(256) = null,
	@PageSize int,
	@PageNumber int = 0,
	@SortColumn varchar(40) = 'Saved',
	@SortDescending bit = 0
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @OrderBy NVARCHAR(MAX)
	SET @OrderBy =
		CASE @SortColumn
			WHEN 'PageName' THEN 'tblPageLanguage.Name'
			WHEN 'ChangedBy' THEN 'tblPageLanguage.ChangedByName'
			WHEN 'Saved' THEN 'tblPageLanguage.Saved'
			WHEN 'Language' THEN 'tblLanguageBranch.LanguageID'
			WHEN 'PageTypeName' THEN 'tblPageType.Name'
		END
	IF(@SortDescending = 1)
		SET @OrderBy = @OrderBy + ' DESC'
	DECLARE @sql NVARCHAR(MAX)
	Set @sql = 'WITH PageCTE AS
	(
		SELECT ROW_NUMBER() OVER(ORDER BY '
			+ @OrderBy
			+ ') AS rownum,
		tblPageLanguage.fkPageID, tblPageLanguage.PublishedVersion, count(*) over () as totcount
		FROM tblPageLanguage 
		INNER JOIN tblTree ON tblTree.fkChildID=tblPageLanguage.fkPageID 
		INNER JOIN tblPage ON tblPage.pkID=tblPageLanguage.fkPageID 
		INNER JOIN tblPageType ON tblPageType.pkID=tblPage.fkPageTypeID 
		INNER JOIN tblLanguageBranch ON tblLanguageBranch.pkID=tblPageLanguage.fkLanguageBranchID 
		WHERE (tblTree.fkParentID=@PageID OR (tblPageLanguage.fkPageID=@PageID AND tblTree.NestingLevel = 1 ))
        AND (@StartDate IS NULL OR tblPageLanguage.Saved>@StartDate)
        AND (@StopDate IS NULL OR tblPageLanguage.Saved<@StopDate)
        AND (@Language = -1 OR tblPageLanguage.fkLanguageBranchID = @Language)
        AND (@ChangedByUserName IS NULL OR tblPageLanguage.ChangedByName = @ChangedByUserName)
        AND (@ChangedByUserName IS NULL OR tblPageLanguage.ChangedByName = @ChangedByUserName)
        AND tblPageLanguage.PublishedVersion IS NOT NULL
	)
	SELECT PageCTE.fkPageID, PageCTE.PublishedVersion, PageCTE.rownum, totcount
	FROM PageCTE
	WHERE rownum > @PageSize * (@PageNumber)
	AND rownum <= @PageSize * (@PageNumber+1)
	ORDER BY rownum'
	EXEC sp_executesql @sql, N'@PageID int, @StartDate datetime, @StopDate datetime, @Language int, @ChangedByUserName nvarchar(256), @PageSize int, @PageNumber int',
		@PageID = @PageID, 
		@StartDate = @StartDate, 
		@StopDate = @StopDate, 
		@Language = @Language, 
		@ChangedByUserName = @ChangedByUserName, 
		@PageSize = @PageSize, 
		@PageNumber = @PageNumber
END
GO
/****** Object:  Table [dbo].[tblChangeLog]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblChangeLog](
	[pkID] [bigint] IDENTITY(1,1) NOT NULL,
	[LogData] [nvarchar](max) NULL,
	[ChangeDate] [datetime] NOT NULL,
	[Category] [int] NOT NULL,
	[Action] [int] NOT NULL,
	[ChangedBy] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_tblChangeLog] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblChangeLog_ChangeDate] ON [dbo].[tblChangeLog] 
(
	[ChangeDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblChangeLog_Pkid_ChangeDate] ON [dbo].[tblChangeLog] 
(
	[pkID] ASC,
	[ChangeDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBigTableStoreConfig]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBigTableStoreConfig](
	[pkId] [bigint] IDENTITY(1,1) NOT NULL,
	[StoreName] [nvarchar](375) NOT NULL,
	[TableName] [nvarchar](128) NULL,
	[EntityTypeId] [int] NULL,
 CONSTRAINT [PK_tblBigTableStoreConfig] PRIMARY KEY CLUSTERED 
(
	[pkId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTableStoreConfig_StoreName] ON [dbo].[tblBigTableStoreConfig] 
(
	[StoreName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblBigTableStoreConfig] ON
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (1, N'XFormFolders', N'tblSystemBigTable', NULL)
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (2, N'EPiServer.Shell.Search.SearchProviderSetting', N'tblBigTable', NULL)
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (3, N'EPiServer.Core.PropertySettings.PropertySettingsWrapper', N'tblSystemBigTable', NULL)
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (4, N'EPiServer.Personalization.VisitorGroups.Criteria.ViewedCategoriesModel', N'tblBigTable', NULL)
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (5, N'EPiServer.Core.IndexingInformation', N'tblBigTable', NULL)
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (6, N'IndexRequestQueueDataStore', N'tblIndexRequestLog', NULL)
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (7, N'EPiServer.Packaging.Storage.StorageUpdateEntity', N'tblSystemBigTable', NULL)
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (8, N'EPiServer.Packaging.Storage.PackageEntity', N'tblSystemBigTable', NULL)
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (9, N'VisitorGroup', N'tblSystemBigTable', NULL)
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (11, N'EPiServer.Shell.Storage.PersonalizedViewSettingsStorage', N'tblSystemBigTable', NULL)
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (12, N'EPiServer.Shell.Profile.ProfileData', N'tblSystemBigTable', NULL)
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (13, N'BackgroundJobStore', N'tblSystemBigTable', NULL)
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (14, N'InUseNotifications', N'tblBigTable', NULL)
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (15, N'Drafts', N'tblBigTable', NULL)
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (16, N'EPiServer.Shell.Storage.ContainerData', N'tblBigTable', NULL)
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (17, N'EPiServer.Shell.Storage.ComponentData', N'tblBigTable', NULL)
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (18, N'System.Object', N'tblBigTable', NULL)
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (19, N'EPiServer.Cms.Shell.UI.Models.NotesData', N'tblSystemBigTable', NULL)
INSERT [dbo].[tblBigTableStoreConfig] ([pkId], [StoreName], [TableName], [EntityTypeId]) VALUES (20, N'QuickLinkSettings', N'tblSystemBigTable', NULL)
SET IDENTITY_INSERT [dbo].[tblBigTableStoreConfig] OFF
/****** Object:  Table [dbo].[tblCategory]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCategory](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkParentID] [int] NULL,
	[CategoryGUID] [uniqueidentifier] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[Available] [bit] NOT NULL,
	[Selectable] [bit] NOT NULL,
	[SuperCategory] [bit] NOT NULL,
	[CategoryName] [nvarchar](50) NOT NULL,
	[CategoryDescription] [nvarchar](255) NULL,
 CONSTRAINT [PK_tblCategory] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblCategory] ON
INSERT [dbo].[tblCategory] ([pkID], [fkParentID], [CategoryGUID], [SortOrder], [Available], [Selectable], [SuperCategory], [CategoryName], [CategoryDescription]) VALUES (1, NULL, N'885bd5b6-9f33-4615-8e85-6390072e824c', 1, 0, 0, 0, N'Root', N'Starting point')
SET IDENTITY_INSERT [dbo].[tblCategory] OFF
/****** Object:  Table [dbo].[tblBigTableIdentity]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBigTableIdentity](
	[pkId] [bigint] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[StoreName] [nvarchar](375) NOT NULL,
 CONSTRAINT [PK_tblBigTableIdentity] PRIMARY KEY CLUSTERED 
(
	[pkId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTableIdentity_Guid] ON [dbo].[tblBigTableIdentity] 
(
	[Guid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblBigTableIdentity] ON
INSERT [dbo].[tblBigTableIdentity] ([pkId], [Guid], [StoreName]) VALUES (3, N'16a9d8a3-c2c2-4ca9-9489-34b10a129318', N'EPiServer.Core.IndexingInformation')
INSERT [dbo].[tblBigTableIdentity] ([pkId], [Guid], [StoreName]) VALUES (4, N'668fee39-4739-42a3-82f9-c8ed4aaf143e', N'EPiServer.Packaging.Storage.PackageEntity')
INSERT [dbo].[tblBigTableIdentity] ([pkId], [Guid], [StoreName]) VALUES (5, N'c8492ee7-8c2f-4545-b37b-5fe85c4c096a', N'EPiServer.Packaging.Storage.PackageEntity')
INSERT [dbo].[tblBigTableIdentity] ([pkId], [Guid], [StoreName]) VALUES (6, N'6e7fe457-7efd-43a5-aa58-26baef041ec3', N'EPiServer.Packaging.Storage.PackageEntity')
INSERT [dbo].[tblBigTableIdentity] ([pkId], [Guid], [StoreName]) VALUES (7, N'edc685f1-fa19-44c8-a9db-c77a68ecc26a', N'EPiServer.Packaging.Storage.PackageEntity')
INSERT [dbo].[tblBigTableIdentity] ([pkId], [Guid], [StoreName]) VALUES (8, N'10fefe07-d364-4d98-baaf-dd4403c1315d', N'EPiServer.Packaging.Storage.StorageUpdateEntity')
INSERT [dbo].[tblBigTableIdentity] ([pkId], [Guid], [StoreName]) VALUES (9, N'7fa6f788-dad1-483a-ad2b-66fe88aa8f52', N'EPiServer.Shell.Storage.PersonalizedViewSettingsStorage')
INSERT [dbo].[tblBigTableIdentity] ([pkId], [Guid], [StoreName]) VALUES (10, N'cb73a62b-b5d4-b093-8b91-5658d7930239', N'EPiServer.Shell.Storage.ContainerData')
INSERT [dbo].[tblBigTableIdentity] ([pkId], [Guid], [StoreName]) VALUES (11, N'b0d8f074-5197-b209-4c35-bc94e846463a', N'EPiServer.Shell.Storage.ContainerData')
INSERT [dbo].[tblBigTableIdentity] ([pkId], [Guid], [StoreName]) VALUES (12, N'cf5275fc-6626-4081-a08a-a839bcf489ac', N'EPiServer.Shell.Storage.ComponentData')
INSERT [dbo].[tblBigTableIdentity] ([pkId], [Guid], [StoreName]) VALUES (14, N'994bf1fb-9e5d-4143-9157-27c6ac84e103', N'System.Object')
INSERT [dbo].[tblBigTableIdentity] ([pkId], [Guid], [StoreName]) VALUES (15, N'14ee97b5-4366-4bdb-a294-69bda49843da', N'EPiServer.Shell.Storage.ComponentData')
INSERT [dbo].[tblBigTableIdentity] ([pkId], [Guid], [StoreName]) VALUES (16, N'6ce14104-a0d8-48d4-aa20-c9503e6f15b2', N'System.Object')
SET IDENTITY_INSERT [dbo].[tblBigTableIdentity] OFF
/****** Object:  Table [dbo].[tblBigTable]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBigTable](
	[pkId] [bigint] NOT NULL,
	[Row] [int] NOT NULL,
	[StoreName] [nvarchar](375) NOT NULL,
	[ItemType] [nvarchar](2000) NOT NULL,
	[Boolean01] [bit] NULL,
	[Boolean02] [bit] NULL,
	[Boolean03] [bit] NULL,
	[Boolean04] [bit] NULL,
	[Boolean05] [bit] NULL,
	[Integer01] [int] NULL,
	[Integer02] [int] NULL,
	[Integer03] [int] NULL,
	[Integer04] [int] NULL,
	[Integer05] [int] NULL,
	[Integer06] [int] NULL,
	[Integer07] [int] NULL,
	[Integer08] [int] NULL,
	[Integer09] [int] NULL,
	[Integer10] [int] NULL,
	[Long01] [bigint] NULL,
	[Long02] [bigint] NULL,
	[Long03] [bigint] NULL,
	[Long04] [bigint] NULL,
	[Long05] [bigint] NULL,
	[DateTime01] [datetime] NULL,
	[DateTime02] [datetime] NULL,
	[DateTime03] [datetime] NULL,
	[DateTime04] [datetime] NULL,
	[DateTime05] [datetime] NULL,
	[Guid01] [uniqueidentifier] NULL,
	[Guid02] [uniqueidentifier] NULL,
	[Guid03] [uniqueidentifier] NULL,
	[Float01] [float] NULL,
	[Float02] [float] NULL,
	[Float03] [float] NULL,
	[Float04] [float] NULL,
	[Float05] [float] NULL,
	[Float06] [float] NULL,
	[Float07] [float] NULL,
	[Decimal01] [decimal](18, 3) NULL,
	[Decimal02] [decimal](18, 3) NULL,
	[String01] [nvarchar](max) NULL,
	[String02] [nvarchar](max) NULL,
	[String03] [nvarchar](max) NULL,
	[String04] [nvarchar](max) NULL,
	[String05] [nvarchar](max) NULL,
	[String06] [nvarchar](max) NULL,
	[String07] [nvarchar](max) NULL,
	[String08] [nvarchar](max) NULL,
	[String09] [nvarchar](max) NULL,
	[String10] [nvarchar](max) NULL,
	[Binary01] [varbinary](max) NULL,
	[Binary02] [varbinary](max) NULL,
	[Binary03] [varbinary](max) NULL,
	[Binary04] [varbinary](max) NULL,
	[Binary05] [varbinary](max) NULL,
	[Indexed_Boolean01] [bit] NULL,
	[Indexed_Integer01] [int] NULL,
	[Indexed_Integer02] [int] NULL,
	[Indexed_Integer03] [int] NULL,
	[Indexed_Long01] [bigint] NULL,
	[Indexed_Long02] [bigint] NULL,
	[Indexed_DateTime01] [datetime] NULL,
	[Indexed_Guid01] [uniqueidentifier] NULL,
	[Indexed_Float01] [float] NULL,
	[Indexed_Float02] [float] NULL,
	[Indexed_Float03] [float] NULL,
	[Indexed_Decimal01] [decimal](18, 3) NULL,
	[Indexed_String01] [nvarchar](450) NULL,
	[Indexed_String02] [nvarchar](450) NULL,
	[Indexed_String03] [nvarchar](450) NULL,
	[Indexed_Binary01] [varbinary](900) NULL,
 CONSTRAINT [PK_tblBigTable] PRIMARY KEY CLUSTERED 
(
	[pkId] ASC,
	[Row] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTable_Indexed_Binary01] ON [dbo].[tblBigTable] 
(
	[Indexed_Binary01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTable_Indexed_Boolean01] ON [dbo].[tblBigTable] 
(
	[Indexed_Boolean01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTable_Indexed_DateTime01] ON [dbo].[tblBigTable] 
(
	[Indexed_DateTime01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTable_Indexed_Decimal01] ON [dbo].[tblBigTable] 
(
	[Indexed_Decimal01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTable_Indexed_Float01] ON [dbo].[tblBigTable] 
(
	[Indexed_Float01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTable_Indexed_Float02] ON [dbo].[tblBigTable] 
(
	[Indexed_Float02] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTable_Indexed_Float03] ON [dbo].[tblBigTable] 
(
	[Indexed_Float03] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTable_Indexed_Guid01] ON [dbo].[tblBigTable] 
(
	[Indexed_Guid01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTable_Indexed_Integer01] ON [dbo].[tblBigTable] 
(
	[Indexed_Integer01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTable_Indexed_Integer02] ON [dbo].[tblBigTable] 
(
	[Indexed_Integer02] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTable_Indexed_Integer03] ON [dbo].[tblBigTable] 
(
	[Indexed_Integer03] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTable_Indexed_Long01] ON [dbo].[tblBigTable] 
(
	[Indexed_Long01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTable_Indexed_Long02] ON [dbo].[tblBigTable] 
(
	[Indexed_Long02] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTable_Indexed_String01] ON [dbo].[tblBigTable] 
(
	[Indexed_String01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTable_Indexed_String02] ON [dbo].[tblBigTable] 
(
	[Indexed_String02] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTable_Indexed_String03] ON [dbo].[tblBigTable] 
(
	[Indexed_String03] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTable_StoreName] ON [dbo].[tblBigTable] 
(
	[StoreName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
INSERT [dbo].[tblBigTable] ([pkId], [Row], [StoreName], [ItemType], [Boolean01], [Boolean02], [Boolean03], [Boolean04], [Boolean05], [Integer01], [Integer02], [Integer03], [Integer04], [Integer05], [Integer06], [Integer07], [Integer08], [Integer09], [Integer10], [Long01], [Long02], [Long03], [Long04], [Long05], [DateTime01], [DateTime02], [DateTime03], [DateTime04], [DateTime05], [Guid01], [Guid02], [Guid03], [Float01], [Float02], [Float03], [Float04], [Float05], [Float06], [Float07], [Decimal01], [Decimal02], [String01], [String02], [String03], [String04], [String05], [String06], [String07], [String08], [String09], [String10], [Binary01], [Binary02], [Binary03], [Binary04], [Binary05], [Indexed_Boolean01], [Indexed_Integer01], [Indexed_Integer02], [Indexed_Integer03], [Indexed_Long01], [Indexed_Long02], [Indexed_DateTime01], [Indexed_Guid01], [Indexed_Float01], [Indexed_Float02], [Indexed_Float03], [Indexed_Decimal01], [Indexed_String01], [Indexed_String02], [Indexed_String03], [Indexed_Binary01]) VALUES (3, 1, N'EPiServer.Core.IndexingInformation', N'EPiServer.Core.IndexingInformation, EPiServer, Version=7.0.586.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A11801554086 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTable] ([pkId], [Row], [StoreName], [ItemType], [Boolean01], [Boolean02], [Boolean03], [Boolean04], [Boolean05], [Integer01], [Integer02], [Integer03], [Integer04], [Integer05], [Integer06], [Integer07], [Integer08], [Integer09], [Integer10], [Long01], [Long02], [Long03], [Long04], [Long05], [DateTime01], [DateTime02], [DateTime03], [DateTime04], [DateTime05], [Guid01], [Guid02], [Guid03], [Float01], [Float02], [Float03], [Float04], [Float05], [Float06], [Float07], [Decimal01], [Decimal02], [String01], [String02], [String03], [String04], [String05], [String06], [String07], [String08], [String09], [String10], [Binary01], [Binary02], [Binary03], [Binary04], [Binary05], [Indexed_Boolean01], [Indexed_Integer01], [Indexed_Integer02], [Indexed_Integer03], [Indexed_Long01], [Indexed_Long02], [Indexed_DateTime01], [Indexed_Guid01], [Indexed_Float01], [Indexed_Float02], [Indexed_Float03], [Indexed_Decimal01], [Indexed_String01], [Indexed_String02], [Indexed_String03], [Indexed_Binary01]) VALUES (10, 1, N'EPiServer.Shell.Storage.ContainerData', N'EPiServer.Shell.Storage.ContainerData, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'EPiServer.Shell.ViewComposition.Containers.TabContainer', N'/episerver/shell/dashboard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTable] ([pkId], [Row], [StoreName], [ItemType], [Boolean01], [Boolean02], [Boolean03], [Boolean04], [Boolean05], [Integer01], [Integer02], [Integer03], [Integer04], [Integer05], [Integer06], [Integer07], [Integer08], [Integer09], [Integer10], [Long01], [Long02], [Long03], [Long04], [Long05], [DateTime01], [DateTime02], [DateTime03], [DateTime04], [DateTime05], [Guid01], [Guid02], [Guid03], [Float01], [Float02], [Float03], [Float04], [Float05], [Float06], [Float07], [Decimal01], [Decimal02], [String01], [String02], [String03], [String04], [String05], [String06], [String07], [String08], [String09], [String10], [Binary01], [Binary02], [Binary03], [Binary04], [Binary05], [Indexed_Boolean01], [Indexed_Integer01], [Indexed_Integer02], [Indexed_Integer03], [Indexed_Long01], [Indexed_Long02], [Indexed_DateTime01], [Indexed_Guid01], [Indexed_Float01], [Indexed_Float02], [Indexed_Float03], [Indexed_Decimal01], [Indexed_String01], [Indexed_String02], [Indexed_String03], [Indexed_Binary01]) VALUES (11, 1, N'EPiServer.Shell.Storage.ContainerData', N'EPiServer.Shell.Storage.ContainerData, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'EPiServer.Shell.ViewComposition.Containers.ComponentContainer', N'/episerver/shell/dashboard/defaulttab', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTable] ([pkId], [Row], [StoreName], [ItemType], [Boolean01], [Boolean02], [Boolean03], [Boolean04], [Boolean05], [Integer01], [Integer02], [Integer03], [Integer04], [Integer05], [Integer06], [Integer07], [Integer08], [Integer09], [Integer10], [Long01], [Long02], [Long03], [Long04], [Long05], [DateTime01], [DateTime02], [DateTime03], [DateTime04], [DateTime05], [Guid01], [Guid02], [Guid03], [Float01], [Float02], [Float03], [Float04], [Float05], [Float06], [Float07], [Decimal01], [Decimal02], [String01], [String02], [String03], [String04], [String05], [String06], [String07], [String08], [String09], [String10], [Binary01], [Binary02], [Binary03], [Binary04], [Binary05], [Indexed_Boolean01], [Indexed_Integer01], [Indexed_Integer02], [Indexed_Integer03], [Indexed_Long01], [Indexed_Long02], [Indexed_DateTime01], [Indexed_Guid01], [Indexed_Float01], [Indexed_Float02], [Indexed_Float03], [Indexed_Decimal01], [Indexed_String01], [Indexed_String02], [Indexed_String03], [Indexed_Binary01]) VALUES (12, 1, N'EPiServer.Shell.Storage.ComponentData', N'EPiServer.Shell.Storage.ComponentData, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'EPiServer.Cms.Shell.UI.Controllers.NotesController', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTable] ([pkId], [Row], [StoreName], [ItemType], [Boolean01], [Boolean02], [Boolean03], [Boolean04], [Boolean05], [Integer01], [Integer02], [Integer03], [Integer04], [Integer05], [Integer06], [Integer07], [Integer08], [Integer09], [Integer10], [Long01], [Long02], [Long03], [Long04], [Long05], [DateTime01], [DateTime02], [DateTime03], [DateTime04], [DateTime05], [Guid01], [Guid02], [Guid03], [Float01], [Float02], [Float03], [Float04], [Float05], [Float06], [Float07], [Decimal01], [Decimal02], [String01], [String02], [String03], [String04], [String05], [String06], [String07], [String08], [String09], [String10], [Binary01], [Binary02], [Binary03], [Binary04], [Binary05], [Indexed_Boolean01], [Indexed_Integer01], [Indexed_Integer02], [Indexed_Integer03], [Indexed_Long01], [Indexed_Long02], [Indexed_DateTime01], [Indexed_Guid01], [Indexed_Float01], [Indexed_Float02], [Indexed_Float03], [Indexed_Decimal01], [Indexed_String01], [Indexed_String02], [Indexed_String03], [Indexed_Binary01]) VALUES (14, 1, N'System.Object', N'System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTable] ([pkId], [Row], [StoreName], [ItemType], [Boolean01], [Boolean02], [Boolean03], [Boolean04], [Boolean05], [Integer01], [Integer02], [Integer03], [Integer04], [Integer05], [Integer06], [Integer07], [Integer08], [Integer09], [Integer10], [Long01], [Long02], [Long03], [Long04], [Long05], [DateTime01], [DateTime02], [DateTime03], [DateTime04], [DateTime05], [Guid01], [Guid02], [Guid03], [Float01], [Float02], [Float03], [Float04], [Float05], [Float06], [Float07], [Decimal01], [Decimal02], [String01], [String02], [String03], [String04], [String05], [String06], [String07], [String08], [String09], [String10], [Binary01], [Binary02], [Binary03], [Binary04], [Binary05], [Indexed_Boolean01], [Indexed_Integer01], [Indexed_Integer02], [Indexed_Integer03], [Indexed_Long01], [Indexed_Long02], [Indexed_DateTime01], [Indexed_Guid01], [Indexed_Float01], [Indexed_Float02], [Indexed_Float03], [Indexed_Decimal01], [Indexed_String01], [Indexed_String02], [Indexed_String03], [Indexed_Binary01]) VALUES (15, 1, N'EPiServer.Shell.Storage.ComponentData', N'EPiServer.Shell.Storage.ComponentData, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'EPiServer.Cms.Shell.UI.Controllers.QuickLinksController', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTable] ([pkId], [Row], [StoreName], [ItemType], [Boolean01], [Boolean02], [Boolean03], [Boolean04], [Boolean05], [Integer01], [Integer02], [Integer03], [Integer04], [Integer05], [Integer06], [Integer07], [Integer08], [Integer09], [Integer10], [Long01], [Long02], [Long03], [Long04], [Long05], [DateTime01], [DateTime02], [DateTime03], [DateTime04], [DateTime05], [Guid01], [Guid02], [Guid03], [Float01], [Float02], [Float03], [Float04], [Float05], [Float06], [Float07], [Decimal01], [Decimal02], [String01], [String02], [String03], [String04], [String05], [String06], [String07], [String08], [String09], [String10], [Binary01], [Binary02], [Binary03], [Binary04], [Binary05], [Indexed_Boolean01], [Indexed_Integer01], [Indexed_Integer02], [Indexed_Integer03], [Indexed_Long01], [Indexed_Long02], [Indexed_DateTime01], [Indexed_Guid01], [Indexed_Float01], [Indexed_Float02], [Indexed_Float03], [Indexed_Decimal01], [Indexed_String01], [Indexed_String02], [Indexed_String03], [Indexed_Binary01]) VALUES (16, 1, N'System.Object', N'System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
/****** Object:  Table [dbo].[tblBigTableStoreInfo]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBigTableStoreInfo](
	[fkStoreId] [bigint] NOT NULL,
	[PropertyName] [nvarchar](75) NOT NULL,
	[PropertyMapType] [nvarchar](64) NOT NULL,
	[PropertyIndex] [int] NOT NULL,
	[PropertyType] [nvarchar](2000) NOT NULL,
	[Active] [bit] NOT NULL,
	[Version] [int] NOT NULL,
	[ColumnName] [nvarchar](128) NULL,
	[ColumnRowIndex] [int] NULL,
 CONSTRAINT [PK_tblBigTableStoreInfo] PRIMARY KEY CLUSTERED 
(
	[fkStoreId] ASC,
	[PropertyName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (1, N'ParentFolderId', N'1', 1, N'EPiServer.Data.Identity, EPiServer.Data, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', 1, 1, N'Guid01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (1, N'SubPath', N'1', 2, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (2, N'FullName', N'1', 1, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (2, N'IsEnabled', N'1', 2, N'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Boolean01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (2, N'SortIndex', N'1', 3, N'System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Integer01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (3, N'Description', N'1', 1, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (3, N'DisplayName', N'1', 2, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String02', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (3, N'IsDefault', N'1', 3, N'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Boolean01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (3, N'IsGlobal', N'1', 4, N'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Boolean02', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (3, N'PropertySettings', N'2', 5, N'EPiServer.Core.PropertySettings.IPropertySettings, EPiServer, Version=7.0.586.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', 1, 1, NULL, NULL)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (3, N'TypeFullName', N'1', 6, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String03', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (4, N'CategoryGuid', N'1', 1, N'System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Guid01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (4, N'NumberOfPageViews', N'1', 2, N'System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Integer01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (4, N'SelectedCategory', N'1', 3, N'System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Integer02', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (5, N'ExecutionDate', N'1', 1, N'System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'DateTime01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (5, N'ResetIndex', N'1', 2, N'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Boolean01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (6, N'IndexItemId', N'1', 1, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (6, N'NamedIndex', N'1', 2, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Indexed_String01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (6, N'NamedIndexingService', N'1', 3, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Indexed_String01', 2)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (6, N'SyndicationItemXml', N'1', 4, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String02', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (6, N'Timestamp', N'1', 5, N'System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Indexed_DateTime01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (7, N'ServerId', N'1', 1, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (7, N'UpdateDate', N'1', 2, N'System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'DateTime01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (8, N'InstallDate', N'1', 1, N'System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'DateTime01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (8, N'InstalledBy', N'1', 2, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (8, N'NotificationProcessed', N'1', 3, N'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Boolean01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (8, N'PackageId', N'1', 4, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String02', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (8, N'ServerId', N'1', 5, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String03', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (8, N'Version', N'1', 6, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String04', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (9, N'Criteria', N'3', 1, N'System.Collections.Generic.IList`1[[EPiServer.Personalization.VisitorGroups.VisitorGroupCriterion, EPiServer.ApplicationModules, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, NULL, NULL)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (9, N'CriteriaOperator', N'1', 2, N'EPiServer.Personalization.VisitorGroups.CriteriaOperator, EPiServer.ApplicationModules, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', 1, 1, N'Integer01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (9, N'EnableStatistics', N'1', 3, N'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Boolean01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (9, N'IsSecurityRole', N'1', 4, N'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Boolean02', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (9, N'Name', N'1', 5, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (9, N'Notes', N'1', 6, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String02', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (9, N'PointsThreshold', N'1', 7, N'System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Integer02', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (11, N'CustomizedContainers', N'3', 1, N'System.Collections.Generic.IList`1[[EPiServer.Shell.Storage.ComponentData, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, NULL, NULL)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (11, N'UserName', N'1', 2, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Indexed_String01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (11, N'ViewName', N'1', 3, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Indexed_String02', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (12, N'Settings', N'3', 1, N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, NULL, NULL)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (12, N'UserName', N'1', 2, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (13, N'CommandEvents', N'3', 1, N'System.Collections.Generic.IList`1[[CommandDescriptor, EPiServer.UI, Version=7.0.586.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, NULL, NULL)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (13, N'CurrentState', N'1', 2, N'EPiServer.UI.Edit.BackgroundJobs.Store.BackgroundJobState, EPiServer.UI, Version=7.0.586.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', 1, 1, N'Integer01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (13, N'Data', N'1', 3, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (13, N'EndDate', N'1', 4, N'System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'DateTime01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (13, N'LastHeartBeat', N'1', 5, N'System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'DateTime02', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (13, N'Message', N'1', 6, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String02', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (13, N'Owner', N'1', 7, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String03', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (13, N'StartDate', N'1', 8, N'System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'DateTime03', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (13, N'Title', N'1', 9, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String04', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (14, N'AddedManually', N'1', 1, N'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 2, N'Boolean01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (14, N'ContentGuid', N'1', 2, N'System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 2, N'Indexed_Guid01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (14, N'CreateTime', N'1', 3, N'System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 2, N'DateTime01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (14, N'LanguageBranch', N'1', 4, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 2, N'Indexed_String01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (14, N'Modified', N'1', 5, N'System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 2, N'DateTime02', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (14, N'User', N'1', 6, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 2, N'Indexed_String02', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (15, N'IsNewLanguageBranch', N'1', 1, N'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Boolean01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (15, N'IsNewPage', N'1', 2, N'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Boolean02', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (15, N'IsNotified', N'1', 3, N'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Boolean03', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (15, N'LanguageBranch', N'1', 4, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Indexed_String01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (15, N'PageGuid', N'1', 5, N'System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Indexed_Guid01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (15, N'PageLink', N'1', 6, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Indexed_String02', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (15, N'PageName', N'1', 7, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (15, N'PagePath', N'1', 8, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String02', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (15, N'PageType', N'1', 9, N'System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Integer01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (15, N'ParentLink', N'1', 10, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String03', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (15, N'Properties', N'3', 11, N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, NULL, NULL)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (15, N'Saved', N'1', 12, N'System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Indexed_DateTime01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (15, N'SavedBy', N'1', 13, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Indexed_String03', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (16, N'Components', N'3', 1, N'System.Collections.Generic.IList`1[[EPiServer.Shell.Storage.ComponentData, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, NULL, NULL)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (16, N'DefinitionName', N'1', 2, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (16, N'PlugInArea', N'1', 3, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String02', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (16, N'Settings', N'3', 4, N'System.Collections.Generic.IDictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, NULL, NULL)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (17, N'DefinitionName', N'1', 1, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (17, N'Settings', N'3', 2, N'System.Collections.Generic.IDictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, NULL, NULL)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (19, N'BackgroundColor', N'1', 1, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (19, N'Content', N'1', 2, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String02', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (19, N'FontSize', N'1', 3, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String03', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (19, N'GadgetId', N'1', 4, N'System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Guid01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (19, N'Title', N'1', 5, N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'String04', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (20, N'DisabledLinks', N'3', 1, N'System.Collections.Generic.IList`1[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, NULL, NULL)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (20, N'GadgetId', N'1', 2, N'System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, N'Guid01', 1)
INSERT [dbo].[tblBigTableStoreInfo] ([fkStoreId], [PropertyName], [PropertyMapType], [PropertyIndex], [PropertyType], [Active], [Version], [ColumnName], [ColumnRowIndex]) VALUES (20, N'PersonalLinks', N'3', 3, N'System.Collections.Generic.IList`1[[EPiServer.Cms.Shell.QuickLinks.PersonalQuickLink, EPiServer.Cms.Shell.UI, Version=1.0.431.0, Culture=neutral, PublicKeyToken=8fe83dea738b45b7]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', 1, 1, NULL, NULL)
/****** Object:  Table [dbo].[tblBigTableReference]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBigTableReference](
	[pkId] [bigint] NOT NULL,
	[Type] [int] NOT NULL,
	[PropertyName] [nvarchar](75) NOT NULL,
	[CollectionType] [nvarchar](2000) NULL,
	[ElementType] [nvarchar](2000) NULL,
	[ElementStoreName] [nvarchar](375) NULL,
	[IsKey] [bit] NOT NULL,
	[Index] [int] NOT NULL,
	[BooleanValue] [bit] NULL,
	[IntegerValue] [int] NULL,
	[LongValue] [bigint] NULL,
	[DateTimeValue] [datetime] NULL,
	[GuidValue] [uniqueidentifier] NULL,
	[FloatValue] [float] NULL,
	[StringValue] [nvarchar](max) NULL,
	[BinaryValue] [varbinary](max) NULL,
	[RefIdValue] [bigint] NULL,
	[ExternalIdValue] [bigint] NULL,
 CONSTRAINT [PK_tblBigTableReference] PRIMARY KEY CLUSTERED 
(
	[pkId] ASC,
	[PropertyName] ASC,
	[IsKey] ASC,
	[Index] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IDX_tblBigTableReference_RefIdValue] ON [dbo].[tblBigTableReference] 
(
	[RefIdValue] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (9, 3, N'CustomizedContainers', N'System.Collections.Generic.List`1[[EPiServer.Shell.Storage.ComponentData, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, NULL, 0, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (9, 3, N'CustomizedContainers', N'System.Collections.Generic.List`1[[EPiServer.Shell.Storage.ComponentData, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'EPiServer.Shell.Storage.ContainerData, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', N'EPiServer.Shell.Storage.ContainerData', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (10, 3, N'Components', N'System.Collections.Generic.List`1[[EPiServer.Shell.Storage.ComponentData, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, NULL, 0, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (10, 3, N'Components', N'System.Collections.Generic.List`1[[EPiServer.Shell.Storage.ComponentData, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'EPiServer.Shell.Storage.ContainerData, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', N'EPiServer.Shell.Storage.ContainerData', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (10, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, NULL, 0, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (10, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, N'top', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (10, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (10, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 0, 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (10, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, N'/episerver/shell/dashboard', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (10, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'EPiServer.Shell.ViewComposition.Containers.BorderContainerRegion, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', NULL, 0, 4, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (10, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, NULL, 1, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (10, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, N'tabPosition', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (10, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'useMenu', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (10, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, N'useSlider', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (10, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, N'containersPlugInArea', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (10, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 1, 4, NULL, NULL, NULL, NULL, NULL, NULL, N'region', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (11, 3, N'Components', N'System.Collections.Generic.List`1[[EPiServer.Shell.Storage.ComponentData, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, NULL, 0, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (11, 3, N'Components', N'System.Collections.Generic.List`1[[EPiServer.Shell.Storage.ComponentData, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'EPiServer.Shell.Storage.ComponentData, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', N'EPiServer.Shell.Storage.ComponentData', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (11, 3, N'Components', N'System.Collections.Generic.List`1[[EPiServer.Shell.Storage.ComponentData, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'EPiServer.Shell.Storage.ComponentData, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', N'EPiServer.Shell.Storage.ComponentData', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (11, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, NULL, 0, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (11, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, N'dashboard', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (11, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'3', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (11, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, N'New Tab', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (11, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 0, 3, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (11, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, NULL, 1, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (11, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, N'componentCategory', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (11, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'numberOfColumns', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (11, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, N'title', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (11, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, N'closable', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (12, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, NULL, 0, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (12, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (12, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.Object', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (12, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, N'/EPiServer/CMS/1.0.431/ClientResources/Notes/Notes.js', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (12, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, NULL, 1, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (12, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, N'column', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (12, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'gadgetActions[0]', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (12, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, N'scriptPaths[0]', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (15, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, NULL, 0, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (15, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (15, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.Object', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (15, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, N'/EPiServer/CMS/1.0.431/ClientResources/QuickLinks/QuickLinks.js', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (15, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, N'/EPiServer/CMS/1.0.431/ClientResources/QuickLinks/QuickLinks.css', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (15, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, NULL, 1, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (15, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, N'column', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (15, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'gadgetActions[0]', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (15, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, N'scriptPaths[0]', NULL, NULL, NULL)
INSERT [dbo].[tblBigTableReference] ([pkId], [Type], [PropertyName], [CollectionType], [ElementType], [ElementStoreName], [IsKey], [Index], [BooleanValue], [IntegerValue], [LongValue], [DateTimeValue], [GuidValue], [FloatValue], [StringValue], [BinaryValue], [RefIdValue], [ExternalIdValue]) VALUES (15, 3, N'Settings', N'System.Collections.Generic.Dictionary`2[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089],[System.Object, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', N'System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089', NULL, 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, N'stylePaths[0]', NULL, NULL, NULL)
/****** Object:  StoredProcedure [dbo].[SchemaDelete]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SchemaDelete]
(
	@SchemaId	NVARCHAR(256)
)
AS
BEGIN
	DELETE FROM tblSchema WHERE [SchemaId]=@SchemaId
END
GO
/****** Object:  StoredProcedure [dbo].[RetrieveNonblockingInstanceStateIds]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RetrieveNonblockingInstanceStateIds]
@ownerID uniqueidentifier = NULL,
@ownedUntil datetime = NULL,
@now datetime
AS
    SELECT uidInstanceID FROM [dbo].[InstanceState] WITH (TABLOCK,UPDLOCK,HOLDLOCK)
    WHERE blocked=0 AND status<>1 AND status<>3 AND status<>2 -- not blocked and not completed and not terminated and not suspended
 		AND ( ownerID IS NULL OR ownedUntil<GETUTCDATE() )
    if ( @@ROWCOUNT > 0 )
    BEGIN
        -- lock the table entries that are returned
        Update [dbo].[InstanceState]  
        set ownerID = @ownerID,
	    ownedUntil = @ownedUntil
        WHERE blocked=0 AND status<>1 AND status<>3 AND status<>2
 		AND ( ownerID IS NULL OR ownedUntil<GETUTCDATE() )
    END
GO
/****** Object:  StoredProcedure [dbo].[RetrieveInstanceState]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[RetrieveInstanceState]
@uidInstanceID uniqueidentifier,
@ownerID uniqueidentifier = NULL,
@ownedUntil datetime = NULL,
@result int output,
@currentOwnerID uniqueidentifier output
As
Begin
    declare @localized_string_RetrieveInstanceState_Failed_Ownership nvarchar(256)
    set @localized_string_RetrieveInstanceState_Failed_Ownership = N'Instance ownership conflict'
    set @result = 0
    set @currentOwnerID = @ownerID
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
	BEGIN TRANSACTION
    -- Possible workflow status: 0 for executing; 1 for completed; 2 for suspended; 3 for terminated; 4 for invalid
	if @ownerID IS NOT NULL	-- if id is null then just loading readonly state, so ignore the ownership check
	begin
		  Update [dbo].[InstanceState]  
		  set	ownerID = @ownerID,
				ownedUntil = @ownedUntil
		  where uidInstanceID = @uidInstanceID AND (    ownerID = @ownerID 
													 OR ownerID IS NULL 
													 OR ownedUntil<GETUTCDATE()
													)
		  if ( @@ROWCOUNT = 0 )
		  BEGIN
			-- RAISERROR(@localized_string_RetrieveInstanceState_Failed_Ownership, 16, -1)
			select @currentOwnerID=ownerID from [dbo].[InstanceState] Where uidInstanceID = @uidInstanceID 
			if (  @@ROWCOUNT = 0 )
				set @result = -1
			else
				set @result = -2
			GOTO DONE
		  END
	end
    Select state from [dbo].[InstanceState]  
    Where uidInstanceID = @uidInstanceID
	set @result = @@ROWCOUNT;
    if ( @result = 0 )
	begin
		set @result = -1
		GOTO DONE
	end
DONE:
	COMMIT TRANSACTION
	RETURN
End
GO
/****** Object:  StoredProcedure [dbo].[RetrieveExpiredTimerIds]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RetrieveExpiredTimerIds]
@ownerID uniqueidentifier = NULL,
@ownedUntil datetime = NULL,
@now datetime
AS
    SELECT uidInstanceID FROM [dbo].[InstanceState]
    WHERE nextTimer<@now AND status<>1 AND status<>3 AND status<>2 -- not blocked and not completed and not terminated and not suspended
        AND ((unlocked=1 AND ownerID IS NULL) OR ownedUntil<GETUTCDATE() )
GO
/****** Object:  StoredProcedure [dbo].[RetrieveCompletedScope]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RetrieveCompletedScope]
@completedScopeID uniqueidentifier,
@result int output
AS
BEGIN
    SELECT state FROM [dbo].[CompletedScope] WHERE completedScopeID=@completedScopeID
	set @result = @@ROWCOUNT;
End
GO
/****** Object:  StoredProcedure [dbo].[RetrieveANonblockingInstanceStateId]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RetrieveANonblockingInstanceStateId]
@ownerID uniqueidentifier = NULL,
@ownedUntil datetime = NULL,
@uidInstanceID uniqueidentifier = NULL output,
@found bit = NULL output
AS
 BEGIN
		--
		-- Guarantee that no one else grabs this record between the select and update
		SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
		BEGIN TRANSACTION
SET ROWCOUNT 1
		SELECT	@uidInstanceID = uidInstanceID
		FROM	[dbo].[InstanceState] WITH (updlock) 
		WHERE	blocked=0 
		AND	status NOT IN ( 1,2,3 )
 		AND	( ownerID IS NULL OR ownedUntil<GETUTCDATE() )
SET ROWCOUNT 0
		IF @uidInstanceID IS NOT NULL
		 BEGIN
			UPDATE	[dbo].[InstanceState]  
			SET		ownerID = @ownerID,
					ownedUntil = @ownedUntil
			WHERE	uidInstanceID = @uidInstanceID
			SET @found = 1
		 END
		ELSE
		 BEGIN
			SET @found = 0
		 END
		COMMIT TRANSACTION
 END
GO
/****** Object:  StoredProcedure [dbo].[RetrieveAllInstanceDescriptions]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[RetrieveAllInstanceDescriptions]
As
	SELECT uidInstanceID, status, blocked, info, nextTimer
	FROM [dbo].[InstanceState]
GO
/****** Object:  StoredProcedure [dbo].[netWinRolesList]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netWinRolesList] 
(
	@GroupName NVARCHAR(255) = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
    SELECT
        GroupName
    FROM
        tblWindowsGroup
    WHERE
        (@GroupName IS NULL) OR
        (LoweredGroupName=LOWER(@GroupName))
    ORDER BY
        GroupName     
END
GO
/****** Object:  StoredProcedure [dbo].[netWinRolesGroupInsert]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netWinRolesGroupInsert] 
(
	@GroupName NVARCHAR(255)
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	DECLARE @LoweredName NVARCHAR(255)
    /* Check if group exists, insert it if not */
	SET @LoweredName=LOWER(@GroupName)
    INSERT INTO tblWindowsGroup
        (GroupName, 
		LoweredGroupName)
	SELECT
	    @GroupName,
	    @LoweredName
	WHERE NOT EXISTS(SELECT pkID FROM tblWindowsGroup WHERE LoweredGroupName=@LoweredName)
    /* Inserted group, return the id */
    IF (@@ROWCOUNT > 0)
    BEGIN
        RETURN  SCOPE_IDENTITY() 
    END
	DECLARE @GroupID INT
	SELECT @GroupID=pkID FROM tblWindowsGroup WHERE LoweredGroupName=@LoweredName
	RETURN @GroupID
END
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[netWinRolesList]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[netWinRolesList]
GO
/****** Object:  StoredProcedure [dbo].[netSchedulerLoadJob]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSchedulerLoadJob] 
	@pkID UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    SELECT CONVERT(NVARCHAR(40),pkID) AS pkID,Name,CONVERT(INT,Enabled) AS Enabled,LastExec,LastStatus,LastText,NextExec,[DatePart],Interval,MethodName,CONVERT(INT,fStatic) AS fStatic,TypeName,AssemblyName,InstanceData, IsRunning, CurrentStatusMessage, DateDiff(second, LastPing, GetDate()) as SecondsAfterLastPing
	FROM tblScheduledItem
	WHERE pkID = @pkID
END
GO
/****** Object:  StoredProcedure [dbo].[SchemaList]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SchemaList]
AS
BEGIN
	SELECT
		[pkID] AS DatabaseSchemaId,
		[SchemaId],
		[IdType]
	FROM 
		tblSchema
	ORDER BY
		[SchemaId]
END
GO
/****** Object:  StoredProcedure [dbo].[netPropertyDefinitionTypeSave]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPropertyDefinitionTypeSave]
(
	@ID 			INT OUTPUT,
	@Property 		INT,
	@Name 			NVARCHAR(255),
	@TypeName 		NVARCHAR(255) = NULL,
	@AssemblyName 	NVARCHAR(255) = NULL,
	@BlockTypeID	uniqueidentifier = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	/* In case several sites start up at sametime, e.g. in enterprise it may occour that both sites tries to insert at same time. 
	Therefore a check is made to see it it already exist an entry with same guid, and if so an update is performed instead of insert.*/
	IF @ID <= 0
	BEGIN
		SET @ID = ISNULL((SELECT pkId FROM tblPropertyDefinitionType WHERE fkContentTypeGUID = @BlockTypeID), @ID)
	END
	IF @ID<0
	BEGIN
		IF @AssemblyName='EPiServer'
			SELECT @ID = Max(pkID)+1 FROM tblPropertyDefinitionType WHERE pkID<1000
		ELSE
			SELECT @ID = CASE WHEN Max(pkID)<1000 THEN 1000 ELSE Max(pkID)+1 END FROM tblPropertyDefinitionType
		INSERT INTO tblPropertyDefinitionType
		(
			pkID,
			Property,
			Name,
			TypeName,
			AssemblyName,
			fkContentTypeGUID
		)
		VALUES
		(
			@ID,
			@Property,
			@Name,
			@TypeName,
			@AssemblyName,
			@BlockTypeID
		)
	END
	ELSE
		UPDATE tblPropertyDefinitionType SET
			Name 		= @Name,
			Property		= @Property,
			TypeName 	= @TypeName,
			AssemblyName 	= @AssemblyName,
			fkContentTypeGUID = @BlockTypeID
		WHERE pkID=@ID
END
GO
/****** Object:  StoredProcedure [dbo].[netPlugInSynchronize]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPlugInSynchronize]
(
	@AssemblyName NVARCHAR(255),
	@TypeName NVARCHAR(255),
	@DefaultEnabled Bit
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @id INT
	SELECT @id = pkID FROM tblPlugIn WHERE AssemblyName=@AssemblyName AND TypeName=@TypeName
	IF @id IS NULL
	BEGIN
		INSERT INTO tblPlugIn(AssemblyName,TypeName,Enabled) VALUES(@AssemblyName,@TypeName,@DefaultEnabled)
		SET @id =  SCOPE_IDENTITY() 
	END
	SELECT pkID, TypeName, AssemblyName, Saved, Created, Enabled FROM tblPlugIn WHERE pkID = @id
END
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[netPlugInSaveSettings]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPlugInSaveSettings]
@PlugInID 		INT,
@Settings 		NVARCHAR(MAX)
AS
BEGIN
	UPDATE tblPlugIn SET
		Settings 	= @Settings,
		Saved		= GetDate()
	WHERE pkID = @PlugInID
END
GO
/****** Object:  StoredProcedure [dbo].[netPlugInSave]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPlugInSave]
@PlugInID 		INT,
@Enabled 		BIT
AS
BEGIN
	UPDATE tblPlugIn SET
		Enabled 	= @Enabled,
		Saved		= GetDate()
	WHERE pkID = @PlugInID
END
GO
/****** Object:  StoredProcedure [dbo].[netPlugInLoad]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPlugInLoad]
@PlugInID INT
AS
BEGIN
	SELECT pkID, AssemblyName, TypeName, Settings, Saved, Created, Enabled
	FROM tblPlugIn
	WHERE pkID = @PlugInID OR @PlugInID = -1
END
GO
/****** Object:  StoredProcedure [dbo].[netPropertySearchStringMeta]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPropertySearchStringMeta]
(
	@PageID				INT,
	@PropertyName 		NVARCHAR(255),
	@UseWildCardsBefore	BIT = 0,
	@UseWildCardsAfter	BIT = 0,
	@String				NVARCHAR(2000) = NULL,
	@LanguageBranch		NCHAR(17) = NULL
)
AS
BEGIN
	DECLARE @SearchString NVARCHAR(2010)
	DECLARE @DynSqlSelect NVARCHAR(2000)
	DECLARE @DynSqlWhere NVARCHAR(2000)
	DECLARE @LangBranchID NCHAR(17)
	SELECT @LangBranchID=pkID FROM tblLanguageBranch WHERE LanguageID=@LanguageBranch
	IF @LangBranchID IS NULL 
	BEGIN 
		IF @LanguageBranch IS NOT NULL
			RAISERROR('Language branch %s is not defined',16,1, @LanguageBranch)
		ELSE
			SET @LangBranchID = -1
	END
	SELECT @SearchString=CASE    WHEN @UseWildCardsBefore=0 AND @UseWildCardsAfter=0 THEN @String
						WHEN @UseWildCardsBefore=1 AND @UseWildCardsAfter=0 THEN N'%' + @String
						WHEN @UseWildCardsBefore=0 AND @UseWildCardsAfter=1 THEN @String + N'%'
						ELSE N'%' + @String + N'%'
					END
	SET @DynSqlSelect = 'SELECT tblPageLanguage.fkPageID FROM tblPageLanguage INNER JOIN tblTree ON tblTree.fkChildID=tblPageLanguage.fkPageID INNER JOIN tblContent as tblPage ON tblPageLanguage.fkPageID=tblPage.pkID'
	SET @DynSqlWhere = ' WHERE tblPage.ContentType = 0 AND tblTree.fkParentID=@PageID'
	IF (@LangBranchID <> -1)
		SET @DynSqlWhere = @DynSqlWhere + ' AND tblPageLanguage.fkLanguageBranchID=@LangBranchID'
	IF (@PropertyName = 'PageName')
	BEGIN
		IF (@String IS NULL)
			SET @DynSqlWhere = @DynSqlWhere + ' AND tblPageLanguage.Name IS NULL'
		ELSE
			SET @DynSqlWhere = @DynSqlWhere + ' AND tblPageLanguage.Name LIKE @SearchString'
	END
	ELSE IF (@PropertyName = 'PageLinkURL')
	BEGIN
		IF (@String IS NULL)
			SET @DynSqlWhere = @DynSqlWhere + ' AND tblPageLanguage.LinkURL IS NULL'
		ELSE
			SET @DynSqlWhere = @DynSqlWhere + ' AND tblPageLanguage.LinkURL LIKE @SearchString'
	END
	ELSE IF (@PropertyName = 'PageCreatedBy')
	BEGIN
		IF (@String IS NULL)
			SET @DynSqlWhere = @DynSqlWhere + ' AND tblPageLanguage.CreatorName IS NULL'
		ELSE
			SET @DynSqlWhere = @DynSqlWhere + ' AND tblPageLanguage.CreatorName LIKE @SearchString'
	END
	ELSE IF (@PropertyName = 'PageChangedBy')
	BEGIN
		IF (@String IS NULL)
			SET @DynSqlWhere = @DynSqlWhere + ' AND tblPageLanguage.ChangedByName IS NULL'
		ELSE
			SET @DynSqlWhere = @DynSqlWhere + ' AND tblPageLanguage.ChangedByName LIKE @SearchString'
	END
	ELSE IF (@PropertyName = 'PageTypeName')
	BEGIN
		SET @DynSqlSelect = @DynSqlSelect + ' INNER JOIN tblPageType ON tblPageType.pkID=tblPage.fkContentTypeID'
		IF (@String IS NULL)
			SET @DynSqlWhere = @DynSqlWhere + ' AND tblPageType.Name IS NULL'
		ELSE
			SET @DynSqlWhere = @DynSqlWhere + ' AND tblPageType.Name LIKE @SearchString'
	END
	ELSE IF (@PropertyName = 'PageExternalURL')
	BEGIN
		IF (@String IS NULL)
			SET @DynSqlWhere = @DynSqlWhere + ' AND tblPageLanguage.ExternalURL IS NULL'
		ELSE
			SET @DynSqlWhere = @DynSqlWhere + ' AND tblPageLanguage.ExternalURL LIKE @SearchString'
	END
	ELSE IF (@PropertyName = 'PageLanguageBranch')
	BEGIN
        SET @DynSqlSelect = @DynSqlSelect + ' INNER JOIN tblLanguageBranch ON tblLanguageBranch.pkid = tblPageLanguage.fklanguagebranchid'
		IF (@String IS NULL)
			SET @DynSqlWhere = @DynSqlWhere + ' AND tblLanguageBranch.languageid IS NULL'
		ELSE
			SET @DynSqlWhere = @DynSqlWhere + ' AND RTRIM(tblLanguageBranch.languageid) LIKE @SearchString'
	END
	ELSE IF (@PropertyName = 'PageShortcutLink')
	BEGIN
	    IF (@String IS NULL)
	        SET @DynSqlWhere = @DynSqlWhere + ' AND tblPageLanguage.PageLinkGUID IS NULL' 
	    ELSE
	        SET @DynSqlWhere = @DynSqlWhere + ' AND tblPageLanguage.PageLinkGUID LIKE @SearchString'
	END
	ELSE IF (@PropertyName = 'PageArchiveLink')
	BEGIN
	    IF (@String IS NULL)
	        SET @DynSqlWhere = @DynSqlWhere + ' AND tblPage.ArchiveContentGUID IS NULL' 
	    ELSE
	        SET @DynSqlWhere = @DynSqlWhere + ' AND tblPage.ArchiveContentGUID LIKE @SearchString'
	END
	ELSE IF (@PropertyName = 'PageURLSegment')
	BEGIN
	    IF (@String IS NULL)
	        SET @DynSqlWhere = @DynSqlWhere + ' AND tblPageLanguage.URLSegment IS NULL' 
	    ELSE
	        SET @DynSqlWhere = @DynSqlWhere + ' AND tblPageLanguage.URLSegment LIKE @SearchString'
	END
	SET @DynSqlSelect = @DynSqlSelect + @DynSqlWhere
	EXEC sp_executesql @DynSqlSelect, 
		N'@PageID INT, @LangBranchID NCHAR(17), @SearchString NVARCHAR(2010)',
		@PageID=@PageID,
		@LangBranchID=@LangBranchID, 
		@SearchString=@SearchString
END
GO
/****** Object:  StoredProcedure [dbo].[netPropertySearchValueMeta]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPropertySearchValueMeta]
(
	@PageID			INT,
	@PropertyName 	NVARCHAR(255),
	@Equals			BIT = 0,
	@NotEquals		BIT = 0,
	@GreaterThan	BIT = 0,
	@LessThan		BIT = 0,
	@Boolean		BIT = NULL,
	@Number			INT = NULL,
	@FloatNumber	FLOAT = NULL,
	@PageType		INT = NULL,
	@PageLink		INT = NULL,
	@Date			DATETIME = NULL,
	@LanguageBranch	NCHAR(17) = NULL
)
AS
BEGIN
	DECLARE @LangBranchID NCHAR(17)
	SELECT @LangBranchID=pkID FROM tblLanguageBranch WHERE LanguageID=@LanguageBranch
	IF @LangBranchID IS NULL 
	BEGIN 
		if @LanguageBranch IS NOT NULL
			RAISERROR('Language branch %s is not defined',16,1, @LanguageBranch)
		else
			SET @LangBranchID = -1
	END
	DECLARE @DynSql NVARCHAR(2000)
	DECLARE @compare NVARCHAR(2)
	IF (@Equals = 1)
	BEGIN
	    SET @compare = '='
	END
	ELSE IF (@GreaterThan = 1)
	BEGIN
	    SET @compare = '>'
	END
	ELSE IF (@LessThan = 1)
	BEGIN
	    SET @compare = '<'
	END
	ELSE IF (@NotEquals = 1)
	BEGIN
	    SET @compare = '<>'
	END
	ELSE
	BEGIN
	    RAISERROR('No compare condition is defined.',16,1)
	END
	SET @DynSql = 'SELECT PageLanguages.fkPageID FROM tblPageLanguage as PageLanguages INNER JOIN tblTree ON tblTree.fkChildID=PageLanguages.fkPageID INNER JOIN tblContent as Pages ON Pages.pkID=PageLanguages.fkPageID'
	IF (@PropertyName = 'PageArchiveLink')
	BEGIN
		SET @DynSql = @DynSql + ' INNER JOIN tblContent as Pages2 ON Pages.ArchiveContentGUID = Pages2.ContentGUID'
	END
	IF (@PropertyName = 'PageShortcutLink')
	BEGIN
		SET @DynSql = @DynSql + ' INNER JOIN tblContent as Pages2 ON PageLanguages.PageLinkGUID = Pages2.ContentGUID'
	END
	SET @DynSql = @DynSql + ' WHERE Pages.ContentType = 0 AND tblTree.fkParentID=@PageID'
	IF (@LangBranchID <> -1)
	BEGIN
	    SET @DynSql = @DynSql + ' AND PageLanguages.fkLanguageBranchID=@LangBranchID'
	END
	IF (@PropertyName = 'PageVisibleInMenu')
	BEGIN
	    SET @DynSql = @DynSql + ' AND Pages.VisibleInMenu=@Boolean'
	END
	ELSE IF (@PropertyName = 'PageTypeID')
	BEGIN
	    IF (@Equals=1)
	        SET @DynSql = @DynSql + ' AND (Pages.fkContentTypeID = @PageType OR (@PageType IS NULL AND Pages.fkContentTypeID IS NULL))'
	    ELSE
	        SET @DynSql = @DynSql + ' AND (Pages.fkContentTypeID' + @compare + '@PageType OR (@PageType IS NULL AND NOT Pages.fkContentTypeID IS NULL))'
	END
	ELSE IF (@PropertyName = 'PageLink')
	BEGIN
	    IF (@Equals=1)
	        SET @DynSql = @DynSql + ' AND (PageLanguages.fkPageID = @PageLink OR (@PageLink IS NULL AND PageLanguages.fkPageID IS NULL))'
	    ELSE
	        SET @DynSql = @DynSql + ' AND (PageLanguages.fkPageID' + @compare + '@PageLink OR (@PageLink IS NULL AND NOT PageLanguages.fkPageID IS NULL))'
	END
	ELSE IF (@PropertyName = 'PageParentLink')
	BEGIN
	    IF (@Equals=1)
	        SET @DynSql = @DynSql + ' AND (Pages.fkParentID = @PageLink OR (@PageLink IS NULL AND Pages.fkParentID IS NULL))'
	    ELSE
	        SET @DynSql = @DynSql + ' AND (Pages.fkParentID' + @compare + '@PageLink OR (@PageLink IS NULL AND NOT Pages.fkParentID IS NULL))'
	END
	ELSE IF (@PropertyName = 'PageShortcutLink')
	BEGIN
		SET @DynSql = @DynSql + ' AND (Pages2.pkID' + @compare + '@PageLink OR (@PageLink IS NULL AND NOT PageLanguages.PageLinkGUID IS NULL))'
	END
	ELSE IF (@PropertyName = 'PageArchiveLink')
	BEGIN
		SET @DynSql = @DynSql + ' AND (Pages2.pkID' + @compare + '@PageLink OR (@PageLink IS NULL AND NOT Pages.ArchiveContentGUID IS NULL))'
	END
	ELSE IF (@PropertyName = 'PageChanged')
	BEGIN
	    IF (@Equals=1)
	        SET @DynSql = @DynSql + ' AND (PageLanguages.Changed = @Date OR (@Date IS NULL AND PageLanguages.Changed IS NULL))'
	    ELSE
	        SET @DynSql = @DynSql + ' AND (PageLanguages.Changed' + @compare + '@Date OR (@Date IS NULL AND NOT PageLanguages.Changed IS NULL))'
	END
	ELSE IF (@PropertyName = 'PageCreated')
	BEGIN
	    IF (@Equals=1)
	        SET @DynSql = @DynSql + ' AND (PageLanguages.Created = @Date OR (@Date IS NULL AND PageLanguages.Created IS NULL))'
	    ELSE
	        SET @DynSql = @DynSql + ' AND (PageLanguages.Created' + @compare + '@Date OR (@Date IS NULL AND NOT PageLanguages.Created IS NULL))'
	END
	ELSE IF (@PropertyName = 'PageSaved')
	BEGIN
	    IF (@Equals=1)
	        SET @DynSql = @DynSql + ' AND (PageLanguages.Saved = @Date OR (@Date IS NULL AND PageLanguages.Saved IS NULL))'
	    ELSE
	        SET @DynSql = @DynSql + ' AND (PageLanguages.Saved' + @compare + '@Date  OR (@Date IS NULL AND NOT PageLanguages.Saved IS NULL))'
	END
	ELSE IF (@PropertyName = 'PageStartPublish')
	BEGIN
	    IF (@Equals=1)
	        SET @DynSql = @DynSql + ' AND (PageLanguages.StartPublish = @Date OR (@Date IS NULL AND PageLanguages.StartPublish IS NULL))'
	    ELSE
	        SET @DynSql = @DynSql + ' AND (PageLanguages.StartPublish' + @compare + '@Date OR (@Date IS NULL AND NOT PageLanguages.StartPublish IS NULL))'
	END
	ELSE IF (@PropertyName = 'PageStopPublish')
	BEGIN
	    IF (@Equals=1)
	        SET @DynSql = @DynSql + ' AND (PageLanguages.StopPublish = @Date OR (@Date IS NULL AND PageLanguages.StopPublish IS NULL))'
	    ELSE
	        SET @DynSql = @DynSql + ' AND (PageLanguages.StopPublish' + @compare + '@Date OR (@Date IS NULL AND NOT PageLanguages.StopPublish IS NULL))'
	END
	ELSE IF (@PropertyName = 'PageDeleted')
	BEGIN
		SET @DynSql = @DynSql + ' AND Pages.Deleted = @Boolean'
	END
	ELSE IF (@PropertyName = 'PagePendingPublish')
	BEGIN
		SET @DynSql = @DynSql + ' AND PageLanguages.PendingPublish = @Boolean'
	END
	ELSE IF (@PropertyName = 'PageFolderID')
	BEGIN
	    IF (@Equals=1)
	        SET @DynSql = @DynSql + ' AND (Pages.ExternalFolderID = @Number OR (@Number IS NULL AND Pages.ExternalFolderID IS NULL))'
	    ELSE
	        SET @DynSql = @DynSql + ' AND (Pages.ExternalFolderID' + @compare + '@Number OR (@Number IS NULL AND NOT Pages.ExternalFolderID IS NULL))'
	END
	ELSE IF (@PropertyName = 'PageShortcutType')
	BEGIN
	    IF (@Number=0)
	        SET @DynSql = @DynSql + ' AND PageLanguages.AutomaticLink=1 AND PageLanguages.PageLinkGUID IS NULL'
	    ELSE IF (@Number=1)
	        SET @DynSql = @DynSql + ' AND PageLanguages.AutomaticLink=1 AND NOT PageLanguages.PageLinkGUID IS NULL AND PageLanguages.FetchData=0'
	    ELSE IF (@Number=2)
	        SET @DynSql = @DynSql + ' AND PageLanguages.AutomaticLink=0 AND PageLanguages.LinkURL<>N''#'''
	    ELSE IF (@Number=3)
	        SET @DynSql = @DynSql + ' AND PageLanguages.AutomaticLink=0 AND PageLanguages.LinkURL=N''#'''
	    ELSE IF (@Number=4)
	        SET @DynSql = @DynSql + ' AND PageLanguages.AutomaticLink=1 AND PageLanguages.FetchData=1'
	END
	EXEC sp_executesql @DynSql, 
		N'@PageID INT, @LangBranchID NCHAR(17), @Boolean BIT, @Number INT, @PageType INT, @PageLink INT, @Date DATETIME',
		@PageID=@PageID,
		@LangBranchID=@LangBranchID, 
		@Boolean=@Boolean,
		@Number=@Number,
		@PageType=@PageType,
		@PageLink=@PageLink,
		@Date=@Date
END
GO
/****** Object:  StoredProcedure [dbo].[netRemoteSiteSave]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netRemoteSiteSave]
(
	@ID				INT OUTPUT,
	@Name			NVARCHAR(100),
	@Url			NVARCHAR(255),
	@IsTrusted		BIT = 0,
	@UserName		NVARCHAR(50) = NULL,
	@Password		NVARCHAR(50) = NULL,
	@Domain			NVARCHAR(50) = NULL,
	@AllowUrlLookup BIT = 0
)
AS
BEGIN
	IF @ID=0
	BEGIN
		INSERT INTO tblRemoteSite(Name,Url,IsTrusted,UserName,Password,Domain,AllowUrlLookup) VALUES(@Name,@Url,@IsTrusted,@UserName,@Password,@Domain,@AllowUrlLookup)
		SET @ID =  SCOPE_IDENTITY() 
	END
	ELSE
		UPDATE tblRemoteSite SET Name=@Name,Url=@Url,IsTrusted=@IsTrusted,UserName=@UserName,Password=@Password,Domain=@Domain,AllowUrlLookup=@AllowUrlLookup WHERE pkID=@ID
END
GO
/****** Object:  StoredProcedure [dbo].[netRemoteSiteLoad]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netRemoteSiteLoad]
(
	@ID INT
)
AS
BEGIN
	SELECT pkID,Name,Url,IsTrusted,UserName,[Password],Domain,AllowUrlLookup
	FROM tblRemoteSite
	WHERE pkID=@ID
END
GO
/****** Object:  StoredProcedure [dbo].[netRemoteSiteList]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netRemoteSiteList]
AS
BEGIN
SELECT pkID,Name,Url,IsTrusted,UserName,[Password],Domain,AllowUrlLookup
FROM tblRemoteSite
ORDER BY Name ASC
END
GO
/****** Object:  StoredProcedure [dbo].[netRemoteSiteDelete]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netRemoteSiteDelete]
(
	@ID INT OUTPUT
)
AS
BEGIN
	DELETE FROM tblRemoteSite WHERE pkID=@ID
END
GO
/****** Object:  StoredProcedure [dbo].[netSiteConfigSet]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSiteConfigSet]
	@SiteID VARCHAR(250),
	@PropertyName VARCHAR(250),
	@PropertyValue NVARCHAR(max)
AS
BEGIN
	DECLARE @Id AS INT
	SELECT @Id = pkID FROM tblSiteConfig WHERE SiteID = @SiteID AND PropertyName = @PropertyName
	IF @Id IS NOT NULL
	BEGIN
		-- Update
		UPDATE tblSiteConfig SET PropertyValue = @PropertyValue WHERE pkID = @Id
	END
	ELSE
	BEGIN
		INSERT INTO tblSiteConfig(SiteID, PropertyName, PropertyValue) VALUES(@SiteID, @PropertyName, @PropertyValue)
	END
END
GO
/****** Object:  StoredProcedure [dbo].[netSiteConfigGet]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSiteConfigGet]
	@SiteID VARCHAR(250) = NULL,
	@PropertyName VARCHAR(250)
AS
BEGIN
	IF @SiteID IS NULL
	BEGIN
		SELECT * FROM tblSiteConfig WHERE PropertyName = @PropertyName
	END
	ELSE
	BEGIN
		SELECT * FROM tblSiteConfig WHERE SiteID = @SiteID AND PropertyName = @PropertyName
	END
END
GO
/****** Object:  StoredProcedure [dbo].[netSiteConfigDelete]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSiteConfigDelete]
	@SiteID VARCHAR(250),
	@PropertyName VARCHAR(250)
AS
BEGIN
	DELETE FROM tblSiteConfig WHERE SiteID = @SiteID AND PropertyName = @PropertyName
END
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[netSchedulerSetRunningState]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSchedulerSetRunningState]
	@pkID UNIQUEIDENTIFIER,
	@IsRunning bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    UPDATE tblScheduledItem SET IsRunning = @IsRunning, LastPing = GETDATE(), CurrentStatusMessage = NULL WHERE pkID = @pkID
END
GO
/****** Object:  StoredProcedure [dbo].[netSchedulerSetCurrentStatus]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSchedulerSetCurrentStatus] 
	@pkID UNIQUEIDENTIFIER,
	@StatusMessage nvarchar(2048)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE tblScheduledItem SET CurrentStatusMessage = @StatusMessage
	WHERE pkID = @pkID
END
GO
/****** Object:  StoredProcedure [dbo].[netSchedulerSave]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSchedulerSave]
(
@pkID		UNIQUEIDENTIFIER,
@Name		NVARCHAR(50),
@Enabled	BIT = 0,
@NextExec 	DATETIME,
@DatePart	NCHAR(2) = NULL,
@Interval	INT = 0,
@MethodName NVARCHAR(100),
@fStatic 	BIT,
@TypeName 	NVARCHAR(100),
@AssemblyName NVARCHAR(100),
@InstanceData	IMAGE = NULL
)
AS
BEGIN
IF EXISTS(SELECT * FROM tblScheduledItem WHERE pkID=@pkID)
	UPDATE tblScheduledItem SET
		Name 		= @Name,
		Enabled 	= @Enabled,
		NextExec 	= @NextExec,
		[DatePart] 	= @DatePart,
		Interval 		= @Interval,
		MethodName 	= @MethodName,
		fStatic 		= @fStatic,
		TypeName 	= @TypeName,
		AssemblyName 	= @AssemblyName,
		InstanceData	= @InstanceData
	WHERE pkID = @pkID
ELSE
	INSERT INTO tblScheduledItem(pkID,Name,Enabled,NextExec,[DatePart],Interval,MethodName,fStatic,TypeName,AssemblyName,InstanceData)
	VALUES(@pkID,@Name,@Enabled,@NextExec,@DatePart,@Interval, @MethodName,@fStatic,@TypeName,@AssemblyName,@InstanceData)
END
GO
/****** Object:  StoredProcedure [dbo].[netSchedulerList]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSchedulerList]
AS
BEGIN
	SELECT CONVERT(NVARCHAR(40),pkID) AS pkID,Name,CONVERT(INT,Enabled) AS Enabled,LastExec,LastStatus,LastText,NextExec,[DatePart],Interval,MethodName,CONVERT(INT,fStatic) AS fStatic,TypeName,AssemblyName,InstanceData, IsRunning, CurrentStatusMessage, DateDiff(second, LastPing, GetDate()) as SecondsAfterLastPing
	FROM tblScheduledItem
	ORDER BY Name ASC
END
GO
/****** Object:  StoredProcedure [dbo].[netSchedulerGetNext]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSchedulerGetNext]
(
	@ID			UNIQUEIDENTIFIER OUTPUT,	-- returned id of new item to queue 
	@NextExec	DATETIME		 OUTPUT		-- returned nextExec of new item to queue
)
AS
BEGIN
	SET NOCOUNT ON
	SET @ID = NULL
	SET @NextExec = NULL
	SELECT TOP 1 @ID = tblScheduledItem.pkID, @NextExec = tblScheduledItem.NextExec
	FROM tblScheduledItem
	WHERE NextExec IS NOT NULL AND
		Enabled = 1
	ORDER BY NextExec ASC
END
GO
/****** Object:  StoredProcedure [dbo].[netSchedulerExecute]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSchedulerExecute]
(
	@pkID     uniqueidentifier,
	@nextExec datetime,
	@utcnow datetime,
	@pingSeconds int
)
as
begin
	set nocount on
	/**
	 * is the scheduled nextExec still valid? 
	 * (that is, no one else has already begun executing it?)
	 */
	if exists( select * from tblScheduledItem with (rowlock,holdlock) where pkID = @pkID and NextExec = @nextExec and Enabled = 1 and (IsRunning <> 1 OR (GetDate() > DATEADD(second, @pingSeconds, LastPing))) )
	begin
		/**
		 * ya, calculate and set nextexec for the item 
		 * (or set to null if not recurring)
		 */
		update tblScheduledItem set NextExec =  case when coalesce(Interval,0) > 0 and [DatePart] is not null then 
															case [DatePart] when 'ms' then dateadd( ms, Interval, case when dateadd( ms, Interval, NextExec ) < @utcnow then @utcnow else NextExec end )
																			when 'ss' then dateadd( ss, Interval, case when dateadd( ss, Interval, NextExec ) < @utcnow then @utcnow else NextExec end )
																			when 'mi' then dateadd( mi, Interval, case when dateadd( mi, Interval, NextExec ) < @utcnow then @utcnow else NextExec end )
																			when 'hh' then dateadd( hh, Interval, case when dateadd( hh, Interval, NextExec ) < @utcnow then @utcnow else NextExec end )
																			when 'dd' then dateadd( dd, Interval, case when dateadd( dd, Interval, NextExec ) < @utcnow then @utcnow else NextExec end )
																			when 'wk' then dateadd( wk, Interval, case when dateadd( wk, Interval, NextExec ) < @utcnow then @utcnow else NextExec end )
																			when 'mm' then dateadd( mm, Interval, case when dateadd( mm, Interval, NextExec ) < @utcnow then @utcnow else NextExec end )
																			when 'yy' then dateadd( yy, Interval, case when dateadd( yy, Interval, NextExec ) < @utcnow then @utcnow else NextExec end )
															end
													 else null
									            end
		from   tblScheduledItem
		where  pkID = @pkID
		/**
		 * now retrieve all detailed data (type, assembly & instance) 
		 * for the job
		 */
		select	tblScheduledItem.MethodName,
				tblScheduledItem.fStatic,
				tblScheduledItem.TypeName,
				tblScheduledItem.AssemblyName,
				tblScheduledItem.InstanceData
		from	tblScheduledItem
		where	pkID = @pkID
	end
end
GO
/****** Object:  StoredProcedure [dbo].[netSchedulerAdd]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSchedulerAdd]
(
	@out_Id			uniqueidentifier output,
	@methodName		nvarchar(100),
	@fStatic		bit,
	@typeName		nvarchar(100),
	@assemblyName	nvarchar(100),
	@data			image,
	@dtExec			datetime,
	@sRecurDatePart	nchar(2),
	@Interval		int,
	@out_fRefresh	bit output
)
as
begin
	set nocount on
	select @out_Id = newid()
	select @out_fRefresh = case when exists( select * from tblScheduledItem where NextExec < @dtExec ) then 0 else 1 end
	insert into tblScheduledItem( pkID, Enabled, MethodName, fStatic, TypeName, AssemblyName, NextExec, [DatePart], [Interval], InstanceData )
	   values( @out_Id, 1, @methodName, @fStatic, @typeName, @assemblyName, @dtExec, @sRecurDatePart, @Interval, @data )
	return
end
GO
/****** Object:  StoredProcedure [dbo].[netUnifiedPathSave]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netUnifiedPathSave]
(
    @id int output, 
    @virtualPath nvarchar(4000), 
    @inheritAcl int
)
AS
BEGIN
	IF ((SELECT Count(*) FROM tblUnifiedPath WHERE pkID = @id) > 0)
	BEGIN
		UPDATE tblUnifiedPath
		SET InheritAcl = @inheritAcl
		WHERE pkID = @id
	END
	ELSE
	BEGIN
		IF ((SELECT Count(*) FROM tblUnifiedPath WHERE [Path]=@virtualPath) > 0)
		BEGIN 
			RAISERROR('A UnifiedPath %s already exist',16,1, @virtualPath)
		END
		INSERT INTO tblUnifiedPath([Path], InheritAcl)
		VALUES(@virtualPath, @inheritAcl)	
		SET @id =  SCOPE_IDENTITY() 
	END
END
GO
/****** Object:  StoredProcedure [dbo].[netUnifiedPathMove]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netUnifiedPathMove]
(
    @fromVirtualPath nvarchar(4000), 
    @toVirtualPath nvarchar(4000)
)
AS
BEGIN
UPDATE tblUnifiedPath
	SET [Path] = @toVirtualPath + SUBSTRING(Path, LEN(@fromVirtualPath) + 1, LEN([Path]) ) 
	WHERE LOWER([Path]) LIKE  LOWER(LOWER(@fromVirtualPath)) + '%'
END
GO
/****** Object:  StoredProcedure [dbo].[netWinMembershipListUsers]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netWinMembershipListUsers]
(
	@UserNameToMatch NVARCHAR(255) = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	SET @UserNameToMatch = LOWER(@UserNameToMatch)
	SELECT
		UserName
	FROM
		tblWindowsUser
	WHERE
	    (tblWindowsUser.LoweredUserName LIKE @UserNameToMatch) OR (@UserNameToMatch IS NULL)
	ORDER BY UserName
END
GO
/****** Object:  StoredProcedure [dbo].[netUniqueSequenceNext]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netUniqueSequenceNext]
(
    @Name NVARCHAR (100),
    @Steps INT,
    @NextValue INT OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT OFF
	DECLARE @ErrorVal INT
	/* Assume that row exists and try to do an update to get the next value */
	UPDATE tblUniqueSequence SET @NextValue = LastValue = LastValue + @Steps WHERE Name = @Name
	/* If no rows were updated, the row did not exist */
	IF @@ROWCOUNT=0
	BEGIN
		/* Try to insert row. The reason for not starting with insert is that this operation is only
		needed ONCE for a sequence, the first update will succeed after this initial insert. */
		INSERT INTO tblUniqueSequence (Name, LastValue) VALUES (@Name, @Steps)
		SET @ErrorVal=@@ERROR	
		/* An extra safety precaution - parallell execution caused another instance of this proc to
		insert the relevant row between our first update and our insert. This causes a unique constraint
		violation. Note that SET XACT_ABORT OFF prevents error from propagating to calling code. */
		IF @ErrorVal <> 0
		BEGIN
			IF @ErrorVal = 2627
			BEGIN
				/* Unique constraint violation - do the update again since the row now exists */
				UPDATE tblUniqueSequence SET @NextValue = LastValue = LastValue + @Steps WHERE Name = @Name
			END
			ELSE
			BEGIN
				/* Some other error than unique key violation, very unlikely but raise an error to make 
				sure it gets noticed. */
				RAISERROR(50001, 14, 1)
			END
		END
		ELSE
		BEGIN
			/* No error from insert, the "next value" will be equal to the requested number of steps. */
			SET @NextValue = @Steps
		END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[netPermissionSave]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPermissionSave]
(
	@Name NVARCHAR(255) = NULL,
	@IsRole INT = NULL,
	@Permission INT,
	@ClearByName INT = NULL,
	@ClearByPermission INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	IF (NOT @ClearByName IS NULL)
		DELETE FROM 
		    tblUserPermission 
		WHERE 
		    Name=@Name AND 
		IsRole=@IsRole
	IF (NOT @ClearByPermission IS NULL)
		DELETE FROM 
		    tblUserPermission 
		WHERE 
		    Permission=@Permission	
    IF ((@Name IS NULL) OR (@IsRole IS NULL))
        RETURN
	IF (NOT EXISTS(SELECT Name FROM tblUserPermission WHERE Name=@Name AND IsRole=@IsRole AND Permission=@Permission))
		INSERT INTO tblUserPermission 
		    (Name, 
		    IsRole, 
		    Permission) 
		VALUES 
		    (@Name, 
		    @IsRole, 
		    @Permission)
END
GO
/****** Object:  StoredProcedure [dbo].[netPermissionRoles]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPermissionRoles]
(
	@Permission	INT
)
AS
BEGIN
    SET NOCOUNT ON
    SELECT
        Name,
        IsRole
    FROM
        tblUserPermission
    WHERE
        Permission=@Permission
    ORDER BY
        IsRole
END
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[netPermissionDeleteMembership]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPermissionDeleteMembership]
(
	@Name	NVARCHAR(255) = NULL,
	@IsRole int = NULL
)
AS
BEGIN
    SET NOCOUNT ON
	IF(@Name IS NOT NULL AND @IsRole IS NOT NULL)
	BEGIN
		DELETE
		FROM
			tblUserPermission
		WHERE
			Name=@Name 
			AND 
			IsRole=@IsRole
    END
END
GO
/****** Object:  StoredProcedure [dbo].[netLanguageBranchUpdate]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netLanguageBranchUpdate]
(
	@ID INT,
	@Name NVARCHAR(255) = NULL,
	@LanguageID NCHAR(17),
	@SortIndex INT,
	@SystemIconPath NVARCHAR(255) = NULL,
	@URLSegment NVARCHAR(255) = NULL,
	@Enabled BIT,
	@ACL NVARCHAR(MAX) = NULL
)
AS
BEGIN
	UPDATE
		tblLanguageBranch
	SET
		[Name] = @Name,
		LanguageID = @LanguageID,
		SortIndex = @SortIndex,
		SystemIconPath = @SystemIconPath,
		URLSegment = @URLSegment,
		Enabled = @Enabled,
		ACL = @ACL
	WHERE
		pkID = @ID
END
GO
/****** Object:  StoredProcedure [dbo].[netLanguageBranchList]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netLanguageBranchList]
AS
BEGIN
	SELECT 
		pkID AS ID,
		Name,
		LanguageID,
		SortIndex,
		SystemIconPath,
		URLSegment,
		Enabled,
		ACL
	FROM 
		tblLanguageBranch
	ORDER BY 
		SortIndex
END
GO
/****** Object:  StoredProcedure [dbo].[netLanguageBranchInsert]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netLanguageBranchInsert]
(
	@ID INT OUTPUT,
	@Name NVARCHAR(50) = NULL,
	@LanguageID NCHAR(17),
	@SortIndex INT = 0,
	@SystemIconPath NVARCHAR(255) = NULL,
	@URLSegment NVARCHAR(255) = NULL,
	@Enabled BIT,
	@ACL NVARCHAR(MAX) = NULL
)
AS
BEGIN
	INSERT INTO tblLanguageBranch
	(
		LanguageID,
		[Name],
		SortIndex,
		SystemIconPath,
		URLSegment,
		Enabled,
		ACL
	)
	VALUES
	(
		@LanguageID,
		@Name,
		@SortIndex,
		@SystemIconPath,
		@URLSegment,
		@Enabled,
		@ACL
	)
	SET @ID	=  SCOPE_IDENTITY() 
END
GO
/****** Object:  StoredProcedure [dbo].[netLanguageBranchDelete]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netLanguageBranchDelete]
(
	@ID INT
)
AS
BEGIN
	DELETE FROM tblLanguageBranch WHERE pkID = @ID
END
GO
/****** Object:  StoredProcedure [dbo].[netFrameUpdate]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netFrameUpdate]
(
	@FrameID			INT,
	@FrameName			NVARCHAR(100),
	@FrameDescription		NVARCHAR(255)
)
AS
BEGIN
	SET NOCOUNT ON
	UPDATE 
		tblFrame 
	SET 
		FrameName='target="' + @FrameName + '"', 
		FrameDescription=@FrameDescription
	WHERE
		pkID=@FrameID
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[netFrameList]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netFrameList]
AS
BEGIN
	SET NOCOUNT ON
	SELECT
		pkID AS FrameID, 
		CASE
			WHEN FrameName IS NULL THEN
				N''
			ELSE
				SUBSTRING(FrameName, 9, LEN(FrameName) - 9)
		END AS FrameName,
		FrameDescription,
		'' AS FrameDescriptionLocalized,
		CONVERT(INT, SystemFrame) AS SystemFrame
	FROM
		tblFrame
	ORDER BY
		SystemFrame DESC,
		FrameName
END
GO
/****** Object:  StoredProcedure [dbo].[netFrameInsert]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netFrameInsert]
(
	@FrameID		INTEGER OUTPUT,
	@FrameName		NVARCHAR(100),
	@FrameDescription	NVARCHAR(255)
)
AS
BEGIN
	SET NOCOUNT ON
	INSERT INTO tblFrame
		(FrameName,
		FrameDescription)
	VALUES
		('target="' + @FrameName + '"', 
		@FrameDescription)
	SET @FrameID =  SCOPE_IDENTITY() 
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[netChangeLogTruncBySeqNDate]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netChangeLogTruncBySeqNDate]
(
	@LowestSequenceNumber BIGINT = NULL,
	@OlderThan DATETIME = NULL
)
AS
BEGIN	
	DELETE FROM tblChangeLog WHERE
	((@LowestSequenceNumber IS NULL) OR (pkID < @LowestSequenceNumber)) AND
	((@OlderThan IS NULL) OR (ChangeDate < @OlderThan))
END
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[netChangeLogTruncByRowsNDate]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netChangeLogTruncByRowsNDate]
(
	@RowsToTruncate BIGINT = NULL,
	@OlderThan DATETIME = NULL
)
AS
BEGIN	
	IF (@RowsToTruncate IS NOT NULL)
	BEGIN
		DELETE TOP(@RowsToTruncate) FROM tblChangeLog WHERE
		((@OlderThan IS NULL) OR (ChangeDate < @OlderThan))
		RETURN		
	END
	DELETE FROM tblChangeLog WHERE
	((@OlderThan IS NULL) OR (ChangeDate < @OlderThan))
END
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[netChangeLogSave]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netChangeLogSave]
  (@LogData          [nvarchar](max),
   @Category         INTEGER = 0,
   @Action			 INTEGER = 0,
   @ChangedBy        [nvarchar](255),
   @SequenceNumber   BIGINT OUTPUT,
   @ChangeDate       DATETIME OUTPUT
)
AS            
BEGIN
	SET @ChangeDate = getutcdate()
       INSERT INTO tblChangeLog VALUES(@LogData,
                                       @ChangeDate,
                                       @Category,
                                       @Action,
                                       @ChangedBy)
	SET @SequenceNumber = SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[netChangeLogGetRowsForwards]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netChangeLogGetRowsForwards]
(
	@from 	                 DATETIME = NULL,
	@to	                     DATETIME = NULL,
	@category 				 INT = NULL,
	@action 				 INT = NULL,
	@changedBy				 [nvarchar](255) = NULL,
	@startSequence			 BIGINT = NULL,
	@maxRows				 BIGINT)
AS
BEGIN    
        SELECT top(@maxRows) *
        FROM tblChangeLog TCL
        WHERE 
        ((@startSequence IS NULL) OR (TCL.pkID >= @startSequence)) AND
		((@from IS NULL) OR (TCL.ChangeDate >= @from)) AND
		((@to IS NULL) OR (TCL.ChangeDate <= @to)) AND  
        ((@category IS NULL) OR (@category = TCL.Category)) AND
        ((@action IS NULL) OR (@action = TCL.Action)) AND
        ((@changedBy IS NULL) OR (@changedBy = TCL.ChangedBy))
		ORDER BY TCL.pkID ASC
END
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[netChangeLogGetRowsBackwards]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netChangeLogGetRowsBackwards]
(
	@from 	                 DATETIME = NULL,
	@to	                     DATETIME = NULL,
	@category 				 INT = NULL,
	@action 				 INT = NULL,
	@changedBy				 [nvarchar](255) = NULL,
	@startSequence			 BIGINT = NULL,
	@maxRows				 BIGINT)
AS
BEGIN    
        SELECT top(@maxRows) *
        FROM tblChangeLog TCL
        WHERE 
        ((@startSequence IS NULL) OR (TCL.pkID <= @startSequence)) AND
		((@from IS NULL) OR (TCL.ChangeDate >= @from)) AND
		((@to IS NULL) OR (TCL.ChangeDate <= @to)) AND  
        ((@category IS NULL) OR (@category = TCL.Category)) AND
        ((@action IS NULL) OR (@action = TCL.Action)) AND
        ((@changedBy IS NULL) OR (@changedBy = TCL.ChangedBy))
		ORDER BY TCL.pkID DESC
END
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[netChangeLogGetHighestSeqNum]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netChangeLogGetHighestSeqNum]
(
	@count BIGINT = 0 OUTPUT
)
AS
BEGIN
	select @count = MAX(pkID) from tblChangeLog
END
SET QUOTED_IDENTIFIER ON 
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[netChangeLogGetCountBackwards]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netChangeLogGetCountBackwards]
(
	@from 	                 DATETIME = NULL,
	@to	                     DATETIME = NULL,
	@category 				 INT = 0,
	@action 				 INT = 0,
	@changedBy				 [nvarchar](255) = NULL,
	@startSequence			 BIGINT = 0,
	@count                   BIGINT = 0 OUTPUT)
AS
BEGIN    
        SELECT @count = count(*)
        FROM tblChangeLog TCL
        WHERE 
        (TCL.pkID <= @startSequence) AND
		((@from IS NULL) OR (TCL.ChangeDate >= @from)) AND
		((@to IS NULL) OR (TCL.ChangeDate <= @to)) AND  
        ((@category = 0) OR (@category = TCL.Category)) AND
        ((@action = 0) OR (@action = TCL.Action)) AND
        ((@changedBy IS NULL) OR (@changedBy = TCL.ChangedBy))
END
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[netChangeLogGetCount]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netChangeLogGetCount]
(
	@from 	                 DATETIME = NULL,
	@to	                     DATETIME = NULL,
	@category 				 INT = 0,
	@action 				 INT = 0,
	@changedBy				 [nvarchar](255) = NULL,
	@startSequence			 BIGINT = 0,
	@count                   BIGINT = 0 OUTPUT)
AS
BEGIN    
        SELECT @count = count(*)
        FROM tblChangeLog TCL
        WHERE 
        ((@startSequence = 0) OR (TCL.pkID >= @startSequence)) AND
		((@from IS NULL) OR (TCL.ChangeDate >= @from)) AND
		((@to IS NULL) OR (TCL.ChangeDate <= @to)) AND  
        ((@category = 0) OR (@category = TCL.Category)) AND
        ((@action = 0) OR (@action = TCL.Action)) AND
        ((@changedBy IS NULL) OR (@changedBy = TCL.ChangedBy))
END
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[netCategoryStringToTable]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netCategoryStringToTable]
(
	@CategoryList	NVARCHAR(2000)
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE		@DotPos INT
	DECLARE		@Category NVARCHAR(255)
	WHILE (DATALENGTH(@CategoryList) > 0)
	BEGIN
		SET @DotPos = CHARINDEX(N',', @CategoryList)
		IF @DotPos > 0
			SET @Category = LEFT(@CategoryList,@DotPos-1)
		ELSE
		BEGIN
			SET @Category = @CategoryList
			SET @CategoryList = NULL
		END
		BEGIN TRY
		    INSERT INTO #category SELECT pkID FROM tblCategory WHERE pkID = CAST(@Category AS INT)
		END TRY
		BEGIN CATCH
		     INSERT INTO #category SELECT pkID FROM tblCategory WHERE CategoryName = @Category
		END CATCH
		IF (DATALENGTH(@CategoryList) > 0)
			SET @CategoryList = SUBSTRING(@CategoryList,@DotPos+1,255)
	END
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[netCategorySave]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netCategorySave]
(
	@CategoryID		INT OUTPUT,
	@CategoryName	NVARCHAR(50),
	@Description	NVARCHAR(255),
	@Values			INT,
	@SortOrder		INT,
	@ParentID		INT = NULL,
	@Guid			UNIQUEIDENTIFIER = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	IF (@CategoryID IS NULL)
	BEGIN
			SELECT @SortOrder=Max(SortOrder) + 10 FROM tblCategory 
			IF (@SortOrder IS NULL)
				SET @SortOrder=100
			INSERT INTO tblCategory 
				(CategoryName, 
				CategoryDescription, 
				fkParentID, 
				Available, 
				Selectable,
				SortOrder,
				CategoryGUID) 
			VALUES 
				(@CategoryName,
				@Description,
				@ParentID,
				case when (@Values & 1) = 1 then 1 else 0 end,
				case when (@Values & 2) = 2 then 1 else 0 end,
				@SortOrder,
				COALESCE(@Guid,NewId()))
		SET @CategoryID =  SCOPE_IDENTITY() 
	END
	ELSE
	BEGIN
		UPDATE 
			tblCategory 
		SET 
			CategoryName		= @CategoryName,
			CategoryDescription	= @Description,
			SortOrder		= @SortOrder,
			Available			= case when (@Values & 1) = 1 then 1 else 0 end,
			Selectable			= case when (@Values & 2) = 2 then 1 else 0 end,
			CategoryGUID		= COALESCE(@Guid,CategoryGUID)
		WHERE 
			pkID=@CategoryID
	END
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[netCategoryListAll]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netCategoryListAll]
(
	@CompactList	INT = 0
)
AS
BEGIN
	SET NOCOUNT ON
	IF @CompactList = 1 
		SELECT
			pkID AS CategoryID,
			CategoryName
		FROM
			tblCategory
		WHERE 
			pkID <> 1
		ORDER BY
			SortOrder
	ELSE	
		SELECT
			pkID AS CategoryID,
			fkParentID AS ParentID,
			(SELECT COUNT(TC.pkID) FROM tblCategory AS TC WHERE TC.fkParentID=tblCategory.pkID) AS ChildCount,
			CategoryName,
			CategoryDescription,
			Available + Selectable * 2 AS CategoryValue,
			SortOrder,
			CONVERT(NVARCHAR(38), CategoryGUID) AS CategoryGUID
		FROM
			tblCategory
		WHERE 
			pkID <> 1
		ORDER BY
			SortOrder
	RETURN 0
END
GO
/****** Object:  Table [dbo].[aspnet_Users]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_Users](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[LoweredUserName] [nvarchar](256) NOT NULL,
	[MobileAlias] [nvarchar](16) NULL,
	[IsAnonymous] [bit] NOT NULL,
	[LastActivityDate] [datetime] NOT NULL,
PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_Users_Index] ON [dbo].[aspnet_Users] 
(
	[ApplicationId] ASC,
	[LoweredUserName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [aspnet_Users_Index2] ON [dbo].[aspnet_Users] 
(
	[ApplicationId] ASC,
	[LastActivityDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
INSERT [dbo].[aspnet_Users] ([ApplicationId], [UserId], [UserName], [LoweredUserName], [MobileAlias], [IsAnonymous], [LastActivityDate]) VALUES (N'cd4c8921-a5d2-485f-a0ac-5d2aa7f8bad1', N'999d6269-8d39-411e-b8af-64f21ed637b9', N'vladimir.levchuk', N'vladimir.levchuk', NULL, 0, CAST(0x0000A1190081B3AA AS DateTime))
/****** Object:  StoredProcedure [dbo].[aspnet_UnRegisterSchemaVersion]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UnRegisterSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128)
AS
BEGIN
    DELETE FROM dbo.aspnet_SchemaVersions
        WHERE   Feature = LOWER(@Feature) AND @CompatibleSchemaVersion = CompatibleSchemaVersion
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_CheckSchemaVersion]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_CheckSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128)
AS
BEGIN
    IF (EXISTS( SELECT  *
                FROM    dbo.aspnet_SchemaVersions
                WHERE   Feature = LOWER( @Feature ) AND
                        CompatibleSchemaVersion = @CompatibleSchemaVersion ))
        RETURN 0

    RETURN 1
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Applications_CreateApplication]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Applications_CreateApplication]
    @ApplicationName      nvarchar(256),
    @ApplicationId        uniqueidentifier OUTPUT
AS
BEGIN
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName

    IF(@ApplicationId IS NULL)
    BEGIN
        DECLARE @TranStarted   bit
        SET @TranStarted = 0

        IF( @@TRANCOUNT = 0 )
        BEGIN
	        BEGIN TRANSACTION
	        SET @TranStarted = 1
        END
        ELSE
    	    SET @TranStarted = 0

        SELECT  @ApplicationId = ApplicationId
        FROM dbo.aspnet_Applications WITH (UPDLOCK, HOLDLOCK)
        WHERE LOWER(@ApplicationName) = LoweredApplicationName

        IF(@ApplicationId IS NULL)
        BEGIN
            SELECT  @ApplicationId = NEWID()
            INSERT  dbo.aspnet_Applications (ApplicationId, ApplicationName, LoweredApplicationName)
            VALUES  (@ApplicationId, @ApplicationName, LOWER(@ApplicationName))
        END


        IF( @TranStarted = 1 )
        BEGIN
            IF(@@ERROR = 0)
            BEGIN
	        SET @TranStarted = 0
	        COMMIT TRANSACTION
            END
            ELSE
            BEGIN
                SET @TranStarted = 0
                ROLLBACK TRANSACTION
            END
        END
    END
END
GO
/****** Object:  StoredProcedure [dbo].[BigTableDeleteItem]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BigTableDeleteItem]
@StoreId BIGINT = NULL,
@ExternalId uniqueidentifier = NULL
AS
BEGIN
	IF @StoreId IS NULL
	BEGIN
		SELECT @StoreId = pkId FROM tblBigTableIdentity WHERE [Guid] = @ExternalId
	END
	IF @StoreId IS NULL RAISERROR(N'No object exists for the unique identifier passed', 1, 1)
	CREATE TABLE #deletes(Id BIGINT, NestLevel INT, ObjectPath varchar(max))
	CREATE INDEX IX_Deletes_Id ON #deletes(Id)
	CREATE INDEX IX_Deletes_Id_NestLevel ON #deletes(Id, NestLevel)
	INSERT INTO #deletes(Id, NestLevel, ObjectPath) VALUES(@StoreId, 1, '/' + CAST(@StoreId AS varchar) + '/')
	EXEC ('BigTableDeleteItemInternal')
	DROP INDEX IX_Deletes_Id_NestLevel ON #deletes
	DROP INDEX IX_Deletes_Id ON #deletes
	DROP TABLE #deletes
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_WebEvent_LogEvent]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_WebEvent_LogEvent]
        @EventId         char(32),
        @EventTimeUtc    datetime,
        @EventTime       datetime,
        @EventType       nvarchar(256),
        @EventSequence   decimal(19,0),
        @EventOccurrence decimal(19,0),
        @EventCode       int,
        @EventDetailCode int,
        @Message         nvarchar(1024),
        @ApplicationPath nvarchar(256),
        @ApplicationVirtualPath nvarchar(256),
        @MachineName    nvarchar(256),
        @RequestUrl      nvarchar(1024),
        @ExceptionType   nvarchar(256),
        @Details         ntext
AS
BEGIN
    INSERT
        dbo.aspnet_WebEvent_Events
        (
            EventId,
            EventTimeUtc,
            EventTime,
            EventType,
            EventSequence,
            EventOccurrence,
            EventCode,
            EventDetailCode,
            Message,
            ApplicationPath,
            ApplicationVirtualPath,
            MachineName,
            RequestUrl,
            ExceptionType,
            Details
        )
    VALUES
    (
        @EventId,
        @EventTimeUtc,
        @EventTime,
        @EventType,
        @EventSequence,
        @EventOccurrence,
        @EventCode,
        @EventDetailCode,
        @Message,
        @ApplicationPath,
        @ApplicationVirtualPath,
        @MachineName,
        @RequestUrl,
        @ExceptionType,
        @Details
    )
END
GO
/****** Object:  StoredProcedure [dbo].[EntityTypeGetNameByID]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EntityTypeGetNameByID]
@intObjectTypeID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT strName FROM dbo.tblEntityType WHERE intID = @intObjectTypeID
END
GO
/****** Object:  StoredProcedure [dbo].[EntityTypeGetIDByName]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EntityTypeGetIDByName]
@strObjectType varchar(400)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @intID int
	SELECT @intID = intID FROM dbo.tblEntityType WHERE strName = @strObjectType
	IF @intID IS NULL
	BEGIN
		INSERT INTO dbo.tblEntityType (strName) VALUES(@strObjectType)
		SET @intID = SCOPE_IDENTITY()
	END
	SELECT @intID
END
GO
/****** Object:  StoredProcedure [dbo].[EntitySetEntry]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[EntitySetEntry]
@intObjectTypeID int,
@intObjectID int,
@uniqueID uniqueidentifier
AS
BEGIN
	INSERT INTO tblEntityGuid
			(intObjectTypeID, intObjectID, unqID)
	VALUES
			(@intObjectTypeID, @intObjectID, @uniqueID)
END
GO
/****** Object:  StoredProcedure [dbo].[EntityGetIdByGuidFromIdentity]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[EntityGetIdByGuidFromIdentity]
@uniqueID uniqueidentifier
AS
BEGIN
	SELECT tblBigTableStoreConfig.EntityTypeId as EntityTypeId, tblBigTableIdentity.pkId as ObjectId  
		FROM tblBigTableIdentity INNER JOIN tblBigTableStoreConfig 
		ON tblBigTableIdentity.StoreName = tblBigTableStoreConfig.StoreName
		WHERE tblBigTableIdentity.Guid = @uniqueID
END
GO
/****** Object:  StoredProcedure [dbo].[EntityGetIDByGuid]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[EntityGetIDByGuid]
@unqID uniqueidentifier
AS
BEGIN
	SELECT 
		intObjectTypeID, intObjectID
	FROM tblEntityGuid
	WHERE unqID = @unqID
END
GO
/****** Object:  StoredProcedure [dbo].[EntityGetGuidByIdFromIdentity]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[EntityGetGuidByIdFromIdentity]
@intObjectTypeID int,
@intObjectID int
AS
BEGIN
	SELECT Guid FROM tblBigTableIdentity INNER JOIN tblBigTableStoreConfig 
		ON tblBigTableIdentity.StoreName = tblBigTableStoreConfig.StoreName
		WHERE tblBigTableIdentity.pkId = @intObjectID AND
			tblBigTableStoreConfig.EntityTypeId = @intObjectTypeID
END
GO
/****** Object:  StoredProcedure [dbo].[EntityGetGuidByID]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[EntityGetGuidByID]
@intObjectTypeID int,
@intObjectID int
AS
BEGIN
	SELECT unqID FROM tblEntityGuid WHERE intObjectTypeID = @intObjectTypeID AND intObjectID = @intObjectID
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteCompletedScope]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteCompletedScope]
@completedScopeID uniqueidentifier
AS
DELETE FROM [dbo].[CompletedScope] WHERE completedScopeID=@completedScopeID
GO
/****** Object:  StoredProcedure [dbo].[ItemLoad]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ItemLoad]
(
	@Id	NVARCHAR(100)
)
AS
BEGIN
	SELECT ItemData FROM tblItem WHERE pkID=@Id
END
GO
/****** Object:  StoredProcedure [dbo].[ItemList]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ItemList]
AS
BEGIN
	SELECT pkID, ItemData FROM tblItem
END
GO
/****** Object:  StoredProcedure [dbo].[InsertInstanceState]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[InsertInstanceState]
@uidInstanceID uniqueidentifier,
@state image,
@status int,
@unlocked int,
@blocked int,
@info ntext,
@ownerID uniqueidentifier = NULL,
@ownedUntil datetime = NULL,
@nextTimer datetime,
@result int output,
@currentOwnerID uniqueidentifier output
As
    declare @localized_string_InsertInstanceState_Failed_Ownership nvarchar(256)
    set @localized_string_InsertInstanceState_Failed_Ownership = N'Instance ownership conflict'
    set @result = 0
    set @currentOwnerID = @ownerID
    declare @now datetime
    set @now = GETUTCDATE()
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    set nocount on
    IF @status=1 OR @status=3
    BEGIN
	DELETE FROM [dbo].[InstanceState] WHERE uidInstanceID=@uidInstanceID AND ((ownerID = @ownerID AND ownedUntil>=@now) OR (ownerID IS NULL AND @ownerID IS NULL ))
	if ( @@ROWCOUNT = 0 )
	begin
		set @currentOwnerID = NULL
    		select  @currentOwnerID=ownerID from [dbo].[InstanceState] Where uidInstanceID = @uidInstanceID
		if ( @currentOwnerID IS NOT NULL )
		begin	-- cannot delete the instance state because of an ownership conflict
			-- RAISERROR(@localized_string_InsertInstanceState_Failed_Ownership, 16, -1)				
			set @result = -2
			return
		end
	end
	else
	BEGIN
		DELETE FROM [dbo].[CompletedScope] WHERE uidInstanceID=@uidInstanceID
	end
    END
    ELSE BEGIN
  	    if not exists ( Select 1 from [dbo].[InstanceState] Where uidInstanceID = @uidInstanceID )
		  BEGIN
			  --Insert Operation
			  IF @unlocked = 0
			  begin
			     Insert into [dbo].[InstanceState] 
			     Values(@uidInstanceID,@state,@status,@unlocked,@blocked,@info,@now,@ownerID,@ownedUntil,@nextTimer) 
			  end
			  else
			  begin
			     Insert into [dbo].[InstanceState] 
			     Values(@uidInstanceID,@state,@status,@unlocked,@blocked,@info,@now,null,null,@nextTimer) 
			  end
		  END
		  ELSE BEGIN
				IF @unlocked = 0
				begin
					Update [dbo].[InstanceState]  
					Set state = @state,
						status = @status,
						unlocked = @unlocked,
						blocked = @blocked,
						info = @info,
						modified = @now,
						ownedUntil = @ownedUntil,
						nextTimer = @nextTimer
					Where uidInstanceID = @uidInstanceID AND ((ownerID = @ownerID AND ownedUntil>=@now) OR (ownerID IS NULL AND @ownerID IS NULL ))
					if ( @@ROWCOUNT = 0 )
					BEGIN
						-- RAISERROR(@localized_string_InsertInstanceState_Failed_Ownership, 16, -1)
						select @currentOwnerID=ownerID from [dbo].[InstanceState] Where uidInstanceID = @uidInstanceID  
						set @result = -2
						return
					END
				end
				else
				begin
					Update [dbo].[InstanceState]  
					Set state = @state,
						status = @status,
						unlocked = @unlocked,
						blocked = @blocked,
						info = @info,
						modified = @now,
						ownerID = NULL,
						ownedUntil = NULL,
						nextTimer = @nextTimer
					Where uidInstanceID = @uidInstanceID AND ((ownerID = @ownerID AND ownedUntil>=@now) OR (ownerID IS NULL AND @ownerID IS NULL ))
					if ( @@ROWCOUNT = 0 )
					BEGIN
						-- RAISERROR(@localized_string_InsertInstanceState_Failed_Ownership, 16, -1)
						select @currentOwnerID=ownerID from [dbo].[InstanceState] Where uidInstanceID = @uidInstanceID  
						set @result = -2
						return
					END
				end
		  END
    END
		RETURN
Return
GO
/****** Object:  StoredProcedure [dbo].[InsertCompletedScope]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertCompletedScope]
@instanceID uniqueidentifier,
@completedScopeID uniqueidentifier,
@state image
As
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
SET NOCOUNT ON
		UPDATE [dbo].[CompletedScope] WITH(ROWLOCK UPDLOCK) 
		    SET state = @state,
		    modified = GETUTCDATE()
		    WHERE completedScopeID=@completedScopeID 
		IF ( @@ROWCOUNT = 0 )
		BEGIN
			--Insert Operation
			INSERT INTO [dbo].[CompletedScope] WITH(ROWLOCK)
			VALUES(@instanceID, @completedScopeID, @state, GETUTCDATE()) 
		END
		RETURN
RETURN
GO
/****** Object:  Table [dbo].[aspnet_Paths]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_Paths](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[PathId] [uniqueidentifier] NOT NULL,
	[Path] [nvarchar](256) NOT NULL,
	[LoweredPath] [nvarchar](256) NOT NULL,
PRIMARY KEY NONCLUSTERED 
(
	[PathId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_Paths_index] ON [dbo].[aspnet_Paths] 
(
	[ApplicationId] ASC,
	[LoweredPath] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Personalization_GetApplicationId]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Personalization_GetApplicationId] (
    @ApplicationName NVARCHAR(256),
    @ApplicationId UNIQUEIDENTIFIER OUT)
AS
BEGIN
    SELECT @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
END
GO
/****** Object:  Table [dbo].[aspnet_Roles]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_Roles](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[RoleName] [nvarchar](256) NOT NULL,
	[LoweredRoleName] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](256) NULL,
PRIMARY KEY NONCLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_Roles_index1] ON [dbo].[aspnet_Roles] 
(
	[ApplicationId] ASC,
	[LoweredRoleName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_RegisterSchemaVersion]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_RegisterSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128),
    @IsCurrentVersion          bit,
    @RemoveIncompatibleSchema  bit
AS
BEGIN
    IF( @RemoveIncompatibleSchema = 1 )
    BEGIN
        DELETE FROM dbo.aspnet_SchemaVersions WHERE Feature = LOWER( @Feature )
    END
    ELSE
    BEGIN
        IF( @IsCurrentVersion = 1 )
        BEGIN
            UPDATE dbo.aspnet_SchemaVersions
            SET IsCurrentVersion = 0
            WHERE Feature = LOWER( @Feature )
        END
    END

    INSERT  dbo.aspnet_SchemaVersions( Feature, CompatibleSchemaVersion, IsCurrentVersion )
    VALUES( LOWER( @Feature ), @CompatibleSchemaVersion, @IsCurrentVersion )
END
GO
/****** Object:  Table [dbo].[tblContentTypeToContentType]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblContentTypeToContentType](
	[fkContentTypeParentID] [int] NOT NULL,
	[fkContentTypeChildID] [int] NOT NULL,
	[Access] [int] NOT NULL,
	[Availability] [int] NOT NULL,
	[Allow] [bit] NULL,
 CONSTRAINT [PK_tblContentTypeToContentType] PRIMARY KEY CLUSTERED 
(
	[fkContentTypeParentID] ASC,
	[fkContentTypeChildID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblContentTypeDefault]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblContentTypeDefault](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkContentTypeID] [int] NOT NULL,
	[fkContentLinkID] [int] NULL,
	[fkFrameID] [int] NULL,
	[fkArchiveContentID] [int] NULL,
	[Name] [nvarchar](255) NULL,
	[VisibleInMenu] [bit] NOT NULL,
	[StartPublishOffsetValue] [int] NULL,
	[StartPublishOffsetType] [nchar](1) NULL,
	[StopPublishOffsetValue] [int] NULL,
	[StopPublishOffsetType] [nchar](1) NULL,
	[ChildOrderRule] [int] NOT NULL,
	[PeerOrder] [int] NOT NULL,
	[StartPublishOffset] [int] NULL,
	[StopPublishOffset] [int] NULL,
 CONSTRAINT [PK_tblContentTypeDefault] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblContent]    Script Date: 12/06/2012 12:39:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblContent](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkContentTypeID] [int] NOT NULL,
	[fkParentID] [int] NULL,
	[ArchiveContentGUID] [uniqueidentifier] NULL,
	[CreatorName] [nvarchar](255) NULL,
	[ContentGUID] [uniqueidentifier] NOT NULL,
	[VisibleInMenu] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[PendingPublish] [bit] NOT NULL,
	[ChildOrderRule] [int] NOT NULL,
	[PeerOrder] [int] NOT NULL,
	[ExternalFolderID] [int] NULL,
	[PublishedVersion] [int] NULL,
	[DeletedBy] [nvarchar](255) NULL,
	[DeletedDate] [datetime] NULL,
	[fkMasterLanguageBranchID] [int] NOT NULL,
	[ContentPath] [varchar](900) NOT NULL,
	[ContentType] [int] NOT NULL,
 CONSTRAINT [PK_tblContent] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IDX_tblContent_ArchiveContentGUID] ON [dbo].[tblContent] 
(
	[ArchiveContentGUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IDX_tblContent_ContentGUID] ON [dbo].[tblContent] 
(
	[ContentGUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblContent_ContentPath] ON [dbo].[tblContent] 
(
	[ContentPath] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblContent_ContentType] ON [dbo].[tblContent] 
(
	[ContentType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblContent_ExternalFolderID] ON [dbo].[tblContent] 
(
	[ExternalFolderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblContent_fkContentTypeID] ON [dbo].[tblContent] 
(
	[fkContentTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblContent_fkParentID] ON [dbo].[tblContent] 
(
	[fkParentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblContent] ON
INSERT [dbo].[tblContent] ([pkID], [fkContentTypeID], [fkParentID], [ArchiveContentGUID], [CreatorName], [ContentGUID], [VisibleInMenu], [Deleted], [PendingPublish], [ChildOrderRule], [PeerOrder], [ExternalFolderID], [PublishedVersion], [DeletedBy], [DeletedDate], [fkMasterLanguageBranchID], [ContentPath], [ContentType]) VALUES (1, 1, NULL, NULL, N'', N'43f936c9-9b23-4ea3-97b2-61c538ad07c9', 1, 0, 0, 4, 100, 1, NULL, NULL, NULL, 1, N'.', 0)
INSERT [dbo].[tblContent] ([pkID], [fkContentTypeID], [fkParentID], [ArchiveContentGUID], [CreatorName], [ContentGUID], [VisibleInMenu], [Deleted], [PendingPublish], [ChildOrderRule], [PeerOrder], [ExternalFolderID], [PublishedVersion], [DeletedBy], [DeletedDate], [fkMasterLanguageBranchID], [ContentPath], [ContentType]) VALUES (2, 2, 1, NULL, N'', N'2f40ba47-f4fc-47ae-a244-0b909d4cf988', 1, 0, 0, 1, 10, 2, NULL, NULL, NULL, 1, N'.1.', 0)
INSERT [dbo].[tblContent] ([pkID], [fkContentTypeID], [fkParentID], [ArchiveContentGUID], [CreatorName], [ContentGUID], [VisibleInMenu], [Deleted], [PendingPublish], [ChildOrderRule], [PeerOrder], [ExternalFolderID], [PublishedVersion], [DeletedBy], [DeletedDate], [fkMasterLanguageBranchID], [ContentPath], [ContentType]) VALUES (3, 3, 1, NULL, N'', N'e56f85d0-e833-4e02-976a-2d11fe4d598c', 1, 0, 0, 4, 100, 0, NULL, NULL, NULL, 1, N'.1.', 2)
SET IDENTITY_INSERT [dbo].[tblContent] OFF
/****** Object:  View [dbo].[tblPageType]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[tblPageType]
AS
SELECT
  [pkID],
  [ContentTypeGUID] AS PageTypeGUID,
  [Created],
  [DefaultWebFormTemplate],
  [DefaultMvcController],
  [Filename],
  [ModelType],
  [Name],
  [DisplayName],
  [Description],
  [IdString],
  [Available],
  [SortOrder],
  [MetaDataInherit],
  [MetaDataDefault],
  [WorkflowEditFields],
  [ACL],
  [ContentType]
FROM    dbo.tblContentType
GO
/****** Object:  View [dbo].[tblPageDefinitionType]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[tblPageDefinitionType]
AS
SELECT
	[pkID],
	[Property],
	[Name],
	[TypeName],
	[AssemblyName],
	[fkContentTypeGUID] AS fkPageTypeGUID
FROM    dbo.tblPropertyDefinitionType
GO
/****** Object:  View [dbo].[tblPageDefinitionGroup]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[tblPageDefinitionGroup]
AS
SELECT
	[pkID],
	[SystemGroup],
	[Access],
	[GroupVisible],
	[GroupOrder],
	[Name],
	[DisplayName]
FROM    dbo.tblPropertyDefinitionGroup
GO
/****** Object:  Table [dbo].[tblIndexRequestLog]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblIndexRequestLog](
	[pkId] [bigint] NOT NULL,
	[Row] [int] NOT NULL,
	[StoreName] [nvarchar](375) NOT NULL,
	[ItemType] [nvarchar](2000) NOT NULL,
	[Boolean01] [bit] NULL,
	[Boolean02] [bit] NULL,
	[Boolean03] [bit] NULL,
	[Boolean04] [bit] NULL,
	[Boolean05] [bit] NULL,
	[Integer01] [int] NULL,
	[Integer02] [int] NULL,
	[Integer03] [int] NULL,
	[Integer04] [int] NULL,
	[Integer05] [int] NULL,
	[Integer06] [int] NULL,
	[Integer07] [int] NULL,
	[Integer08] [int] NULL,
	[Integer09] [int] NULL,
	[Integer10] [int] NULL,
	[Long01] [bigint] NULL,
	[Long02] [bigint] NULL,
	[Long03] [bigint] NULL,
	[Long04] [bigint] NULL,
	[Long05] [bigint] NULL,
	[DateTime01] [datetime] NULL,
	[DateTime02] [datetime] NULL,
	[DateTime03] [datetime] NULL,
	[DateTime04] [datetime] NULL,
	[DateTime05] [datetime] NULL,
	[Guid01] [uniqueidentifier] NULL,
	[Guid02] [uniqueidentifier] NULL,
	[Guid03] [uniqueidentifier] NULL,
	[Float01] [float] NULL,
	[Float02] [float] NULL,
	[Float03] [float] NULL,
	[Float04] [float] NULL,
	[Float05] [float] NULL,
	[Float06] [float] NULL,
	[Float07] [float] NULL,
	[String01] [nvarchar](max) NULL,
	[String02] [nvarchar](max) NULL,
	[String03] [nvarchar](max) NULL,
	[String04] [nvarchar](max) NULL,
	[String05] [nvarchar](max) NULL,
	[String06] [nvarchar](max) NULL,
	[String07] [nvarchar](max) NULL,
	[String08] [nvarchar](max) NULL,
	[String09] [nvarchar](max) NULL,
	[String10] [nvarchar](max) NULL,
	[Binary01] [varbinary](max) NULL,
	[Binary02] [varbinary](max) NULL,
	[Binary03] [varbinary](max) NULL,
	[Binary04] [varbinary](max) NULL,
	[Binary05] [varbinary](max) NULL,
	[Indexed_Boolean01] [bit] NULL,
	[Indexed_Integer01] [int] NULL,
	[Indexed_Integer02] [int] NULL,
	[Indexed_Integer03] [int] NULL,
	[Indexed_Long01] [bigint] NULL,
	[Indexed_Long02] [bigint] NULL,
	[Indexed_DateTime01] [datetime] NULL,
	[Indexed_Guid01] [uniqueidentifier] NULL,
	[Indexed_Float01] [float] NULL,
	[Indexed_Float02] [float] NULL,
	[Indexed_Float03] [float] NULL,
	[Indexed_String01] [nvarchar](450) NULL,
	[Indexed_String02] [nvarchar](450) NULL,
	[Indexed_String03] [nvarchar](450) NULL,
	[Indexed_Binary01] [varbinary](900) NULL,
 CONSTRAINT [PK_tblIndexRequestLog] PRIMARY KEY CLUSTERED 
(
	[pkId] ASC,
	[Row] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IDX_tblIndexRequestLog_Indexed_DateTime01] ON [dbo].[tblIndexRequestLog] 
(
	[Indexed_DateTime01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblIndexRequestLog_Indexed_String01] ON [dbo].[tblIndexRequestLog] 
(
	[Indexed_String01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblIndexRequestLog_StoreName] ON [dbo].[tblIndexRequestLog] 
(
	[StoreName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTask]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTask](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](2000) NULL,
	[DueDate] [datetime] NULL,
	[OwnerName] [nvarchar](255) NOT NULL,
	[AssignedToName] [nvarchar](255) NOT NULL,
	[AssignedIsRole] [bit] NOT NULL,
	[fkPlugInID] [int] NULL,
	[Status] [int] NOT NULL,
	[Activity] [nvarchar](max) NULL,
	[State] [nvarchar](max) NULL,
	[Created] [datetime] NOT NULL,
	[Changed] [datetime] NOT NULL,
	[WorkflowInstanceId] [nvarchar](36) NULL,
	[EventActivityName] [nvarchar](255) NULL,
 CONSTRAINT [PK_tblTask] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSystemBigTable]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSystemBigTable](
	[pkId] [bigint] NOT NULL,
	[Row] [int] NOT NULL,
	[StoreName] [nvarchar](375) NOT NULL,
	[ItemType] [nvarchar](2000) NOT NULL,
	[Boolean01] [bit] NULL,
	[Boolean02] [bit] NULL,
	[Boolean03] [bit] NULL,
	[Boolean04] [bit] NULL,
	[Boolean05] [bit] NULL,
	[Integer01] [int] NULL,
	[Integer02] [int] NULL,
	[Integer03] [int] NULL,
	[Integer04] [int] NULL,
	[Integer05] [int] NULL,
	[Integer06] [int] NULL,
	[Integer07] [int] NULL,
	[Integer08] [int] NULL,
	[Integer09] [int] NULL,
	[Integer10] [int] NULL,
	[Long01] [bigint] NULL,
	[Long02] [bigint] NULL,
	[Long03] [bigint] NULL,
	[Long04] [bigint] NULL,
	[Long05] [bigint] NULL,
	[DateTime01] [datetime] NULL,
	[DateTime02] [datetime] NULL,
	[DateTime03] [datetime] NULL,
	[DateTime04] [datetime] NULL,
	[DateTime05] [datetime] NULL,
	[Guid01] [uniqueidentifier] NULL,
	[Guid02] [uniqueidentifier] NULL,
	[Guid03] [uniqueidentifier] NULL,
	[Float01] [float] NULL,
	[Float02] [float] NULL,
	[Float03] [float] NULL,
	[Float04] [float] NULL,
	[Float05] [float] NULL,
	[Float06] [float] NULL,
	[Float07] [float] NULL,
	[String01] [nvarchar](max) NULL,
	[String02] [nvarchar](max) NULL,
	[String03] [nvarchar](max) NULL,
	[String04] [nvarchar](max) NULL,
	[String05] [nvarchar](max) NULL,
	[String06] [nvarchar](max) NULL,
	[String07] [nvarchar](max) NULL,
	[String08] [nvarchar](max) NULL,
	[String09] [nvarchar](max) NULL,
	[String10] [nvarchar](max) NULL,
	[Binary01] [varbinary](max) NULL,
	[Binary02] [varbinary](max) NULL,
	[Binary03] [varbinary](max) NULL,
	[Binary04] [varbinary](max) NULL,
	[Binary05] [varbinary](max) NULL,
	[Indexed_Boolean01] [bit] NULL,
	[Indexed_Integer01] [int] NULL,
	[Indexed_Integer02] [int] NULL,
	[Indexed_Integer03] [int] NULL,
	[Indexed_Long01] [bigint] NULL,
	[Indexed_Long02] [bigint] NULL,
	[Indexed_DateTime01] [datetime] NULL,
	[Indexed_Guid01] [uniqueidentifier] NULL,
	[Indexed_Float01] [float] NULL,
	[Indexed_Float02] [float] NULL,
	[Indexed_Float03] [float] NULL,
	[Indexed_String01] [nvarchar](450) NULL,
	[Indexed_String02] [nvarchar](450) NULL,
	[Indexed_String03] [nvarchar](450) NULL,
	[Indexed_Binary01] [varbinary](900) NULL,
 CONSTRAINT [PK_tblSystemBigTable] PRIMARY KEY CLUSTERED 
(
	[pkId] ASC,
	[Row] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IDX_tblSystemBigTable_Indexed_Binary01] ON [dbo].[tblSystemBigTable] 
(
	[Indexed_Binary01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblSystemBigTable_Indexed_Boolean01] ON [dbo].[tblSystemBigTable] 
(
	[Indexed_Boolean01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblSystemBigTable_Indexed_DateTime01] ON [dbo].[tblSystemBigTable] 
(
	[Indexed_DateTime01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblSystemBigTable_Indexed_Float01] ON [dbo].[tblSystemBigTable] 
(
	[Indexed_Float01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblSystemBigTable_Indexed_Float02] ON [dbo].[tblSystemBigTable] 
(
	[Indexed_Float02] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblSystemBigTable_Indexed_Float03] ON [dbo].[tblSystemBigTable] 
(
	[Indexed_Float03] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblSystemBigTable_Indexed_Guid01] ON [dbo].[tblSystemBigTable] 
(
	[Indexed_Guid01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblSystemBigTable_Indexed_Integer01] ON [dbo].[tblSystemBigTable] 
(
	[Indexed_Integer01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblSystemBigTable_Indexed_Integer02] ON [dbo].[tblSystemBigTable] 
(
	[Indexed_Integer02] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblSystemBigTable_Indexed_Integer03] ON [dbo].[tblSystemBigTable] 
(
	[Indexed_Integer03] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblSystemBigTable_Indexed_Long01] ON [dbo].[tblSystemBigTable] 
(
	[Indexed_Long01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblSystemBigTable_Indexed_Long02] ON [dbo].[tblSystemBigTable] 
(
	[Indexed_Long02] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblSystemBigTable_Indexed_String01] ON [dbo].[tblSystemBigTable] 
(
	[Indexed_String01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblSystemBigTable_Indexed_String02] ON [dbo].[tblSystemBigTable] 
(
	[Indexed_String02] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblSystemBigTable_Indexed_String03] ON [dbo].[tblSystemBigTable] 
(
	[Indexed_String03] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblSystemBigTable_StoreName] ON [dbo].[tblSystemBigTable] 
(
	[StoreName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
INSERT [dbo].[tblSystemBigTable] ([pkId], [Row], [StoreName], [ItemType], [Boolean01], [Boolean02], [Boolean03], [Boolean04], [Boolean05], [Integer01], [Integer02], [Integer03], [Integer04], [Integer05], [Integer06], [Integer07], [Integer08], [Integer09], [Integer10], [Long01], [Long02], [Long03], [Long04], [Long05], [DateTime01], [DateTime02], [DateTime03], [DateTime04], [DateTime05], [Guid01], [Guid02], [Guid03], [Float01], [Float02], [Float03], [Float04], [Float05], [Float06], [Float07], [String01], [String02], [String03], [String04], [String05], [String06], [String07], [String08], [String09], [String10], [Binary01], [Binary02], [Binary03], [Binary04], [Binary05], [Indexed_Boolean01], [Indexed_Integer01], [Indexed_Integer02], [Indexed_Integer03], [Indexed_Long01], [Indexed_Long02], [Indexed_DateTime01], [Indexed_Guid01], [Indexed_Float01], [Indexed_Float02], [Indexed_Float03], [Indexed_String01], [Indexed_String02], [Indexed_String03], [Indexed_Binary01]) VALUES (4, 1, N'EPiServer.Packaging.Storage.PackageEntity', N'EPiServer.Packaging.Storage.PackageEntity, EPiServer.Packaging, Version=1.0.1092.0, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0xFFFF2E4600000000 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'EPiServer.Packaging', N'0200b897-cf20-49c8-95c4-28bdfb34d493', N'1.0.1092', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblSystemBigTable] ([pkId], [Row], [StoreName], [ItemType], [Boolean01], [Boolean02], [Boolean03], [Boolean04], [Boolean05], [Integer01], [Integer02], [Integer03], [Integer04], [Integer05], [Integer06], [Integer07], [Integer08], [Integer09], [Integer10], [Long01], [Long02], [Long03], [Long04], [Long05], [DateTime01], [DateTime02], [DateTime03], [DateTime04], [DateTime05], [Guid01], [Guid02], [Guid03], [Float01], [Float02], [Float03], [Float04], [Float05], [Float06], [Float07], [String01], [String02], [String03], [String04], [String05], [String06], [String07], [String08], [String09], [String10], [Binary01], [Binary02], [Binary03], [Binary04], [Binary05], [Indexed_Boolean01], [Indexed_Integer01], [Indexed_Integer02], [Indexed_Integer03], [Indexed_Long01], [Indexed_Long02], [Indexed_DateTime01], [Indexed_Guid01], [Indexed_Float01], [Indexed_Float02], [Indexed_Float03], [Indexed_String01], [Indexed_String02], [Indexed_String03], [Indexed_Binary01]) VALUES (5, 1, N'EPiServer.Packaging.Storage.PackageEntity', N'EPiServer.Packaging.Storage.PackageEntity, EPiServer.Packaging, Version=1.0.1092.0, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0xFFFF2E4600000000 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'CMS', N'0200b897-cf20-49c8-95c4-28bdfb34d493', N'1.0.431', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblSystemBigTable] ([pkId], [Row], [StoreName], [ItemType], [Boolean01], [Boolean02], [Boolean03], [Boolean04], [Boolean05], [Integer01], [Integer02], [Integer03], [Integer04], [Integer05], [Integer06], [Integer07], [Integer08], [Integer09], [Integer10], [Long01], [Long02], [Long03], [Long04], [Long05], [DateTime01], [DateTime02], [DateTime03], [DateTime04], [DateTime05], [Guid01], [Guid02], [Guid03], [Float01], [Float02], [Float03], [Float04], [Float05], [Float06], [Float07], [String01], [String02], [String03], [String04], [String05], [String06], [String07], [String08], [String09], [String10], [Binary01], [Binary02], [Binary03], [Binary04], [Binary05], [Indexed_Boolean01], [Indexed_Integer01], [Indexed_Integer02], [Indexed_Integer03], [Indexed_Long01], [Indexed_Long02], [Indexed_DateTime01], [Indexed_Guid01], [Indexed_Float01], [Indexed_Float02], [Indexed_Float03], [Indexed_String01], [Indexed_String02], [Indexed_String03], [Indexed_Binary01]) VALUES (6, 1, N'EPiServer.Packaging.Storage.PackageEntity', N'EPiServer.Packaging.Storage.PackageEntity, EPiServer.Packaging, Version=1.0.1092.0, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0xFFFF2E4600000000 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'EPiServer.Packaging.UI', N'0200b897-cf20-49c8-95c4-28bdfb34d493', N'1.0.1092', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblSystemBigTable] ([pkId], [Row], [StoreName], [ItemType], [Boolean01], [Boolean02], [Boolean03], [Boolean04], [Boolean05], [Integer01], [Integer02], [Integer03], [Integer04], [Integer05], [Integer06], [Integer07], [Integer08], [Integer09], [Integer10], [Long01], [Long02], [Long03], [Long04], [Long05], [DateTime01], [DateTime02], [DateTime03], [DateTime04], [DateTime05], [Guid01], [Guid02], [Guid03], [Float01], [Float02], [Float03], [Float04], [Float05], [Float06], [Float07], [String01], [String02], [String03], [String04], [String05], [String06], [String07], [String08], [String09], [String10], [Binary01], [Binary02], [Binary03], [Binary04], [Binary05], [Indexed_Boolean01], [Indexed_Integer01], [Indexed_Integer02], [Indexed_Integer03], [Indexed_Long01], [Indexed_Long02], [Indexed_DateTime01], [Indexed_Guid01], [Indexed_Float01], [Indexed_Float02], [Indexed_Float03], [Indexed_String01], [Indexed_String02], [Indexed_String03], [Indexed_Binary01]) VALUES (7, 1, N'EPiServer.Packaging.Storage.PackageEntity', N'EPiServer.Packaging.Storage.PackageEntity, EPiServer.Packaging, Version=1.0.1092.0, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0xFFFF2E4600000000 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Shell', N'0200b897-cf20-49c8-95c4-28bdfb34d493', N'1.0.456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblSystemBigTable] ([pkId], [Row], [StoreName], [ItemType], [Boolean01], [Boolean02], [Boolean03], [Boolean04], [Boolean05], [Integer01], [Integer02], [Integer03], [Integer04], [Integer05], [Integer06], [Integer07], [Integer08], [Integer09], [Integer10], [Long01], [Long02], [Long03], [Long04], [Long05], [DateTime01], [DateTime02], [DateTime03], [DateTime04], [DateTime05], [Guid01], [Guid02], [Guid03], [Float01], [Float02], [Float03], [Float04], [Float05], [Float06], [Float07], [String01], [String02], [String03], [String04], [String05], [String06], [String07], [String08], [String09], [String10], [Binary01], [Binary02], [Binary03], [Binary04], [Binary05], [Indexed_Boolean01], [Indexed_Integer01], [Indexed_Integer02], [Indexed_Integer03], [Indexed_Long01], [Indexed_Long02], [Indexed_DateTime01], [Indexed_Guid01], [Indexed_Float01], [Indexed_Float02], [Indexed_Float03], [Indexed_String01], [Indexed_String02], [Indexed_String03], [Indexed_Binary01]) VALUES (8, 1, N'EPiServer.Packaging.Storage.StorageUpdateEntity', N'EPiServer.Packaging.Storage.StorageUpdateEntity, EPiServer.Packaging, Version=1.0.1092.0, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A118009FEDE3 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0200b897-cf20-49c8-95c4-28bdfb34d493', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblSystemBigTable] ([pkId], [Row], [StoreName], [ItemType], [Boolean01], [Boolean02], [Boolean03], [Boolean04], [Boolean05], [Integer01], [Integer02], [Integer03], [Integer04], [Integer05], [Integer06], [Integer07], [Integer08], [Integer09], [Integer10], [Long01], [Long02], [Long03], [Long04], [Long05], [DateTime01], [DateTime02], [DateTime03], [DateTime04], [DateTime05], [Guid01], [Guid02], [Guid03], [Float01], [Float02], [Float03], [Float04], [Float05], [Float06], [Float07], [String01], [String02], [String03], [String04], [String05], [String06], [String07], [String08], [String09], [String10], [Binary01], [Binary02], [Binary03], [Binary04], [Binary05], [Indexed_Boolean01], [Indexed_Integer01], [Indexed_Integer02], [Indexed_Integer03], [Indexed_Long01], [Indexed_Long02], [Indexed_DateTime01], [Indexed_Guid01], [Indexed_Float01], [Indexed_Float02], [Indexed_Float03], [Indexed_String01], [Indexed_String02], [Indexed_String03], [Indexed_Binary01]) VALUES (9, 1, N'EPiServer.Shell.Storage.PersonalizedViewSettingsStorage', N'EPiServer.Shell.Storage.PersonalizedViewSettingsStorage, EPiServer.Shell, Version=7.0.859.1, Culture=neutral, PublicKeyToken=8fe83dea738b45b7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'vladimir.levchuk', N'/episerver/dashboard', NULL, NULL)
/****** Object:  Table [dbo].[tblPropertyDefinition]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPropertyDefinition](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkContentTypeID] [int] NULL,
	[fkPropertyDefinitionTypeID] [int] NULL,
	[FieldOrder] [int] NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Property] [int] NOT NULL,
	[Required] [bit] NULL,
	[Advanced] [int] NULL,
	[Searchable] [bit] NULL,
	[EditCaption] [nvarchar](255) NULL,
	[HelpText] [nvarchar](2000) NULL,
	[ObjectProgID] [nvarchar](255) NULL,
	[DefaultValueType] [int] NOT NULL,
	[LongStringSettings] [int] NOT NULL,
	[SettingsID] [uniqueidentifier] NULL,
	[LanguageSpecific] [int] NOT NULL,
	[DisplayEditUI] [bit] NULL,
	[ExistsOnModel] [bit] NOT NULL,
 CONSTRAINT [PK_tblPropertyDefinition] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblPropertyDefinition_fkContentTypeID] ON [dbo].[tblPropertyDefinition] 
(
	[fkContentTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblPropertyDefinition_fkPropertyDefinitionTypeID] ON [dbo].[tblPropertyDefinition] 
(
	[fkPropertyDefinitionTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblPropertyDefinition_Name] ON [dbo].[tblPropertyDefinition] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  View [dbo].[tblPropertyDefault]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[tblPropertyDefault]
AS
SELECT
	[pkID],
	[fkPropertyDefinitionID] AS fkPageDefinitionID,
	[Boolean],
	[Number],
	[FloatNumber],
	[ContentType] AS PageType,
	[ContentLink] AS PageLink,
	[Date],
	[String],
	[LongString],
	[LinkGuid]
FROM    dbo.tblPropertyDefinitionDefault
GO
/****** Object:  Table [dbo].[tblRelation]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRelation](
	[FromId] [nvarchar](100) NOT NULL,
	[ToId] [nvarchar](100) NOT NULL,
	[ToName] [nvarchar](255) NULL
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [IX_tblRelation_FromId_ToName] ON [dbo].[tblRelation] 
(
	[FromId] ASC,
	[ToName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblRelation_ToId] ON [dbo].[tblRelation] 
(
	[ToId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
INSERT [dbo].[tblRelation] ([FromId], [ToId], [ToName]) VALUES (N'd2667d10-5351-4670-8ceb-5e457632eaa9', N'12f5974d-cd47-4d4c-9977-b912be06a41a', N'Documents')
INSERT [dbo].[tblRelation] ([FromId], [ToId], [ToName]) VALUES (N'd2667d10-5351-4670-8ceb-5e457632eaa9', N'490344e2-1661-42f2-826f-4594a1fa3586', N'PageFiles')
INSERT [dbo].[tblRelation] ([FromId], [ToId], [ToName]) VALUES (N'd2667d10-5351-4670-8ceb-5e457632eaa9', N'a8aef1fd-8716-4fb7-85e6-36b41722a22c', N'Global')
/****** Object:  Table [dbo].[tblScheduledItemLog]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblScheduledItemLog](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkScheduledItemId] [uniqueidentifier] NOT NULL,
	[Exec] [datetime] NOT NULL,
	[Status] [int] NULL,
	[Text] [nvarchar](2048) NULL,
 CONSTRAINT [PK_tblScheduledItemLog] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblScheduledItemLog] ON
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (1, N'7d8fad39-2682-43b6-b49c-d7c8199d5195', CAST(0x0000A118013467A7 AS DateTime), 0, N'0 user profiles were found. 0 subscription e-mails were sent.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (41, N'7d8fad39-2682-43b6-b49c-d7c8199d5195', CAST(0x0000A1180144E35C AS DateTime), 0, N'0 user profiles were found. 0 subscription e-mails were sent.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (81, N'7d8fad39-2682-43b6-b49c-d7c8199d5195', CAST(0x0000A11801555ECE AS DateTime), 0, N'0 user profiles were found. 0 subscription e-mails were sent.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (121, N'7d8fad39-2682-43b6-b49c-d7c8199d5195', CAST(0x0000A1180165D829 AS DateTime), 0, N'0 user profiles were found. 0 subscription e-mails were sent.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (161, N'7d8fad39-2682-43b6-b49c-d7c8199d5195', CAST(0x0000A118017653BD AS DateTime), 0, N'0 user profiles were found. 0 subscription e-mails were sent.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (199, N'7d8fad39-2682-43b6-b49c-d7c8199d5195', CAST(0x0000A1180186CD9E AS DateTime), 0, N'0 user profiles were found. 0 subscription e-mails were sent.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (238, N'7d8fad39-2682-43b6-b49c-d7c8199d5195', CAST(0x0000A119000BCF27 AS DateTime), 0, N'0 user profiles were found. 0 subscription e-mails were sent.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (278, N'7d8fad39-2682-43b6-b49c-d7c8199d5195', CAST(0x0000A119001C411E AS DateTime), 0, N'0 user profiles were found. 0 subscription e-mails were sent.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (317, N'7d8fad39-2682-43b6-b49c-d7c8199d5195', CAST(0x0000A119002CBCC0 AS DateTime), 0, N'0 user profiles were found. 0 subscription e-mails were sent.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (355, N'7d8fad39-2682-43b6-b49c-d7c8199d5195', CAST(0x0000A119003D4131 AS DateTime), 0, N'0 user profiles were found. 0 subscription e-mails were sent.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (394, N'7d8fad39-2682-43b6-b49c-d7c8199d5195', CAST(0x0000A119004DB1E0 AS DateTime), 0, N'0 user profiles were found. 0 subscription e-mails were sent.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (417, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190056FBAD AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (418, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900573257 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (419, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119005785F6 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (420, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190057BF00 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (421, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190058129B AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (422, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900584BA5 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (423, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190058935E AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (424, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190058E7E7 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (425, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900591E99 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (426, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900596654 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (427, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119005C78ED AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (428, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119005CC137 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (429, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119005D15C2 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (430, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119005D4DDA AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (431, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119005DA262 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (432, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119005DF6C3 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (433, N'7d8fad39-2682-43b6-b49c-d7c8199d5195', CAST(0x0000A119005E2C49 AS DateTime), 0, N'0 user profiles were found. 0 subscription e-mails were sent.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (434, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119005E3814 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (435, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119005E8A7A AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (436, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119005EABB8 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (437, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119005EF39B AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (438, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119005F481B AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (439, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119005F9C76 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (440, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119005FF0D5 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (441, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900604530 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (442, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190060998F AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (443, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190060EDF1 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (444, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900614253 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (445, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119006196AF AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (446, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190061EB12 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (447, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900650BF9 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (448, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190065545C AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (449, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190065A8FC AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (450, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190065DF93 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (451, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900662751 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (452, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900666DA3 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (453, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190066C22D AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (454, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900671692 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (455, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900676AF1 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (456, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900678516 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (457, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190067D91C AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (458, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900682D80 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (459, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119006881DF AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (460, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900689E65 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (461, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190068F265 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (462, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119006946DD AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (463, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119006971A5 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (464, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190069C572 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (465, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119006A19D7 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (466, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119006A6E36 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (467, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119006D9D8C AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (468, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119006DE5CF AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (469, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119006E3A59 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (470, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119006E8EC3 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (471, N'7d8fad39-2682-43b6-b49c-d7c8199d5195', CAST(0x0000A119006EA699 AS DateTime), 0, N'0 user profiles were found. 0 subscription e-mails were sent.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (472, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119006EB723 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (473, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119006F0AF7 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (474, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119006F4567 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (475, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119006F99F2 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (476, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119006FEE52 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (477, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190070185C AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (478, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900706CE8 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (479, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190070C147 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (480, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119007115AA AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (481, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900716A0F AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (482, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119007175F1 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (483, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190071BE48 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (484, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900720499 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (485, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900725922 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (486, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190072AD87 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (487, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190072D7A2 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (488, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900731DE0 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (489, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900736432 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (490, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190073AA84 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (491, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190073FF0D AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (492, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900745383 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (493, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190074A7D5 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (494, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190074FC34 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (495, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900755098 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (496, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190075A4F7 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (497, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190075DF6B AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (498, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119007633F7 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (499, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900766C0F AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (500, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A11900786558 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (501, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190078ADA2 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (502, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190079022A AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (503, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190079568E AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (504, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190079A91A AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (505, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A1190079C5AC AS DateTime), 0, N'0 contents were published.')
GO
print 'Processed 100 total records'
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (506, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119007A0DA9 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (507, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119007B3300 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (508, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119007B7B74 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (509, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119007C1469 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (510, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119007C5CD1 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (511, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119007CA322 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (512, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119007DD539 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (513, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119007E1DAB AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (514, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119007EFBFF AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (515, N'7d8fad39-2682-43b6-b49c-d7c8199d5195', CAST(0x0000A119007F2211 AS DateTime), 0, N'1 user profiles were found. 0 subscription e-mails were sent.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (516, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119007F4347 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (517, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119007F8ABD AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (518, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119007FDF48 AS DateTime), 0, N'0 contents were published.')
INSERT [dbo].[tblScheduledItemLog] ([pkID], [fkScheduledItemId], [Exec], [Status], [Text]) VALUES (519, N'8de3f2b0-6e12-4716-a6f9-736f7e2f8944', CAST(0x0000A119008033A6 AS DateTime), 0, N'0 contents were published.')
SET IDENTITY_INSERT [dbo].[tblScheduledItemLog] OFF
/****** Object:  Table [dbo].[tblUnifiedPathProperty]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUnifiedPathProperty](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkUnifiedPathID] [int] NOT NULL,
	[KeyName] [nvarchar](255) NOT NULL,
	[StringValue] [nvarchar](2000) NOT NULL,
 CONSTRAINT [PK_tblUnifiedPathProperty] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUnifiedPathAcl]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUnifiedPathAcl](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkUnifiedPathID] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[IsRole] [int] NOT NULL,
	[AccessMask] [int] NOT NULL,
 CONSTRAINT [PK_tblUnifiedPathAcl] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblVisitorGroupStatistic]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblVisitorGroupStatistic](
	[pkId] [bigint] NOT NULL,
	[Row] [int] NOT NULL,
	[StoreName] [nvarchar](375) NOT NULL,
	[ItemType] [nvarchar](2000) NOT NULL,
	[Boolean01] [bit] NULL,
	[Boolean02] [bit] NULL,
	[Boolean03] [bit] NULL,
	[Boolean04] [bit] NULL,
	[Boolean05] [bit] NULL,
	[Integer01] [int] NULL,
	[Integer02] [int] NULL,
	[Integer03] [int] NULL,
	[Integer04] [int] NULL,
	[Integer05] [int] NULL,
	[Integer06] [int] NULL,
	[Integer07] [int] NULL,
	[Integer08] [int] NULL,
	[Integer09] [int] NULL,
	[Integer10] [int] NULL,
	[Long01] [bigint] NULL,
	[Long02] [bigint] NULL,
	[Long03] [bigint] NULL,
	[Long04] [bigint] NULL,
	[Long05] [bigint] NULL,
	[DateTime01] [datetime] NULL,
	[DateTime02] [datetime] NULL,
	[DateTime03] [datetime] NULL,
	[DateTime04] [datetime] NULL,
	[DateTime05] [datetime] NULL,
	[Guid01] [uniqueidentifier] NULL,
	[Guid02] [uniqueidentifier] NULL,
	[Guid03] [uniqueidentifier] NULL,
	[Float01] [float] NULL,
	[Float02] [float] NULL,
	[Float03] [float] NULL,
	[Float04] [float] NULL,
	[Float05] [float] NULL,
	[Float06] [float] NULL,
	[Float07] [float] NULL,
	[String01] [nvarchar](max) NULL,
	[String02] [nvarchar](max) NULL,
	[String03] [nvarchar](max) NULL,
	[String04] [nvarchar](max) NULL,
	[String05] [nvarchar](max) NULL,
	[String06] [nvarchar](max) NULL,
	[String07] [nvarchar](max) NULL,
	[String08] [nvarchar](max) NULL,
	[String09] [nvarchar](max) NULL,
	[String10] [nvarchar](max) NULL,
	[Binary01] [varbinary](max) NULL,
	[Binary02] [varbinary](max) NULL,
	[Binary03] [varbinary](max) NULL,
	[Binary04] [varbinary](max) NULL,
	[Binary05] [varbinary](max) NULL,
	[Indexed_Boolean01] [bit] NULL,
	[Indexed_Integer01] [int] NULL,
	[Indexed_Integer02] [int] NULL,
	[Indexed_Integer03] [int] NULL,
	[Indexed_Long01] [bigint] NULL,
	[Indexed_Long02] [bigint] NULL,
	[Indexed_DateTime01] [datetime] NULL,
	[Indexed_Guid01] [uniqueidentifier] NULL,
	[Indexed_Float01] [float] NULL,
	[Indexed_Float02] [float] NULL,
	[Indexed_Float03] [float] NULL,
	[Indexed_String01] [nvarchar](450) NULL,
	[Indexed_String02] [nvarchar](450) NULL,
	[Indexed_String03] [nvarchar](450) NULL,
	[Indexed_Binary01] [varbinary](900) NULL,
 CONSTRAINT [PK_tblVisitorGroupStatistic] PRIMARY KEY CLUSTERED 
(
	[pkId] ASC,
	[Row] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IDX_tblVisitorGroupStatistic_Indexed_Binary01] ON [dbo].[tblVisitorGroupStatistic] 
(
	[Indexed_Binary01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblVisitorGroupStatistic_Indexed_Boolean01] ON [dbo].[tblVisitorGroupStatistic] 
(
	[Indexed_Boolean01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblVisitorGroupStatistic_Indexed_DateTime01] ON [dbo].[tblVisitorGroupStatistic] 
(
	[Indexed_DateTime01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblVisitorGroupStatistic_Indexed_Float01] ON [dbo].[tblVisitorGroupStatistic] 
(
	[Indexed_Float01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblVisitorGroupStatistic_Indexed_Float02] ON [dbo].[tblVisitorGroupStatistic] 
(
	[Indexed_Float02] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblVisitorGroupStatistic_Indexed_Float03] ON [dbo].[tblVisitorGroupStatistic] 
(
	[Indexed_Float03] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblVisitorGroupStatistic_Indexed_Guid01] ON [dbo].[tblVisitorGroupStatistic] 
(
	[Indexed_Guid01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblVisitorGroupStatistic_Indexed_Integer01] ON [dbo].[tblVisitorGroupStatistic] 
(
	[Indexed_Integer01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblVisitorGroupStatistic_Indexed_Integer02] ON [dbo].[tblVisitorGroupStatistic] 
(
	[Indexed_Integer02] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblVisitorGroupStatistic_Indexed_Integer03] ON [dbo].[tblVisitorGroupStatistic] 
(
	[Indexed_Integer03] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblVisitorGroupStatistic_Indexed_Long01] ON [dbo].[tblVisitorGroupStatistic] 
(
	[Indexed_Long01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblVisitorGroupStatistic_Indexed_Long02] ON [dbo].[tblVisitorGroupStatistic] 
(
	[Indexed_Long02] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblVisitorGroupStatistic_Indexed_String01] ON [dbo].[tblVisitorGroupStatistic] 
(
	[Indexed_String01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblVisitorGroupStatistic_Indexed_String02] ON [dbo].[tblVisitorGroupStatistic] 
(
	[Indexed_String02] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblVisitorGroupStatistic_Indexed_String03] ON [dbo].[tblVisitorGroupStatistic] 
(
	[Indexed_String03] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblVisitorGroupStatistic_StoreName] ON [dbo].[tblVisitorGroupStatistic] 
(
	[StoreName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSchemaItem]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSchemaItem](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkSchemaId] [int] NOT NULL,
	[FieldName] [nvarchar](256) NOT NULL,
	[FieldType] [nvarchar](256) NOT NULL,
	[Indexed] [int] NOT NULL,
 CONSTRAINT [PK_tblSchemaItem] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblSchemaItem] ON
INSERT [dbo].[tblSchemaItem] ([pkID], [fkSchemaId], [FieldName], [FieldType], [Indexed]) VALUES (1, 1, N'_id', N'System.Object', 0)
INSERT [dbo].[tblSchemaItem] ([pkID], [fkSchemaId], [FieldName], [FieldType], [Indexed]) VALUES (2, 1, N'_name', N'System.String', 0)
INSERT [dbo].[tblSchemaItem] ([pkID], [fkSchemaId], [FieldName], [FieldType], [Indexed]) VALUES (3, 2, N'_id', N'System.Object', 0)
INSERT [dbo].[tblSchemaItem] ([pkID], [fkSchemaId], [FieldName], [FieldType], [Indexed]) VALUES (4, 2, N'_name', N'System.String', 0)
INSERT [dbo].[tblSchemaItem] ([pkID], [fkSchemaId], [FieldName], [FieldType], [Indexed]) VALUES (5, 2, N'_createdBy', N'System.String', 0)
INSERT [dbo].[tblSchemaItem] ([pkID], [fkSchemaId], [FieldName], [FieldType], [Indexed]) VALUES (6, 2, N'_created', N'System.DateTime', 0)
SET IDENTITY_INSERT [dbo].[tblSchemaItem] OFF
/****** Object:  StoredProcedure [dbo].[SchemaSave]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[SchemaSave]
(
	@SchemaId	NVARCHAR(256),
	@IdType		NVARCHAR(256)
)
AS
BEGIN
	DECLARE @Id INT
	SELECT @Id=pkID FROM tblSchema WHERE SchemaId=@SchemaId
	IF (@@ROWCOUNT = 0)
	BEGIN
		INSERT INTO tblSchema 
			([SchemaId], [IdType]) 
		VALUES 
			(@SchemaId, @IdType)
		SET @Id =  SCOPE_IDENTITY() 
	END
	RETURN @Id
END
GO
/****** Object:  Table [dbo].[tblWindowsRelations]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblWindowsRelations](
	[fkWindowsUser] [int] NOT NULL,
	[fkWindowsGroup] [int] NOT NULL,
 CONSTRAINT [PK_tblWindowsRelations] PRIMARY KEY CLUSTERED 
(
	[fkWindowsUser] ASC,
	[fkWindowsGroup] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 1)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 2)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 3)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 4)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 5)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 6)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 7)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 8)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 9)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 10)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 11)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 12)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 13)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 14)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 15)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 16)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 17)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 18)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 19)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 20)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 21)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 22)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 23)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 24)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 25)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 26)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 27)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 28)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 29)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 30)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 31)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 32)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 33)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 34)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 35)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 36)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 37)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 38)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 39)
INSERT [dbo].[tblWindowsRelations] ([fkWindowsUser], [fkWindowsGroup]) VALUES (1, 40)
/****** Object:  View [dbo].[vw_aspnet_Applications]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_Applications]
  AS SELECT [dbo].[aspnet_Applications].[ApplicationName], [dbo].[aspnet_Applications].[LoweredApplicationName], [dbo].[aspnet_Applications].[ApplicationId], [dbo].[aspnet_Applications].[Description]
  FROM [dbo].[aspnet_Applications]
GO
/****** Object:  StoredProcedure [dbo].[UnlockInstanceState]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[UnlockInstanceState]
@uidInstanceID uniqueidentifier,
@ownerID uniqueidentifier = NULL
As
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
set nocount on
		Update [dbo].[InstanceState]  
		Set ownerID = NULL,
		     unlocked = 1,
			ownedUntil = NULL
		Where uidInstanceID = @uidInstanceID AND ((ownerID = @ownerID AND ownedUntil>=GETUTCDATE()) OR (ownerID IS NULL AND @ownerID IS NULL ))
GO
/****** Object:  Table [dbo].[tblXFormData]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblXFormData](
	[pkId] [bigint] NOT NULL,
	[Row] [int] NOT NULL,
	[StoreName] [nvarchar](375) NOT NULL,
	[ItemType] [nvarchar](2000) NOT NULL,
	[ChannelOptions] [int] NULL,
	[DatePosted] [datetime] NULL,
	[FormId] [uniqueidentifier] NULL,
	[PageGuid] [uniqueidentifier] NULL,
	[UserName] [nvarchar](450) NULL,
	[String01] [nvarchar](max) NULL,
	[String02] [nvarchar](max) NULL,
	[String03] [nvarchar](max) NULL,
	[String04] [nvarchar](max) NULL,
	[String05] [nvarchar](max) NULL,
	[String06] [nvarchar](max) NULL,
	[String07] [nvarchar](max) NULL,
	[String08] [nvarchar](max) NULL,
	[String09] [nvarchar](max) NULL,
	[String10] [nvarchar](max) NULL,
	[String11] [nvarchar](max) NULL,
	[String12] [nvarchar](max) NULL,
	[String13] [nvarchar](max) NULL,
	[String14] [nvarchar](max) NULL,
	[String15] [nvarchar](max) NULL,
	[String16] [nvarchar](max) NULL,
	[String17] [nvarchar](max) NULL,
	[String18] [nvarchar](max) NULL,
	[String19] [nvarchar](max) NULL,
	[String20] [nvarchar](max) NULL,
	[Indexed_String01] [nvarchar](450) NULL,
	[Indexed_String02] [nvarchar](450) NULL,
	[Indexed_String03] [nvarchar](450) NULL,
 CONSTRAINT [PK_tblXFormData] PRIMARY KEY CLUSTERED 
(
	[pkId] ASC,
	[Row] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblXFormData_String01] ON [dbo].[tblXFormData] 
(
	[Indexed_String01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblXFormData_String02] ON [dbo].[tblXFormData] 
(
	[Indexed_String02] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblXFormData_String03] ON [dbo].[tblXFormData] 
(
	[Indexed_String03] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblWorkContent]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblWorkContent](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkContentID] [int] NOT NULL,
	[fkMasterVersionID] [int] NULL,
	[ContentLinkGUID] [uniqueidentifier] NULL,
	[fkFrameID] [int] NULL,
	[ArchiveContentGUID] [uniqueidentifier] NULL,
	[ChangedByName] [nvarchar](255) NOT NULL,
	[NewStatusByName] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[URLSegment] [nvarchar](255) NULL,
	[LinkURL] [nvarchar](255) NULL,
	[ExternalURL] [nvarchar](255) NULL,
	[VisibleInMenu] [bit] NOT NULL,
	[LinkType] [int] NOT NULL,
	[Created] [datetime] NOT NULL,
	[Saved] [datetime] NOT NULL,
	[StartPublish] [datetime] NULL,
	[StopPublish] [datetime] NULL,
	[ChildOrderRule] [int] NOT NULL,
	[PeerOrder] [int] NOT NULL,
	[ReadyToPublish] [bit] NOT NULL,
	[ChangedOnPublish] [bit] NOT NULL,
	[HasBeenPublished] [bit] NOT NULL,
	[Rejected] [bit] NOT NULL,
	[DelayedPublish] [bit] NOT NULL,
	[RejectComment] [nvarchar](2000) NULL,
	[fkLanguageBranchID] [int] NOT NULL,
	[CommonDraft] [bit] NOT NULL,
 CONSTRAINT [PK_tblWorkContent] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblWorkContent_ArchiveContentGUID] ON [dbo].[tblWorkContent] 
(
	[ArchiveContentGUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblWorkContent_ChangedByName] ON [dbo].[tblWorkContent] 
(
	[ChangedByName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblWorkContent_ContentLinkGUID] ON [dbo].[tblWorkContent] 
(
	[ContentLinkGUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblWorkContent_fkContentID] ON [dbo].[tblWorkContent] 
(
	[fkContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblWorkContent_fkMasterVersionID] ON [dbo].[tblWorkContent] 
(
	[fkMasterVersionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblWorkContent_StatusFields] ON [dbo].[tblWorkContent] 
(
	[ReadyToPublish] ASC,
	[DelayedPublish] ASC,
	[HasBeenPublished] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblWorkContent] ON
INSERT [dbo].[tblWorkContent] ([pkID], [fkContentID], [fkMasterVersionID], [ContentLinkGUID], [fkFrameID], [ArchiveContentGUID], [ChangedByName], [NewStatusByName], [Name], [URLSegment], [LinkURL], [ExternalURL], [VisibleInMenu], [LinkType], [Created], [Saved], [StartPublish], [StopPublish], [ChildOrderRule], [PeerOrder], [ReadyToPublish], [ChangedOnPublish], [HasBeenPublished], [Rejected], [DelayedPublish], [RejectComment], [fkLanguageBranchID], [CommonDraft]) VALUES (1, 1, NULL, NULL, NULL, NULL, N'', NULL, N'Root', NULL, N'~/UI/edit/workspace.aspx?id=1', NULL, 1, 0, CAST(0x00008D3F00000000 AS DateTime), CAST(0x00008D3F00000000 AS DateTime), CAST(0x00008D3F00000000 AS DateTime), NULL, 4, 100, 1, 0, 1, 0, 0, NULL, 1, 0)
INSERT [dbo].[tblWorkContent] ([pkID], [fkContentID], [fkMasterVersionID], [ContentLinkGUID], [fkFrameID], [ArchiveContentGUID], [ChangedByName], [NewStatusByName], [Name], [URLSegment], [LinkURL], [ExternalURL], [VisibleInMenu], [LinkType], [Created], [Saved], [StartPublish], [StopPublish], [ChildOrderRule], [PeerOrder], [ReadyToPublish], [ChangedOnPublish], [HasBeenPublished], [Rejected], [DelayedPublish], [RejectComment], [fkLanguageBranchID], [CommonDraft]) VALUES (2, 2, NULL, NULL, NULL, NULL, N'', NULL, N'Recycle Bin', NULL, N'~/UI/edit/WastebasketPage.aspx?id=2', NULL, 1, 0, CAST(0x00008D3F00000000 AS DateTime), CAST(0x00008D3F00000000 AS DateTime), CAST(0x00008D3F00000000 AS DateTime), NULL, 1, 10, 1, 0, 1, 0, 0, NULL, 1, 0)
INSERT [dbo].[tblWorkContent] ([pkID], [fkContentID], [fkMasterVersionID], [ContentLinkGUID], [fkFrameID], [ArchiveContentGUID], [ChangedByName], [NewStatusByName], [Name], [URLSegment], [LinkURL], [ExternalURL], [VisibleInMenu], [LinkType], [Created], [Saved], [StartPublish], [StopPublish], [ChildOrderRule], [PeerOrder], [ReadyToPublish], [ChangedOnPublish], [HasBeenPublished], [Rejected], [DelayedPublish], [RejectComment], [fkLanguageBranchID], [CommonDraft]) VALUES (3, 3, NULL, NULL, NULL, NULL, N'', NULL, N'Shared Block Root', NULL, NULL, NULL, 1, 0, CAST(0x00008D3F00000000 AS DateTime), CAST(0x00008D3F00000000 AS DateTime), CAST(0x00008D3F00000000 AS DateTime), NULL, 4, 100, 1, 0, 1, 0, 0, NULL, 1, 0)
SET IDENTITY_INSERT [dbo].[tblWorkContent] OFF
/****** Object:  View [dbo].[vw_aspnet_Users]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_Users]
  AS SELECT [dbo].[aspnet_Users].[ApplicationId], [dbo].[aspnet_Users].[UserId], [dbo].[aspnet_Users].[UserName], [dbo].[aspnet_Users].[LoweredUserName], [dbo].[aspnet_Users].[MobileAlias], [dbo].[aspnet_Users].[IsAnonymous], [dbo].[aspnet_Users].[LastActivityDate]
  FROM [dbo].[aspnet_Users]
GO
/****** Object:  View [dbo].[vw_aspnet_Roles]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_Roles]
  AS SELECT [dbo].[aspnet_Roles].[ApplicationId], [dbo].[aspnet_Roles].[RoleId], [dbo].[aspnet_Roles].[RoleName], [dbo].[aspnet_Roles].[LoweredRoleName], [dbo].[aspnet_Roles].[Description]
  FROM [dbo].[aspnet_Roles]
GO
/****** Object:  StoredProcedure [dbo].[TestClearItems]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TestClearItems]
AS
BEGIN
	DELETE FROM tblIndexString
	DELETE FROM tblIndexInt
	DELETE FROM tblIndexDateTime
	DELETE FROM tblIndexDecimal
	DELETE FROM tblIndexFloat
	DELETE FROM tblIndexGuid
	DELETE FROM tblRelation
	DELETE FROM tblItem
	DELETE FROM tblSchema
END
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_Paths]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_WebPartState_Paths]
  AS SELECT [dbo].[aspnet_Paths].[ApplicationId], [dbo].[aspnet_Paths].[PathId], [dbo].[aspnet_Paths].[Path], [dbo].[aspnet_Paths].[LoweredPath]
  FROM [dbo].[aspnet_Paths]
GO
/****** Object:  View [dbo].[VW_XFormFolders]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_XFormFolders] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType],
R01.Guid01 as "ParentFolderId",
R01.String01 as "SubPath"
from [tblSystemBigTable] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
where R01.StoreName='XFormFolders' and R01.Row=1
GO
/****** Object:  View [dbo].[VW_VisitorGroup]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_VisitorGroup] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType],
R01.Integer01 as "CriteriaOperator",
R01.Boolean01 as "EnableStatistics",
R01.Boolean02 as "IsSecurityRole",
R01.String01 as "Name",
R01.String02 as "Notes",
R01.Integer02 as "PointsThreshold"
from [tblSystemBigTable] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
where R01.StoreName='VisitorGroup' and R01.Row=1
GO
/****** Object:  View [dbo].[VW_System.Object]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_System.Object] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType]
from [tblBigTable] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
where R01.StoreName='System.Object' and R01.Row=1
GO
/****** Object:  View [dbo].[VW_QuickLinkSettings]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_QuickLinkSettings] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType],
R01.Guid01 as "GadgetId"
from [tblSystemBigTable] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
where R01.StoreName='QuickLinkSettings' and R01.Row=1
GO
/****** Object:  View [dbo].[VW_InUseNotifications]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_InUseNotifications] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType],
R01.Boolean01 as "AddedManually",
R01.Indexed_Guid01 as "ContentGuid",
R01.DateTime01 as "CreateTime",
R01.Indexed_String01 as "LanguageBranch",
R01.DateTime02 as "Modified",
R01.Indexed_String02 as "User"
from [tblBigTable] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
where R01.StoreName='InUseNotifications' and R01.Row=1
GO
/****** Object:  View [dbo].[VW_IndexRequestQueueDataStore]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_IndexRequestQueueDataStore] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType],
R01.String01 as "IndexItemId",
R01.Indexed_String01 as "NamedIndex",
R02.Indexed_String01 as "NamedIndexingService",
R01.String02 as "SyndicationItemXml",
R01.Indexed_DateTime01 as "Timestamp"
from [tblIndexRequestLog] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
left outer join tblIndexRequestLog as R02 on R01.pkId = R02.pkId and R02.Row=2
where R01.StoreName='IndexRequestQueueDataStore' and R01.Row=1
GO
/****** Object:  View [dbo].[VW_EPiServer.Shell.Storage.PersonalizedViewSettingsStorage]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_EPiServer.Shell.Storage.PersonalizedViewSettingsStorage] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType],
R01.Indexed_String01 as "UserName",
R01.Indexed_String02 as "ViewName"
from [tblSystemBigTable] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
where R01.StoreName='EPiServer.Shell.Storage.PersonalizedViewSettingsStorage' and R01.Row=1
GO
/****** Object:  View [dbo].[VW_EPiServer.Shell.Storage.ContainerData]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_EPiServer.Shell.Storage.ContainerData] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType],
R01.String01 as "DefinitionName",
R01.String02 as "PlugInArea"
from [tblBigTable] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
where R01.StoreName='EPiServer.Shell.Storage.ContainerData' and R01.Row=1
GO
/****** Object:  View [dbo].[VW_EPiServer.Shell.Storage.ComponentData]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_EPiServer.Shell.Storage.ComponentData] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType],
R01.String01 as "DefinitionName"
from [tblBigTable] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
where R01.StoreName='EPiServer.Shell.Storage.ComponentData' and R01.Row=1
GO
/****** Object:  View [dbo].[VW_EPiServer.Shell.Search.SearchProviderSetting]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_EPiServer.Shell.Search.SearchProviderSetting] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType],
R01.String01 as "FullName",
R01.Boolean01 as "IsEnabled",
R01.Integer01 as "SortIndex"
from [tblBigTable] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
where R01.StoreName='EPiServer.Shell.Search.SearchProviderSetting' and R01.Row=1
GO
/****** Object:  View [dbo].[VW_EPiServer.Shell.Profile.ProfileData]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_EPiServer.Shell.Profile.ProfileData] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType],
R01.String01 as "UserName"
from [tblSystemBigTable] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
where R01.StoreName='EPiServer.Shell.Profile.ProfileData' and R01.Row=1
GO
/****** Object:  View [dbo].[VW_EPiServer.Personalization.VisitorGroups.Criteria.ViewedCategoriesModel]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_EPiServer.Personalization.VisitorGroups.Criteria.ViewedCategoriesModel] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType],
R01.Guid01 as "CategoryGuid",
R01.Integer01 as "NumberOfPageViews",
R01.Integer02 as "SelectedCategory"
from [tblBigTable] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
where R01.StoreName='EPiServer.Personalization.VisitorGroups.Criteria.ViewedCategoriesModel' and R01.Row=1
GO
/****** Object:  View [dbo].[VW_EPiServer.Packaging.Storage.StorageUpdateEntity]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_EPiServer.Packaging.Storage.StorageUpdateEntity] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType],
R01.String01 as "ServerId",
R01.DateTime01 as "UpdateDate"
from [tblSystemBigTable] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
where R01.StoreName='EPiServer.Packaging.Storage.StorageUpdateEntity' and R01.Row=1
GO
/****** Object:  View [dbo].[VW_EPiServer.Packaging.Storage.PackageEntity]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_EPiServer.Packaging.Storage.PackageEntity] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType],
R01.DateTime01 as "InstallDate",
R01.String01 as "InstalledBy",
R01.Boolean01 as "NotificationProcessed",
R01.String02 as "PackageId",
R01.String03 as "ServerId",
R01.String04 as "Version"
from [tblSystemBigTable] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
where R01.StoreName='EPiServer.Packaging.Storage.PackageEntity' and R01.Row=1
GO
/****** Object:  View [dbo].[VW_EPiServer.Core.PropertySettings.PropertySettingsWrapper]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_EPiServer.Core.PropertySettings.PropertySettingsWrapper] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType],
R01.String01 as "Description",
R01.String02 as "DisplayName",
R01.Boolean01 as "IsDefault",
R01.Boolean02 as "IsGlobal",
R01.String03 as "TypeFullName"
from [tblSystemBigTable] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
where R01.StoreName='EPiServer.Core.PropertySettings.PropertySettingsWrapper' and R01.Row=1
GO
/****** Object:  View [dbo].[VW_EPiServer.Core.IndexingInformation]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_EPiServer.Core.IndexingInformation] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType],
R01.DateTime01 as "ExecutionDate",
R01.Boolean01 as "ResetIndex"
from [tblBigTable] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
where R01.StoreName='EPiServer.Core.IndexingInformation' and R01.Row=1
GO
/****** Object:  View [dbo].[VW_EPiServer.Cms.Shell.UI.Models.NotesData]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_EPiServer.Cms.Shell.UI.Models.NotesData] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType],
R01.String01 as "BackgroundColor",
R01.String02 as "Content",
R01.String03 as "FontSize",
R01.Guid01 as "GadgetId",
R01.String04 as "Title"
from [tblSystemBigTable] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
where R01.StoreName='EPiServer.Cms.Shell.UI.Models.NotesData' and R01.Row=1
GO
/****** Object:  View [dbo].[VW_Drafts]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_Drafts] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType],
R01.Boolean01 as "IsNewLanguageBranch",
R01.Boolean02 as "IsNewPage",
R01.Boolean03 as "IsNotified",
R01.Indexed_String01 as "LanguageBranch",
R01.Indexed_Guid01 as "PageGuid",
R01.Indexed_String02 as "PageLink",
R01.String01 as "PageName",
R01.String02 as "PagePath",
R01.Integer01 as "PageType",
R01.String03 as "ParentLink",
R01.Indexed_DateTime01 as "Saved",
R01.Indexed_String03 as "SavedBy"
from [tblBigTable] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
where R01.StoreName='Drafts' and R01.Row=1
GO
/****** Object:  View [dbo].[VW_BackgroundJobStore]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[VW_BackgroundJobStore] as select
CAST (R01.pkId as varchar(50)) + ':' + UPPER(CAST([Identity].Guid as varchar(50))) as [Id], R01.pkId as [StoreId], [Identity].Guid as [ExternalId], R01.ItemType as [ItemType],
R01.Integer01 as "CurrentState",
R01.String01 as "Data",
R01.DateTime01 as "EndDate",
R01.DateTime02 as "LastHeartBeat",
R01.String02 as "Message",
R01.String03 as "Owner",
R01.DateTime03 as "StartDate",
R01.String04 as "Title"
from [tblSystemBigTable] as R01 
inner join tblBigTableIdentity as [Identity] on R01.pkId=[Identity].pkId 
where R01.StoreName='BackgroundJobStore' and R01.Row=1
GO
/****** Object:  StoredProcedure [dbo].[SchemaLoad]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SchemaLoad]
	@SchemaId	NVARCHAR(256)
AS
BEGIN
	SELECT
		[tblSchemaItem].[pkID] AS SchemaItemId,
		[FieldName],
		[FieldType],
		[Indexed]
	FROM 
		tblSchemaItem
	INNER JOIN
		tblSchema ON fkSchemaId=tblSchema.pkID
	WHERE
		SchemaId=@SchemaId
END
GO
/****** Object:  View [dbo].[tblPageTypeToPageType]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[tblPageTypeToPageType]
AS
SELECT
	[fkContentTypeParentID] AS fkPageTypeParentID,
	[fkContentTypeChildID] AS fkPageTypeChildID,
	[Access],
	[Availability],
	[Allow]
FROM    dbo.tblContentTypeToContentType
GO
/****** Object:  View [dbo].[tblPageTypeDefault]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[tblPageTypeDefault]
AS
SELECT
	[pkID],
	[fkContentTypeID] AS fkPageTypeID,
	[fkContentLinkID] AS fkPageLinkID,
	[fkFrameID],
	[fkArchiveContentID] AS fkArchivePageID,
	[Name],
	[VisibleInMenu],
	[StartPublishOffsetValue],
	[StartPublishOffsetType],
	[StopPublishOffsetValue],
	[StopPublishOffsetType],
	[ChildOrderRule],
	[PeerOrder],
	[StartPublishOffset],
	[StopPublishOffset]
FROM    dbo.tblContentTypeDefault
GO
/****** Object:  Table [dbo].[tblContentSoftlink]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblContentSoftlink](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkOwnerContentID] [int] NOT NULL,
	[fkReferencedContentGUID] [uniqueidentifier] NULL,
	[OwnerLanguageID] [int] NULL,
	[ReferencedLanguageID] [int] NULL,
	[LinkURL] [nvarchar](255) NOT NULL,
	[LinkType] [int] NOT NULL,
	[LinkProtocol] [nvarchar](10) NULL,
	[ContentLink] [nvarchar](255) NULL,
	[LastCheckedDate] [datetime] NULL,
	[FirstDateBroken] [datetime] NULL,
	[HttpStatusCode] [int] NULL,
	[LinkStatus] [int] NULL,
 CONSTRAINT [PK_tblContentSoftlink] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblContentSoftlink_fkContentID] ON [dbo].[tblContentSoftlink] 
(
	[fkOwnerContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblContentSoftlink_fkReferencedContentGUID] ON [dbo].[tblContentSoftlink] 
(
	[fkReferencedContentGUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblContentProperty]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblContentProperty](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkPropertyDefinitionID] [int] NOT NULL,
	[fkContentID] [int] NOT NULL,
	[fkLanguageBranchID] [int] NOT NULL,
	[ScopeName] [nvarchar](450) NULL,
	[guid] [uniqueidentifier] NOT NULL,
	[Boolean] [bit] NOT NULL,
	[Number] [int] NULL,
	[FloatNumber] [float] NULL,
	[ContentType] [int] NULL,
	[ContentLink] [int] NULL,
	[Date] [datetime] NULL,
	[String] [nvarchar](450) NULL,
	[LongString] [nvarchar](max) NULL,
	[LongStringLength] [int] NULL,
	[LinkGuid] [uniqueidentifier] NULL,
 CONSTRAINT [PK_tblContentProperty] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblContentProperty_ContentLink] ON [dbo].[tblContentProperty] 
(
	[ContentLink] ASC,
	[LinkGuid] ASC
)
INCLUDE ( [fkPropertyDefinitionID],
[fkContentID],
[fkLanguageBranchID]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblContentProperty_ContentTypeID] ON [dbo].[tblContentProperty] 
(
	[ContentType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblContentProperty_fkContentID] ON [dbo].[tblContentProperty] 
(
	[fkContentID] ASC,
	[fkLanguageBranchID] ASC
)
INCLUDE ( [fkPropertyDefinitionID]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblContentProperty_fkPropertyDefinitionID] ON [dbo].[tblContentProperty] 
(
	[fkPropertyDefinitionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblContentProperty_ScopeName] ON [dbo].[tblContentProperty] 
(
	[ScopeName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tblContentProperty] ON [dbo].[tblContentProperty] 
(
	[guid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblContentLanguageSetting]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblContentLanguageSetting](
	[fkContentID] [int] NOT NULL,
	[fkLanguageBranchID] [int] NOT NULL,
	[fkReplacementBranchID] [int] NULL,
	[LanguageBranchFallback] [nvarchar](1000) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_tblContentLanguageSetting] PRIMARY KEY CLUSTERED 
(
	[fkContentID] ASC,
	[fkLanguageBranchID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblContentLanguage]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblContentLanguage](
	[fkContentID] [int] NOT NULL,
	[fkLanguageBranchID] [int] NOT NULL,
	[ContentLinkGUID] [uniqueidentifier] NULL,
	[fkFrameID] [int] NULL,
	[CreatorName] [nvarchar](255) NULL,
	[ChangedByName] [nvarchar](255) NULL,
	[ContentGUID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[URLSegment] [nvarchar](255) NULL,
	[LinkURL] [nvarchar](255) NULL,
	[ExternalURL] [nvarchar](255) NULL,
	[AutomaticLink] [bit] NOT NULL,
	[FetchData] [bit] NOT NULL,
	[PendingPublish] [bit] NOT NULL,
	[Created] [datetime] NOT NULL,
	[Changed] [datetime] NOT NULL,
	[Saved] [datetime] NOT NULL,
	[StartPublish] [datetime] NULL,
	[StopPublish] [datetime] NULL,
	[PublishedVersion] [int] NULL,
 CONSTRAINT [PK_tblContentLanguage] PRIMARY KEY CLUSTERED 
(
	[fkContentID] ASC,
	[fkLanguageBranchID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IDX_tblContentLanguage_ContentGUID] ON [dbo].[tblContentLanguage] 
(
	[ContentGUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblContentLanguage_ContentLinkGUID] ON [dbo].[tblContentLanguage] 
(
	[ContentLinkGUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblContentLanguage_ExternalURL] ON [dbo].[tblContentLanguage] 
(
	[ExternalURL] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblContentLanguage_Name] ON [dbo].[tblContentLanguage] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
INSERT [dbo].[tblContentLanguage] ([fkContentID], [fkLanguageBranchID], [ContentLinkGUID], [fkFrameID], [CreatorName], [ChangedByName], [ContentGUID], [Name], [URLSegment], [LinkURL], [ExternalURL], [AutomaticLink], [FetchData], [PendingPublish], [Created], [Changed], [Saved], [StartPublish], [StopPublish], [PublishedVersion]) VALUES (1, 1, NULL, NULL, N'', N'', N'43f936c9-9b23-4ea3-97b2-61c538ad07c9', N'Root', NULL, N'~/UI/edit/workspace.aspx?id=1', NULL, 1, 0, 0, CAST(0x00008D3F00000000 AS DateTime), CAST(0x00008D3F00000000 AS DateTime), CAST(0x00008D3F00000000 AS DateTime), CAST(0x00008D3F00000000 AS DateTime), NULL, 1)
INSERT [dbo].[tblContentLanguage] ([fkContentID], [fkLanguageBranchID], [ContentLinkGUID], [fkFrameID], [CreatorName], [ChangedByName], [ContentGUID], [Name], [URLSegment], [LinkURL], [ExternalURL], [AutomaticLink], [FetchData], [PendingPublish], [Created], [Changed], [Saved], [StartPublish], [StopPublish], [PublishedVersion]) VALUES (2, 1, NULL, NULL, N'', N'', N'2f40ba47-f4fc-47ae-a244-0b909d4cf988', N'Recycle Bin', N'Recycle-Bin', N'~/UI/edit/WastebasketPage.aspx?id=2', NULL, 1, 0, 0, CAST(0x00008D3F00000000 AS DateTime), CAST(0x00008D3F00000000 AS DateTime), CAST(0x00008D3F00000000 AS DateTime), CAST(0x00008D3F00000000 AS DateTime), NULL, 2)
INSERT [dbo].[tblContentLanguage] ([fkContentID], [fkLanguageBranchID], [ContentLinkGUID], [fkFrameID], [CreatorName], [ChangedByName], [ContentGUID], [Name], [URLSegment], [LinkURL], [ExternalURL], [AutomaticLink], [FetchData], [PendingPublish], [Created], [Changed], [Saved], [StartPublish], [StopPublish], [PublishedVersion]) VALUES (3, 1, NULL, NULL, N'', N'', N'e56f85d0-e833-4e02-976a-2d11fe4d598c', N'Shared Block Root', NULL, NULL, NULL, 1, 0, 0, CAST(0x00008D3F00000000 AS DateTime), CAST(0x00008D3F00000000 AS DateTime), CAST(0x00008D3F00000000 AS DateTime), CAST(0x00008D3F00000000 AS DateTime), NULL, 3)
/****** Object:  Table [dbo].[tblContentCategory]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblContentCategory](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkContentID] [int] NOT NULL,
	[fkCategoryID] [int] NOT NULL,
	[CategoryType] [int] NOT NULL,
	[fkLanguageBranchID] [int] NOT NULL,
	[ScopeName] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_tblContentCategory] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblContentCategory_fkContentID] ON [dbo].[tblContentCategory] 
(
	[fkContentID] ASC,
	[CategoryType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblContentAccess]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblContentAccess](
	[fkContentID] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[IsRole] [int] NOT NULL,
	[AccessMask] [int] NOT NULL,
 CONSTRAINT [PK_tblContentAccess] PRIMARY KEY CLUSTERED 
(
	[fkContentID] ASC,
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[tblContentAccess] ([fkContentID], [Name], [IsRole], [AccessMask]) VALUES (1, N'Administrators', 1, 63)
INSERT [dbo].[tblContentAccess] ([fkContentID], [Name], [IsRole], [AccessMask]) VALUES (1, N'Everyone', 1, 1)
INSERT [dbo].[tblContentAccess] ([fkContentID], [Name], [IsRole], [AccessMask]) VALUES (2, N'Administrators', 1, 63)
INSERT [dbo].[tblContentAccess] ([fkContentID], [Name], [IsRole], [AccessMask]) VALUES (2, N'Everyone', 1, 1)
INSERT [dbo].[tblContentAccess] ([fkContentID], [Name], [IsRole], [AccessMask]) VALUES (3, N'Administrators', 1, 63)
INSERT [dbo].[tblContentAccess] ([fkContentID], [Name], [IsRole], [AccessMask]) VALUES (3, N'Everyone', 1, 1)
/****** Object:  View [dbo].[tblPageDefinition]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[tblPageDefinition]
AS
SELECT  [pkID],
		[fkContentTypeID] AS fkPageTypeID,
		[fkPropertyDefinitionTypeID] AS fkPageDefinitionTypeID,
		[FieldOrder],
		[Name],
		[Property],
		[Required],
		[Advanced],
		[Searchable],
		[EditCaption],
		[HelpText],
		[ObjectProgID],
		[DefaultValueType],
		[LongStringSettings],
		[SettingsID],
		[LanguageSpecific],
		[DisplayEditUI],
		[ExistsOnModel]
FROM    dbo.tblPropertyDefinition
GO
/****** Object:  View [dbo].[tblPage]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[tblPage]
AS
SELECT  [pkID],
		[fkContentTypeID] AS fkPageTypeID,
		[fkParentID],
		[ArchiveContentGUID] AS ArchivePageGUID,
		[CreatorName],
		[ContentGUID] AS PageGUID,
		[VisibleInMenu],
		[Deleted],
		[PendingPublish],
		[ChildOrderRule],
		[PeerOrder],
		[ExternalFolderID],
		[PublishedVersion],
		[fkMasterLanguageBranchID],
		[ContentPath] AS PagePath,
		[ContentType],
		[DeletedBy],
		[DeletedDate]
FROM    dbo.tblContent
GO
/****** Object:  Table [dbo].[aspnet_PersonalizationPerUser]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_PersonalizationPerUser](
	[Id] [uniqueidentifier] NOT NULL,
	[PathId] [uniqueidentifier] NULL,
	[UserId] [uniqueidentifier] NULL,
	[PageSettings] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_PersonalizationPerUser_index1] ON [dbo].[aspnet_PersonalizationPerUser] 
(
	[PathId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [aspnet_PersonalizationPerUser_ncindex2] ON [dbo].[aspnet_PersonalizationPerUser] 
(
	[UserId] ASC,
	[PathId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aspnet_Profile]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_Profile](
	[UserId] [uniqueidentifier] NOT NULL,
	[PropertyNames] [ntext] NOT NULL,
	[PropertyValuesString] [ntext] NOT NULL,
	[PropertyValuesBinary] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[aspnet_Profile] ([UserId], [PropertyNames], [PropertyValuesString], [PropertyValuesBinary], [LastUpdatedDate]) VALUES (N'999d6269-8d39-411e-b8af-64f21ed637b9', N'FileManagerFavourites:S:0:157:EditTreeSettings:S:157:908:', N'<?xml version="1.0" encoding="utf-16"?>
<ArrayOfString xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" /><?xml version="1.0" encoding="utf-16"?>
<GuiSettings xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <ShowLanguageMissing>true</ShowLanguageMissing>
  <ShowLanguageFallback>true</ShowLanguageFallback>
  <ShowNew>true</ShowNew>
  <ShowNotPublished>true</ShowNotPublished>
  <ShowNotStarted>true</ShowNotStarted>
  <ShowExpired>true</ShowExpired>
  <ShowReadOnly>true</ShowReadOnly>
  <ShowNotVisibleInMenu>true</ShowNotVisibleInMenu>
  <ShowPageProviderIcon>true</ShowPageProviderIcon>
  <ShowShortcutIcon>true</ShowShortcutIcon>
  <ShowExternalIcon>true</ShowExternalIcon>
  <ShowInactiveIcon>true</ShowInactiveIcon>
  <ShowFetchDataIcon>true</ShowFetchDataIcon>
  <ShowEditingInProgressIcon>true</ShowEditingInProgressIcon>
  <ShowContainerPageIcon>true</ShowContainerPageIcon>
  <SettingsEnabled>false</SettingsEnabled>
</GuiSettings>', 0x, CAST(0x0000A119007EEBDE AS DateTime))
/****** Object:  Table [dbo].[aspnet_Membership]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_Membership](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[PasswordFormat] [int] NOT NULL,
	[PasswordSalt] [nvarchar](128) NOT NULL,
	[MobilePIN] [nvarchar](16) NULL,
	[Email] [nvarchar](256) NULL,
	[LoweredEmail] [nvarchar](256) NULL,
	[PasswordQuestion] [nvarchar](256) NULL,
	[PasswordAnswer] [nvarchar](128) NULL,
	[IsApproved] [bit] NOT NULL,
	[IsLockedOut] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastLoginDate] [datetime] NOT NULL,
	[LastPasswordChangedDate] [datetime] NOT NULL,
	[LastLockoutDate] [datetime] NOT NULL,
	[FailedPasswordAttemptCount] [int] NOT NULL,
	[FailedPasswordAttemptWindowStart] [datetime] NOT NULL,
	[FailedPasswordAnswerAttemptCount] [int] NOT NULL,
	[FailedPasswordAnswerAttemptWindowStart] [datetime] NOT NULL,
	[Comment] [ntext] NULL,
PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [aspnet_Membership_index] ON [dbo].[aspnet_Membership] 
(
	[ApplicationId] ASC,
	[LoweredEmail] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Paths_CreatePath]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Paths_CreatePath]
    @ApplicationId UNIQUEIDENTIFIER,
    @Path           NVARCHAR(256),
    @PathId         UNIQUEIDENTIFIER OUTPUT
AS
BEGIN
    BEGIN TRANSACTION
    IF (NOT EXISTS(SELECT * FROM dbo.aspnet_Paths WHERE LoweredPath = LOWER(@Path) AND ApplicationId = @ApplicationId))
    BEGIN
        INSERT dbo.aspnet_Paths (ApplicationId, Path, LoweredPath) VALUES (@ApplicationId, @Path, LOWER(@Path))
    END
    COMMIT TRANSACTION
    SELECT @PathId = PathId FROM dbo.aspnet_Paths WHERE LOWER(@Path) = LoweredPath AND ApplicationId = @ApplicationId
END
GO
/****** Object:  Table [dbo].[aspnet_PersonalizationAllUsers]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_PersonalizationAllUsers](
	[PathId] [uniqueidentifier] NOT NULL,
	[PageSettings] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PathId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[GetScopedBlockProperties]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetScopedBlockProperties] 
(
	@ContentTypeID int
)
RETURNS @ScopedPropertiesTable TABLE 
(
	ScopeName nvarchar(450)
)
AS
BEGIN
	WITH ScopedProperties(ContentTypeID, PropertyDefinitionID, Scope, Level)
	AS
	(
		--Top level statement
		SELECT T1.pkID as ContentTypeID, tblPropertyDefinition.pkID as PropertyDefinitionID, 
			Cast('.' + CAST(tblPropertyDefinition.pkID as VARCHAR) + '.' as varchar) as Scope, 0 as Level
		FROM tblPropertyDefinition
		INNER JOIN tblContentType AS T1 ON T1.pkID=tblPropertyDefinition.fkContentTypeID
		INNER JOIN tblPropertyDefinitionType ON tblPropertyDefinitionType.pkID = tblPropertyDefinition.fkPropertyDefinitionTypeID
		INNER JOIN tblContentType ON tblPropertyDefinitionType.fkContentTypeGUID = tblContentType.ContentTypeGUID
		WHERE tblContentType.pkID = @ContentTypeID
		UNION ALL
		--Recursive statement
		SELECT T1.pkID as ContentTypeID, tblPropertyDefinition.pkID as PropertyDefinitionID, 
			Cast('.' + CAST(tblPropertyDefinition.pkID as VARCHAR) + Scope as varchar ) as Scope, ScopedProperties.Level+1 as Level
		FROM tblPropertyDefinition
		INNER JOIN tblContentType AS T1 ON T1.pkID=tblPropertyDefinition.fkContentTypeID
		INNER JOIN tblPropertyDefinitionType ON tblPropertyDefinitionType.pkID = tblPropertyDefinition.fkPropertyDefinitionTypeID
		INNER JOIN tblContentType ON tblPropertyDefinitionType.fkContentTypeGUID = tblContentType.ContentTypeGUID
		INNER JOIN ScopedProperties ON ScopedProperties.ContentTypeID = tblContentType.pkID
	)
	INSERT INTO @ScopedPropertiesTable(ScopeName) SELECT Scope from ScopedProperties
	RETURN 
END
GO
/****** Object:  StoredProcedure [dbo].[ItemFindByName]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
@Id = Id of object to search under
*/
CREATE PROCEDURE [dbo].[ItemFindByName]
(
	@Id	NVARCHAR(100) = NULL OUTPUT,
	@Name NVARCHAR(256)
)
AS
BEGIN
	DECLARE @ParentId NVARCHAR(256)
	SET @ParentId	= @Id
	SET @Id			= NULL
	IF (SUBSTRING(@Name, 1, 1) = '/')
	BEGIN
		SELECT
			@Id=pkID
		FROM tblItem
		WHERE [Name]=@Name
	END
	ELSE
	BEGIN
		SELECT 
			@Id=ToId
		FROM 
			tblRelation
		WHERE 
			[ToName]=@Name AND
			FromId=@ParentId
	END
END
GO
/****** Object:  StoredProcedure [dbo].[ItemDelete]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ItemDelete]
(
	@Id	NVARCHAR(256)
)
AS
BEGIN
	DELETE FROM tblRelation WHERE FromId=@Id OR ToId=@Id
	DELETE FROM tblItem WHERE pkID=@Id
END
GO
/****** Object:  StoredProcedure [dbo].[ItemSave]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ItemSave]
(
	@Id			NVARCHAR(100),
	@DbSchemaId	INT,
	@Name		NVARCHAR(256) = NULL,
	@ItemData	IMAGE = NULL
)
AS
BEGIN
	IF (SUBSTRING(@Name, 1, 1) = '/')
		IF (EXISTS(SELECT pkID FROM tblItem WHERE [Name]=@Name AND pkID<>@Id))
		BEGIN
			RAISERROR('Root item with name %s already exists.', 16, 1, @Name)
			RETURN
		END
	IF (EXISTS(SELECT pkID FROM tblItem WHERE pkID=@Id))
	BEGIN
		UPDATE tblItem SET
			[Name]=@Name,
			[fkSchemaId]=@DbSchemaId,
			ItemData=@ItemData
		WHERE
			pkID=@Id
		UPDATE tblRelation SET ToName=@Name WHERE ToId=@Id
		RETURN 0
	END
	INSERT INTO tblItem 
		(pkID,
		[Name],
		[fkSchemaId],
		ItemData) 
	VALUES 
		(@Id,
		@Name,
		@DbSchemaId,
		@ItemData)
	RETURN 1
END
GO
/****** Object:  StoredProcedure [dbo].[BigTableSaveReference]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BigTableSaveReference]
	@Id bigint,
	@Type int,
	@PropertyName nvarchar(75),
	@CollectionType nvarchar(2000) = NULL,
	@ElementType nvarchar(2000) = NULL,
	@ElementStoreName nvarchar(375) = null,
	@IsKey bit,
	@Index int,
	@BooleanValue bit = NULL,
	@IntegerValue int = NULL,
	@LongValue bigint = NULL,
	@DateTimeValue datetime = NULL,
	@GuidValue uniqueidentifier = NULL,
	@FloatValue float = NULL,	
	@StringValue nvarchar(max) = NULL,
	@BinaryValue varbinary(max) = NULL,
	@RefIdValue bigint = NULL,
	@ExternalIdValue bigint = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if not exists(select * from tblBigTableReference where pkId = @Id and PropertyName = @PropertyName and IsKey = @IsKey and [Index] = @Index)
	begin
		-- insert
		insert into tblBigTableReference
		values
		(
			@Id,
			@Type,
			@PropertyName,
			@CollectionType,
			@ElementType,
			@ElementStoreName,
			@IsKey,
			@Index,
			@BooleanValue,
			@IntegerValue,
			@LongValue,
			@DateTimeValue,
			@GuidValue,
			@FloatValue,
			@StringValue,
			@BinaryValue,
			@RefIdValue,
			@ExternalIdValue
		)
	end
	else
	begin
		-- update
		update tblBigTableReference
		set
		CollectionType = @CollectionType,
		ElementType = @ElementType,
		ElementStoreName  = @ElementStoreName,
		BooleanValue = @BooleanValue,
		IntegerValue = @IntegerValue,
		LongValue = @LongValue,
		DateTimeValue = @DateTimeValue,
		GuidValue = @GuidValue,
		FloatValue = @FloatValue,
		StringValue = @StringValue,
		BinaryValue = @BinaryValue,
		RefIdValue = @RefIdValue,
		ExternalIdValue = @ExternalIdValue
		where
		pkId = @Id and PropertyName = @PropertyName and IsKey = @IsKey and [Index] = @Index
	end   
END
GO
/****** Object:  StoredProcedure [dbo].[BigTableDeleteItemInternal]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BigTableDeleteItemInternal]
@forceDelete bit = 0
AS
BEGIN
	DECLARE @nestLevel int
	SET @nestLevel = 1
	WHILE @@ROWCOUNT > 0
	BEGIN
		SET @nestLevel = @nestLevel + 1
		-- insert all items contained in the ones matching the _previous_ nestlevel and give them _this_ nestLevel
		-- exclude those items that are also referred by some other item not already in #deletes
		-- IMPORTANT: Make sure that this insert is the last statement that can affect @@ROWCOUNT in the while-loop
		INSERT INTO #deletes(Id, NestLevel, ObjectPath)
		SELECT DISTINCT RefIdValue, @nestLevel, #deletes.ObjectPath + '/' + CAST(RefIdValue AS VARCHAR) + '/'
		FROM tblBigTableReference R1
		INNER JOIN #deletes ON #deletes.Id=R1.pkId
		WHERE #deletes.NestLevel=@nestLevel-1
		AND RefIdValue NOT IN(SELECT Id FROM #deletes)
	END 
	DELETE #deletes FROM #deletes
	INNER JOIN 
	(
		SELECT innerDelete.Id
		FROM #deletes as innerDelete
		INNER JOIN tblBigTableReference ON tblBigTableReference.RefIdValue=innerDelete.Id
		WHERE NOT EXISTS(SELECT * FROM #deletes WHERE #deletes.Id=tblBigTableReference.pkId)
	) ReferencedObjects ON #deletes.ObjectPath LIKE '%/' + CAST(ReferencedObjects.Id AS VARCHAR) + '/%'
	WHERE @forceDelete = 0 OR #deletes.NestLevel > 1
	BEGIN TRAN
		DELETE FROM tblBigTableReference WHERE RefIdValue in (SELECT Id FROM #deletes)
		DELETE FROM tblBigTableReference WHERE pkId in (SELECT Id FROM #deletes)
		-- Go through each big table and delete any rows associated with the item being deleted
		DECLARE @tableName NVARCHAR(128)
		DECLARE tableNameCursor CURSOR READ_ONLY 
		FOR SELECT DISTINCT TableName FROM tblBigTableStoreConfig WHERE TableName IS NOT NULL				
		OPEN tableNameCursor
		FETCH NEXT FROM tableNameCursor INTO @tableName
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC ('DELETE FROM ' + @tableName  +  ' WHERE pkId in (SELECT Id FROM #deletes)')
			FETCH NEXT FROM tableNameCursor INTO @tableName
		END
		CLOSE tableNameCursor
		DEALLOCATE tableNameCursor 			
		DELETE FROM tblBigTableIdentity WHERE pkId in (SELECT Id FROM #deletes)
	COMMIT TRAN
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_CreateRole]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Roles_CreateRole]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
        SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF (EXISTS(SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId))
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    INSERT INTO dbo.aspnet_Roles
                (ApplicationId, RoleName, LoweredRoleName)
         VALUES (@ApplicationId, @RoleName, LOWER(@RoleName))

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        COMMIT TRANSACTION
    END

    RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[BigTableDeleteExcessReferences]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BigTableDeleteExcessReferences]
	@Id bigint,
	@PropertyName nvarchar(75),
	@StartIndex int
AS
BEGIN
BEGIN TRAN
	IF @StartIndex > -1
	BEGIN
		-- Creates temporary store with id's of references that has no other reference
		CREATE TABLE #deletes(Id BIGINT, NestLevel INT, ObjectPath varchar(max))
		CREATE INDEX IX_Deletes_Id ON #deletes(Id)
		CREATE INDEX IX_Deletes_Id_NestLevel ON #deletes(Id, NestLevel)
		INSERT INTO #deletes(Id, NestLevel, ObjectPath)
		SELECT DISTINCT R1.RefIdValue, 1, '/' + CAST(R1.RefIdValue AS VARCHAR) + '/' FROM tblBigTableReference AS R1
		LEFT OUTER JOIN tblBigTableReference AS R2 ON R1.RefIdValue = R2.pkId
		WHERE R1.pkId = @Id AND R1.PropertyName = @PropertyName AND R1.[Index] >= @StartIndex AND 
				R1.RefIdValue IS NOT NULL AND R2.RefIdValue IS NULL
		-- Remove reference on main store
		DELETE FROM tblBigTableReference WHERE pkId = @Id and PropertyName = @PropertyName and [Index] >= @StartIndex
		IF((select count(*) from #deletes) > 0)
		BEGIN
			EXEC ('BigTableDeleteItemInternal')
		END
		DROP INDEX IX_Deletes_Id_NestLevel ON #deletes
		DROP INDEX IX_Deletes_Id ON #deletes
		DROP TABLE #deletes
	END
	ELSE
		-- Remove reference on main store
		DELETE FROM tblBigTableReference WHERE pkId = @Id and PropertyName = @PropertyName and [Index] >= @StartIndex
COMMIT TRAN
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_CreateUser]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Users_CreateUser]
    @ApplicationId    uniqueidentifier,
    @UserName         nvarchar(256),
    @IsUserAnonymous  bit,
    @LastActivityDate DATETIME,
    @UserId           uniqueidentifier OUTPUT
AS
BEGIN
    IF( @UserId IS NULL )
        SELECT @UserId = NEWID()
    ELSE
    BEGIN
        IF( EXISTS( SELECT UserId FROM dbo.aspnet_Users
                    WHERE @UserId = UserId ) )
            RETURN -1
    END

    INSERT dbo.aspnet_Users (ApplicationId, UserId, UserName, LoweredUserName, IsAnonymous, LastActivityDate)
    VALUES (@ApplicationId, @UserId, @UserName, LOWER(@UserName), @IsUserAnonymous, @LastActivityDate)

    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_RoleExists]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Roles_RoleExists]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(0)
    IF (EXISTS (SELECT RoleName FROM dbo.aspnet_Roles WHERE LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId ))
        RETURN(1)
    ELSE
        RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_GetAllRoles]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Roles_GetAllRoles] (
    @ApplicationName           nvarchar(256))
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN
    SELECT RoleName
    FROM   dbo.aspnet_Roles WHERE ApplicationId = @ApplicationId
    ORDER BY RoleName
END
GO
/****** Object:  Table [dbo].[aspnet_UsersInRoles]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aspnet_UsersInRoles](
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [aspnet_UsersInRoles_index] ON [dbo].[aspnet_UsersInRoles] 
(
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[netContentTypeSave]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netContentTypeSave]
(
	@ContentTypeID			INT					OUTPUT,
	@ContentTypeGUID		UNIQUEIDENTIFIER	OUTPUT,
	@Name				NVARCHAR(50),
	@DisplayName		NVARCHAR(50)    = NULL,
	@Description		NVARCHAR(255)	= NULL,
	@DefaultWebFormTemplate	NVARCHAR(1024)   = NULL,
	@DefaultMvcController NVARCHAR(1024)   = NULL,
	@DefaultMvcPartialView			NVARCHAR(255)   = NULL,
	@Filename			NVARCHAR(255)   = NULL,
	@Available			BIT				= NULL,
	@SortOrder			INT				= NULL,
	@ModelType			NVARCHAR(1024)	= NULL,
	@DefaultID			INT				= NULL,
	@DefaultName 		NVARCHAR(100)	= NULL,
	@StartPublishOffset	INT				= NULL,
	@StopPublishOffset	INT				= NULL,
	@VisibleInMenu		BIT				= NULL,
	@PeerOrder 			INT				= NULL,
	@ChildOrderRule 	INT				= NULL,
	@ArchiveContentID 		INT				= NULL,
	@FrameID 			INT				= NULL,
	@ACL				NVARCHAR(MAX)	= NULL,	
	@ContentType		INT				= 0
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @IdString NVARCHAR(255)
	IF @ContentTypeID <= 0
	BEGIN
		SET @ContentTypeID = ISNULL((SELECT pkId FROM tblContentType where Name = @Name), @ContentTypeID)
	END
	IF (@ContentTypeID <= 0)
	BEGIN
		SELECT TOP 1 @IdString=IdString FROM tblContentType
		INSERT INTO tblContentType
			(Name,
			DisplayName,
			DefaultMvcController,
			DefaultWebFormTemplate,
			DefaultMvcPartialView,
			Description,
			Available,
			SortOrder,
			ModelType,
			Filename,
			IdString,
			ContentTypeGUID,
			ACL,
			ContentType)
		VALUES
			(@Name,
			@DisplayName,
			@DefaultMvcController,
			@DefaultWebFormTemplate,
			@DefaultMvcPartialView,
			@Description,
			@Available,
			@SortOrder,
			@ModelType,
			@Filename,
			@IdString,
			CASE WHEN @ContentTypeGUID IS NULL THEN NewId() ELSE @ContentTypeGUID END,
			@ACL,
			@ContentType)
		SET @ContentTypeID= SCOPE_IDENTITY() 
	END
	ELSE
	BEGIN
		BEGIN
			UPDATE tblContentType
			SET
				Name=@Name,
				DisplayName=@DisplayName,
				Description=@Description,
				DefaultWebFormTemplate=@DefaultWebFormTemplate,
				DefaultMvcController=@DefaultMvcController,
				DefaultMvcPartialView=@DefaultMvcPartialView,
				Available=@Available,
				SortOrder=@SortOrder,
				ModelType = @ModelType,
				Filename = @Filename,
				ACL=@ACL,
				ContentType = @ContentType
			WHERE
				pkID=@ContentTypeID
		END
	END
	SELECT @ContentTypeGUID=ContentTypeGUID FROM tblContentType WHERE pkID=@ContentTypeID
	IF (@DefaultID IS NULL)
	BEGIN
		DELETE FROM tblContentTypeDefault WHERE fkContentTypeID=@ContentTypeID
		RETURN 0
	END
	IF (EXISTS (SELECT pkID FROM tblContentTypeDefault WHERE fkContentTypeID=@ContentTypeID))
	BEGIN
		UPDATE tblContentTypeDefault SET
			Name 				= @DefaultName,
			StartPublishOffset 	= @StartPublishOffset,
			StopPublishOffset 	= @StopPublishOffset,
			VisibleInMenu 		= @VisibleInMenu,
			PeerOrder 			= @PeerOrder,
			ChildOrderRule 		= @ChildOrderRule,
			fkArchiveContentID 	= @ArchiveContentID,
			fkFrameID 			= @FrameID
		WHERE fkContentTypeID=@ContentTypeID
	END
	ELSE
	BEGIN
		INSERT INTO tblContentTypeDefault 
			(fkContentTypeID,
			Name,
			StartPublishOffset,
			StopPublishOffset,
			VisibleInMenu,
			PeerOrder,
			ChildOrderRule,
			fkArchiveContentID,
			fkFrameID)
		VALUES
			(@ContentTypeID,
			@DefaultName,
			@StartPublishOffset,
			@StopPublishOffset,
			@VisibleInMenu,
			@PeerOrder,
			@ChildOrderRule,
			@ArchiveContentID,
			@FrameID)
	END
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[netContentTypeList]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netContentTypeList]
AS
BEGIN
	SET NOCOUNT ON
	SELECT	CT.pkID AS ID,
			CONVERT(NVARCHAR(38),CT.ContentTypeGUID) AS Guid,
			CT.Name,
			CT.DisplayName,
			CT.Description,
			CT.DefaultWebFormTemplate,
			CT.DefaultMvcController,
			CT.DefaultMvcPartialView,
			CT.Available,
			CT.SortOrder,
			CT.ModelType,
			CT.Filename,
			CT.ACL,
			CT.ContentType,
			CTD.pkID AS DefaultID,
			CTD.Name AS DefaultName,
			CTD.StartPublishOffset,
			CTD.StopPublishOffset,
			CONVERT(INT,CTD.VisibleInMenu) AS VisibleInMenu,
			CTD.PeerOrder,
			CTD.ChildOrderRule,
			CTD.fkFrameID AS FrameID,
			CTD.fkArchiveContentID AS ArchiveContentLink
	FROM tblContentType CT
	LEFT JOIN tblContentTypeDefault AS CTD ON CTD.fkContentTypeID=CT.pkID 
	ORDER BY CT.SortOrder
END
GO
/****** Object:  StoredProcedure [dbo].[netPageDefinitionDefault]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageDefinitionDefault]
(
	@PageDefinitionID	INT,
	@Boolean			BIT				= NULL,
	@Number				INT				= NULL,
	@FloatNumber		FLOAT			= NULL,
	@PageType			INT				= NULL,
	@PageReference		INT				= NULL,
	@Date				DATETIME		= NULL,
	@String				NVARCHAR(450)	= NULL,
	@LongString			NVARCHAR(MAX)	= NULL
)
AS
BEGIN
	SET NOCOUNT ON
	DELETE FROM tblPropertyDefault WHERE fkPageDefinitionID=@PageDefinitionID
	IF (@Boolean IS NULL AND @Number IS NULL AND @FloatNumber IS NULL AND @PageType IS NULL AND 
		@PageReference IS NULL AND @Date IS NULL AND @String IS NULL AND @LongString IS NULL)
		RETURN
	IF (@Boolean IS NULL)
		SET @Boolean=0
	INSERT INTO tblPropertyDefault 
		(fkPageDefinitionID, Boolean, Number, FloatNumber, PageType, PageLink, Date, String, LongString) 
	VALUES
		(@PageDefinitionID, @Boolean, @Number, @FloatNumber, @PageType, @PageReference, @Date, @String, @LongString)
	RETURN 
END
GO
/****** Object:  StoredProcedure [dbo].[netPageDefinitionTypeList]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageDefinitionTypeList]
AS
BEGIN
	SELECT 	DT.pkID AS ID,
			DT.Name,
			DT.Property,
			DT.TypeName,
			DT.AssemblyName, 
			DT.fkPageTypeGUID AS BlockTypeID,
			PT.Name as BlockTypeName,
			PT.ModelType as BlockTypeModel
	FROM tblPageDefinitionType as DT
		LEFT OUTER JOIN tblPageType as PT ON DT.fkPageTypeGUID = PT.PageTypeGUID
	ORDER BY DT.Name
END
GO
/****** Object:  StoredProcedure [dbo].[netUnifiedPathSearch]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netUnifiedPathSearch]
(
    @Path 		NVARCHAR(4000),
    @Param1 	NVARCHAR(255),
    @Value1 	NVARCHAR(255),
    @Param2 	NVARCHAR(255) = NULL,
    @Value2 	NVARCHAR(255) = NULL,
    @Param3 	NVARCHAR(255) = NULL,
    @Value3 	NVARCHAR(255) = NULL
)
AS
BEGIN
    SELECT DISTINCT 
        tblUnifiedPath.Path
    FROM 
        tblUnifiedPath
    WHERE 
        tblUnifiedPath.Path LIKE (@Path + '%') AND 
        (@Param1 IS NULL OR EXISTS(SELECT * FROM tblUnifiedPathProperty WHERE tblUnifiedPathProperty.fkUnifiedPathID=tblUnifiedPath.pkID AND (@Param1='*' OR tblUnifiedPathProperty.KeyName=@Param1) AND tblUnifiedPathProperty.StringValue LIKE( '%' + @Value1 + '%'))) AND
        (@Param2 IS NULL OR EXISTS(SELECT * FROM tblUnifiedPathProperty WHERE tblUnifiedPathProperty.fkUnifiedPathID=tblUnifiedPath.pkID AND (@Param2='*' OR tblUnifiedPathProperty.KeyName=@Param2) AND tblUnifiedPathProperty.StringValue LIKE ('%' + @Value2 + '%'))) AND 
        (@Param3 IS NULL OR EXISTS(SELECT * FROM tblUnifiedPathProperty WHERE tblUnifiedPathProperty.fkUnifiedPathID=tblUnifiedPath.pkID AND (@Param3='*' OR tblUnifiedPathProperty.KeyName=@Param3) AND tblUnifiedPathProperty.StringValue LIKE ('%' + @Value3 + '%')))
END
GO
/****** Object:  StoredProcedure [dbo].[netUnifiedPathSavePropEntry]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netUnifiedPathSavePropEntry]
( 
    @id int, 
    @key nvarchar(255), 
    @value nvarchar(2000) 
)
AS
BEGIN
	IF EXISTS(SELECT pkID FROM tblUnifiedPathProperty 
		WHERE fkUnifiedPathID = @id 
		AND LOWER(KeyName) = LOWER(@key))
		BEGIN
			UPDATE tblUnifiedPathProperty
			SET StringValue = @value
			WHERE fkUnifiedPathID = @id
			AND LOWER(KeyName) = LOWER(@key)  
		END
	ELSE
		BEGIN
			INSERT INTO tblUnifiedPathProperty(fkUnifiedPathID, KeyName, StringValue)
			VALUES( @id, @key, @value ) 
		END
END
GO
/****** Object:  StoredProcedure [dbo].[netUnifiedPathSaveAclEntry]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netUnifiedPathSaveAclEntry]
( 
    @id int, 
    @name nvarchar(255), 
    @isRole int, 
    @accessMask int
)
AS
BEGIN
	IF EXISTS(SELECT pkID FROM tblUnifiedPathAcl 
		WHERE fkUnifiedPathID = @id 
		AND LOWER([Name]) = LOWER(@name) 
		AND IsRole = @isRole)
		BEGIN
			UPDATE tblUnifiedPathAcl
			SET AccessMask = @accessMask
			WHERE IsRole = @isRole
			AND fkUnifiedPathID = @id
			AND LOWER([Name]) = LOWER(@name)  
		END
	ELSE
		BEGIN
			INSERT INTO tblUnifiedPathAcl(fkUnifiedPathID, [Name], IsRole, AccessMask)
			VALUES( @id, @name, @isRole, @accessMask ) 
		END
END
GO
/****** Object:  StoredProcedure [dbo].[netWinMembershipGroupList]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netWinMembershipGroupList] 
(
    @UserID INT OUT,
	@UserName NVARCHAR(255)
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	DECLARE @LoweredName NVARCHAR(255)
	/* Get the id for the user name */
	SET @LoweredName=LOWER(@UserName)
	SELECT 
		@UserID=pkID 
	FROM
		tblWindowsUser
	WHERE
		LoweredUserName=@LoweredName
    /* Get Group name and id */
    SELECT
        GroupName,
        fkWindowsGroup AS GroupID
    FROM
        tblWindowsRelations AS WR
    INNER JOIN
        tblWindowsGroup AS WG
    ON
        WR.fkWindowsGroup=WG.pkID
    WHERE
        WR.fkWindowsUser=@UserID
    ORDER BY
        GroupName
END
GO
/****** Object:  StoredProcedure [dbo].[netWinMembershipGroupInsert]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netWinMembershipGroupInsert] 
(
	@UserID INT OUT,
	@UserName NVARCHAR(255),
	@GroupName NVARCHAR(255)
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	DECLARE @GroupID INT
	DECLARE @LoweredName NVARCHAR(255)
	IF (@UserID IS NULL)
	BEGIN
		/* Get the id for the user name */
		SET @LoweredName=LOWER(@UserName)
		INSERT INTO tblWindowsUser 
			(UserName, 
			LoweredUserName) 
		SELECT 
			@UserName, 
			@LoweredName
		WHERE NOT EXISTS (SELECT pkID FROM tblWindowsUser WHERE LoweredUserName=@LoweredName)
		IF (@@ROWCOUNT > 0)
		BEGIN
		    SET @UserID= SCOPE_IDENTITY() 
		END
		ELSE
		BEGIN
		    SELECT @UserID=pkID FROM tblWindowsUser WHERE LoweredUserName=@LoweredName
		END
	END
	/* We have a valid UserID, now get the group id */
	EXEC @GroupID=netWinRolesGroupInsert @GroupName=@GroupName
	IF (@GroupID IS NULL)
	BEGIN
		RAISERROR(50001, 14, 1)
	END
	/* We now have a valid role id and user id, add to relations table */
	INSERT INTO tblWindowsRelations
		(fkWindowsUser,
		fkWindowsGroup)
	SELECT 
		@UserID,
		@GroupID
	WHERE NOT EXISTS (SELECT fkWindowsUser FROM tblWindowsRelations WHERE fkWindowsUser=@UserID AND fkWindowsGroup=@GroupID)
END
GO
/****** Object:  StoredProcedure [dbo].[netWinMembershipGroupDelete]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netWinMembershipGroupDelete] 
(
	@UserID INT OUT,
	@UserName NVARCHAR(255),
	@GroupName NVARCHAR(255)
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT OFF
	DECLARE @ErrorVal INT
	DECLARE @GroupID INT
	DECLARE @LoweredName NVARCHAR(255)
	IF (@UserID IS NULL)
	BEGIN
		/* Get the id for the user name */
		SET @LoweredName=LOWER(@UserName)
		SELECT 
			@UserID=pkID 
		FROM
			tblWindowsUser
		WHERE
			LoweredUserName=@LoweredName
		/* User does not exist, nothing to do */
		IF (@UserID IS NULL)
		BEGIN
		    RETURN
		END
	END
	/* We have a valid UserID, now get the group id */
	SET @LoweredName=LOWER(@GroupName)
	SET @GroupID=NULL
	SELECT
		@GroupID = pkID
	FROM
		tblWindowsGroup
	WHERE
		LoweredGroupName=@LoweredName
	/* Group does not exist, nothing to do */
	IF (@GroupID IS NULL)
	BEGIN
        RETURN
    END
	/* We now have a valid role id and user id, remove from relations table */
	DELETE FROM 
	    tblWindowsRelations
	WHERE
		fkWindowsUser=@UserID AND
		fkWindowsGroup=@GroupID
END
GO
/****** Object:  StoredProcedure [dbo].[netUnifiedPathLoad]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netUnifiedPathLoad]
( 
    @virtualPath nvarchar(4000) 
)
AS
BEGIN
	SELECT up.pkID, up.InheritAcl, up.[Path], upa.[Name], upa.IsRole, upa.AccessMask, upp.KeyName, upp.StringValue
	FROM tblUnifiedPath up
		LEFT OUTER JOIN tblUnifiedPathAcl upa ON upa.fkUnifiedPathID = up.pkID
		LEFT OUTER JOIN tblUnifiedPathProperty upp ON upp.fkUnifiedPathID = up.pkID
	WHERE LOWER(up.[Path]) = LOWER(@virtualPath)	
END
GO
/****** Object:  StoredProcedure [dbo].[netUnifiedPathDeleteMembership]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netUnifiedPathDeleteMembership]
(
	@UserName NVARCHAR(255) = NULL,
	@IsRole INT = NULL
)
AS
BEGIN
	IF(@UserName IS NOT NULL AND @IsRole IS NOT NULL)
	BEGIN
		DELETE FROM 
		    tblUnifiedPathAcl 
		WHERE 
		    Name=@UserName AND
		    IsRole=@IsRole
	END
END
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[netUnifiedPathDeleteAll]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netUnifiedPathDeleteAll]
AS
BEGIN
	DELETE FROM tblUnifiedPathAcl
	WHERE fkUnifiedPathID IN (SELECT pkID FROM fkUnifiedPath)
	DELETE FROM tblUnifiedPathProperty
	WHERE fkUnifiedPathID IN (SELECT pkID FROM fkUnifiedPath)
	DELETE FROM tblUnifiedPath
END
GO
/****** Object:  StoredProcedure [dbo].[netUnifiedPathDeleteAclAndMeta]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netUnifiedPathDeleteAclAndMeta]
( 
    @id int 
)
AS
BEGIN
	DELETE FROM tblUnifiedPathAcl
	WHERE fkUnifiedPathID = @id
	DELETE FROM tblUnifiedPathProperty
	WHERE fkUnifiedPathID = @id
	DELETE FROM tblUnifiedPath
	WHERE pkID = @id
END
GO
/****** Object:  StoredProcedure [dbo].[netUnifiedPathDelete]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netUnifiedPathDelete]
(
	@UnifiedPathID INT,
	@UserName NVARCHAR(255) = NULL,
	@IsRole INT = NULL
)
AS
BEGIN
	IF (@UserName IS NULL)
    BEGIN
		DELETE FROM 
		    tblUnifiedPathAcl 
		WHERE 
		    fkUnifiedPathID=@UnifiedPathID
		DELETE FROM 
		    tblUnifiedPathProperty 
		WHERE 
		    fkUnifiedPathID=@UnifiedPathID
		DELETE FROM 
		    tblUnifiedPath 
		WHERE 
		    pkID=@UnifiedPathID
	END 
	ELSE
	BEGIN
		DELETE FROM 
		    tblUnifiedPathAcl 
		WHERE 
		    fkUnifiedPathID=@UnifiedPathID AND 
		    Name=@UserName AND
		    IsRole=@IsRole
	END
END
GO
/****** Object:  StoredProcedure [dbo].[netTaskWorkflowList]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netTaskWorkflowList]
(
	@WorkflowInstanceId NVARCHAR(36)
)
AS
BEGIN
	SET NOCOUNT ON
	SELECT
	    pkID AS TaskID,
	    COALESCE(Subject, N'-') AS Subject,
	    [Description],
	    DueDate,
	    Status,
	    Activity,
	    Created,
	    Changed,
	    OwnerName,
	    AssignedToName,
	    AssignedIsRole,
	    State,
	    fkPlugInID,
        WorkflowInstanceId,
	    EventActivityName
	FROM 
	    tblTask
	WHERE
		WorkflowInstanceId=@WorkflowInstanceId
	ORDER BY 
	    Status ASC,
	    DueDate DESC, 
	    Changed DESC
END
GO
/****** Object:  StoredProcedure [dbo].[netTaskSaveActivity]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netTaskSaveActivity]
(
    @TaskID INT,
    @Activity NVARCHAR(MAX) = NULL
)
AS
BEGIN
	UPDATE 
	    tblTask 
	SET
		Activity = @Activity
	WHERE 
	    pkID = @TaskID
END
GO
/****** Object:  StoredProcedure [dbo].[netTaskSave]    Script Date: 12/06/2012 12:39:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netTaskSave]
(
    @TaskID INT OUTPUT,
    @Subject NVARCHAR(255),
    @Description NVARCHAR(2000) = NULL,
    @DueDate DATETIME = NULL,
    @OwnerName NVARCHAR(255),
    @AssignedToName NVARCHAR(255),
    @AssignedIsRole BIT,
    @Status INT,
    @PlugInID INT = NULL,
    @Activity NVARCHAR(MAX) = NULL,
    @State NVARCHAR(MAX) = NULL,
    @WorkflowInstanceId NVARCHAR(36) = NULL,
    @EventActivityName NVARCHAR(255) = NULL
)
AS
BEGIN
    -- Create new task
	IF @TaskID = 0
	BEGIN
		INSERT INTO tblTask
		    (Subject,
		    Description,
		    DueDate,
		    OwnerName,
		    AssignedToName,
		    AssignedIsRole,
		    Status,
		    Activity,
		    fkPlugInID,
		    State,
		    WorkflowInstanceId,
		    EventActivityName) 
		VALUES
		    (@Subject,
		    @Description,
		    @DueDate,
		    @OwnerName,
		    @AssignedToName,
		    @AssignedIsRole,
		    @Status,
		    @Activity,
		    @PlugInID,
		    @State,
		    @WorkflowInstanceId,
	            @EventActivityName)
		SET @TaskID= SCOPE_IDENTITY() 
		RETURN
	END
    -- Update existing task
	UPDATE tblTask SET
		Subject = @Subject,
		Description = @Description,
		DueDate = @DueDate,
		OwnerName = @OwnerName,
		AssignedToName = @AssignedToName,
		AssignedIsRole = @AssignedIsRole,
		Status = @Status,
		Activity = CASE WHEN @Activity IS NULL THEN Activity ELSE @Activity END,
		State = @State,
		fkPlugInID = @PlugInID,
		WorkflowInstanceId = @WorkflowInstanceId,
		EventActivityName = @EventActivityName
	WHERE pkID = @TaskID
END
GO
/****** Object:  StoredProcedure [dbo].[netTaskLoad]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netTaskLoad]
(
	@TaskID INT
)
AS
BEGIN
	SET NOCOUNT ON
	SELECT
	    pkID AS TaskID,
	    COALESCE(Subject, N'-') AS Subject,
	    [Description],
	    DueDate,
	    Status,
	    Activity,
	    Created,
	    Changed,
	    OwnerName,
	    AssignedToName,
	    AssignedIsRole,
	    State,
	    fkPlugInID,
 	    WorkflowInstanceId,
	    EventActivityName
	FROM 
	    tblTask
	WHERE 
	    pkID=@TaskID
END
GO
/****** Object:  StoredProcedure [dbo].[netTaskList]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netTaskList]
(
	@UserName NVARCHAR(255) = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	SELECT
	    pkID AS TaskID,
	    COALESCE(Subject, N'-') AS Subject,
	    [Description],
	    DueDate,
	    Status,
	    Activity,
	    Created,
	    Changed,
	    OwnerName,
	    AssignedToName,
	    AssignedIsRole,
	    State,
	    fkPlugInID,
            WorkflowInstanceId,
	    EventActivityName
	FROM 
	    tblTask
	WHERE
	    OwnerName=@UserName OR
	    AssignedToName=@UserName OR
	    AssignedIsRole=1 OR
	    @UserName IS NULL
	ORDER BY 
	    Status ASC,
	    DueDate DESC, 
	    Changed DESC
END
GO
/****** Object:  StoredProcedure [dbo].[netTaskDelete]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netTaskDelete]
(
	@TaskID INT
)
AS
BEGIN
	SET NOCOUNT ON
	DELETE FROM 
	    tblTask 
	WHERE 
	    pkID=@TaskID
	RETURN @@ROWCOUNT   
END
GO
/****** Object:  StoredProcedure [dbo].[netTabInfoUpdate]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netTabInfoUpdate]
(
	@pkID int,
	@Name nvarchar(100),
	@DisplayName nvarchar(100),
	@GroupOrder int,
	@Access int
)
AS
BEGIN
	UPDATE tblPageDefinitionGroup	SET 
		Name = @Name,
		DisplayName = @DisplayName,
		GroupOrder = @GroupOrder,
		Access = @Access
	WHERE pkID = @pkID
END
GO
/****** Object:  StoredProcedure [dbo].[netTabInfoList]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netTabInfoList] AS
BEGIN
	SELECT 	pkID as TabID, 
			Name,
			DisplayName,
			GroupOrder,
			Access AS AccessMask,
			CONVERT(INT,SystemGroup) AS SystemGroup
	FROM tblPageDefinitionGroup 
	ORDER BY GroupOrder
END
GO
/****** Object:  StoredProcedure [dbo].[netTabInfoInsert]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netTabInfoInsert]
(
	@pkID INT OUTPUT,
	@Name NVARCHAR(100),
	@DisplayName NVARCHAR(100),
	@GroupOrder INT,
	@Access INT
)
AS
BEGIN
	INSERT INTO tblPageDefinitionGroup (Name, DisplayName, GroupOrder, Access)
	VALUES (@Name, @DisplayName, @GroupOrder, @Access)
	SET @pkID =  SCOPE_IDENTITY() 
END
GO
/****** Object:  StoredProcedure [dbo].[netSchedulerReport]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSchedulerReport]
@ScheduledItemId	UNIQUEIDENTIFIER,
@Status INT,
@Text	NVARCHAR(2048) = null,
@utcnow DATETIME,
@MaxHistoryCount	INT = NULL
AS
BEGIN
	UPDATE tblScheduledItem SET LastExec = @utcnow,
								LastStatus = @Status,
								LastText = @Text
	FROM tblScheduledItem
	WHERE pkID = @ScheduledItemId
	INSERT INTO tblScheduledItemLog( fkScheduledItemId, [Exec], Status, [Text] ) VALUES(@ScheduledItemId,@utcnow,@Status,@Text)
	WHILE (SELECT COUNT(pkID) FROM tblScheduledItemLog WHERE fkScheduledItemId = @ScheduledItemId) > @MaxHistoryCount
	BEGIN
		DELETE tblScheduledItemLog FROM (SELECT TOP 1 * FROM tblScheduledItemLog WHERE fkScheduledItemId = @ScheduledItemId ORDER BY tblScheduledItemLog.pkID) AS T1
		WHERE tblScheduledItemLog.pkID = T1.pkID
	END	
END
GO
/****** Object:  StoredProcedure [dbo].[netSchedulerRemove]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[netSchedulerRemove]
@ID	uniqueidentifier
as
begin
	set nocount on
	delete from tblScheduledItemLog where fkScheduledItemId = @ID
	delete from tblScheduledItem where pkID = @ID
	return
end
GO
/****** Object:  StoredProcedure [dbo].[SchemaItemSave]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SchemaItemSave]
(
	@SchemaId	NVARCHAR(256),
	@FieldName	NVARCHAR(256),
	@FieldType	NVARCHAR(256),
	@Indexed	INT
)
AS
BEGIN
	DECLARE @fkSchemaId INT
	DECLARE @SchemaItemId INT
	SELECT @fkSchemaId=pkID FROM tblSchema WHERE [SchemaId]=@SchemaId
	IF (@@ROWCOUNT = 0)
		RAISERROR('Schema %s does not exist.', 16, 1, @SchemaId)
	SELECT @SchemaItemId=pkID FROM tblSchemaItem WHERE fkSchemaId=@fkSchemaId AND FieldName=@FieldName
	IF (@@ROWCOUNT = 0)
	BEGIN
		INSERT INTO tblSchemaItem 
			(fkSchemaId,
			FieldName,
			FieldType,
			Indexed)
		VALUES
			(@fkSchemaId,
			@FieldName,
			@FieldType,
			@Indexed)
		SET @SchemaItemId = SCOPE_IDENTITY() 
	END
	ELSE
	BEGIN
		UPDATE tblSchemaItem SET
			FieldType=@FieldType,
			Indexed=@Indexed
		WHERE
			pkID=@SchemaItemId
	END
	RETURN @SchemaItemId
END
GO
/****** Object:  StoredProcedure [dbo].[netSchedulerListLog]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSchedulerListLog]
(
	@pkID UNIQUEIDENTIFIER
)
AS
BEGIN
	SELECT TOP 100 [Exec], Status, [Text]
	FROM tblScheduledItemLog
	WHERE fkScheduledItemId=@pkID
	ORDER BY [Exec] DESC
END
GO
/****** Object:  StoredProcedure [dbo].[netWinRolesGroupDelete]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netWinRolesGroupDelete]
(
	@GroupName NVARCHAR(255),
	@ForceDelete INT
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	DECLARE @GroupID INT
	DECLARE @LoweredName NVARCHAR(255)
    /* Check if group exists */
	SET @LoweredName=LOWER(@GroupName)
	SET @GroupID=NULL
	SELECT
		@GroupID = pkID
	FROM
		tblWindowsGroup
	WHERE
		LoweredGroupName=@LoweredName
	/* Group does not exist - do nothing */	
    IF (@GroupID IS NULL)
    BEGIN
        RETURN 0
    END
    IF (@ForceDelete = 0)
    BEGIN
        IF (EXISTS(SELECT fkWindowsGroup FROM tblWindowsRelations WHERE fkWindowsGroup=@GroupID))
        BEGIN
            RETURN 1    /* Indicate failure - no force delete and group is populated */
        END
    END
    DELETE FROM
        tblWindowsRelations
    WHERE
        fkWindowsGroup=@GroupID
    DELETE FROM
        tblWindowsGroup
    WHERE
        pkID=@GroupID
    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[RelationRemove]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RelationRemove]
(
	@FromId	NVARCHAR(100),
	@ToId NVARCHAR(100)
)
AS
BEGIN
	DELETE FROM tblRelation WHERE FromId=@FromId AND ToId=@ToId
END
GO
/****** Object:  StoredProcedure [dbo].[RelationListTo]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RelationListTo]
(
	@ToId	NVARCHAR(100),
	@SchemaId	INT = 0
)
AS
BEGIN
	SELECT FromId AS Id
	FROM tblRelation
	JOIN tblItem ON tblRelation.FromId = tblItem.pkID
	WHERE tblRelation.ToId=@ToId
	AND ( tblItem.fkSchemaId = @SchemaId OR @SchemaId = 0)
END
GO
/****** Object:  StoredProcedure [dbo].[RelationListFrom]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RelationListFrom]
(
	@FromId		NVARCHAR(100),
	@SchemaId	INT = 0
)
AS
BEGIN
	SELECT ToId AS Id
	FROM tblRelation
	JOIN tblItem ON tblRelation.ToId = tblItem.pkID
	WHERE tblRelation.FromId=@FromId
	AND ( tblItem.fkSchemaId = @SchemaId OR @SchemaId = 0)
END
GO
/****** Object:  StoredProcedure [dbo].[RelationAdd]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[RelationAdd]
(
	@FromId	NVARCHAR(100),
	@ToId NVARCHAR(100)
)
AS
BEGIN
	DECLARE @Name NVARCHAR(256)
	SELECT @Name=[Name] FROM tblItem WHERE pkID=@ToId
	IF (@Name IS NULL)
	BEGIN
		INSERT INTO tblRelation (FromId, ToId) VALUES (@FromId,@ToId)
		RETURN
	END
	IF (EXISTS(SELECT * FROM tblRelation WHERE ToName=@Name AND FromId=@FromId))
	BEGIN
		RAISERROR('Item with name %s already exists on same level.', 16, 1, @Name)
		RETURN
	END
	INSERT INTO tblRelation (FromId, ToId,ToName) VALUES (@FromId,@ToId,@Name)
END
GO
/****** Object:  StoredProcedure [dbo].[netWinRolesUserList]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netWinRolesUserList] 
(
	@GroupName NVARCHAR(255),
	@UserNameToMatch NVARCHAR(255) = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	DECLARE @GroupID INT
	SELECT 
	    @GroupID=pkID
	FROM
	    tblWindowsGroup
	WHERE
	    LoweredGroupName=LOWER(@GroupName)
	IF (@GroupID IS NULL)
	BEGIN
	    RETURN -1   /* Role does not exist */
	END
	SELECT
	    UserName
	FROM
	    tblWindowsRelations AS WR
	INNER JOIN
	    tblWindowsUser AS WU
	ON
	    WU.pkID=WR.fkWindowsUser
	WHERE
	    WR.fkWindowsGroup=@GroupID AND
	    ((WU.LoweredUserName LIKE LOWER(@UserNameToMatch)) OR (@UserNameToMatch IS NULL))
END
GO
/****** Object:  StoredProcedure [dbo].[Save_XFormFolders]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_XFormFolders]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048),
@ParentFolderId uniqueidentifier = NULL,
@SubPath nvarchar(max) = NULL
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'XFormFolders')

		select @Id = SCOPE_IDENTITY()
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,Guid01,String01)
		values(@Id, 'XFormFolders', 1, @ItemType ,@ParentFolderId,@SubPath)

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblSystemBigTable] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,Guid01,String01)
		values(@Id, 'XFormFolders', 1, @ItemType ,@ParentFolderId,@SubPath)

		end
		else begin
				update [tblSystemBigTable] set 
		ItemType = @ItemType,
		Guid01 = @ParentFolderId,
		String01 = @SubPath
                from [tblSystemBigTable]
                where pkId=@pkId
                and Row=1
		end
		commit transaction
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Save_VisitorGroup]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_VisitorGroup]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048),
@CriteriaOperator int = NULL,
@EnableStatistics bit = NULL,
@IsSecurityRole bit = NULL,
@Name nvarchar(max) = NULL,
@Notes nvarchar(max) = NULL,
@PointsThreshold int = NULL
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'VisitorGroup')

		select @Id = SCOPE_IDENTITY()
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,Integer01,Boolean01,Boolean02,String01,String02,Integer02)
		values(@Id, 'VisitorGroup', 1, @ItemType ,@CriteriaOperator,@EnableStatistics,@IsSecurityRole,@Name,@Notes,@PointsThreshold)

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblSystemBigTable] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,Integer01,Boolean01,Boolean02,String01,String02,Integer02)
		values(@Id, 'VisitorGroup', 1, @ItemType ,@CriteriaOperator,@EnableStatistics,@IsSecurityRole,@Name,@Notes,@PointsThreshold)

		end
		else begin
				update [tblSystemBigTable] set 
		ItemType = @ItemType,
		Integer01 = @CriteriaOperator,
		Boolean01 = @EnableStatistics,
		Boolean02 = @IsSecurityRole,
		String01 = @Name,
		String02 = @Notes,
		Integer02 = @PointsThreshold
                from [tblSystemBigTable]
                where pkId=@pkId
                and Row=1
		end
		commit transaction
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Save_System.Object]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_System.Object]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048)
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'System.Object')

		select @Id = SCOPE_IDENTITY()
				insert into [tblBigTable] (pkId, StoreName, Row, ItemType)
		values(@Id, 'System.Object', 1, @ItemType )

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblBigTable] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblBigTable] (pkId, StoreName, Row, ItemType)
		values(@Id, 'System.Object', 1, @ItemType )

		end
		commit transaction
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Save_QuickLinkSettings]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_QuickLinkSettings]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048),
@GadgetId uniqueidentifier = NULL
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'QuickLinkSettings')

		select @Id = SCOPE_IDENTITY()
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,Guid01)
		values(@Id, 'QuickLinkSettings', 1, @ItemType ,@GadgetId)

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblSystemBigTable] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,Guid01)
		values(@Id, 'QuickLinkSettings', 1, @ItemType ,@GadgetId)

		end
		else begin
				update [tblSystemBigTable] set 
		ItemType = @ItemType,
		Guid01 = @GadgetId
                from [tblSystemBigTable]
                where pkId=@pkId
                and Row=1
		end
		commit transaction
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Save_InUseNotifications]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_InUseNotifications]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048),
@AddedManually bit = NULL,
@ContentGuid uniqueidentifier = NULL,
@CreateTime datetime = NULL,
@LanguageBranch nvarchar(450) = NULL,
@Modified datetime = NULL,
@User nvarchar(450) = NULL
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'InUseNotifications')

		select @Id = SCOPE_IDENTITY()
				insert into [tblBigTable] (pkId, StoreName, Row, ItemType,Boolean01,Indexed_Guid01,DateTime01,Indexed_String01,DateTime02,Indexed_String02)
		values(@Id, 'InUseNotifications', 1, @ItemType ,@AddedManually,@ContentGuid,@CreateTime,@LanguageBranch,@Modified,@User)

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblBigTable] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblBigTable] (pkId, StoreName, Row, ItemType,Boolean01,Indexed_Guid01,DateTime01,Indexed_String01,DateTime02,Indexed_String02)
		values(@Id, 'InUseNotifications', 1, @ItemType ,@AddedManually,@ContentGuid,@CreateTime,@LanguageBranch,@Modified,@User)

		end
		else begin
				update [tblBigTable] set 
		ItemType = @ItemType,
		Boolean01 = @AddedManually,
		Indexed_Guid01 = @ContentGuid,
		DateTime01 = @CreateTime,
		Indexed_String01 = @LanguageBranch,
		DateTime02 = @Modified,
		Indexed_String02 = @User
                from [tblBigTable]
                where pkId=@pkId
                and Row=1
		end
		commit transaction
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Save_IndexRequestQueueDataStore]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_IndexRequestQueueDataStore]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048),
@IndexItemId nvarchar(max) = NULL,
@NamedIndex nvarchar(450) = NULL,
@NamedIndexingService nvarchar(450) = NULL,
@SyndicationItemXml nvarchar(max) = NULL,
@Timestamp datetime = NULL
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'IndexRequestQueueDataStore')

		select @Id = SCOPE_IDENTITY()
				insert into [tblIndexRequestLog] (pkId, StoreName, Row, ItemType,String01,Indexed_String01,String02,Indexed_DateTime01)
		values(@Id, 'IndexRequestQueueDataStore', 1, @ItemType ,@IndexItemId,@NamedIndex,@SyndicationItemXml,@Timestamp)

				insert into [tblIndexRequestLog] (pkId, StoreName, Row, ItemType,Indexed_String01)
		values(@Id, 'IndexRequestQueueDataStore', 2, @ItemType ,@NamedIndexingService)

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblIndexRequestLog] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblIndexRequestLog] (pkId, StoreName, Row, ItemType,String01,Indexed_String01,String02,Indexed_DateTime01)
		values(@Id, 'IndexRequestQueueDataStore', 1, @ItemType ,@IndexItemId,@NamedIndex,@SyndicationItemXml,@Timestamp)

		end
		else begin
				update [tblIndexRequestLog] set 
		ItemType = @ItemType,
		String01 = @IndexItemId,
		Indexed_String01 = @NamedIndex,
		String02 = @SyndicationItemXml,
		Indexed_DateTime01 = @Timestamp
                from [tblIndexRequestLog]
                where pkId=@pkId
                and Row=1
		end
		if(@rows < 2) begin
				insert into [tblIndexRequestLog] (pkId, StoreName, Row, ItemType,Indexed_String01)
		values(@Id, 'IndexRequestQueueDataStore', 2, @ItemType ,@NamedIndexingService)

		end
		else begin
				update [tblIndexRequestLog] set 
		ItemType = @ItemType,
		Indexed_String01 = @NamedIndexingService
                from [tblIndexRequestLog]
                where pkId=@pkId
                and Row=2
		end
		commit transaction
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Save_EPiServer.Shell.Storage.PersonalizedViewSettingsStorage]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_EPiServer.Shell.Storage.PersonalizedViewSettingsStorage]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048),
@UserName nvarchar(450) = NULL,
@ViewName nvarchar(450) = NULL
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'EPiServer.Shell.Storage.PersonalizedViewSettingsStorage')

		select @Id = SCOPE_IDENTITY()
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,Indexed_String01,Indexed_String02)
		values(@Id, 'EPiServer.Shell.Storage.PersonalizedViewSettingsStorage', 1, @ItemType ,@UserName,@ViewName)

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblSystemBigTable] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,Indexed_String01,Indexed_String02)
		values(@Id, 'EPiServer.Shell.Storage.PersonalizedViewSettingsStorage', 1, @ItemType ,@UserName,@ViewName)

		end
		else begin
				update [tblSystemBigTable] set 
		ItemType = @ItemType,
		Indexed_String01 = @UserName,
		Indexed_String02 = @ViewName
                from [tblSystemBigTable]
                where pkId=@pkId
                and Row=1
		end
		commit transaction
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Save_EPiServer.Shell.Storage.ContainerData]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_EPiServer.Shell.Storage.ContainerData]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048),
@DefinitionName nvarchar(max) = NULL,
@PlugInArea nvarchar(max) = NULL
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'EPiServer.Shell.Storage.ContainerData')

		select @Id = SCOPE_IDENTITY()
				insert into [tblBigTable] (pkId, StoreName, Row, ItemType,String01,String02)
		values(@Id, 'EPiServer.Shell.Storage.ContainerData', 1, @ItemType ,@DefinitionName,@PlugInArea)

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblBigTable] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblBigTable] (pkId, StoreName, Row, ItemType,String01,String02)
		values(@Id, 'EPiServer.Shell.Storage.ContainerData', 1, @ItemType ,@DefinitionName,@PlugInArea)

		end
		else begin
				update [tblBigTable] set 
		ItemType = @ItemType,
		String01 = @DefinitionName,
		String02 = @PlugInArea
                from [tblBigTable]
                where pkId=@pkId
                and Row=1
		end
		commit transaction
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Save_EPiServer.Shell.Storage.ComponentData]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_EPiServer.Shell.Storage.ComponentData]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048),
@DefinitionName nvarchar(max) = NULL
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'EPiServer.Shell.Storage.ComponentData')

		select @Id = SCOPE_IDENTITY()
				insert into [tblBigTable] (pkId, StoreName, Row, ItemType,String01)
		values(@Id, 'EPiServer.Shell.Storage.ComponentData', 1, @ItemType ,@DefinitionName)

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblBigTable] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblBigTable] (pkId, StoreName, Row, ItemType,String01)
		values(@Id, 'EPiServer.Shell.Storage.ComponentData', 1, @ItemType ,@DefinitionName)

		end
		else begin
				update [tblBigTable] set 
		ItemType = @ItemType,
		String01 = @DefinitionName
                from [tblBigTable]
                where pkId=@pkId
                and Row=1
		end
		commit transaction
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Save_EPiServer.Shell.Search.SearchProviderSetting]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_EPiServer.Shell.Search.SearchProviderSetting]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048),
@FullName nvarchar(max) = NULL,
@IsEnabled bit = NULL,
@SortIndex int = NULL
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'EPiServer.Shell.Search.SearchProviderSetting')

		select @Id = SCOPE_IDENTITY()
				insert into [tblBigTable] (pkId, StoreName, Row, ItemType,String01,Boolean01,Integer01)
		values(@Id, 'EPiServer.Shell.Search.SearchProviderSetting', 1, @ItemType ,@FullName,@IsEnabled,@SortIndex)

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblBigTable] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblBigTable] (pkId, StoreName, Row, ItemType,String01,Boolean01,Integer01)
		values(@Id, 'EPiServer.Shell.Search.SearchProviderSetting', 1, @ItemType ,@FullName,@IsEnabled,@SortIndex)

		end
		else begin
				update [tblBigTable] set 
		ItemType = @ItemType,
		String01 = @FullName,
		Boolean01 = @IsEnabled,
		Integer01 = @SortIndex
                from [tblBigTable]
                where pkId=@pkId
                and Row=1
		end
		commit transaction
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Save_EPiServer.Shell.Profile.ProfileData]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_EPiServer.Shell.Profile.ProfileData]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048),
@UserName nvarchar(max) = NULL
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'EPiServer.Shell.Profile.ProfileData')

		select @Id = SCOPE_IDENTITY()
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,String01)
		values(@Id, 'EPiServer.Shell.Profile.ProfileData', 1, @ItemType ,@UserName)

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblSystemBigTable] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,String01)
		values(@Id, 'EPiServer.Shell.Profile.ProfileData', 1, @ItemType ,@UserName)

		end
		else begin
				update [tblSystemBigTable] set 
		ItemType = @ItemType,
		String01 = @UserName
                from [tblSystemBigTable]
                where pkId=@pkId
                and Row=1
		end
		commit transaction
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Save_EPiServer.Personalization.VisitorGroups.Criteria.ViewedCategoriesModel]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_EPiServer.Personalization.VisitorGroups.Criteria.ViewedCategoriesModel]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048),
@CategoryGuid uniqueidentifier = NULL,
@NumberOfPageViews int = NULL,
@SelectedCategory int = NULL
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'EPiServer.Personalization.VisitorGroups.Criteria.ViewedCategoriesModel')

		select @Id = SCOPE_IDENTITY()
				insert into [tblBigTable] (pkId, StoreName, Row, ItemType,Guid01,Integer01,Integer02)
		values(@Id, 'EPiServer.Personalization.VisitorGroups.Criteria.ViewedCategoriesModel', 1, @ItemType ,@CategoryGuid,@NumberOfPageViews,@SelectedCategory)

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblBigTable] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblBigTable] (pkId, StoreName, Row, ItemType,Guid01,Integer01,Integer02)
		values(@Id, 'EPiServer.Personalization.VisitorGroups.Criteria.ViewedCategoriesModel', 1, @ItemType ,@CategoryGuid,@NumberOfPageViews,@SelectedCategory)

		end
		else begin
				update [tblBigTable] set 
		ItemType = @ItemType,
		Guid01 = @CategoryGuid,
		Integer01 = @NumberOfPageViews,
		Integer02 = @SelectedCategory
                from [tblBigTable]
                where pkId=@pkId
                and Row=1
		end
		commit transaction
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Save_EPiServer.Packaging.Storage.StorageUpdateEntity]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_EPiServer.Packaging.Storage.StorageUpdateEntity]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048),
@ServerId nvarchar(max) = NULL,
@UpdateDate datetime = NULL
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'EPiServer.Packaging.Storage.StorageUpdateEntity')

		select @Id = SCOPE_IDENTITY()
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,String01,DateTime01)
		values(@Id, 'EPiServer.Packaging.Storage.StorageUpdateEntity', 1, @ItemType ,@ServerId,@UpdateDate)

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblSystemBigTable] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,String01,DateTime01)
		values(@Id, 'EPiServer.Packaging.Storage.StorageUpdateEntity', 1, @ItemType ,@ServerId,@UpdateDate)

		end
		else begin
				update [tblSystemBigTable] set 
		ItemType = @ItemType,
		String01 = @ServerId,
		DateTime01 = @UpdateDate
                from [tblSystemBigTable]
                where pkId=@pkId
                and Row=1
		end
		commit transaction
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Save_EPiServer.Packaging.Storage.PackageEntity]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_EPiServer.Packaging.Storage.PackageEntity]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048),
@InstallDate datetime = NULL,
@InstalledBy nvarchar(max) = NULL,
@NotificationProcessed bit = NULL,
@PackageId nvarchar(max) = NULL,
@ServerId nvarchar(max) = NULL,
@Version nvarchar(max) = NULL
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'EPiServer.Packaging.Storage.PackageEntity')

		select @Id = SCOPE_IDENTITY()
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,DateTime01,String01,Boolean01,String02,String03,String04)
		values(@Id, 'EPiServer.Packaging.Storage.PackageEntity', 1, @ItemType ,@InstallDate,@InstalledBy,@NotificationProcessed,@PackageId,@ServerId,@Version)

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblSystemBigTable] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,DateTime01,String01,Boolean01,String02,String03,String04)
		values(@Id, 'EPiServer.Packaging.Storage.PackageEntity', 1, @ItemType ,@InstallDate,@InstalledBy,@NotificationProcessed,@PackageId,@ServerId,@Version)

		end
		else begin
				update [tblSystemBigTable] set 
		ItemType = @ItemType,
		DateTime01 = @InstallDate,
		String01 = @InstalledBy,
		Boolean01 = @NotificationProcessed,
		String02 = @PackageId,
		String03 = @ServerId,
		String04 = @Version
                from [tblSystemBigTable]
                where pkId=@pkId
                and Row=1
		end
		commit transaction
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Save_EPiServer.Core.PropertySettings.PropertySettingsWrapper]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_EPiServer.Core.PropertySettings.PropertySettingsWrapper]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048),
@Description nvarchar(max) = NULL,
@DisplayName nvarchar(max) = NULL,
@IsDefault bit = NULL,
@IsGlobal bit = NULL,
@TypeFullName nvarchar(max) = NULL
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'EPiServer.Core.PropertySettings.PropertySettingsWrapper')

		select @Id = SCOPE_IDENTITY()
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,String01,String02,Boolean01,Boolean02,String03)
		values(@Id, 'EPiServer.Core.PropertySettings.PropertySettingsWrapper', 1, @ItemType ,@Description,@DisplayName,@IsDefault,@IsGlobal,@TypeFullName)

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblSystemBigTable] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,String01,String02,Boolean01,Boolean02,String03)
		values(@Id, 'EPiServer.Core.PropertySettings.PropertySettingsWrapper', 1, @ItemType ,@Description,@DisplayName,@IsDefault,@IsGlobal,@TypeFullName)

		end
		else begin
				update [tblSystemBigTable] set 
		ItemType = @ItemType,
		String01 = @Description,
		String02 = @DisplayName,
		Boolean01 = @IsDefault,
		Boolean02 = @IsGlobal,
		String03 = @TypeFullName
                from [tblSystemBigTable]
                where pkId=@pkId
                and Row=1
		end
		commit transaction
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Save_EPiServer.Core.IndexingInformation]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_EPiServer.Core.IndexingInformation]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048),
@ExecutionDate datetime = NULL,
@ResetIndex bit = NULL
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'EPiServer.Core.IndexingInformation')

		select @Id = SCOPE_IDENTITY()
				insert into [tblBigTable] (pkId, StoreName, Row, ItemType,DateTime01,Boolean01)
		values(@Id, 'EPiServer.Core.IndexingInformation', 1, @ItemType ,@ExecutionDate,@ResetIndex)

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblBigTable] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblBigTable] (pkId, StoreName, Row, ItemType,DateTime01,Boolean01)
		values(@Id, 'EPiServer.Core.IndexingInformation', 1, @ItemType ,@ExecutionDate,@ResetIndex)

		end
		else begin
				update [tblBigTable] set 
		ItemType = @ItemType,
		DateTime01 = @ExecutionDate,
		Boolean01 = @ResetIndex
                from [tblBigTable]
                where pkId=@pkId
                and Row=1
		end
		commit transaction
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Save_EPiServer.Cms.Shell.UI.Models.NotesData]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_EPiServer.Cms.Shell.UI.Models.NotesData]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048),
@BackgroundColor nvarchar(max) = NULL,
@Content nvarchar(max) = NULL,
@FontSize nvarchar(max) = NULL,
@GadgetId uniqueidentifier = NULL,
@Title nvarchar(max) = NULL
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'EPiServer.Cms.Shell.UI.Models.NotesData')

		select @Id = SCOPE_IDENTITY()
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,String01,String02,String03,Guid01,String04)
		values(@Id, 'EPiServer.Cms.Shell.UI.Models.NotesData', 1, @ItemType ,@BackgroundColor,@Content,@FontSize,@GadgetId,@Title)

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblSystemBigTable] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,String01,String02,String03,Guid01,String04)
		values(@Id, 'EPiServer.Cms.Shell.UI.Models.NotesData', 1, @ItemType ,@BackgroundColor,@Content,@FontSize,@GadgetId,@Title)

		end
		else begin
				update [tblSystemBigTable] set 
		ItemType = @ItemType,
		String01 = @BackgroundColor,
		String02 = @Content,
		String03 = @FontSize,
		Guid01 = @GadgetId,
		String04 = @Title
                from [tblSystemBigTable]
                where pkId=@pkId
                and Row=1
		end
		commit transaction
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Save_Drafts]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_Drafts]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048),
@IsNewLanguageBranch bit = NULL,
@IsNewPage bit = NULL,
@IsNotified bit = NULL,
@LanguageBranch nvarchar(450) = NULL,
@PageGuid uniqueidentifier = NULL,
@PageLink nvarchar(450) = NULL,
@PageName nvarchar(max) = NULL,
@PagePath nvarchar(max) = NULL,
@PageType int = NULL,
@ParentLink nvarchar(max) = NULL,
@Saved datetime = NULL,
@SavedBy nvarchar(450) = NULL
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'Drafts')

		select @Id = SCOPE_IDENTITY()
				insert into [tblBigTable] (pkId, StoreName, Row, ItemType,Boolean01,Boolean02,Boolean03,Indexed_String01,Indexed_Guid01,Indexed_String02,String01,String02,Integer01,String03,Indexed_DateTime01,Indexed_String03)
		values(@Id, 'Drafts', 1, @ItemType ,@IsNewLanguageBranch,@IsNewPage,@IsNotified,@LanguageBranch,@PageGuid,@PageLink,@PageName,@PagePath,@PageType,@ParentLink,@Saved,@SavedBy)

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblBigTable] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblBigTable] (pkId, StoreName, Row, ItemType,Boolean01,Boolean02,Boolean03,Indexed_String01,Indexed_Guid01,Indexed_String02,String01,String02,Integer01,String03,Indexed_DateTime01,Indexed_String03)
		values(@Id, 'Drafts', 1, @ItemType ,@IsNewLanguageBranch,@IsNewPage,@IsNotified,@LanguageBranch,@PageGuid,@PageLink,@PageName,@PagePath,@PageType,@ParentLink,@Saved,@SavedBy)

		end
		else begin
				update [tblBigTable] set 
		ItemType = @ItemType,
		Boolean01 = @IsNewLanguageBranch,
		Boolean02 = @IsNewPage,
		Boolean03 = @IsNotified,
		Indexed_String01 = @LanguageBranch,
		Indexed_Guid01 = @PageGuid,
		Indexed_String02 = @PageLink,
		String01 = @PageName,
		String02 = @PagePath,
		Integer01 = @PageType,
		String03 = @ParentLink,
		Indexed_DateTime01 = @Saved,
		Indexed_String03 = @SavedBy
                from [tblBigTable]
                where pkId=@pkId
                and Row=1
		end
		commit transaction
	end
end
GO
/****** Object:  StoredProcedure [dbo].[Save_BackgroundJobStore]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Save_BackgroundJobStore]
@Id bigint output,
@UniqueId uniqueidentifier,
@ItemType nvarchar(2048),
@CurrentState int = NULL,
@Data nvarchar(max) = NULL,
@EndDate datetime = NULL,
@LastHeartBeat datetime = NULL,
@Message nvarchar(max) = NULL,
@Owner nvarchar(max) = NULL,
@StartDate datetime = NULL,
@Title nvarchar(max) = NULL
as
begin
declare @pkId bigint
	select @pkId = pkId from tblBigTableIdentity where [Guid] = @UniqueId
	if @pkId IS NULL
	begin
		begin transaction
		insert into [tblBigTableIdentity]([Guid], [StoreName]) values(@UniqueId, 'BackgroundJobStore')

		select @Id = SCOPE_IDENTITY()
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,Integer01,String01,DateTime01,DateTime02,String02,String03,DateTime03,String04)
		values(@Id, 'BackgroundJobStore', 1, @ItemType ,@CurrentState,@Data,@EndDate,@LastHeartBeat,@Message,@Owner,@StartDate,@Title)

		commit transaction
	end
	else
	begin
		begin transaction
		select @Id = @pkId
		DECLARE @rows int
		select @rows = count(*) from [tblSystemBigTable] where pkId = @Id
		if(@rows < 1) begin
				insert into [tblSystemBigTable] (pkId, StoreName, Row, ItemType,Integer01,String01,DateTime01,DateTime02,String02,String03,DateTime03,String04)
		values(@Id, 'BackgroundJobStore', 1, @ItemType ,@CurrentState,@Data,@EndDate,@LastHeartBeat,@Message,@Owner,@StartDate,@Title)

		end
		else begin
				update [tblSystemBigTable] set 
		ItemType = @ItemType,
		Integer01 = @CurrentState,
		String01 = @Data,
		DateTime01 = @EndDate,
		DateTime02 = @LastHeartBeat,
		String02 = @Message,
		String03 = @Owner,
		DateTime03 = @StartDate,
		String04 = @Title
                from [tblSystemBigTable]
                where pkId=@pkId
                and Row=1
		end
		commit transaction
	end
end
GO
/****** Object:  View [dbo].[tblCategoryPage]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[tblCategoryPage]
AS
SELECT  [fkContentID] AS fkPageID,
		[fkCategoryID],
		[CategoryType],
		[fkLanguageBranchID]
FROM    dbo.tblContentCategory
GO
/****** Object:  StoredProcedure [dbo].[netQuickSearchByFolderID]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netQuickSearchByFolderID]
(
	@FolderID	INT
)
AS
BEGIN
	SET NOCOUNT ON
	SELECT TOP 1 pkID
	FROM tblPage
	WHERE ExternalFolderID=@FolderID
END
GO
/****** Object:  StoredProcedure [dbo].[netPersonalRejectedList]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPersonalRejectedList]
(
	@UserName NVARCHAR(255),
    @Offset INT = 0,
    @Count INT = 50,
    @LanguageBranch NCHAR(17) = NULL
)
AS
BEGIN
	SET NOCOUNT ON
    DECLARE @LangBranchID NCHAR(17);
	SELECT @LangBranchID = pkID FROM tblLanguageBranch WHERE LanguageID = @LanguageBranch
	IF @LangBranchID IS NULL 
	BEGIN 
		if @LanguageBranch IS NOT NULL
			RAISERROR('Language branch %s is not defined',16,1, @LanguageBranch)
		else
			SET @LangBranchID = -1
	END
	-- Determine the first record and last record
	DECLARE @FirstRecord int, @LastRecord int
	SELECT @FirstRecord = @Offset
	SELECT @LastRecord = (@Offset + @Count + 1);
	WITH TempResult as
	(
		SELECT	ROW_NUMBER() OVER(ORDER BY W.Saved DESC) as RowNumber,
			W.fkContentID AS ContentID,
			W.pkID AS WorkID,
			1 AS VersionStatus,
			W.ChangedByName AS UserName,
			W.Saved AS ItemCreated,
			W.Name,
			W.fkLanguageBranchID as LanguageBranch,
			W.CommonDraft,
			W.fkMasterVersionID as MasterVersion,
			CASE WHEN C.fkMasterLanguageBranchID=W.fkLanguageBranchID THEN 1 ELSE 0 END AS IsMasterLanguageBranch
		FROM
			tblWorkContent AS W
		INNER JOIN
			tblContent AS C ON C.pkID=W.fkContentID
		WHERE
			W.ChangedByName=@UserName AND
			W.HasBeenPublished=0 AND
			W.Rejected=1 AND
			C.Deleted=0 AND
			(@LanguageBranch = NULL OR W.fkLanguageBranchID = @LangBranchID)
	)
	SELECT  TOP (@LastRecord - 1) *
	FROM    TempResult
	WHERE   RowNumber > @FirstRecord AND
		  RowNumber < @LastRecord
END
GO
/****** Object:  StoredProcedure [dbo].[netPersonalNotReadyList]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPersonalNotReadyList]
(
    @UserName NVARCHAR(255),
    @Offset INT = 0,
    @Count INT = 50,
    @LanguageBranch NCHAR(17) = NULL
)
AS
BEGIN	
	SET NOCOUNT ON
    DECLARE @LangBranchID NCHAR(17);
	SELECT @LangBranchID = pkID FROM tblLanguageBranch WHERE LanguageID = @LanguageBranch
	IF @LangBranchID IS NULL 
	BEGIN 
		if @LanguageBranch IS NOT NULL
			RAISERROR('Language branch %s is not defined',16,1, @LanguageBranch)
		else
			SET @LangBranchID = -1
	END
	-- Determine the first record and last record
	DECLARE @FirstRecord int, @LastRecord int
	SELECT @FirstRecord = @Offset
	SELECT @LastRecord = (@Offset + @Count + 1);
	WITH TempResult as
	(
		SELECT	ROW_NUMBER() OVER(ORDER BY W.Saved DESC) as RowNumber,
			W.fkContentID AS ContentID,
			W.pkID AS WorkID,
			2 AS VersionStatus,
			W.ChangedByName AS UserName,
			W.Saved AS ItemCreated,
			W.Name,
			W.fkLanguageBranchID as LanguageBranch,
			W.CommonDraft,
			W.fkMasterVersionID as MasterVersion,
			CASE WHEN C.fkMasterLanguageBranchID=W.fkLanguageBranchID THEN 1 ELSE 0 END AS IsMasterLanguageBranch
		FROM
			tblWorkContent AS W
		INNER JOIN
			tblContent AS C ON C.pkID=W.fkContentID
		WHERE
			W.ChangedByName=@UserName AND
			W.HasBeenPublished=0 AND
			W.ReadyToPublish=0 AND
			W.Rejected=0 AND
			C.Deleted=0 AND
            (@LanguageBranch = NULL OR W.fkLanguageBranchID = @LangBranchID)
	)
	SELECT  TOP (@LastRecord - 1) *
	FROM    TempResult
	WHERE   RowNumber > @FirstRecord AND
		  RowNumber < @LastRecord
END
GO
/****** Object:  StoredProcedure [dbo].[netReadyToPublishList]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netReadyToPublishList]
(
    @Offset INT = 0,
    @Count INT = 50,
    @LanguageBranch NCHAR(17) = NULL
)
AS
BEGIN
	SET NOCOUNT ON
    DECLARE @LangBranchID NCHAR(17);
	SELECT @LangBranchID = pkID FROM tblLanguageBranch WHERE LanguageID = @LanguageBranch
	IF @LangBranchID IS NULL 
	BEGIN 
		if @LanguageBranch IS NOT NULL
			RAISERROR('Language branch %s is not defined',16,1, @LanguageBranch)
		else
			SET @LangBranchID = -1
	END
	-- Determine the first record and last record
	DECLARE @FirstRecord int, @LastRecord int
	SELECT @FirstRecord = @Offset
	SELECT @LastRecord = (@Offset + @Count + 1);
	WITH TempResult as
	(
		SELECT	ROW_NUMBER() OVER(ORDER BY W.Saved DESC) as RowNumber,
			W.fkContentID AS ContentID,
			W.pkID AS WorkID,
			3 AS VersionStatus,
			W.ChangedByName AS UserName,
			W.Saved AS ItemCreated,
			W.Name,
			W.fkLanguageBranchID as LanguageBranch,
			W.CommonDraft,
			W.fkMasterVersionID as MasterVersion,
			CASE WHEN C.fkMasterLanguageBranchID=W.fkLanguageBranchID THEN 1 ELSE 0 END AS IsMasterLanguageBranch
		FROM
			tblWorkContent AS W
		INNER JOIN
			tblContent AS C ON C.pkID=W.fkContentID
		WHERE
			W.HasBeenPublished=0 AND
			W.ReadyToPublish=1 AND
			W.DelayedPublish=0 AND
			W.Rejected=0 AND
			C.Deleted=0 AND
			((C.PublishedVersion<>W.pkID) OR (C.PublishedVersion IS NULL)) AND
			(@LanguageBranch = NULL OR W.fkLanguageBranchID = @LangBranchID)
	)
	SELECT  TOP (@LastRecord - 1) *
	FROM    TempResult
	WHERE   RowNumber > @FirstRecord AND
		  RowNumber < @LastRecord
END
GO
/****** Object:  StoredProcedure [dbo].[netQuickSearchListFolderID]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netQuickSearchListFolderID]
AS
BEGIN
	SET NOCOUNT ON
	SELECT ExternalFolderID,pkID AS PageID
	FROM tblPage
	ORDER BY ExternalFolderID ASC
END
GO
/****** Object:  StoredProcedure [dbo].[netSoftLinksUpdateStatus]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSoftLinksUpdateStatus]
(
	@pkID int,
	@LastCheckedDate datetime,
	@FirstDateBroken datetime,
	@HttpStatusCode int,
	@LinkStatus int
)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE tblContentSoftlink SET
		LastCheckedDate = @LastCheckedDate,
		FirstDateBroken = @FirstDateBroken,
		HttpStatusCode = @HttpStatusCode,
		LinkStatus = @LinkStatus
	WHERE pkID = @pkID
END
GO
/****** Object:  StoredProcedure [dbo].[netSoftLinksGetUnchecked]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSoftLinksGetUnchecked]
(
	@LastCheckedDate	datetime,
	@LastCheckedDateBroken	datetime,
	@MaxNumberOfLinks INT = 1000
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT TOP(@MaxNumberOfLinks) *
	FROM tblContentSoftlink
	WHERE (LinkProtocol like 'http%' OR LinkProtocol is NULL) AND 
		(LastCheckedDate < @LastCheckedDate OR (LastCheckedDate < @LastCheckedDateBroken AND LinkStatus <> 0) OR LastCheckedDate IS NULL)
	ORDER BY LastCheckedDate
END
GO
/****** Object:  StoredProcedure [dbo].[netSoftLinksGetBrokenCount]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSoftLinksGetBrokenCount]
	@OwnerContentID int
AS
BEGIN
		SELECT Count(*)
		FROM [tblContentSoftlink]
		INNER JOIN tblTree ON tblContentSoftlink.fkOwnerContentID = tblTree.fkChildID
		WHERE (tblTree.fkParentID = @OwnerContentID OR (tblContentSoftlink.fkOwnerContentID = @OwnerContentID AND tblTree.NestingLevel = 1)) AND LinkStatus <> 0
END
GO
/****** Object:  StoredProcedure [dbo].[netSoftLinksGetBroken]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSoftLinksGetBroken]
	@SkipCount int,
	@MaxResults int,
	@RootPageId int
AS
BEGIN
	SELECT [pkID]
		,[fkOwnerContentID]
		,[fkReferencedContentGUID]
		,[OwnerLanguageID]
		,[ReferencedLanguageID]
		,[LinkURL]
		,[LinkType]
		,[LinkProtocol]
		,[ContentLink]
		,[LastCheckedDate]
		,[FirstDateBroken]
		,[HttpStatusCode]
		,[LinkStatus]
	FROM (
		SELECT [pkID]
			,[fkOwnerContentID]
			,[fkReferencedContentGUID]
			,[OwnerLanguageID]
			,[ReferencedLanguageID]
			,[LinkURL]
			,[LinkType]
			,[LinkProtocol]
			,[ContentLink]
			,[LastCheckedDate]
			,[FirstDateBroken]
			,[HttpStatusCode]
			,[LinkStatus]
			,ROW_NUMBER() OVER (ORDER BY pkID ASC) as RowNumber
		FROM [tblContentSoftlink]
		INNER JOIN tblTree ON tblContentSoftlink.fkOwnerContentID = tblTree.fkChildID 
		WHERE (tblTree.fkParentID = @RootPageId OR (tblContentSoftlink.fkOwnerContentID = @RootPageId AND tblTree.NestingLevel = 1)) AND LinkStatus <> 0
		) BrokenLinks
	WHERE BrokenLinks.RowNumber > @SkipCount AND BrokenLinks.RowNumber <= @SkipCount+@MaxResults
END
GO
/****** Object:  StoredProcedure [dbo].[netSoftLinkList]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSoftLinkList]
(
	@ReferenceGUID	uniqueidentifier,
	@Reversed INT = 0
)
AS
BEGIN
	SET NOCOUNT ON
	IF @Reversed = 1
		SELECT 
			pkID,
			fkOwnerContentID AS OwnerContentID,
			fkReferencedContentGUID AS ReferencedContentGUID,
			OwnerLanguageID,
			ReferencedLanguageID,
			LinkURL,
			LinkType,
			LinkProtocol,
			LastCheckedDate,
			FirstDateBroken,
			HttpStatusCode,
			LinkStatus
		FROM tblContentSoftlink 
		WHERE fkReferencedContentGUID=@ReferenceGUID
	ELSE
		SELECT 
			SoftLink.pkID,
			Content.pkID AS OwnerContentID,
			SoftLink.fkReferencedContentGUID AS ReferencedContentGUID,
			SoftLink.OwnerLanguageID,
			SoftLink.ReferencedLanguageID,
			SoftLink.LinkURL,
			SoftLink.LinkType,
			SoftLink.LinkProtocol,
			SoftLink.LastCheckedDate,
			SoftLink.FirstDateBroken,
			SoftLink.HttpStatusCode,
			SoftLink.LinkStatus
		FROM tblContentSoftlink AS SoftLink
		INNER JOIN tblContent as Content ON SoftLink.fkOwnerContentID = Content.pkID
		WHERE Content.ContentGUID=@ReferenceGUID
END
GO
/****** Object:  StoredProcedure [dbo].[netSoftLinkInsert]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSoftLinkInsert]
(
	@OwnerContentID	INT,
	@ReferencedContentGUID uniqueidentifier,
	@LinkURL	NVARCHAR(255),
	@LinkType	INT,
	@LinkProtocol	NVARCHAR(10),
	@ContentLink	NVARCHAR(255),
	@LastCheckedDate datetime,
	@FirstDateBroken datetime,
	@HttpStatusCode int,
	@LinkStatus int,
	@OwnerLanguageID int,
	@ReferencedLanguageID int
)
AS
BEGIN
	INSERT INTO tblContentSoftlink
		(fkOwnerContentID,
		fkReferencedContentGUID,
	    OwnerLanguageID,
		ReferencedLanguageID,
		LinkURL,
		LinkType,
		LinkProtocol,
		ContentLink,
		LastCheckedDate,
		FirstDateBroken,
		HttpStatusCode,
		LinkStatus)
	VALUES
		(@OwnerContentID,
		@ReferencedContentGUID,
		@OwnerLanguageID,
		@ReferencedLanguageID,
		@LinkURL,
		@LinkType,
		@LinkProtocol,
		@ContentLink,
		@LastCheckedDate,
		@FirstDateBroken,
		@HttpStatusCode,
		@LinkStatus)
END
GO
/****** Object:  StoredProcedure [dbo].[netSoftLinkDelete]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSoftLinkDelete]
(
	@OwnerContentID	INT,
	@LanguageBranch nchar(17) = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @LangBranchID INT
	IF NOT @LanguageBranch IS NULL
	BEGIN
		SELECT @LangBranchID = pkID FROM tblLanguageBranch WHERE LanguageID=@LanguageBranch
		IF @LangBranchID IS NULL
		BEGIN
			RAISERROR (N'netSoftLinkDelete: LanguageBranchID is null, possibly empty table tblLanguageBranch', 16, 1)
			RETURN 0
		END
	END
	DELETE FROM tblContentSoftlink WHERE fkOwnerContentID = @OwnerContentID AND (@LanguageBranch IS NULL OR OwnerLanguageID=@LangBranchID)
END
GO
/****** Object:  StoredProcedure [dbo].[netSoftLinkByUrl]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSoftLinkByUrl]
(
	@LinkURL NVARCHAR(255),
	@ExactMatch INT = 1
)
AS
BEGIN
	SET NOCOUNT ON
	SELECT 
		pkID,
		fkOwnerContentID AS OwnerContentID,
		fkReferencedContentGUID AS ReferencedContentGUID,
		OwnerLanguageID,
		ReferencedLanguageID,
		LinkURL,
		LinkType,
		LinkProtocol,
		LastCheckedDate,
		FirstDateBroken,
		HttpStatusCode,
		LinkStatus
	FROM tblContentSoftlink 
	WHERE (@ExactMatch=1 AND LinkURL LIKE @LinkURL) OR (@ExactMatch=0 AND LinkURL LIKE (@LinkURL + '%'))
END
GO
/****** Object:  StoredProcedure [dbo].[netSoftLinkByPageLink]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[netSoftLinkByPageLink]
(
	@PageLink NVARCHAR(255)
)
AS
BEGIN
	SET NOCOUNT ON
	SELECT 
		pkID,
		fkOwnerContentID AS OwnerID,
		fkReferencedContentGUID AS ReferencedGUID,
		NULL AS OwnerName,
		NULL AS ReferencedName,
		OwnerLanguageID,
		ReferencedLanguageID,
		LinkURL,
		LinkType as ReferenceType ,
		LinkProtocol,
		LastCheckedDate,
		FirstDateBroken,
		HttpStatusCode,
		LinkStatus
	FROM tblContentSoftlink 
	WHERE [ContentLink] = @PageLink
END
GO
/****** Object:  StoredProcedure [dbo].[netTabInfoDelete]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netTabInfoDelete]
(
	@pkID		INT,
	@ReplaceID	INT = NULL
)
AS
BEGIN
	IF NOT @ReplaceID IS NULL
		UPDATE tblPageDefinition SET Advanced = @ReplaceID WHERE Advanced = @pkID
	DELETE FROM tblPageDefinitionGroup WHERE pkID = @pkID AND SystemGroup = 0
END
GO
/****** Object:  StoredProcedure [dbo].[netTabListDependencies]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netTabListDependencies]
(
	@TabID INT
)
AS
BEGIN
	SELECT tblPageDefinitionGroup.pkID as TabID,
		tblPageDefinition.Name as PropertyName,
		tblPageDefinitionGroup.Name as TabName
	FROM tblPageDefinition 
	INNER JOIN tblPageDefinitionGroup
	ON tblPageDefinitionGroup.pkID = tblPageDefinition.Advanced
	WHERE Advanced = @TabID
END
GO
/****** Object:  StoredProcedure [dbo].[netPageTypeDeleteAvailable]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageTypeDeleteAvailable]
	@PageTypeID INT,
	@ChildID INT = 0
AS
BEGIN
	IF (@ChildID = 0)
		DELETE FROM tblPageTypeToPageType WHERE fkPageTypeParentID = @PageTypeID
	ELSE
		DELETE FROM tblPageTypeToPageType WHERE fkPageTypeParentID = @PageTypeID AND fkPageTypeChildID = @ChildID
END
GO
/****** Object:  StoredProcedure [dbo].[netPageTypeAddAvailable]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageTypeAddAvailable]
(
	@PageTypeID INT,
	@ChildID INT,
	@Availability INT = 0,
	@Access INT = 2,
	@Allow BIT = NULL
)
AS
BEGIN
	IF (@Availability = 1 OR @Availability = 2)
		DELETE FROM tblPageTypeToPageType WHERE
			fkPageTypeParentID = @PageTypeID
	ELSE
		DELETE FROM tblPageTypeToPageType WHERE
			fkPageTypeParentID = @PageTypeID AND fkPageTypeChildID = @ChildID
	INSERT INTO tblPageTypeToPageType
	(fkPageTypeParentID, fkPageTypeChildID, Access, Availability, Allow)
	VALUES
	(@PageTypeID, @ChildID, @Access, @Availability, @Allow)
END
GO
/****** Object:  StoredProcedure [dbo].[netPageTypeToPageTypeList]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageTypeToPageTypeList]
AS
BEGIN
	SET NOCOUNT ON
	SELECT
		fkPageTypeParentID AS ID,
		fkPageTypeChildID AS ChildID,
		Access AS AccessMask,
		Availability,
		Allow
	FROM tblPageTypeToPageType
	ORDER BY fkPageTypeParentID
END
GO
/****** Object:  StoredProcedure [dbo].[netPageDefinitionWithContentType]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageDefinitionWithContentType]
(
	@ContentTypeID	UNIQUEIDENTIFIER
)
AS
BEGIN
	SET NOCOUNT ON
	SELECT count(*) FROM tblPageDefinition INNER JOIN 
	tblPageDefinitionType ON tblPageDefinition.fkPageDefinitionTypeID = tblPageDefinitionType.pkID
	WHERE
	fkPageTypeGUID = @ContentTypeID
END
GO
/****** Object:  StoredProcedure [dbo].[netDelayPublishList]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netDelayPublishList]
(
	@UntilDate	DATETIME,
	@ContentID		INT
)
AS
BEGIN
	SET NOCOUNT ON
	SELECT 
		fkContentID AS ContentID,
		pkID AS ContentWorkID,
		StartPublish
	FROM
		tblWorkContent
	WHERE
		DelayedPublish = 1 AND
		ReadyToPublish = 1 AND
		StartPublish <= @UntilDate AND
		(fkContentID = @ContentID OR @ContentID IS NULL)
	ORDER BY
		StartPublish
END
GO
/****** Object:  StoredProcedure [dbo].[netPageDefinitionTypeDelete]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageDefinitionTypeDelete]
(
	@ID INT
)
AS
BEGIN
	SET NOCOUNT ON
	SELECT DISTINCT pkID
	FROM tblPageDefinition 
	WHERE fkPageDefinitionTypeID=@ID
	IF (@@ROWCOUNT <> 0)
		RETURN
	IF @ID>=1000
		DELETE FROM tblPageDefinitionType WHERE pkID=@ID
	ELSE
		RAISERROR('Cannot delete system types',16,1)
END
GO
/****** Object:  StoredProcedure [dbo].[netPageListExternalFolderID]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageListExternalFolderID] AS
	SET NOCOUNT ON
	select ExternalFolderID 
	from tblPage
GO
/****** Object:  StoredProcedure [dbo].[netPageListEndPoints]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageListEndPoints]
(
	@PagingSize INT = 100000,
	@LastPageID INT = 0
)
AS
BEGIN
	SET NOCOUNT ON
	SELECT TOP(@PagingSize) tblPage.pkID, tblPage.ContentType
	FROM tblPage
	LEFT JOIN tblPage Page2 ON Page2.fkParentID=tblPage.pkID
	WHERE tblPage.pkID>@LastPageID AND Page2.pkID IS NULL
	ORDER BY tblPage.pkID ASC
END
GO
/****** Object:  StoredProcedure [dbo].[netPagePath]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[netPagePath]
(
	@PageID INT
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT PagePath FROM tblPage where tblPage.pkID = @PageID
END
GO
/****** Object:  StoredProcedure [dbo].[netPageMaxFolderId]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageMaxFolderId]
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @FolderID INT
	SELECT @FolderID=COALESCE(MAX(ExternalFolderID) + 1, 1) FROM tblPage
	RETURN @FolderID
END
GO
/****** Object:  StoredProcedure [dbo].[netPageListAll]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageListAll]
(
	@PageID INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	IF @PageID IS NULL
	BEGIN
		SELECT tblPage.pkID as "PageID", tblPage.fkParentID AS "ParentID", tblPage.ContentType
		FROM tblPage
	END
	ELSE
	BEGIN
		SELECT tblPage.pkID as "PageID", tblPage.fkParentID AS "ParentID", tblPage.ContentType
		FROM tblPage
		INNER JOIN tblTree ON tblTree.fkChildID=tblPage.pkID
		WHERE tblTree.fkParentID=@PageID
		ORDER BY NestingLevel DESC
	END
END
GO
/****** Object:  StoredProcedure [dbo].[netPageDefinitionList]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageDefinitionList]
(
	@PageTypeID INT = NULL
)
AS
BEGIN
	SELECT tblPageDefinition.pkID AS ID,
		fkPageTypeID AS PageTypeID,
		COALESCE(fkPageDefinitionTypeID,tblPageDefinition.Property) AS PageDefinitionTypeID,
		tblPageDefinition.Name,
		COALESCE(tblPageDefinitionType.Property,tblPageDefinition.Property) AS Type,
		CONVERT(INT,Required) AS Required,
		Advanced,
		CONVERT(INT,Searchable) AS Searchable,
		DefaultValueType,
		EditCaption,
		HelpText,
		ObjectProgID,
		LongStringSettings,
		SettingsID,
		CONVERT(INT,Boolean) AS Boolean,
		Number AS IntNumber,
		FloatNumber,
		PageType,
		PageLink,
		Date AS DateValue,
		String,
		LongString,
		NULL AS OldType,
		FieldOrder,
		LanguageSpecific,
		DisplayEditUI,
		ExistsOnModel
	FROM tblPageDefinition
	LEFT JOIN tblPropertyDefault ON tblPropertyDefault.fkPageDefinitionID=tblPageDefinition.pkID
	LEFT JOIN tblPageDefinitionType ON tblPageDefinitionType.pkID=tblPageDefinition.fkPageDefinitionTypeID
	WHERE (fkPageTypeID = @PageTypeID) OR (fkPageTypeID IS NULL AND @PageTypeID IS NULL)
	ORDER BY FieldOrder,tblPageDefinition.pkID
END
GO
/****** Object:  StoredProcedure [dbo].[netPageDefinitionGet]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageDefinitionGet]
(
	@PageDefinitionID INT
)
AS
BEGIN
	SELECT tblPageDefinition.pkID AS ID,
		fkPageTypeID AS PageTypeID,
		COALESCE(fkPageDefinitionTypeID,tblPageDefinition.Property) AS PageDefinitionTypeID,
		tblPageDefinition.Name,
		COALESCE(tblPageDefinitionType.Property,tblPageDefinition.Property) AS Type,
		CONVERT(INT,Required) AS Required,
		Advanced,
		CONVERT(INT,Searchable) AS Searchable,
		DefaultValueType,
		EditCaption,
		HelpText,
		ObjectProgID,
		LongStringSettings,
		SettingsID,
		CONVERT(INT,Boolean) AS Boolean,
		Number AS IntNumber,
		FloatNumber,
		PageType,
		PageLink,
		Date AS DateValue,
		String,
		LongString,
		FieldOrder,
		LanguageSpecific,
		DisplayEditUI,
		ExistsOnModel
	FROM tblPageDefinition
	LEFT JOIN tblPropertyDefault ON tblPropertyDefault.fkPageDefinitionID=tblPageDefinition.pkID
	LEFT JOIN tblPageDefinitionType ON tblPageDefinitionType.pkID=tblPageDefinition.fkPageDefinitionTypeID
	WHERE tblPageDefinition.pkID = @PageDefinitionID
END
GO
/****** Object:  StoredProcedure [dbo].[netConvertPageType]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netConvertPageType]
(
	@PageID		INT,
	@FromPageType	INT,
	@ToPageType		INT,
	@Recursive		BIT,
	@IsTest			BIT
)
AS
BEGIN
	DECLARE @cnt INT;
	CREATE TABLE #updatepages (fkChildID INT)
	INSERT INTO #updatepages(fkChildID)
	SELECT fkChildID
	FROM tblTree tree
	JOIN tblPage page
	ON page.pkID = tree.fkChildID 
	WHERE @Recursive = 1
	AND tree.fkParentID = @PageID
	AND page.fkPageTypeID = @FromPageType
	UNION (SELECT pkID FROM tblPage WHERE pkID = @PageID AND fkPageTypeID = @FromPageType)
	IF @IsTest = 1
	BEGIN
		SET @cnt = (SELECT COUNT(*) FROM #updatepages)
	END
	ELSE
	BEGIN		
		UPDATE tblPage SET fkPageTypeID=@ToPageType
		WHERE EXISTS (
			SELECT * from #updatepages WHERE fkChildID=pkID)
		SET @cnt = @@rowcount
	END		
	DROP TABLE #updatepages
	RETURN (@cnt)
END
GO
/****** Object:  StoredProcedure [dbo].[netContentAclAdd]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netContentAclAdd]
(
	@Name NVARCHAR(255),
	@IsRole INT,
	@ContentID INT,
	@AccessMask INT
)
AS
BEGIN
	SET NOCOUNT ON
	UPDATE 
	    tblContentAccess 
	SET 
	    AccessMask=@AccessMask
	WHERE 
	    fkContentID=@ContentID AND 
	    Name=@Name AND 
	    IsRole=@IsRole
	IF (@@ROWCOUNT = 0)
	BEGIN
		-- Does not exist, create it
		INSERT INTO tblContentAccess 
		    (fkContentID, Name, IsRole, AccessMask) 
		VALUES 
		    (@ContentID, @Name, @IsRole, @AccessMask)
	END
END
GO
/****** Object:  StoredProcedure [dbo].[netCreatePath]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netCreatePath]
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @RootPage INT
	UPDATE tblPage SET PagePath=''
	SELECT @RootPage=pkID FROM tblPage WHERE fkParentID IS NULL AND PagePath = ''
	UPDATE tblPage SET PagePath='.' WHERE pkID=@RootPage
	WHILE (1 = 1)
	BEGIN
		UPDATE CHILD SET CHILD.PagePath = PARENT.PagePath + CONVERT(VARCHAR, PARENT.pkID) + '.'
		FROM tblPage CHILD INNER JOIN tblPage PARENT ON CHILD.fkParentId = PARENT.pkID
		WHERE CHILD.PagePath = '' AND PARENT.PagePath <> ''
		IF (@@ROWCOUNT = 0)
			BREAK	
	END	
END
GO
/****** Object:  StoredProcedure [dbo].[netPageCreate]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageCreate]
(
	@UserName NVARCHAR(255),
	@ParentID			INT,
	@PageTypeID			INT,
	@FolderID			INT,
	@PageGUID			UNIQUEIDENTIFIER,
	@ContentType		INT,
	@WastebasketID		INT
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	DECLARE @ContentID INT
	DECLARE @Delete		BIT
	/* Create materialized path to page */
	DECLARE @Path VARCHAR(7000)
	SELECT @Path=PagePath + CONVERT(VARCHAR, @ParentID) + '.' FROM tblPage WHERE pkID=@ParentID
    SET @Delete = 0
    IF(@WastebasketID = @ParentID)
		SET @Delete=1
    ELSE IF (EXISTS (SELECT NestingLevel FROM tblTree WHERE fkParentID=@WastebasketID AND fkChildID=@ParentID))
        SET @Delete=1
	/* Create new page */
	INSERT INTO tblPage 
		(fkPageTypeID, CreatorName, fkParentID, ExternalFolderID, PendingPublish, PageGUID, PagePath, ContentType, Deleted)
	VALUES
		(@PageTypeID, @UserName, @ParentID, @FolderID, 1, @PageGUID, @Path, @ContentType, @Delete)
	/* Remember pkID of page */
	SET @ContentID= SCOPE_IDENTITY() 
	/* Update page tree with info about this page */
	INSERT INTO tblTree
		(fkParentID, fkChildID, NestingLevel)
	SELECT 
		fkParentID,
		@ContentID,
		NestingLevel+1
	FROM tblTree
	WHERE fkChildID=@ParentID
	UNION ALL
	SELECT
		@ParentID,
		@ContentID,
		1
 	/* Copy ACL from parent */
	INSERT INTO tblContentAccess
		(fkContentID, Name, IsRole, AccessMask) 
	SELECT @ContentID, Name, IsRole, AccessMask FROM tblContentAccess WHERE fkContentID=@ParentID
	RETURN @ContentID
END
GO
/****** Object:  StoredProcedure [dbo].[netPageCountDescendants]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageCountDescendants]
(
	@PageID INT = NULL
)
AS
BEGIN
	DECLARE @pageCount INT
	SET NOCOUNT ON
	IF @PageID IS NULL
	BEGIN
		SET @pageCount =
			(SELECT COUNT(*) AS PageCount
			 FROM tblPage)
	END
	ELSE
	BEGIN
		SET @pageCount =
			(SELECT COUNT(*) AS PageCount
			 FROM tblPage
			 INNER JOIN tblTree ON tblTree.fkChildID=tblPage.pkID
			 WHERE tblTree.fkParentID=@PageID)
	END
	RETURN @pageCount
END
GO
/****** Object:  StoredProcedure [dbo].[netContentMove]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netContentMove]
(
	@ContentID				INT,
	@DestinationContentID	INT,
	@WastebasketID		INT,
	@Archive			INT,
	@DeletedBy			VARCHAR(255) = NULL,
	@DeletedDate		DATETIME = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @TmpParentID		INT
	DECLARE @SourceParentID		INT
	DECLARE @TmpNestingLevel	INT
	DECLARE @Delete				BIT
	DECLARE @SourcePath VARCHAR(7000)
	DECLARE @TargetPath VARCHAR(7000)
	/* Protect from moving Content under itself */
	IF (EXISTS (SELECT NestingLevel FROM tblTree WHERE fkParentID=@ContentID AND fkChildID=@DestinationContentID) OR @DestinationContentID=@ContentID)
		RETURN -1
    SELECT @SourcePath=ContentPath + CONVERT(VARCHAR, @ContentID) + '.' FROM tblContent WHERE pkID=@ContentID
    SELECT @TargetPath=ContentPath + CONVERT(VARCHAR, @DestinationContentID) + '.' FROM tblContent WHERE pkID=@DestinationContentID
	/* Switch parent to archive Content, disable stop publish and update Saved */
	UPDATE tblContent SET
		@SourceParentID		= fkParentID,
		fkParentID			= @DestinationContentID,
		ContentPath            = @TargetPath
	WHERE pkID=@ContentID
    IF (@Archive = 1)
	BEGIN
		UPDATE tblContentLanguage SET
			StopPublish			= NULL,
			Saved				= GetDate()
		WHERE fkContentID=@ContentID
		UPDATE tblWorkContent SET
			StopPublish			= NULL
		WHERE fkContentID = @ContentID
	END
	/* Remove all references to this Content and its childs, but preserve the 
		information below itself */
	DELETE FROM 
		tblTree 
	WHERE 
		fkChildID IN (SELECT fkChildID FROM tblTree WHERE fkParentID=@ContentID UNION SELECT @ContentID) AND
		fkParentID NOT IN (SELECT fkChildID FROM tblTree WHERE fkParentID=@ContentID UNION SELECT @ContentID)
	/* Insert information about new Contents for all Contents where the destination is a child */
	DECLARE cur CURSOR LOCAL FAST_FORWARD FOR SELECT fkParentID, NestingLevel FROM tblTree WHERE fkChildID=@DestinationContentID
	OPEN cur
	FETCH NEXT FROM cur INTO @TmpParentID, @TmpNestingLevel
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		INSERT INTO tblTree
			(fkParentID,
			fkChildID,
			NestingLevel)
		SELECT
			@TmpParentID,
			fkChildID,
			@TmpNestingLevel + NestingLevel + 1
		FROM
			tblTree
		WHERE
			fkParentID=@ContentID
		UNION ALL
		SELECT
			@TmpParentID,
			@ContentID,
			@TmpNestingLevel + 1
		FETCH NEXT FROM cur INTO @TmpParentID, @TmpNestingLevel
	END
	CLOSE cur
	DEALLOCATE cur
	/* Insert information about new Contents for destination */
	INSERT INTO tblTree
		(fkParentID,
		fkChildID,
		NestingLevel)
	SELECT
		@DestinationContentID,
		fkChildID,
		NestingLevel+1
	FROM
		tblTree
	WHERE
		fkParentID=@ContentID
	UNION
	SELECT
		@DestinationContentID,
		@ContentID,
		1
    /* Determine if destination is somewhere under wastebasket */
    SET @Delete=0
    IF (EXISTS (SELECT NestingLevel FROM tblTree WHERE fkParentID=@WastebasketID AND fkChildID=@ContentID))
        SET @Delete=1
    /* Update deleted bit of Contents */
    UPDATE tblContent  SET 
		Deleted=@Delete,
		DeletedBy = @DeletedBy,
		DeletedDate = @DeletedDate
    WHERE pkID IN (SELECT fkChildID FROM tblTree WHERE fkParentID=@ContentID) OR pkID=@ContentID
	/* Update saved date for Content */
	IF(@Delete > 0)
	BEGIN
		UPDATE tblContentLanguage  SET 
				Saved=GetDate()
   		WHERE fkContentID IN (SELECT fkChildID FROM tblTree WHERE fkParentID=@ContentID) OR fkContentID=@ContentID
	END
    /* Create materialized path to moved Contents */
    UPDATE tblContent
    SET ContentPath=@TargetPath + CONVERT(VARCHAR, @ContentID) + '.' + RIGHT(ContentPath, LEN(ContentPath) - LEN(@SourcePath))
    WHERE pkID IN (SELECT fkChildID FROM tblTree WHERE fkParentID = @ContentID) /* Where Content is below source */    
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[netContentCreateLanguage]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netContentCreateLanguage]
(
	@ContentID			INT,
	@WorkContentID		INT,
	@UserName NVARCHAR(255),
	@MaxVersions	INT = NULL,
	@SavedDate		DATETIME = NULL,
	@LanguageBranch	NCHAR(17)
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	DECLARE @LangBranchID		INT
	IF @SavedDate IS NULL
		SET @SavedDate = GetDate()
	SELECT @LangBranchID = pkID FROM tblLanguageBranch WHERE LanguageID=@LanguageBranch
	IF @LangBranchID IS NULL
	BEGIN
		RAISERROR (N'netContentCreateLanguage: LanguageBranchID is null, possibly empty table tblLanguageBranch', 16, 1, @WorkContentID)
		RETURN 0
	END
	IF NOT EXISTS( SELECT * FROM tblContentLanguage WHERE fkContentID=@ContentID )
		UPDATE tblContent SET fkMasterLanguageBranchID=@LangBranchID WHERE pkID=@ContentID
	INSERT INTO tblContentLanguage(fkContentID, CreatorName, ChangedByName, PendingPublish, fkLanguageBranchID)
	SELECT @ContentID, @UserName, @UserName, 1, @LangBranchID
	FROM tblContent
	INNER JOIN tblContentType ON tblContentType.pkID=tblContent.fkContentTypeID
	WHERE tblContent.pkID=@ContentID
	INSERT INTO tblWorkContent
		(fkContentID,
		ChangedByName,
		ContentLinkGUID,
		fkFrameID,
		ArchiveContentGUID,
		Name,
		LinkURL,
		ExternalURL,
		VisibleInMenu,
		LinkType,
		Created,
		Saved,
		StartPublish,
		StopPublish,
		ChildOrderRule,
		PeerOrder,
		ReadyToPublish,
		fkLanguageBranchID,
		CommonDraft)
	SELECT 
		@ContentID,
		COALESCE(@UserName, tblContentLanguage.CreatorName),
		tblContentLanguage.ContentLinkGUID,
		tblContentLanguage.fkFrameID,
		tblContent.ArchiveContentGUID,
		tblContentLanguage.Name,
		tblContentLanguage.LinkURL,
		tblContentLanguage.ExternalURL,
		tblContent.VisibleInMenu,
		CASE tblContentLanguage.AutomaticLink 
			WHEN 1 THEN 
				(CASE
					WHEN tblContentLanguage.ContentLinkGUID IS NULL THEN 0	/* EPnLinkNormal */
					WHEN tblContentLanguage.FetchData=1 THEN 4				/* EPnLinkFetchdata */
					ELSE 1												/* EPnLinkShortcut */
				END)
			ELSE
				(CASE 
					WHEN tblContentLanguage.LinkURL=N'#' THEN 3			/* EPnLinkInactive */
					ELSE 2												/* EPnLinkExternal */
				END)
		END AS LinkType ,
		tblContentLanguage.Created,
		@SavedDate,
		tblContentLanguage.StartPublish,
		tblContentLanguage.StopPublish,
		tblContent.ChildOrderRule,
		tblContent.PeerOrder,
		0,
		@LangBranchID,
		1
	FROM tblContentLanguage
	INNER JOIN tblContent ON tblContent.pkID=tblContentLanguage.fkContentID
	WHERE 
		tblContentLanguage.fkContentID=@ContentID AND tblContentLanguage.fkLanguageBranchID=@LangBranchID
	RETURN  SCOPE_IDENTITY() 
END
GO
/****** Object:  StoredProcedure [dbo].[netContentChildrenReferences]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netContentChildrenReferences]
(
	@ParentID INT,
	@LanguageID NCHAR(17),
	@ChildOrderRule INT OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON
/*	
		/// <summary>
		/// Most recently created page will be first in list
		/// </summary>
		CreatedDescending		= 1,
		/// <summary>
		/// Oldest created page will be first in list
		/// </summary>
		CreatedAscending		= 2,
		/// <summary>
		/// Sorted alphabetical on name
		/// </summary>
		Alphabetical			= 3,
		/// <summary>
		/// Sorted on page index
		/// </summary>
		Index					= 4,
		/// <summary>
		/// Most recently changed page will be first in list
		/// </summary>
		ChangedDescending		= 5,
		/// <summary>
		/// Sort on ranking, only supported by special controls
		/// </summary>
		Rank					= 6,
		/// <summary>
		/// Oldest published page will be first in list
		/// </summary>
		PublishedAscending		= 7,
		/// <summary>
		/// Most recently published page will be first in list
		/// </summary>
		PublishedDescending		= 8
*/
	SELECT @ChildOrderRule = ChildOrderRule FROM tblContent WHERE pkID=@ParentID
	IF (@ChildOrderRule = 1)
	BEGIN
		SELECT
			pkID AS ContentLinkID, ContentType
		FROM 
			tblContent
		INNER JOIN
			tblContentLanguage ON tblContentLanguage.fkContentID=tblContent.pkID
		WHERE 
			fkParentID=@ParentID AND tblContent.fkMasterLanguageBranchID=tblContentLanguage.fkLanguageBranchID
		ORDER BY 
			Created DESC,ContentLinkID DESC
		RETURN @@ROWCOUNT
	END
	IF (@ChildOrderRule = 2)
	BEGIN
		SELECT
			pkID AS ContentLinkID, ContentType
		FROM 
			tblContent
		INNER JOIN
			tblContentLanguage ON tblContentLanguage.fkContentID=tblContent.pkID
		WHERE 
			fkParentID=@ParentID AND tblContent.fkMasterLanguageBranchID=tblContentLanguage.fkLanguageBranchID
		ORDER BY 
			Created ASC,ContentLinkID ASC
		RETURN @@ROWCOUNT
	END
	IF (@ChildOrderRule = 3)
	BEGIN
		-- Get language branch for listing since we want to sort on name
		DECLARE @LanguageBranchID INT
		SELECT 
			@LanguageBranchID = pkID 
		FROM 
			tblLanguageBranch 
		WHERE 
			LOWER(LanguageID)=LOWER(@LanguageID)
		-- If we did not find a valid language branch, go with master language branch from tblContent
		IF (@@ROWCOUNT < 1)
		BEGIN
			SELECT
				pkID AS ContentLinkID, ContentType
			FROM 
				tblContent
			INNER JOIN
				tblContentLanguage ON tblContentLanguage.fkContentID=tblContent.pkID
			WHERE 
				fkParentID=@ParentID AND tblContent.fkMasterLanguageBranchID=tblContentLanguage.fkLanguageBranchID
			ORDER BY 
				Name ASC
		    RETURN @@ROWCOUNT
		END
		SELECT
			P.pkID AS ContentLinkID, ContentType
		FROM 
			tblContent AS P
		LEFT JOIN
			tblContentLanguage AS PL ON PL.fkContentID=P.pkID AND 
			PL.fkLanguageBranchID=@LanguageBranchID
		WHERE 
			P.fkParentID=@ParentID
		ORDER BY 
			PL.Name ASC
		RETURN @@ROWCOUNT
	END
	IF (@ChildOrderRule = 4)
	BEGIN
		SELECT
			pkID AS ContentLinkID, ContentType
		FROM 
			tblContent
		WHERE 
			fkParentID=@ParentID
		ORDER BY 
			PeerOrder ASC
		RETURN @@ROWCOUNT
	END
	IF (@ChildOrderRule = 5)
	BEGIN
		SELECT
			pkID AS ContentLinkID, ContentType
		FROM 
			tblContent
		INNER JOIN
			tblContentLanguage ON tblContentLanguage.fkContentID=tblContent.pkID
		WHERE 
			fkParentID=@ParentID AND tblContent.fkMasterLanguageBranchID=tblContentLanguage.fkLanguageBranchID
		ORDER BY 
			Changed DESC
		RETURN @@ROWCOUNT
	END
	IF (@ChildOrderRule = 7)
	BEGIN
		SELECT
			pkID AS ContentLinkID, ContentType
		FROM 
			tblContent
		INNER JOIN
			tblContentLanguage ON tblContentLanguage.fkContentID=tblContent.pkID
		WHERE 
			fkParentID=@ParentID AND tblContent.fkMasterLanguageBranchID=tblContentLanguage.fkLanguageBranchID
		ORDER BY 
			StartPublish ASC
		RETURN @@ROWCOUNT
	END
	IF (@ChildOrderRule = 8)
	BEGIN
		SELECT
			pkID AS ContentLinkID, ContentType
		FROM 
			tblContent
		INNER JOIN
			tblContentLanguage ON tblContentLanguage.fkContentID=tblContent.pkID
		WHERE 
			fkParentID=@ParentID AND tblContent.fkMasterLanguageBranchID=tblContentLanguage.fkLanguageBranchID
		ORDER BY 
			StartPublish DESC
		RETURN @@ROWCOUNT
	END
END
GO
/****** Object:  StoredProcedure [dbo].[netContentAclList]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netContentAclList]
(
	@ContentID INT
)
AS
BEGIN
	SET NOCOUNT ON
	SELECT 
		Name,
		IsRole, 
		AccessMask
	FROM 
		tblContentAccess
	WHERE 
		fkContentID=@ContentID
	ORDER BY
		IsRole DESC,
		Name
END
GO
/****** Object:  StoredProcedure [dbo].[netContentAclDelete]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netContentAclDelete]
(
	@Name NVARCHAR(255),
	@IsRole INT,
	@ContentID INT
)
AS
BEGIN
	SET NOCOUNT ON
	DELETE FROM tblContentAccess WHERE fkContentID=@ContentID AND Name=@Name AND IsRole=@IsRole
END
GO
/****** Object:  StoredProcedure [dbo].[netContentAclChildDelete]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netContentAclChildDelete]
(
	@Name NVARCHAR(255),
	@IsRole INT,
	@ContentID	INT
)
AS
BEGIN
    SET NOCOUNT ON
    IF (@Name IS NULL)
    BEGIN
        /* Remove all old ACEs for @ContentID and below */
        DELETE FROM 
           tblContentAccess
        WHERE 
            fkContentID IN (SELECT fkChildID FROM tblTree WHERE fkParentID=@ContentID) OR 
            fkContentID=@ContentID
        RETURN
    END
    /* Remove all old ACEs for @ContentID and below refering to @Name / @IsRole */
    DELETE FROM 
       tblContentAccess
    WHERE 
        (fkContentID IN (SELECT fkChildID FROM tblTree WHERE fkParentID=@ContentID) OR fkContentID=@ContentID) 
        AND Name=@Name
        AND IsRole=@IsRole
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]
	@ApplicationName  nvarchar(256),
	@UserNames		  nvarchar(4000),
	@RoleNames		  nvarchar(4000)
AS
BEGIN
	DECLARE @AppId uniqueidentifier
	SELECT  @AppId = NULL
	SELECT  @AppId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
	IF (@AppId IS NULL)
		RETURN(2)


	DECLARE @TranStarted   bit
	SET @TranStarted = 0

	IF( @@TRANCOUNT = 0 )
	BEGIN
		BEGIN TRANSACTION
		SET @TranStarted = 1
	END

	DECLARE @tbNames  table(Name nvarchar(256) NOT NULL PRIMARY KEY)
	DECLARE @tbRoles  table(RoleId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @tbUsers  table(UserId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @Num	  int
	DECLARE @Pos	  int
	DECLARE @NextPos  int
	DECLARE @Name	  nvarchar(256)
	DECLARE @CountAll int
	DECLARE @CountU	  int
	DECLARE @CountR	  int


	SET @Num = 0
	SET @Pos = 1
	WHILE(@Pos <= LEN(@RoleNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N',', @RoleNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@RoleNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@RoleNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbRoles
	  SELECT RoleId
	  FROM   dbo.aspnet_Roles ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredRoleName AND ar.ApplicationId = @AppId
	SELECT @CountR = @@ROWCOUNT

	IF (@CountR <> @Num)
	BEGIN
		SELECT TOP 1 N'', Name
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT ar.LoweredRoleName FROM dbo.aspnet_Roles ar,  @tbRoles r WHERE r.RoleId = ar.RoleId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(2)
	END


	DELETE FROM @tbNames WHERE 1=1
	SET @Num = 0
	SET @Pos = 1


	WHILE(@Pos <= LEN(@UserNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N',', @UserNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@UserNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@UserNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbUsers
	  SELECT UserId
	  FROM   dbo.aspnet_Users ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredUserName AND ar.ApplicationId = @AppId

	SELECT @CountU = @@ROWCOUNT
	IF (@CountU <> @Num)
	BEGIN
		SELECT TOP 1 Name, N''
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT au.LoweredUserName FROM dbo.aspnet_Users au,  @tbUsers u WHERE u.UserId = au.UserId)

		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(1)
	END

	SELECT  @CountAll = COUNT(*)
	FROM	dbo.aspnet_UsersInRoles ur, @tbUsers u, @tbRoles r
	WHERE   ur.UserId = u.UserId AND ur.RoleId = r.RoleId

	IF (@CountAll <> @CountU * @CountR)
	BEGIN
		SELECT TOP 1 UserName, RoleName
		FROM		 @tbUsers tu, @tbRoles tr, dbo.aspnet_Users u, dbo.aspnet_Roles r
		WHERE		 u.UserId = tu.UserId AND r.RoleId = tr.RoleId AND
					 tu.UserId NOT IN (SELECT ur.UserId FROM dbo.aspnet_UsersInRoles ur WHERE ur.RoleId = tr.RoleId) AND
					 tr.RoleId NOT IN (SELECT ur.RoleId FROM dbo.aspnet_UsersInRoles ur WHERE ur.UserId = tu.UserId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(3)
	END

	DELETE FROM dbo.aspnet_UsersInRoles
	WHERE UserId IN (SELECT UserId FROM @tbUsers)
	  AND RoleId IN (SELECT RoleId FROM @tbRoles)
	IF( @TranStarted = 1 )
		COMMIT TRANSACTION
	RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_IsUserInRole]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_IsUserInRole]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(2)
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    DECLARE @RoleId uniqueidentifier
    SELECT  @RoleId = NULL

    SELECT  @UserId = UserId
    FROM    dbo.aspnet_Users
    WHERE   LoweredUserName = LOWER(@UserName) AND ApplicationId = @ApplicationId

    IF (@UserId IS NULL)
        RETURN(2)

    SELECT  @RoleId = RoleId
    FROM    dbo.aspnet_Roles
    WHERE   LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId

    IF (@RoleId IS NULL)
        RETURN(3)

    IF (EXISTS( SELECT * FROM dbo.aspnet_UsersInRoles WHERE  UserId = @UserId AND RoleId = @RoleId))
        RETURN(1)
    ELSE
        RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_GetUsersInRoles]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_GetUsersInRoles]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
     DECLARE @RoleId uniqueidentifier
     SELECT  @RoleId = NULL

     SELECT  @RoleId = RoleId
     FROM    dbo.aspnet_Roles
     WHERE   LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId

     IF (@RoleId IS NULL)
         RETURN(1)

    SELECT u.UserName
    FROM   dbo.aspnet_Users u, dbo.aspnet_UsersInRoles ur
    WHERE  u.UserId = ur.UserId AND @RoleId = ur.RoleId AND u.ApplicationId = @ApplicationId
    ORDER BY u.UserName
    RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_GetRolesForUser]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_GetRolesForUser]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL

    SELECT  @UserId = UserId
    FROM    dbo.aspnet_Users
    WHERE   LoweredUserName = LOWER(@UserName) AND ApplicationId = @ApplicationId

    IF (@UserId IS NULL)
        RETURN(1)

    SELECT r.RoleName
    FROM   dbo.aspnet_Roles r, dbo.aspnet_UsersInRoles ur
    WHERE  r.RoleId = ur.RoleId AND r.ApplicationId = @ApplicationId AND ur.UserId = @UserId
    ORDER BY r.RoleName
    RETURN (0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_FindUsersInRole]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_FindUsersInRole]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256),
    @UserNameToMatch  nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
     DECLARE @RoleId uniqueidentifier
     SELECT  @RoleId = NULL

     SELECT  @RoleId = RoleId
     FROM    dbo.aspnet_Roles
     WHERE   LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId

     IF (@RoleId IS NULL)
         RETURN(1)

    SELECT u.UserName
    FROM   dbo.aspnet_Users u, dbo.aspnet_UsersInRoles ur
    WHERE  u.UserId = ur.UserId AND @RoleId = ur.RoleId AND u.ApplicationId = @ApplicationId AND LoweredUserName LIKE LOWER(@UserNameToMatch)
    ORDER BY u.UserName
    RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_AddUsersToRoles]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_AddUsersToRoles]
	@ApplicationName  nvarchar(256),
	@UserNames		  nvarchar(4000),
	@RoleNames		  nvarchar(4000),
	@CurrentTimeUtc   datetime
AS
BEGIN
	DECLARE @AppId uniqueidentifier
	SELECT  @AppId = NULL
	SELECT  @AppId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
	IF (@AppId IS NULL)
		RETURN(2)
	DECLARE @TranStarted   bit
	SET @TranStarted = 0

	IF( @@TRANCOUNT = 0 )
	BEGIN
		BEGIN TRANSACTION
		SET @TranStarted = 1
	END

	DECLARE @tbNames	table(Name nvarchar(256) NOT NULL PRIMARY KEY)
	DECLARE @tbRoles	table(RoleId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @tbUsers	table(UserId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @Num		int
	DECLARE @Pos		int
	DECLARE @NextPos	int
	DECLARE @Name		nvarchar(256)

	SET @Num = 0
	SET @Pos = 1
	WHILE(@Pos <= LEN(@RoleNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N',', @RoleNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@RoleNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@RoleNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbRoles
	  SELECT RoleId
	  FROM   dbo.aspnet_Roles ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredRoleName AND ar.ApplicationId = @AppId

	IF (@@ROWCOUNT <> @Num)
	BEGIN
		SELECT TOP 1 Name
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT ar.LoweredRoleName FROM dbo.aspnet_Roles ar,  @tbRoles r WHERE r.RoleId = ar.RoleId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(2)
	END

	DELETE FROM @tbNames WHERE 1=1
	SET @Num = 0
	SET @Pos = 1

	WHILE(@Pos <= LEN(@UserNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N',', @UserNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@UserNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@UserNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbUsers
	  SELECT UserId
	  FROM   dbo.aspnet_Users ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredUserName AND ar.ApplicationId = @AppId

	IF (@@ROWCOUNT <> @Num)
	BEGIN
		DELETE FROM @tbNames
		WHERE LOWER(Name) IN (SELECT LoweredUserName FROM dbo.aspnet_Users au,  @tbUsers u WHERE au.UserId = u.UserId)

		INSERT dbo.aspnet_Users (ApplicationId, UserId, UserName, LoweredUserName, IsAnonymous, LastActivityDate)
		  SELECT @AppId, NEWID(), Name, LOWER(Name), 0, @CurrentTimeUtc
		  FROM   @tbNames

		INSERT INTO @tbUsers
		  SELECT  UserId
		  FROM	dbo.aspnet_Users au, @tbNames t
		  WHERE   LOWER(t.Name) = au.LoweredUserName AND au.ApplicationId = @AppId
	END

	IF (EXISTS (SELECT * FROM dbo.aspnet_UsersInRoles ur, @tbUsers tu, @tbRoles tr WHERE tu.UserId = ur.UserId AND tr.RoleId = ur.RoleId))
	BEGIN
		SELECT TOP 1 UserName, RoleName
		FROM		 dbo.aspnet_UsersInRoles ur, @tbUsers tu, @tbRoles tr, aspnet_Users u, aspnet_Roles r
		WHERE		u.UserId = tu.UserId AND r.RoleId = tr.RoleId AND tu.UserId = ur.UserId AND tr.RoleId = ur.RoleId

		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(3)
	END

	INSERT INTO dbo.aspnet_UsersInRoles (UserId, RoleId)
	SELECT UserId, RoleId
	FROM @tbUsers, @tbRoles

	IF( @TranStarted = 1 )
		COMMIT TRANSACTION
	RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_DeleteUser]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Users_DeleteUser]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @TablesToDeleteFrom int,
    @NumTablesDeletedFrom int OUTPUT
AS
BEGIN
    DECLARE @UserId               uniqueidentifier
    SELECT  @UserId               = NULL
    SELECT  @NumTablesDeletedFrom = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
	SET @TranStarted = 0

    DECLARE @ErrorCode   int
    DECLARE @RowCount    int

    SET @ErrorCode = 0
    SET @RowCount  = 0

    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a
    WHERE   u.LoweredUserName       = LOWER(@UserName)
        AND u.ApplicationId         = a.ApplicationId
        AND LOWER(@ApplicationName) = a.LoweredApplicationName

    IF (@UserId IS NULL)
    BEGIN
        GOTO Cleanup
    END

    -- Delete from Membership table if (@TablesToDeleteFrom & 1) is set
    IF ((@TablesToDeleteFrom & 1) <> 0 AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_MembershipUsers') AND (type = 'V'))))
    BEGIN
        DELETE FROM dbo.aspnet_Membership WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
               @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_UsersInRoles table if (@TablesToDeleteFrom & 2) is set
    IF ((@TablesToDeleteFrom & 2) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_UsersInRoles') AND (type = 'V'))) )
    BEGIN
        DELETE FROM dbo.aspnet_UsersInRoles WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_Profile table if (@TablesToDeleteFrom & 4) is set
    IF ((@TablesToDeleteFrom & 4) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_Profiles') AND (type = 'V'))) )
    BEGIN
        DELETE FROM dbo.aspnet_Profile WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_PersonalizationPerUser table if (@TablesToDeleteFrom & 8) is set
    IF ((@TablesToDeleteFrom & 8) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_WebPartState_User') AND (type = 'V'))) )
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationPerUser WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_Users table if (@TablesToDeleteFrom & 1,2,4 & 8) are all set
    IF ((@TablesToDeleteFrom & 1) <> 0 AND
        (@TablesToDeleteFrom & 2) <> 0 AND
        (@TablesToDeleteFrom & 4) <> 0 AND
        (@TablesToDeleteFrom & 8) <> 0 AND
        (EXISTS (SELECT UserId FROM dbo.aspnet_Users WHERE @UserId = UserId)))
    BEGIN
        DELETE FROM dbo.aspnet_Users WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    IF( @TranStarted = 1 )
    BEGIN
	    SET @TranStarted = 0
	    COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:
    SET @NumTablesDeletedFrom = 0

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
	    ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_DeleteRole]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Roles_DeleteRole]
    @ApplicationName            nvarchar(256),
    @RoleName                   nvarchar(256),
    @DeleteOnlyIfRoleIsEmpty    bit
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
        SET @TranStarted = 0

    DECLARE @RoleId   uniqueidentifier
    SELECT  @RoleId = NULL
    SELECT  @RoleId = RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId

    IF (@RoleId IS NULL)
    BEGIN
        SELECT @ErrorCode = 1
        GOTO Cleanup
    END
    IF (@DeleteOnlyIfRoleIsEmpty <> 0)
    BEGIN
        IF (EXISTS (SELECT RoleId FROM dbo.aspnet_UsersInRoles  WHERE @RoleId = RoleId))
        BEGIN
            SELECT @ErrorCode = 2
            GOTO Cleanup
        END
    END


    DELETE FROM dbo.aspnet_UsersInRoles  WHERE @RoleId = RoleId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    DELETE FROM dbo.aspnet_Roles WHERE @RoleId = RoleId  AND ApplicationId = @ApplicationId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        COMMIT TRANSACTION
    END

    RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode
END
GO
/****** Object:  StoredProcedure [dbo].[editCheckOutContentVersion]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editCheckOutContentVersion]
(
	@WorkContentID INT,
	@UserName NVARCHAR(255)
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	/* Make sure that the content we try to check in is really checked in */
	UPDATE 
		tblWorkContent
	SET
		ReadyToPublish=0,
		Rejected=0,
		NewStatusByName=@UserName
	WHERE
		pkID=@WorkContentID AND 
		ReadyToPublish=1
	IF (@@ROWCOUNT = 0)
		RETURN 1
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[editCheckInContentVersion]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editCheckInContentVersion]
(
	@WorkContentID INT,
	@UserName NVARCHAR(255),
	@DelayedPublish BIT,
	@Saved DATETIME
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	/* Make sure that the content we try to check in is really checked out */
	UPDATE 
		tblWorkContent
	SET
		ReadyToPublish = 1,
		Rejected = 0,
		NewStatusByName = @UserName,
		DelayedPublish = @DelayedPublish,
		Saved = @Saved 
	WHERE
		pkID = @WorkContentID
	IF (@@ROWCOUNT = 0)
		RETURN 1
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_DeleteInactiveProfiles]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Profile_DeleteInactiveProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @InactiveSinceDate      datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
    BEGIN
        SELECT  0
        RETURN
    END

    DELETE
    FROM    dbo.aspnet_Profile
    WHERE   UserId IN
            (   SELECT  UserId
                FROM    dbo.aspnet_Users u
                WHERE   ApplicationId = @ApplicationId
                        AND (LastActivityDate <= @InactiveSinceDate)
                        AND (
                                (@ProfileAuthOptions = 2)
                             OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                             OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
                            )
            )

    SELECT  @@ROWCOUNT
END
GO
/****** Object:  StoredProcedure [dbo].[editRejectCheckedInContentVersion]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editRejectCheckedInContentVersion]
(
	@WorkContentID INT,
	@UserName NVARCHAR(255),
	@RejectComment NVARCHAR(2000),
	@Saved DATETIME
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	/* Make sure that the page we try to reject in is really checked in */
	UPDATE 
		tblWorkContent
	SET
		ReadyToPublish=0,
		Rejected=1,
		NewStatusByName=@UserName,
		RejectComment=@RejectComment,
		Saved = @Saved
	WHERE
		pkID=@WorkContentID AND 
		ReadyToPublish=1
	IF (@@ROWCOUNT = 0)
		RETURN 1
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[editSetCommonDraftVersion]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editSetCommonDraftVersion]
(
	@WorkContentID INT,
	@Force BIT
)
AS
BEGIN
   SET NOCOUNT ON
	SET XACT_ABORT ON
	DECLARE  @ContentLink INT
	DECLARE  @LangID INT
	DECLARE  @CommonDraft INT
	-- Find the ConntentLink and Language for the Page Work ID 
	SELECT   @ContentLink = fkContentID, @LangID = fkLanguageBranchID, @CommonDraft = CommonDraft from tblWorkContent where pkID = @WorkContentID
	-- If the force flag or there is a common draft which is published we will reset the common draft
	if (@Force = 1 OR EXISTS(SELECT * FROM tblWorkContent WHERE fkContentID = @ContentLink AND fkLanguageBranchID = @LangID AND HasBeenPublished = 1 AND CommonDraft = 1))
	BEGIN 	
		-- We should remove the old common draft from other content version repect to language
		UPDATE 
			tblWorkContent
		SET
			CommonDraft = 0
		WHERE
			fkContentID = @ContentLink and fkLanguageBranchID  = @LangID  
	END
	-- If the forct flag or there is no common draft for the page wirh respect to language
	IF (@Force = 1 OR NOT EXISTS(SELECT * from tblWorkContent where fkContentID = @ContentLink AND fkLanguageBranchID = @LangID AND CommonDraft = 1))
	BEGIN
		UPDATE 
			tblWorkContent
		SET
			CommonDraft = 1
		WHERE
			pkID = @WorkContentID
	END	
	IF (@@ROWCOUNT = 0)
		RETURN 1
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUserInfo]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_UpdateUserInfo]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @IsPasswordCorrect              bit,
    @UpdateLastLoginActivityDate    bit,
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int,
    @CurrentTimeUtc                 datetime,
    @LastLoginDate                  datetime,
    @LastActivityDate               datetime
AS
BEGIN
    DECLARE @UserId                                 uniqueidentifier
    DECLARE @IsApproved                             bit
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId,
            @IsApproved = m.IsApproved,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m WITH ( UPDLOCK )
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF ( @@rowcount = 0 )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    IF( @IsLockedOut = 1 )
    BEGIN
        GOTO Cleanup
    END

    IF( @IsPasswordCorrect = 0 )
    BEGIN
        IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAttemptWindowStart ) )
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = 1
        END
        ELSE
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = @FailedPasswordAttemptCount + 1
        END

        BEGIN
            IF( @FailedPasswordAttemptCount >= @MaxInvalidPasswordAttempts )
            BEGIN
                SET @IsLockedOut = 1
                SET @LastLockoutDate = @CurrentTimeUtc
            END
        END
    END
    ELSE
    BEGIN
        IF( @FailedPasswordAttemptCount > 0 OR @FailedPasswordAnswerAttemptCount > 0 )
        BEGIN
            SET @FailedPasswordAttemptCount = 0
            SET @FailedPasswordAttemptWindowStart = CONVERT( datetime, '17540101', 112 )
            SET @FailedPasswordAnswerAttemptCount = 0
            SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 )
            SET @LastLockoutDate = CONVERT( datetime, '17540101', 112 )
        END
    END

    IF( @UpdateLastLoginActivityDate = 1 )
    BEGIN
        UPDATE  dbo.aspnet_Users
        SET     LastActivityDate = @LastActivityDate
        WHERE   @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END

        UPDATE  dbo.aspnet_Membership
        SET     LastLoginDate = @LastLoginDate
        WHERE   UserId = @UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END


    UPDATE dbo.aspnet_Membership
    SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
        FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
        FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
        FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
        FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
    WHERE @UserId = UserId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUser]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_UpdateUser]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @Email                nvarchar(256),
    @Comment              ntext,
    @IsApproved           bit,
    @LastLoginDate        datetime,
    @LastActivityDate     datetime,
    @UniqueEmail          int,
    @CurrentTimeUtc       datetime
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId, @ApplicationId = a.ApplicationId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF (@UserId IS NULL)
        RETURN(1)

    IF (@UniqueEmail = 1)
    BEGIN
        IF (EXISTS (SELECT *
                    FROM  dbo.aspnet_Membership WITH (UPDLOCK, HOLDLOCK)
                    WHERE ApplicationId = @ApplicationId  AND @UserId <> UserId AND LoweredEmail = LOWER(@Email)))
        BEGIN
            RETURN(7)
        END
    END

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
	SET @TranStarted = 0

    UPDATE dbo.aspnet_Users WITH (ROWLOCK)
    SET
         LastActivityDate = @LastActivityDate
    WHERE
       @UserId = UserId

    IF( @@ERROR <> 0 )
        GOTO Cleanup

    UPDATE dbo.aspnet_Membership WITH (ROWLOCK)
    SET
         Email            = @Email,
         LoweredEmail     = LOWER(@Email),
         Comment          = @Comment,
         IsApproved       = @IsApproved,
         LastLoginDate    = @LastLoginDate
    WHERE
       @UserId = UserId

    IF( @@ERROR <> 0 )
        GOTO Cleanup

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN -1
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UnlockUser]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_UnlockUser]
    @ApplicationName                         nvarchar(256),
    @UserName                                nvarchar(256)
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF ( @UserId IS NULL )
        RETURN 1

    UPDATE dbo.aspnet_Membership
    SET IsLockedOut = 0,
        FailedPasswordAttemptCount = 0,
        FailedPasswordAttemptWindowStart = CONVERT( datetime, '17540101', 112 ),
        FailedPasswordAnswerAttemptCount = 0,
        FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 ),
        LastLockoutDate = CONVERT( datetime, '17540101', 112 )
    WHERE @UserId = UserId

    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_SetPassword]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_SetPassword]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @NewPassword      nvarchar(128),
    @PasswordSalt     nvarchar(128),
    @CurrentTimeUtc   datetime,
    @PasswordFormat   int = 0
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF (@UserId IS NULL)
        RETURN(1)

    UPDATE dbo.aspnet_Membership
    SET Password = @NewPassword, PasswordFormat = @PasswordFormat, PasswordSalt = @PasswordSalt,
        LastPasswordChangedDate = @CurrentTimeUtc
    WHERE @UserId = UserId
    RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ResetPassword]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_ResetPassword]
    @ApplicationName             nvarchar(256),
    @UserName                    nvarchar(256),
    @NewPassword                 nvarchar(128),
    @MaxInvalidPasswordAttempts  int,
    @PasswordAttemptWindow       int,
    @PasswordSalt                nvarchar(128),
    @CurrentTimeUtc              datetime,
    @PasswordFormat              int = 0,
    @PasswordAnswer              nvarchar(128) = NULL
AS
BEGIN
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @UserId                                 uniqueidentifier
    SET     @UserId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF ( @UserId IS NULL )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    SELECT @IsLockedOut = IsLockedOut,
           @LastLockoutDate = LastLockoutDate,
           @FailedPasswordAttemptCount = FailedPasswordAttemptCount,
           @FailedPasswordAttemptWindowStart = FailedPasswordAttemptWindowStart,
           @FailedPasswordAnswerAttemptCount = FailedPasswordAnswerAttemptCount,
           @FailedPasswordAnswerAttemptWindowStart = FailedPasswordAnswerAttemptWindowStart
    FROM dbo.aspnet_Membership WITH ( UPDLOCK )
    WHERE @UserId = UserId

    IF( @IsLockedOut = 1 )
    BEGIN
        SET @ErrorCode = 99
        GOTO Cleanup
    END

    UPDATE dbo.aspnet_Membership
    SET    Password = @NewPassword,
           LastPasswordChangedDate = @CurrentTimeUtc,
           PasswordFormat = @PasswordFormat,
           PasswordSalt = @PasswordSalt
    WHERE  @UserId = UserId AND
           ( ( @PasswordAnswer IS NULL ) OR ( LOWER( PasswordAnswer ) = LOWER( @PasswordAnswer ) ) )

    IF ( @@ROWCOUNT = 0 )
        BEGIN
            IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAnswerAttemptWindowStart ) )
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = 1
            END
            ELSE
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount + 1
            END

            BEGIN
                IF( @FailedPasswordAnswerAttemptCount >= @MaxInvalidPasswordAttempts )
                BEGIN
                    SET @IsLockedOut = 1
                    SET @LastLockoutDate = @CurrentTimeUtc
                END
            END

            SET @ErrorCode = 3
        END
    ELSE
        BEGIN
            IF( @FailedPasswordAnswerAttemptCount > 0 )
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = 0
                SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 )
            END
        END

    IF( NOT ( @PasswordAnswer IS NULL ) )
    BEGIN
        UPDATE dbo.aspnet_Membership
        SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
            FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
            FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
            FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
            FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
        WHERE @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByUserId]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByUserId]
    @UserId               uniqueidentifier,
    @CurrentTimeUtc       datetime,
    @UpdateLastActivity   bit = 0
AS
BEGIN
    IF ( @UpdateLastActivity = 1 )
    BEGIN
        UPDATE   dbo.aspnet_Users
        SET      LastActivityDate = @CurrentTimeUtc
        FROM     dbo.aspnet_Users
        WHERE    @UserId = UserId

        IF ( @@ROWCOUNT = 0 ) -- User ID not found
            RETURN -1
    END

    SELECT  m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate, m.LastLoginDate, u.LastActivityDate,
            m.LastPasswordChangedDate, u.UserName, m.IsLockedOut,
            m.LastLockoutDate
    FROM    dbo.aspnet_Users u, dbo.aspnet_Membership m
    WHERE   @UserId = u.UserId AND u.UserId = m.UserId

    IF ( @@ROWCOUNT = 0 ) -- User ID not found
       RETURN -1

    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByName]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByName]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @CurrentTimeUtc       datetime,
    @UpdateLastActivity   bit = 0
AS
BEGIN
    DECLARE @UserId uniqueidentifier

    IF (@UpdateLastActivity = 1)
    BEGIN
        -- select user ID from aspnet_users table
        SELECT TOP 1 @UserId = u.UserId
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE    LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                LOWER(@UserName) = u.LoweredUserName AND u.UserId = m.UserId

        IF (@@ROWCOUNT = 0) -- Username not found
            RETURN -1

        UPDATE   dbo.aspnet_Users
        SET      LastActivityDate = @CurrentTimeUtc
        WHERE    @UserId = UserId

        SELECT m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
                m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate,
                u.UserId, m.IsLockedOut, m.LastLockoutDate
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE  @UserId = u.UserId AND u.UserId = m.UserId 
    END
    ELSE
    BEGIN
        SELECT TOP 1 m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
                m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate,
                u.UserId, m.IsLockedOut,m.LastLockoutDate
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE    LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                LOWER(@UserName) = u.LoweredUserName AND u.UserId = m.UserId

        IF (@@ROWCOUNT = 0) -- Username not found
            RETURN -1
    END

    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByEmail]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByEmail]
    @ApplicationName  nvarchar(256),
    @Email            nvarchar(256)
AS
BEGIN
    IF( @Email IS NULL )
        SELECT  u.UserName
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                u.UserId = m.UserId AND
                m.LoweredEmail IS NULL
    ELSE
        SELECT  u.UserName
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                u.UserId = m.UserId AND
                LOWER(@Email) = m.LoweredEmail

    IF (@@rowcount = 0)
        RETURN(1)
    RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPasswordWithFormat]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetPasswordWithFormat]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @UpdateLastLoginActivityDate    bit,
    @CurrentTimeUtc                 datetime
AS
BEGIN
    DECLARE @IsLockedOut                        bit
    DECLARE @UserId                             uniqueidentifier
    DECLARE @Password                           nvarchar(128)
    DECLARE @PasswordSalt                       nvarchar(128)
    DECLARE @PasswordFormat                     int
    DECLARE @FailedPasswordAttemptCount         int
    DECLARE @FailedPasswordAnswerAttemptCount   int
    DECLARE @IsApproved                         bit
    DECLARE @LastActivityDate                   datetime
    DECLARE @LastLoginDate                      datetime

    SELECT  @UserId          = NULL

    SELECT  @UserId = u.UserId, @IsLockedOut = m.IsLockedOut, @Password=Password, @PasswordFormat=PasswordFormat,
            @PasswordSalt=PasswordSalt, @FailedPasswordAttemptCount=FailedPasswordAttemptCount,
		    @FailedPasswordAnswerAttemptCount=FailedPasswordAnswerAttemptCount, @IsApproved=IsApproved,
            @LastActivityDate = LastActivityDate, @LastLoginDate = LastLoginDate
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF (@UserId IS NULL)
        RETURN 1

    IF (@IsLockedOut = 1)
        RETURN 99

    SELECT   @Password, @PasswordFormat, @PasswordSalt, @FailedPasswordAttemptCount,
             @FailedPasswordAnswerAttemptCount, @IsApproved, @LastLoginDate, @LastActivityDate

    IF (@UpdateLastLoginActivityDate = 1 AND @IsApproved = 1)
    BEGIN
        UPDATE  dbo.aspnet_Membership
        SET     LastLoginDate = @CurrentTimeUtc
        WHERE   UserId = @UserId

        UPDATE  dbo.aspnet_Users
        SET     LastActivityDate = @CurrentTimeUtc
        WHERE   @UserId = UserId
    END


    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPassword]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetPassword]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int,
    @CurrentTimeUtc                 datetime,
    @PasswordAnswer                 nvarchar(128) = NULL
AS
BEGIN
    DECLARE @UserId                                 uniqueidentifier
    DECLARE @PasswordFormat                         int
    DECLARE @Password                               nvarchar(128)
    DECLARE @passAns                                nvarchar(128)
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId,
            @Password = m.Password,
            @passAns = m.PasswordAnswer,
            @PasswordFormat = m.PasswordFormat,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m WITH ( UPDLOCK )
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF ( @@rowcount = 0 )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    IF( @IsLockedOut = 1 )
    BEGIN
        SET @ErrorCode = 99
        GOTO Cleanup
    END

    IF ( NOT( @PasswordAnswer IS NULL ) )
    BEGIN
        IF( ( @passAns IS NULL ) OR ( LOWER( @passAns ) <> LOWER( @PasswordAnswer ) ) )
        BEGIN
            IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAnswerAttemptWindowStart ) )
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = 1
            END
            ELSE
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount + 1
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
            END

            BEGIN
                IF( @FailedPasswordAnswerAttemptCount >= @MaxInvalidPasswordAttempts )
                BEGIN
                    SET @IsLockedOut = 1
                    SET @LastLockoutDate = @CurrentTimeUtc
                END
            END

            SET @ErrorCode = 3
        END
        ELSE
        BEGIN
            IF( @FailedPasswordAnswerAttemptCount > 0 )
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = 0
                SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 )
            END
        END

        UPDATE dbo.aspnet_Membership
        SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
            FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
            FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
            FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
            FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
        WHERE @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    IF( @ErrorCode = 0 )
        SELECT @Password, @PasswordFormat

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetNumberOfUsersOnline]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetNumberOfUsersOnline]
    @ApplicationName            nvarchar(256),
    @MinutesSinceLastInActive   int,
    @CurrentTimeUtc             datetime
AS
BEGIN
    DECLARE @DateActive datetime
    SELECT  @DateActive = DATEADD(minute,  -(@MinutesSinceLastInActive), @CurrentTimeUtc)

    DECLARE @NumOnline int
    SELECT  @NumOnline = COUNT(*)
    FROM    dbo.aspnet_Users u(NOLOCK),
            dbo.aspnet_Applications a(NOLOCK),
            dbo.aspnet_Membership m(NOLOCK)
    WHERE   u.ApplicationId = a.ApplicationId                  AND
            LastActivityDate > @DateActive                     AND
            a.LoweredApplicationName = LOWER(@ApplicationName) AND
            u.UserId = m.UserId
    RETURN(@NumOnline)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetAllUsers]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_GetAllUsers]
    @ApplicationName       nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0


    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
    SELECT u.UserId
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u
    WHERE  u.ApplicationId = @ApplicationId AND u.UserId = m.UserId
    ORDER BY u.UserName

    SELECT @TotalRecords = @@ROWCOUNT

    SELECT u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY u.UserName
    RETURN @TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByName]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_FindUsersByName]
    @ApplicationName       nvarchar(256),
    @UserNameToMatch       nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
        SELECT u.UserId
        FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND u.LoweredUserName LIKE LOWER(@UserNameToMatch)
        ORDER BY u.UserName


    SELECT  u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY u.UserName

    SELECT  @TotalRecords = COUNT(*)
    FROM    #PageIndexForUsers
    RETURN @TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByEmail]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_FindUsersByEmail]
    @ApplicationName       nvarchar(256),
    @EmailToMatch          nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    IF( @EmailToMatch IS NULL )
        INSERT INTO #PageIndexForUsers (UserId)
            SELECT u.UserId
            FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
            WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND m.Email IS NULL
            ORDER BY m.LoweredEmail
    ELSE
        INSERT INTO #PageIndexForUsers (UserId)
            SELECT u.UserId
            FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
            WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND m.LoweredEmail LIKE LOWER(@EmailToMatch)
            ORDER BY m.LoweredEmail

    SELECT  u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY m.LoweredEmail

    SELECT  @TotalRecords = COUNT(*)
    FROM    #PageIndexForUsers
    RETURN @TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_CreateUser]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_CreateUser]
    @ApplicationName                        nvarchar(256),
    @UserName                               nvarchar(256),
    @Password                               nvarchar(128),
    @PasswordSalt                           nvarchar(128),
    @Email                                  nvarchar(256),
    @PasswordQuestion                       nvarchar(256),
    @PasswordAnswer                         nvarchar(128),
    @IsApproved                             bit,
    @CurrentTimeUtc                         datetime,
    @CreateDate                             datetime = NULL,
    @UniqueEmail                            int      = 0,
    @PasswordFormat                         int      = 0,
    @UserId                                 uniqueidentifier OUTPUT
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @NewUserId uniqueidentifier
    SELECT @NewUserId = NULL

    DECLARE @IsLockedOut bit
    SET @IsLockedOut = 0

    DECLARE @LastLockoutDate  datetime
    SET @LastLockoutDate = CONVERT( datetime, '17540101', 112 )

    DECLARE @FailedPasswordAttemptCount int
    SET @FailedPasswordAttemptCount = 0

    DECLARE @FailedPasswordAttemptWindowStart  datetime
    SET @FailedPasswordAttemptWindowStart = CONVERT( datetime, '17540101', 112 )

    DECLARE @FailedPasswordAnswerAttemptCount int
    SET @FailedPasswordAnswerAttemptCount = 0

    DECLARE @FailedPasswordAnswerAttemptWindowStart  datetime
    SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 )

    DECLARE @NewUserCreated bit
    DECLARE @ReturnValue   int
    SET @ReturnValue = 0

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    SET @CreateDate = @CurrentTimeUtc

    SELECT  @NewUserId = UserId FROM dbo.aspnet_Users WHERE LOWER(@UserName) = LoweredUserName AND @ApplicationId = ApplicationId
    IF ( @NewUserId IS NULL )
    BEGIN
        SET @NewUserId = @UserId
        EXEC @ReturnValue = dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, 0, @CreateDate, @NewUserId OUTPUT
        SET @NewUserCreated = 1
    END
    ELSE
    BEGIN
        SET @NewUserCreated = 0
        IF( @NewUserId <> @UserId AND @UserId IS NOT NULL )
        BEGIN
            SET @ErrorCode = 6
            GOTO Cleanup
        END
    END

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @ReturnValue = -1 )
    BEGIN
        SET @ErrorCode = 10
        GOTO Cleanup
    END

    IF ( EXISTS ( SELECT UserId
                  FROM   dbo.aspnet_Membership
                  WHERE  @NewUserId = UserId ) )
    BEGIN
        SET @ErrorCode = 6
        GOTO Cleanup
    END

    SET @UserId = @NewUserId

    IF (@UniqueEmail = 1)
    BEGIN
        IF (EXISTS (SELECT *
                    FROM  dbo.aspnet_Membership m WITH ( UPDLOCK, HOLDLOCK )
                    WHERE ApplicationId = @ApplicationId AND LoweredEmail = LOWER(@Email)))
        BEGIN
            SET @ErrorCode = 7
            GOTO Cleanup
        END
    END

    IF (@NewUserCreated = 0)
    BEGIN
        UPDATE dbo.aspnet_Users
        SET    LastActivityDate = @CreateDate
        WHERE  @UserId = UserId
        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    INSERT INTO dbo.aspnet_Membership
                ( ApplicationId,
                  UserId,
                  Password,
                  PasswordSalt,
                  Email,
                  LoweredEmail,
                  PasswordQuestion,
                  PasswordAnswer,
                  PasswordFormat,
                  IsApproved,
                  IsLockedOut,
                  CreateDate,
                  LastLoginDate,
                  LastPasswordChangedDate,
                  LastLockoutDate,
                  FailedPasswordAttemptCount,
                  FailedPasswordAttemptWindowStart,
                  FailedPasswordAnswerAttemptCount,
                  FailedPasswordAnswerAttemptWindowStart )
         VALUES ( @ApplicationId,
                  @UserId,
                  @Password,
                  @PasswordSalt,
                  @Email,
                  LOWER(@Email),
                  @PasswordQuestion,
                  @PasswordAnswer,
                  @PasswordFormat,
                  @IsApproved,
                  @IsLockedOut,
                  @CreateDate,
                  @CreateDate,
                  @CreateDate,
                  @LastLockoutDate,
                  @FailedPasswordAttemptCount,
                  @FailedPasswordAttemptWindowStart,
                  @FailedPasswordAnswerAttemptCount,
                  @FailedPasswordAnswerAttemptWindowStart )

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
	    SET @TranStarted = 0
	    COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]
    @ApplicationName       nvarchar(256),
    @UserName              nvarchar(256),
    @NewPasswordQuestion   nvarchar(256),
    @NewPasswordAnswer     nvarchar(128)
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Membership m, dbo.aspnet_Users u, dbo.aspnet_Applications a
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId
    IF (@UserId IS NULL)
    BEGIN
        RETURN(1)
    END

    UPDATE dbo.aspnet_Membership
    SET    PasswordQuestion = @NewPasswordQuestion, PasswordAnswer = @NewPasswordAnswer
    WHERE  UserId=@UserId
    RETURN(0)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_AnyDataInTables]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_AnyDataInTables]
    @TablesToCheck int
AS
BEGIN
    -- Check Membership table if (@TablesToCheck & 1) is set
    IF ((@TablesToCheck & 1) <> 0 AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_MembershipUsers') AND (type = 'V'))))
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Membership))
        BEGIN
            SELECT N'aspnet_Membership'
            RETURN
        END
    END

    -- Check aspnet_Roles table if (@TablesToCheck & 2) is set
    IF ((@TablesToCheck & 2) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_Roles') AND (type = 'V'))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 RoleId FROM dbo.aspnet_Roles))
        BEGIN
            SELECT N'aspnet_Roles'
            RETURN
        END
    END

    -- Check aspnet_Profile table if (@TablesToCheck & 4) is set
    IF ((@TablesToCheck & 4) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_Profiles') AND (type = 'V'))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Profile))
        BEGIN
            SELECT N'aspnet_Profile'
            RETURN
        END
    END

    -- Check aspnet_PersonalizationPerUser table if (@TablesToCheck & 8) is set
    IF ((@TablesToCheck & 8) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'vw_aspnet_WebPartState_User') AND (type = 'V'))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_PersonalizationPerUser))
        BEGIN
            SELECT N'aspnet_PersonalizationPerUser'
            RETURN
        END
    END

    -- Check aspnet_PersonalizationPerUser table if (@TablesToCheck & 16) is set
    IF ((@TablesToCheck & 16) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N'aspnet_WebEvent_LogEvent') AND (type = 'P'))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 * FROM dbo.aspnet_WebEvent_Events))
        BEGIN
            SELECT N'aspnet_WebEvent_Events'
            RETURN
        END
    END

    -- Check aspnet_Users table if (@TablesToCheck & 1,2,4 & 8) are all set
    IF ((@TablesToCheck & 1) <> 0 AND
        (@TablesToCheck & 2) <> 0 AND
        (@TablesToCheck & 4) <> 0 AND
        (@TablesToCheck & 8) <> 0 AND
        (@TablesToCheck & 32) <> 0 AND
        (@TablesToCheck & 128) <> 0 AND
        (@TablesToCheck & 256) <> 0 AND
        (@TablesToCheck & 512) <> 0 AND
        (@TablesToCheck & 1024) <> 0)
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Users))
        BEGIN
            SELECT N'aspnet_Users'
            RETURN
        END
        IF (EXISTS(SELECT TOP 1 ApplicationId FROM dbo.aspnet_Applications))
        BEGIN
            SELECT N'aspnet_Applications'
            RETURN
        END
    END
END
GO
/****** Object:  StoredProcedure [dbo].[admDatabaseStatistics]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[admDatabaseStatistics]
AS
BEGIN
	SET NOCOUNT ON
	SELECT
		(SELECT Count(*) FROM tblPage) AS PageCount
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_ResetUserState]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetUserState] (
    @Count                  int                 OUT,
    @ApplicationName        NVARCHAR(256),
    @InactiveSinceDate      DATETIME            = NULL,
    @UserName               NVARCHAR(256)       = NULL,
    @Path                   NVARCHAR(256)       = NULL)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationPerUser
        WHERE Id IN (SELECT PerUser.Id
                     FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
                     WHERE Paths.ApplicationId = @ApplicationId
                           AND PerUser.UserId = Users.UserId
                           AND PerUser.PathId = Paths.PathId
                           AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
                           AND (@UserName IS NULL OR Users.LoweredUserName = LOWER(@UserName))
                           AND (@Path IS NULL OR Paths.LoweredPath = LOWER(@Path)))

        SELECT @Count = @@ROWCOUNT
    END
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_ResetSharedState]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetSharedState] (
    @Count int OUT,
    @ApplicationName NVARCHAR(256),
    @Path NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationAllUsers
        WHERE PathId IN
            (SELECT AllUsers.PathId
             FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
             WHERE Paths.ApplicationId = @ApplicationId
                   AND AllUsers.PathId = Paths.PathId
                   AND Paths.LoweredPath = LOWER(@Path))

        SELECT @Count = @@ROWCOUNT
    END
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_GetCountOfState]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_GetCountOfState] (
    @Count int OUT,
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @Path NVARCHAR(256) = NULL,
    @UserName NVARCHAR(256) = NULL,
    @InactiveSinceDate DATETIME = NULL)
AS
BEGIN

    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
        IF (@AllUsersScope = 1)
            SELECT @Count = COUNT(*)
            FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
            WHERE Paths.ApplicationId = @ApplicationId
                  AND AllUsers.PathId = Paths.PathId
                  AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
        ELSE
            SELECT @Count = COUNT(*)
            FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
            WHERE Paths.ApplicationId = @ApplicationId
                  AND PerUser.UserId = Users.UserId
                  AND PerUser.PathId = Paths.PathId
                  AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
                  AND (@UserName IS NULL OR Users.LoweredUserName LIKE LOWER(@UserName))
                  AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_FindState]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_FindState] (
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @PageIndex              INT,
    @PageSize               INT,
    @Path NVARCHAR(256) = NULL,
    @UserName NVARCHAR(256) = NULL,
    @InactiveSinceDate DATETIME = NULL)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        RETURN

    -- Set the page bounds
    DECLARE @PageLowerBound INT
    DECLARE @PageUpperBound INT
    DECLARE @TotalRecords   INT
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table to store the selected results
    CREATE TABLE #PageIndex (
        IndexId int IDENTITY (0, 1) NOT NULL,
        ItemId UNIQUEIDENTIFIER
    )

    IF (@AllUsersScope = 1)
    BEGIN
        -- Insert into our temp table
        INSERT INTO #PageIndex (ItemId)
        SELECT Paths.PathId
        FROM dbo.aspnet_Paths Paths,
             ((SELECT Paths.PathId
               FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
               WHERE Paths.ApplicationId = @ApplicationId
                      AND AllUsers.PathId = Paths.PathId
                      AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              ) AS SharedDataPerPath
              FULL OUTER JOIN
              (SELECT DISTINCT Paths.PathId
               FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Paths Paths
               WHERE Paths.ApplicationId = @ApplicationId
                      AND PerUser.PathId = Paths.PathId
                      AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              ) AS UserDataPerPath
              ON SharedDataPerPath.PathId = UserDataPerPath.PathId
             )
        WHERE Paths.PathId = SharedDataPerPath.PathId OR Paths.PathId = UserDataPerPath.PathId
        ORDER BY Paths.Path ASC

        SELECT @TotalRecords = @@ROWCOUNT

        SELECT Paths.Path,
               SharedDataPerPath.LastUpdatedDate,
               SharedDataPerPath.SharedDataLength,
               UserDataPerPath.UserDataLength,
               UserDataPerPath.UserCount
        FROM dbo.aspnet_Paths Paths,
             ((SELECT PageIndex.ItemId AS PathId,
                      AllUsers.LastUpdatedDate AS LastUpdatedDate,
                      DATALENGTH(AllUsers.PageSettings) AS SharedDataLength
               FROM dbo.aspnet_PersonalizationAllUsers AllUsers, #PageIndex PageIndex
               WHERE AllUsers.PathId = PageIndex.ItemId
                     AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
              ) AS SharedDataPerPath
              FULL OUTER JOIN
              (SELECT PageIndex.ItemId AS PathId,
                      SUM(DATALENGTH(PerUser.PageSettings)) AS UserDataLength,
                      COUNT(*) AS UserCount
               FROM aspnet_PersonalizationPerUser PerUser, #PageIndex PageIndex
               WHERE PerUser.PathId = PageIndex.ItemId
                     AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
               GROUP BY PageIndex.ItemId
              ) AS UserDataPerPath
              ON SharedDataPerPath.PathId = UserDataPerPath.PathId
             )
        WHERE Paths.PathId = SharedDataPerPath.PathId OR Paths.PathId = UserDataPerPath.PathId
        ORDER BY Paths.Path ASC
    END
    ELSE
    BEGIN
        -- Insert into our temp table
        INSERT INTO #PageIndex (ItemId)
        SELECT PerUser.Id
        FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
        WHERE Paths.ApplicationId = @ApplicationId
              AND PerUser.UserId = Users.UserId
              AND PerUser.PathId = Paths.PathId
              AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              AND (@UserName IS NULL OR Users.LoweredUserName LIKE LOWER(@UserName))
              AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
        ORDER BY Paths.Path ASC, Users.UserName ASC

        SELECT @TotalRecords = @@ROWCOUNT

        SELECT Paths.Path, PerUser.LastUpdatedDate, DATALENGTH(PerUser.PageSettings), Users.UserName, Users.LastActivityDate
        FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths, #PageIndex PageIndex
        WHERE PerUser.Id = PageIndex.ItemId
              AND PerUser.UserId = Users.UserId
              AND PerUser.PathId = Paths.PathId
              AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
        ORDER BY Paths.Path ASC, Users.UserName ASC
    END

    RETURN @TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_DeleteAllState]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_DeleteAllState] (
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @Count int OUT)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        IF (@AllUsersScope = 1)
            DELETE FROM aspnet_PersonalizationAllUsers
            WHERE PathId IN
               (SELECT Paths.PathId
                FROM dbo.aspnet_Paths Paths
                WHERE Paths.ApplicationId = @ApplicationId)
        ELSE
            DELETE FROM aspnet_PersonalizationPerUser
            WHERE PathId IN
               (SELECT Paths.PathId
                FROM dbo.aspnet_Paths Paths
                WHERE Paths.ApplicationId = @ApplicationId)

        SELECT @Count = @@ROWCOUNT
    END
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_SetPageSettings]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_SetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @PageSettings     IMAGE,
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Paths_CreatePath @ApplicationId, @Path, @PathId OUTPUT
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, 0, @CurrentTimeUtc, @UserId OUTPUT
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    IF (EXISTS(SELECT PathId FROM dbo.aspnet_PersonalizationPerUser WHERE UserId = @UserId AND PathId = @PathId))
        UPDATE dbo.aspnet_PersonalizationPerUser SET PageSettings = @PageSettings, LastUpdatedDate = @CurrentTimeUtc WHERE UserId = @UserId AND PathId = @PathId
    ELSE
        INSERT INTO dbo.aspnet_PersonalizationPerUser(UserId, PathId, PageSettings, LastUpdatedDate) VALUES (@UserId, @PathId, @PageSettings, @CurrentTimeUtc)
    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        RETURN
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    DELETE FROM dbo.aspnet_PersonalizationPerUser WHERE PathId = @PathId AND UserId = @UserId
    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_GetPageSettings]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_GetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        RETURN
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    SELECT p.PageSettings FROM dbo.aspnet_PersonalizationPerUser p WHERE p.PathId = @PathId AND p.UserId = @UserId
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path             NVARCHAR(256),
    @PageSettings     IMAGE,
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Paths_CreatePath @ApplicationId, @Path, @PathId OUTPUT
    END

    IF (EXISTS(SELECT PathId FROM dbo.aspnet_PersonalizationAllUsers WHERE PathId = @PathId))
        UPDATE dbo.aspnet_PersonalizationAllUsers SET PageSettings = @PageSettings, LastUpdatedDate = @CurrentTimeUtc WHERE PathId = @PathId
    ELSE
        INSERT INTO dbo.aspnet_PersonalizationAllUsers(PathId, PageSettings, LastUpdatedDate) VALUES (@PathId, @PageSettings, @CurrentTimeUtc)
    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path              NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    DELETE FROM dbo.aspnet_PersonalizationAllUsers WHERE PathId = @PathId
    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path              NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT p.PageSettings FROM dbo.aspnet_PersonalizationAllUsers p WHERE p.PathId = @PathId
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_SetProperties]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Profile_SetProperties]
    @ApplicationName        nvarchar(256),
    @PropertyNames          ntext,
    @PropertyValuesString   ntext,
    @PropertyValuesBinary   image,
    @UserName               nvarchar(256),
    @IsUserAnonymous        bit,
    @CurrentTimeUtc         datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
       BEGIN TRANSACTION
       SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    DECLARE @UserId uniqueidentifier
    DECLARE @LastActivityDate datetime
    SELECT  @UserId = NULL
    SELECT  @LastActivityDate = @CurrentTimeUtc

    SELECT @UserId = UserId
    FROM   dbo.aspnet_Users
    WHERE  ApplicationId = @ApplicationId AND LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
        EXEC dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, @IsUserAnonymous, @LastActivityDate, @UserId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    UPDATE dbo.aspnet_Users
    SET    LastActivityDate=@CurrentTimeUtc
    WHERE  UserId = @UserId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF (EXISTS( SELECT *
               FROM   dbo.aspnet_Profile
               WHERE  UserId = @UserId))
        UPDATE dbo.aspnet_Profile
        SET    PropertyNames=@PropertyNames, PropertyValuesString = @PropertyValuesString,
               PropertyValuesBinary = @PropertyValuesBinary, LastUpdatedDate=@CurrentTimeUtc
        WHERE  UserId = @UserId
    ELSE
        INSERT INTO dbo.aspnet_Profile(UserId, PropertyNames, PropertyValuesString, PropertyValuesBinary, LastUpdatedDate)
             VALUES (@UserId, @PropertyNames, @PropertyValuesString, @PropertyValuesBinary, @CurrentTimeUtc)

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
    	SET @TranStarted = 0
    	COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetProperties]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Profile_GetProperties]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @CurrentTimeUtc       datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN

    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL

    SELECT @UserId = UserId
    FROM   dbo.aspnet_Users
    WHERE  ApplicationId = @ApplicationId AND LoweredUserName = LOWER(@UserName)

    IF (@UserId IS NULL)
        RETURN
    SELECT TOP 1 PropertyNames, PropertyValuesString, PropertyValuesBinary
    FROM         dbo.aspnet_Profile
    WHERE        UserId = @UserId

    IF (@@ROWCOUNT > 0)
    BEGIN
        UPDATE dbo.aspnet_Users
        SET    LastActivityDate=@CurrentTimeUtc
        WHERE  UserId = @UserId
    END
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetProfiles]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Profile_GetProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @PageIndex              int,
    @PageSize               int,
    @UserNameToMatch        nvarchar(256) = NULL,
    @InactiveSinceDate      datetime      = NULL
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
        SELECT  u.UserId
        FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p
        WHERE   ApplicationId = @ApplicationId
            AND u.UserId = p.UserId
            AND (@InactiveSinceDate IS NULL OR LastActivityDate <= @InactiveSinceDate)
            AND (     (@ProfileAuthOptions = 2)
                   OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                   OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
                 )
            AND (@UserNameToMatch IS NULL OR LoweredUserName LIKE LOWER(@UserNameToMatch))
        ORDER BY UserName

    SELECT  u.UserName, u.IsAnonymous, u.LastActivityDate, p.LastUpdatedDate,
            DATALENGTH(p.PropertyNames) + DATALENGTH(p.PropertyValuesString) + DATALENGTH(p.PropertyValuesBinary)
    FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p, #PageIndexForUsers i
    WHERE   u.UserId = p.UserId AND p.UserId = i.UserId AND i.IndexId >= @PageLowerBound AND i.IndexId <= @PageUpperBound

    SELECT COUNT(*)
    FROM   #PageIndexForUsers

    DROP TABLE #PageIndexForUsers
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @InactiveSinceDate      datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
    BEGIN
        SELECT 0
        RETURN
    END

    SELECT  COUNT(*)
    FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p
    WHERE   ApplicationId = @ApplicationId
        AND u.UserId = p.UserId
        AND (LastActivityDate <= @InactiveSinceDate)
        AND (
                (@ProfileAuthOptions = 2)
                OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
            )
END
GO
/****** Object:  View [dbo].[tblPageLanguageSetting]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[tblPageLanguageSetting]
AS
SELECT
		[fkContentID] AS fkPageID,
		[fkLanguageBranchID],
		[fkReplacementBranchID],
    	[LanguageBranchFallback],
    	[Active]
FROM    dbo.tblContentLanguageSetting
GO
/****** Object:  View [dbo].[tblPageLanguage]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[tblPageLanguage]
AS
SELECT
	[fkContentID] AS fkPageID,
	[fkLanguageBranchID],
	[ContentLinkGUID] AS PageLinkGUID,
	[fkFrameID],
	[CreatorName],
    [ChangedByName],
    [ContentGUID] AS PageGUID,
    [Name],
    [URLSegment],
    [LinkURL],
    [ExternalURL],
    [AutomaticLink],
    [FetchData],
    [PendingPublish],
    [Created],
    [Changed],
    [Saved],
    [StartPublish],
    [StopPublish],
    [PublishedVersion]
FROM    dbo.tblContentLanguage
GO
/****** Object:  View [dbo].[tblProperty]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[tblProperty]
AS
SELECT
	[pkID],
	[fkPropertyDefinitionID] AS fkPageDefinitionID,
	[fkContentID] AS fkPageID,
	[fkLanguageBranchID],
	[ScopeName],
	[guid],
	[Boolean],
	[Number],
	[FloatNumber],
	[ContentType] AS PageType,
	[ContentLink] AS PageLink,
	[Date],
	[String],
	[LongString],
	[LongStringLength],
	[LinkGuid]
FROM    dbo.tblContentProperty
GO
/****** Object:  View [dbo].[tblWorkPage]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[tblWorkPage]
AS
SELECT
	[pkID],
    [fkContentID] AS fkPageID,
    [fkMasterVersionID],
    [ContentLinkGUID] AS PageLinkGUID,
    [fkFrameID],
    [ArchiveContentGUID] as ArchivePageGUID,
    [ChangedByName],
    [NewStatusByName],
    [Name],
    [URLSegment],
    [LinkURL],
    [ExternalURL],
    [VisibleInMenu],
    [LinkType],
    [Created],
    [Saved],
    [StartPublish],
    [StopPublish],
    [ChildOrderRule],
    [PeerOrder],
    [ReadyToPublish],
    [ChangedOnPublish],
    [HasBeenPublished],
    [Rejected],
    [DelayedPublish],
    [RejectComment],
    [fkLanguageBranchID],
	[CommonDraft]
FROM    dbo.tblWorkContent
GO
/****** Object:  Table [dbo].[tblWorkContentProperty]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblWorkContentProperty](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkPropertyDefinitionID] [int] NOT NULL,
	[fkWorkContentID] [int] NOT NULL,
	[ScopeName] [nvarchar](450) NULL,
	[Boolean] [bit] NOT NULL,
	[Number] [int] NULL,
	[FloatNumber] [float] NULL,
	[ContentType] [int] NULL,
	[ContentLink] [int] NULL,
	[Date] [datetime] NULL,
	[String] [nvarchar](450) NULL,
	[LongString] [nvarchar](max) NULL,
	[LinkGuid] [uniqueidentifier] NULL,
 CONSTRAINT [PK_tblWorkProperty] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblWorkContentProperty_ContentLink] ON [dbo].[tblWorkContentProperty] 
(
	[ContentLink] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblWorkContentProperty_ContentTypeID] ON [dbo].[tblWorkContentProperty] 
(
	[ContentType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblWorkContentProperty_fkPropertyDefinitionID] ON [dbo].[tblWorkContentProperty] 
(
	[fkPropertyDefinitionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblWorkContentProperty_ScopeName] ON [dbo].[tblWorkContentProperty] 
(
	[ScopeName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tblWorkContentProperty_fkWorkContentID] ON [dbo].[tblWorkContentProperty] 
(
	[fkWorkContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblWorkContentCategory]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblWorkContentCategory](
	[pkID] [int] IDENTITY(1,1) NOT NULL,
	[fkWorkContentID] [int] NOT NULL,
	[fkCategoryID] [int] NOT NULL,
	[CategoryType] [int] NOT NULL,
	[ScopeName] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_tblWorkContentCategory] PRIMARY KEY CLUSTERED 
(
	[pkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_tblWorkContentCategory_fkWorkContentID] ON [dbo].[tblWorkContentCategory] 
(
	[fkWorkContentID] ASC,
	[CategoryType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_User]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_WebPartState_User]
  AS SELECT [dbo].[aspnet_PersonalizationPerUser].[PathId], [dbo].[aspnet_PersonalizationPerUser].[UserId], [DataSize]=DATALENGTH([dbo].[aspnet_PersonalizationPerUser].[PageSettings]), [dbo].[aspnet_PersonalizationPerUser].[LastUpdatedDate]
  FROM [dbo].[aspnet_PersonalizationPerUser]
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_Shared]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_WebPartState_Shared]
  AS SELECT [dbo].[aspnet_PersonalizationAllUsers].[PathId], [DataSize]=DATALENGTH([dbo].[aspnet_PersonalizationAllUsers].[PageSettings]), [dbo].[aspnet_PersonalizationAllUsers].[LastUpdatedDate]
  FROM [dbo].[aspnet_PersonalizationAllUsers]
GO
/****** Object:  View [dbo].[vw_aspnet_UsersInRoles]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_UsersInRoles]
  AS SELECT [dbo].[aspnet_UsersInRoles].[UserId], [dbo].[aspnet_UsersInRoles].[RoleId]
  FROM [dbo].[aspnet_UsersInRoles]
GO
/****** Object:  View [dbo].[vw_aspnet_Profiles]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_Profiles]
  AS SELECT [dbo].[aspnet_Profile].[UserId], [dbo].[aspnet_Profile].[LastUpdatedDate],
      [DataSize]=  DATALENGTH([dbo].[aspnet_Profile].[PropertyNames])
                 + DATALENGTH([dbo].[aspnet_Profile].[PropertyValuesString])
                 + DATALENGTH([dbo].[aspnet_Profile].[PropertyValuesBinary])
  FROM [dbo].[aspnet_Profile]
GO
/****** Object:  View [dbo].[vw_aspnet_MembershipUsers]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_aspnet_MembershipUsers]
  AS SELECT [dbo].[aspnet_Membership].[UserId],
            [dbo].[aspnet_Membership].[PasswordFormat],
            [dbo].[aspnet_Membership].[MobilePIN],
            [dbo].[aspnet_Membership].[Email],
            [dbo].[aspnet_Membership].[LoweredEmail],
            [dbo].[aspnet_Membership].[PasswordQuestion],
            [dbo].[aspnet_Membership].[PasswordAnswer],
            [dbo].[aspnet_Membership].[IsApproved],
            [dbo].[aspnet_Membership].[IsLockedOut],
            [dbo].[aspnet_Membership].[CreateDate],
            [dbo].[aspnet_Membership].[LastLoginDate],
            [dbo].[aspnet_Membership].[LastPasswordChangedDate],
            [dbo].[aspnet_Membership].[LastLockoutDate],
            [dbo].[aspnet_Membership].[FailedPasswordAttemptCount],
            [dbo].[aspnet_Membership].[FailedPasswordAttemptWindowStart],
            [dbo].[aspnet_Membership].[FailedPasswordAnswerAttemptCount],
            [dbo].[aspnet_Membership].[FailedPasswordAnswerAttemptWindowStart],
            [dbo].[aspnet_Membership].[Comment],
            [dbo].[aspnet_Users].[ApplicationId],
            [dbo].[aspnet_Users].[UserName],
            [dbo].[aspnet_Users].[MobileAlias],
            [dbo].[aspnet_Users].[IsAnonymous],
            [dbo].[aspnet_Users].[LastActivityDate]
  FROM [dbo].[aspnet_Membership] INNER JOIN [dbo].[aspnet_Users]
      ON [dbo].[aspnet_Membership].[UserId] = [dbo].[aspnet_Users].[UserId]
GO
/****** Object:  StoredProcedure [dbo].[netPageTypeCheckUsage]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageTypeCheckUsage]
(
	@PageTypeID		INT,
	@OnlyPublished	BIT = 0
)
AS
BEGIN
	SET NOCOUNT ON
	IF (@OnlyPublished = 1)
	BEGIN
		SELECT TOP 1
			tblPage.pkID as PageID, 
			0 AS WorkID
		FROM 
			tblPage
		WHERE
			tblPage.fkPageTypeID = @PageTypeID
	END
	ELSE
	BEGIN
		SELECT TOP 1
			tblPage.pkID as PageID, 
			tblWorkPage.pkID as WorkID
		FROM 
			tblWorkPage
		INNER JOIN 
			tblPage ON tblWorkPage.fkPageID = tblPage.pkID
		WHERE
			tblPage.fkPageTypeID = @PageTypeID
	END
END
GO
/****** Object:  View [dbo].[tblWorkProperty]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[tblWorkProperty]
AS
SELECT
	[pkID],
	[fkPropertyDefinitionID] AS fkPageDefinitionID,
	[fkWorkContentID] AS fkWorkPageID,
	[ScopeName],
	[Boolean],
	[Number],
	[FloatNumber],
	[ContentType] AS PageType,
	[ContentLink] AS PageLink,
	[Date],
	[String],
	[LongString],
	[LinkGuid]
FROM    dbo.tblWorkContentProperty
GO
/****** Object:  View [dbo].[tblWorkCategory]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[tblWorkCategory]
AS
SELECT
	[fkWorkContentID] AS fkWorkPageID,
	[fkCategoryID],
	[CategoryType]
FROM    dbo.tblWorkContentCategory
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_DeleteProfiles]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[aspnet_Profile_DeleteProfiles]
    @ApplicationName        nvarchar(256),
    @UserNames              nvarchar(4000)
AS
BEGIN
    DECLARE @UserName     nvarchar(256)
    DECLARE @CurrentPos   int
    DECLARE @NextPos      int
    DECLARE @NumDeleted   int
    DECLARE @DeletedUser  int
    DECLARE @TranStarted  bit
    DECLARE @ErrorCode    int

    SET @ErrorCode = 0
    SET @CurrentPos = 1
    SET @NumDeleted = 0
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    WHILE (@CurrentPos <= LEN(@UserNames))
    BEGIN
        SELECT @NextPos = CHARINDEX(N',', @UserNames,  @CurrentPos)
        IF (@NextPos = 0 OR @NextPos IS NULL)
            SELECT @NextPos = LEN(@UserNames) + 1

        SELECT @UserName = SUBSTRING(@UserNames, @CurrentPos, @NextPos - @CurrentPos)
        SELECT @CurrentPos = @NextPos+1

        IF (LEN(@UserName) > 0)
        BEGIN
            SELECT @DeletedUser = 0
            EXEC dbo.aspnet_Users_DeleteUser @ApplicationName, @UserName, 4, @DeletedUser OUTPUT
            IF( @@ERROR <> 0 )
            BEGIN
                SET @ErrorCode = -1
                GOTO Cleanup
            END
            IF (@DeletedUser <> 0)
                SELECT @NumDeleted = @NumDeleted + 1
        END
    END
    SELECT @NumDeleted
    IF (@TranStarted = 1)
    BEGIN
    	SET @TranStarted = 0
    	COMMIT TRANSACTION
    END
    SET @TranStarted = 0

    RETURN 0

Cleanup:
    IF (@TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END
    RETURN @ErrorCode
END
GO
/****** Object:  StoredProcedure [dbo].[netCategoryContentLoad]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netCategoryContentLoad]
(
	@ContentID			INT,
	@VersionID		INT,
	@CategoryType	INT,
	@LanguageBranch	NCHAR(17) = NULL,
	@ScopeName NVARCHAR(450)
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @LangBranchID NCHAR(17);
	DECLARE @LanguageSpecific INT;
	IF(@VersionID = 0)
			SET @VersionID = NULL;
	IF @VersionID IS NOT NULL AND @LanguageBranch IS NOT NULL
	BEGIN
		IF NOT EXISTS(	SELECT
							LanguageID
						FROM
							tblWorkContent 
						INNER JOIN
							tblLanguageBranch
						ON
							tblWorkContent.fkLanguageBranchID = tblLanguageBranch.pkID
						WHERE
							LanguageID = @LanguageBranch
						AND
							tblWorkContent.pkID = @VersionID)
			RAISERROR('@LanguageBranch %s is not the same as Language Branch for page version %d' ,16,1, @LanguageBranch,@VersionID)
	END
	IF(@LanguageBranch IS NOT NULL)
		SELECT @LangBranchID = pkID FROM tblLanguageBranch WHERE LanguageID = @LanguageBranch;
	ELSE
		SELECT @LangBranchID = fkLanguageBranchID FROM tblWorkContent WHERE pkID = @VersionID;
	IF(@CategoryType <> 0)
		SELECT @LanguageSpecific = LanguageSpecific FROM tblPageDefinition WHERE pkID = @CategoryType;
	ELSE
		SET @LanguageSpecific = 0;
	IF @LangBranchID IS NULL AND @LanguageSpecific > 2
		RAISERROR('Language branch %s is not defined',16,1, @LanguageBranch)
	IF @LanguageSpecific < 3 AND @VersionID IS NOT NULL
	BEGIN
		IF EXISTS(SELECT pkID FROM tblContent WHERE pkID=@ContentID AND fkMasterLanguageBranchID<>@LangBranchID)
		BEGIN
			IF EXISTS(SELECT pkID FROM tblContent WHERE PendingPublish=1 AND pkID=@ContentID)
				SELECT TOP 1 @VersionID = tblWorkContent.pkID 
				FROM tblWorkContent 
				INNER JOIN tblContent ON tblContent.pkID=tblWorkContent.fkContentID
				WHERE tblWorkContent.fkContentID=@ContentID AND tblWorkContent.fkLanguageBranchID=tblContent.fkMasterLanguageBranchID
				ORDER BY tblWorkContent.Saved DESC
			ELSE
				SELECT @VersionID = tblContentLanguage.PublishedVersion 
				FROM tblContentLanguage 
				INNER JOIN tblContent ON tblContent.pkID=tblContentLanguage.fkContentID
				WHERE tblContent.pkID=@ContentID AND tblContentLanguage.fkLanguageBranchID=tblContent.fkMasterLanguageBranchID			
		END
	END
	IF (@VersionID IS NOT NULL)
	BEGIN
		/* Get info from tblWorkContentCategory */
		SELECT
			fkCategoryID AS CategoryID
		FROM
			tblWorkContentCategory
		WHERE
			ScopeName=@ScopeName AND
			fkWorkContentID=@VersionID
	END
	ELSE
	BEGIN
		/* Get info from tblContentcategory */
		SELECT
			fkCategoryID AS CategoryID
		FROM
			tblContentCategory
		WHERE
			ScopeName=@ScopeName AND
			fkContentID=@ContentID AND
			(fkLanguageBranchID=@LangBranchID OR @LanguageSpecific < 3)
	END
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[netBlockTypeGetUsage]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netBlockTypeGetUsage]
(
	@BlockTypeID		INT,
	@OnlyPublished	BIT = 0
)
AS
BEGIN
	SET NOCOUNT ON
	IF (@OnlyPublished = 1)
	BEGIN
		SELECT DISTINCT
			Property.fkContentID as ContentID, 
			0 AS WorkID,
			ContentLanguage.Name,
			LanguageBranch.LanguageID AS LanguageBranch
		FROM tblContentProperty as Property WITH(INDEX(IDX_tblContentProperty_ScopeName))
		INNER JOIN dbo.GetScopedBlockProperties(@BlockTypeID) as ScopedProperties ON 
			Property.ScopeName LIKE (ScopedProperties.ScopeName + '%')
		INNER JOIN tblContentLanguage as ContentLanguage ON Property.fkContentID=ContentLanguage.fkContentID
		INNER JOIN tblLanguageBranch as LanguageBranch ON LanguageBranch.pkID=ContentLanguage.fkLanguageBranchID
	END
	ELSE
	BEGIN
		SELECT DISTINCT
			WorkContent.fkContentID as ContentID, 
			WorkContent.pkID as WorkID,
			WorkContent.Name,
			LanguageBranch.LanguageID AS LanguageBranch
		FROM tblWorkContentProperty as Property WITH(INDEX(IDX_tblWorkContentProperty_ScopeName))
		INNER JOIN dbo.GetScopedBlockProperties(@BlockTypeID) as ScopedProperties ON 
			Property.ScopeName LIKE (ScopedProperties.ScopeName + '%')
		INNER JOIN tblWorkContent as WorkContent ON WorkContent.pkID = Property.fkWorkContentID
		INNER JOIN tblLanguageBranch as LanguageBranch ON LanguageBranch.pkID=WorkContent.fkLanguageBranchID
	END
END
GO
/****** Object:  StoredProcedure [dbo].[netBlockTypeCheckUsage]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netBlockTypeCheckUsage]
(
	@BlockTypeID		INT,
	@OnlyPublished	BIT = 0
)
AS
BEGIN
	SET NOCOUNT ON
	IF (@OnlyPublished = 1)
	BEGIN
		SELECT TOP 1
			Property.fkContentID as ContentID, 
			0 AS WorkID
		FROM tblContentProperty as Property WITH(INDEX(IDX_tblContentProperty_ScopeName))
		INNER JOIN dbo.GetScopedBlockProperties(@BlockTypeID) as ScopedProperties ON 
			Property.ScopeName LIKE (ScopedProperties.ScopeName + '%')
	END
	ELSE
	BEGIN
		SELECT TOP 1
			WorkContent.fkContentID as ContentID, 
			WorkContent.pkID as WorkID
		FROM tblWorkContentProperty as Property WITH(INDEX(IDX_tblWorkContentProperty_ScopeName))
		INNER JOIN dbo.GetScopedBlockProperties(@BlockTypeID) as ScopedProperties ON 
			Property.ScopeName LIKE (ScopedProperties.ScopeName + '%')
		INNER JOIN
			tblWorkContent as WorkContent ON Property.fkWorkContentID=WorkContent.pkID
	END
END
GO
/****** Object:  StoredProcedure [dbo].[editSavePropertyCategory]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editSavePropertyCategory]
(
	@PageID				INT,
	@WorkPageID			INT,
	@PageDefinitionID	INT,
	@Override			BIT,
	@CategoryString		NVARCHAR(2000),
	@LanguageBranch		NCHAR(17) = NULL,
	@ScopeName			nvarchar(450) = NULL
)
AS
BEGIN
	SET NOCOUNT	ON
	SET XACT_ABORT ON
	DECLARE	@PageIDString			NVARCHAR(20)
	DECLARE	@PageDefinitionIDString	NVARCHAR(20)
	DECLARE @DynProp INT
	DECLARE @retval	INT
	SET @retval = 0
	DECLARE @LangBranchID NCHAR(17);
	SELECT @LangBranchID = pkID FROM tblLanguageBranch WHERE LanguageID = @LanguageBranch
	IF @LangBranchID IS NULL 
	BEGIN 
		if @LanguageBranch IS NOT NULL
			RAISERROR('Language branch %s is not defined',16,1, @LanguageBranch)
		else
			SET @LangBranchID = 1
	END
	DECLARE @IsLanguagePublished BIT;
	IF EXISTS(SELECT fkPageID FROM tblPageLanguage 
		WHERE fkPageID = @PageID AND fkLanguageBranchID = CAST(@LangBranchID AS INT) AND PublishedVersion IS NOT NULL)
		SET @IsLanguagePublished = 1
	ELSE
		SET @IsLanguagePublished = 0
	SELECT @DynProp=pkID FROM tblPageDefinition WHERE pkID=@PageDefinitionID AND fkPageTypeID IS NULL
	IF (@WorkPageID IS NOT NULL)
	BEGIN
		/* Never store dynamic properties in work table */
		IF (@DynProp IS NOT NULL)
			GOTO cleanup
		/* Remove all categories */
		SET @PageIDString = CONVERT(NVARCHAR(20), @WorkPageID)
		SET @PageDefinitionIDString = CONVERT(NVARCHAR(20), @PageDefinitionID)
		DELETE FROM tblWorkContentCategory WHERE fkWorkContentID=@WorkPageID AND ScopeName=@ScopeName
		/* Insert new categories */
		IF (LEN(@CategoryString) > 0)
		BEGIN
			EXEC (N'INSERT INTO tblWorkContentCategory (fkWorkContentID, fkCategoryID, CategoryType, ScopeName) SELECT ' + @PageIDString + N',pkID,' + @PageDefinitionIDString + N', ''' + @ScopeName + N''' FROM tblCategory WHERE pkID IN (' + @CategoryString +N')' )
		END
		/* Finally update the property table */
		IF (@PageDefinitionID <> 0)
		BEGIN
			IF EXISTS(SELECT fkWorkContentID FROM tblWorkContentProperty WHERE fkWorkContentID=@WorkPageID AND fkPropertyDefinitionID=@PageDefinitionID AND ScopeName=@ScopeName)
				UPDATE tblWorkContentProperty SET Number=@PageDefinitionID WHERE fkWorkContentID=@WorkPageID 
					AND fkPropertyDefinitionID=@PageDefinitionID
					AND ((@ScopeName IS NULL AND ScopeName IS NULL) OR (@ScopeName = ScopeName))
			ELSE
				INSERT INTO tblWorkContentProperty (fkWorkContentID, fkPropertyDefinitionID, Number, ScopeName) VALUES (@WorkPageID, @PageDefinitionID, @PageDefinitionID, @ScopeName)
		END
	END
	IF (@WorkPageID IS NULL OR @IsLanguagePublished = 0)
	BEGIN
		/* Insert or update property */
		/* Remove all categories */
		SET @PageIDString = CONVERT(NVARCHAR(20), @PageID)
		SET @PageDefinitionIDString = CONVERT(NVARCHAR(20), @PageDefinitionID)
		DELETE FROM tblContentCategory WHERE fkContentID=@PageID AND ScopeName=@ScopeName
		AND fkLanguageBranchID=@LangBranchID
		/* Insert new categories */
		IF (LEN(@CategoryString) > 0)
		BEGIN
			EXEC (N'INSERT INTO tblContentCategory (fkContentID, fkCategoryID, CategoryType, fkLanguageBranchID, ScopeName) SELECT ' + @PageIDString + N',pkID,' + @PageDefinitionIDString + N', ' + @LangBranchID + N', ''' + @ScopeName + N''' FROM tblCategory WHERE pkID IN (' + @CategoryString +N')' )
		END
		/* Finally update the property table */
		IF (@PageDefinitionID <> 0)
		BEGIN
			IF EXISTS(SELECT fkContentID FROM tblContentProperty WHERE fkContentID=@PageID AND fkPropertyDefinitionID=@PageDefinitionID 
						AND fkLanguageBranchID=@LangBranchID AND ScopeName=@ScopeName)
				UPDATE tblContentProperty SET Number=@PageDefinitionID WHERE fkContentID=@PageID AND fkPropertyDefinitionID=@PageDefinitionID
						AND fkLanguageBranchID=@LangBranchID
						AND ((@ScopeName IS NULL AND ScopeName IS NULL) OR (@ScopeName = ScopeName))
			ELSE
				INSERT INTO tblContentProperty (fkContentID, fkPropertyDefinitionID, Number, fkLanguageBranchID, ScopeName) VALUES (@PageID, @PageDefinitionID, @PageDefinitionID, @LangBranchID, @ScopeName)
		END
		/* Override dynamic property definitions below the current level */
		IF (@DynProp IS NOT NULL)
		BEGIN
			IF (@Override = 1)
				DELETE FROM tblContentProperty WHERE fkPropertyDefinitionID=@PageDefinitionID AND fkContentID IN (SELECT fkChildID FROM tblTree WHERE fkParentID=@PageID)
			SET @retval = 1
		END
	END
cleanup:		
	RETURN @retval
END
GO
/****** Object:  StoredProcedure [dbo].[editSavePageVersionData]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editSavePageVersionData]
(
	@WorkPageID		INT,
	@UserName       NVARCHAR(255),
	@Saved			DATETIME,
	@PageName		NVARCHAR(255)	= NULL,
	@ExternalURL	NVARCHAR(255)	= NULL,
	@Created		DATETIME		= NULL,
	@Changed		BIT				= 0,
	@StartPublish	DATETIME		= NULL,
	@StopPublish	DATETIME		= NULL,
	@ChildOrder		INT				= 1,
	@PeerOrder		INT				= 100,
	@PageLinkGUID   UNIQUEIDENTIFIER				= NULL,
	@LinkURL		NVARCHAR(255)	= NULL,
	@LinkType		INT				= 0,
	@FrameID		INT				= NULL,
	@VisibleInMenu	BIT				= NULL,
	@ArchivePageGUID	UNIQUEIDENTIFIER				= NULL,
	@FolderID		INT				= NULL,
	@DelayedPublish	BIT				= NULL,
	@URLSegment		NVARCHAR(255)	= NULL
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	DECLARE @ChangedDate		DATETIME
	DECLARE @PageID			INT
	DECLARE @PageTypeID		INT
	DECLARE @ParentID			INT
	DECLARE @ExternalFolderID		INT
	DECLARE @MasterPendingPublish		INT
	DECLARE @CurrentLangBranchID INT
	DECLARE @IsMasterLang		BIT
	/* Pull some useful information from the published page */
	SELECT
		@PageID				= fkPageID,
		@ParentID			= fkParentID,
		@PageTypeID			= fkPageTypeID,
		@ExternalFolderID	= ExternalFolderID,
		@MasterPendingPublish = PendingPublish,
		@IsMasterLang		= CASE WHEN tblPage.fkMasterLanguageBranchID=tblWorkPage.fkLanguageBranchID THEN 1 ELSE 0 END,
		@CurrentLangBranchID = fkLanguageBranchID
	FROM
		tblWorkPage
	INNER JOIN tblPage ON tblPage.pkID=tblWorkPage.fkPageID
	INNER JOIN tblPageType ON tblPageType.pkID=tblPage.fkPageTypeID
	WHERE
		tblWorkPage.pkID=@WorkPageID
	if (@PageID IS NULL)
	BEGIN
		RAISERROR (N'editSavePageVersionData: The WorkPageId dosen´t exist (WorkPageID=%d)', 16, 1, @WorkPageID)
		RETURN -1
	END			
		/* Special case for handling external folder id. Only set new value if */
		/* current value of ExternalFolderID is null */
		IF ((@ExternalFolderID IS NULL) AND (@FolderID IS NOT NULL))
		BEGIN
			UPDATE
				tblPage
			SET
				ExternalFolderID=@FolderID
			WHERE
				pkID=@PageID
		END
		/* Set new values for work page */
		UPDATE
			tblWorkPage
		SET
			ChangedByName	= @UserName,
			PageLinkGUID	= @PageLinkGUID,
			ArchivePageGUID	= @ArchivePageGUID,
			fkFrameID		= @FrameID,
			Name			= @PageName,
			LinkURL			= @LinkURL,
			ExternalURL		= @ExternalURL,
			URLSegment		= @URLSegment,
			VisibleInMenu	= @VisibleInMenu,
			LinkType		= @LinkType,
			Created			= COALESCE(@Created, Created),
			Saved			= @Saved,
			StartPublish	= @StartPublish,
			StopPublish		= @StopPublish,
			ChildOrderRule	= @ChildOrder,
			PeerOrder		= COALESCE(@PeerOrder, PeerOrder),
			ChangedOnPublish= @Changed,
			DelayedPublish	= COALESCE(@DelayedPublish, DelayedPublish)
		WHERE
			pkID=@WorkPageID
		/* Set some values needed for proper display in edit tree even though we have not yet published the page */
		IF @IsMasterLang=1 AND @MasterPendingPublish = 1
		BEGIN
			UPDATE
				tblPage
			SET
				ChildOrderRule	= @ChildOrder,
				PeerOrder		= @PeerOrder
			WHERE
				pkID=@PageID 
		END
		IF EXISTS(SELECT * FROM tblPageLanguage WHERE fkPageID=@PageID AND fkLanguageBranchID=@CurrentLangBranchID AND PendingPublish=1)
		BEGIN
			UPDATE
				tblPageLanguage
			SET
				Name			= @PageName,
				Created			= @Created,
				Saved			= @Saved,
				URLSegment		= @URLSegment,
				LinkURL			= @LinkURL
			WHERE
				fkPageID=@PageID AND fkLanguageBranchID=@CurrentLangBranchID
		END
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetExistingScopesForDefinition]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetExistingScopesForDefinition] 
(
	@PropertyDefinitionID int
)
RETURNS @ScopedPropertiesTable TABLE 
(
	ScopeName nvarchar(450)
)
AS
BEGIN
	--Get blocktype if property is block property
	DECLARE @ContentTypeID INT;
	SET @ContentTypeID = (SELECT tblContentType.pkID FROM 
		tblPropertyDefinition
		INNER JOIN tblPropertyDefinitionType ON tblPropertyDefinition.fkPropertyDefinitionTypeID = tblPropertyDefinitionType.pkID
		INNER JOIN tblContentType ON tblPropertyDefinitionType.fkContentTypeGUID = tblContentType.ContentTypeGUID
		WHERE tblPropertyDefinition.pkID = @PropertyDefinitionID);
	IF (@ContentTypeID IS NOT NULL)
	BEGIN
		INSERT INTO @ScopedPropertiesTable
		SELECT Property.ScopeName FROM
			tblWorkContentProperty as Property WITH(INDEX(IDX_tblWorkContentProperty_ScopeName))
			INNER JOIN dbo.GetScopedBlockProperties(@ContentTypeID) as ScopedProperties ON 
				Property.ScopeName LIKE (ScopedProperties.ScopeName + '%')
				WHERE ScopedProperties.ScopeName LIKE ('%.' + CAST(@PropertyDefinitionID as VARCHAR)+ '.')
	END
	RETURN 
END
GO
/****** Object:  StoredProcedure [dbo].[editCreateContentVersion]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editCreateContentVersion]
(
	@ContentID			INT,
	@WorkContentID		INT,
	@UserName		NVARCHAR(255),
	@MaxVersions	INT = NULL,
	@SavedDate		DATETIME = NULL,
	@LanguageBranch	NCHAR(17)
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	DECLARE @NewWorkContentID		INT
	DECLARE @DeleteWorkContentID	INT
	DECLARE @ObsoleteVersions	INT
	DECLARE @retval				INT
	DECLARE @IsMasterLang		BIT
	DECLARE @LangBranchID		INT
	IF @SavedDate IS NULL
		SET @SavedDate = GetDate()
	SELECT @LangBranchID = pkID FROM tblLanguageBranch WHERE LanguageID=@LanguageBranch
	IF @LangBranchID IS NULL
	BEGIN
		RAISERROR (N'editCreateContentVersion: LanguageBranchID is null, possibly empty table tblLanguageBranch', 16, 1, @WorkContentID)
		RETURN 0
	END
	IF (@WorkContentID IS NULL OR @WorkContentID=0 )
	BEGIN
		/* If we have a published version use it, else the latest saved version */
		IF EXISTS(SELECT * FROM tblContentLanguage WHERE PendingPublish=1 AND fkContentID=@ContentID AND fkLanguageBranchID=@LangBranchID)
			SELECT TOP 1 @WorkContentID=pkID FROM tblWorkContent WHERE fkContentID=@ContentID AND fkLanguageBranchID=@LangBranchID ORDER BY Saved DESC
		ELSE
			SELECT @WorkContentID=PublishedVersion FROM tblContentLanguage WHERE fkContentID=@ContentID AND fkLanguageBranchID=@LangBranchID
	END
	IF EXISTS( SELECT * FROM tblContent WHERE pkID=@ContentID AND fkMasterLanguageBranchID IS NULL )
		UPDATE tblContent SET fkMasterLanguageBranchID=@LangBranchID WHERE pkID=@ContentID
	SELECT @IsMasterLang = CASE WHEN @LangBranchID=fkMasterLanguageBranchID THEN 1 ELSE 0 END FROM tblContent WHERE pkID=@ContentID
		IF NOT EXISTS (SELECT * FROM tblContentLanguage 
						WHERE tblContentLanguage.fkContentID=@ContentID
						AND fkLanguageBranchID=@LangBranchID)
		BEGIN
			INSERT INTO tblContentLanguage(fkContentID, CreatorName, ChangedByName, PendingPublish, fkLanguageBranchID)
			SELECT @ContentID, @UserName, @UserName, 1, @LangBranchID
			FROM tblContent
			INNER JOIN tblContentType ON tblContentType.pkID=tblContent.fkContentTypeID
			WHERE tblContent.pkID=@ContentID
		END
		/* Create a new version of this content */
		INSERT INTO tblWorkContent
			(fkContentID,
			fkMasterVersionID,
			ChangedByName,
			ContentLinkGUID,
			fkFrameID,
			ArchiveContentGUID,
			Name,
			LinkURL,
			ExternalURL,
			VisibleInMenu,
			LinkType,
			Created,
			Saved,
			StartPublish,
			StopPublish,
			ChildOrderRule,
			PeerOrder,
			ReadyToPublish,
			fkLanguageBranchID)
		SELECT 
			fkContentID,
			@WorkContentID,
			@UserName,
			ContentLinkGUID,
			fkFrameID,
			ArchiveContentGUID,
			Name,
			LinkURL,
			ExternalURL,
			VisibleInMenu,
			LinkType,
			Created,
			@SavedDate,
			StartPublish,
			StopPublish,
			ChildOrderRule,
			PeerOrder,
			0,
			@LangBranchID
		FROM 
			tblWorkContent 
		WHERE 
			pkID=@WorkContentID
		IF (@@ROWCOUNT = 1)
		BEGIN
			/* Remember version number */
			SET @NewWorkContentID= SCOPE_IDENTITY() 
			/* Copy all properties as well */
			INSERT INTO tblWorkContentProperty
				(fkPropertyDefinitionID,
				fkWorkContentID,
				ScopeName,
				Boolean,
				Number,
				FloatNumber,
				ContentType,
				ContentLink,
				Date,
				String,
				LongString,
                LinkGuid)          
			SELECT
				fkPropertyDefinitionID,
				@NewWorkContentID,
				ScopeName,
				Boolean,
				Number,
				FloatNumber,
				ContentType,
				ContentLink,
				Date,
				String,
				LongString,
                LinkGuid
			FROM
				tblWorkContentProperty
			INNER JOIN tblPropertyDefinition ON tblPropertyDefinition.pkID=tblWorkContentProperty.fkPropertyDefinitionID
			WHERE
				fkWorkContentID=@WorkContentID
				AND (tblPropertyDefinition.LanguageSpecific>2 OR @IsMasterLang=1)--Only lang specific on non-master 
			/* Finally take care of categories */
			INSERT INTO tblWorkContentCategory
				(fkWorkContentID,
				fkCategoryID,
				CategoryType,
				ScopeName)
			SELECT
				@NewWorkContentID,
				fkCategoryID,
				CategoryType,
				ScopeName
			FROM
				tblWorkContentCategory
			WHERE
				fkWorkContentID=@WorkContentID
				AND (CategoryType<>0 OR @IsMasterLang=1)--No content category on languages
		END
		ELSE
		BEGIN
			/* We did not have anything corresponding to the WorkContentID, create new work content from tblContent */
			INSERT INTO tblWorkContent
				(fkContentID,
				ChangedByName,
				ContentLinkGUID,
				fkFrameID,
				ArchiveContentGUID,
				Name,
				LinkURL,
				ExternalURL,
				VisibleInMenu,
				LinkType,
				Created,
				Saved,
				StartPublish,
				StopPublish,
				ChildOrderRule,
				PeerOrder,
				ReadyToPublish,
				fkLanguageBranchID)
			SELECT 
				@ContentID,
				COALESCE(@UserName, tblContentLanguage.CreatorName),
				tblContentLanguage.ContentLinkGUID,
				tblContentLanguage.fkFrameID,
				tblContent.ArchiveContentGUID,
				tblContentLanguage.Name,
				tblContentLanguage.LinkURL,
				tblContentLanguage.ExternalURL,
				tblContent.VisibleInMenu,
				CASE tblContentLanguage.AutomaticLink 
					WHEN 1 THEN 
						(CASE
							WHEN tblContentLanguage.ContentLinkGUID IS NULL THEN 0	/* EPnLinkNormal */
							WHEN tblContentLanguage.FetchData=1 THEN 4				/* EPnLinkFetchdata */
							ELSE 1								/* EPnLinkShortcut */
						END)
					ELSE
						(CASE 
							WHEN tblContentLanguage.LinkURL=N'#' THEN 3				/* EPnLinkInactive */
							ELSE 2								/* EPnLinkExternal */
						END)
				END AS LinkType ,
				tblContentLanguage.Created,
				@SavedDate,
				tblContentLanguage.StartPublish,
				tblContentLanguage.StopPublish,
				tblContent.ChildOrderRule,
				tblContent.PeerOrder,
				0,
				@LangBranchID
			FROM tblContentLanguage
			INNER JOIN tblContent ON tblContent.pkID=tblContentLanguage.fkContentID
			WHERE 
				tblContentLanguage.fkContentID=@ContentID AND tblContentLanguage.fkLanguageBranchID=@LangBranchID
			IF (@@ROWCOUNT = 1)
			BEGIN
				/* Remember version number */
				SET @NewWorkContentID= SCOPE_IDENTITY() 
				/* Copy all non-dynamic properties as well */
				INSERT INTO tblWorkContentProperty
					(fkPropertyDefinitionID,
					fkWorkContentID,
					ScopeName,
					Boolean,
					Number,
					FloatNumber,
					ContentType,
					ContentLink,
					Date,
					String,
					LongString,
                    LinkGuid)
				SELECT
					P.fkPropertyDefinitionID,
					@NewWorkContentID,
					P.ScopeName,
					P.Boolean,
					P.Number,
					P.FloatNumber,
					P.ContentType,
					P.ContentLink,
					P.Date,
					P.String,
					P.LongString,
                    P.LinkGuid
				FROM
					tblContentProperty AS P
				INNER JOIN
					tblPropertyDefinition AS PD ON P.fkPropertyDefinitionID=PD.pkID
				WHERE
					P.fkContentID=@ContentID AND (PD.fkContentTypeID IS NOT NULL)
					AND P.fkLanguageBranchID = @LangBranchID
					AND (PD.LanguageSpecific>2 OR @IsMasterLang=1)--Only lang specific on non-master 
				/* Finally take care of categories */
				INSERT INTO tblWorkContentCategory
					(fkWorkContentID,
					fkCategoryID,
					CategoryType)
				SELECT DISTINCT
					@NewWorkContentID,
					fkCategoryID,
					CategoryType
				FROM
					tblCategoryContent
				LEFT JOIN
					tblPropertyDefinition AS PD ON tblCategoryContent.CategoryType = PD.pkID
				WHERE
					tblCategoryContent.fkContentID=@ContentID 
					AND (PD.fkContentTypeID IS NOT NULL OR tblCategoryContent.CategoryType = 0) --Not dynamic properties
					AND (PD.LanguageSpecific=1 OR @IsMasterLang=1) --No content category on languages
			END
			ELSE
			BEGIN
				RAISERROR (N'Failed to create new version for content %d', 16, 1, @ContentID)
				RETURN 0
			END
		END
	/* Set NewWorkContentID as Common draft version if there is no common draft or the common draft is the published version */
	EXEC editSetCommonDraftVersion @WorkContentID = @NewWorkContentID, @Force = 0			
	RETURN @NewWorkContentID
END
GO
/****** Object:  StoredProcedure [dbo].[editDeletePageCheckInternal]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editDeletePageCheckInternal]
AS
BEGIN
	SET NOCOUNT ON
    SELECT
		tblPageLanguage.fkLanguageBranchID AS OwnerLanguageID,
		NULL AS ReferencedLanguageID,
        tblPageLanguage.fkPageID AS OwnerID, 
        tblPageLanguage.Name As OwnerName,
        PageLink As ReferencedID,
        tpl.Name AS ReferencedName,
        0 AS ReferenceType
    FROM 
        tblProperty 
    INNER JOIN 
        tblPage ON tblProperty.fkPageID=tblPage.pkID
	INNER JOIN 
        tblPageLanguage ON tblPageLanguage.fkPageID=tblPage.pkID
    INNER JOIN
        tblPage AS tp ON PageLink=tp.pkID
    INNER JOIN
		tblPageLanguage AS tpl ON tpl.fkPageID=tp.pkID
    WHERE 
        (tblProperty.fkPageID NOT IN (SELECT pkID FROM #pages)) AND
        (PageLink IN (SELECT pkID FROM #pages)) AND
        tblPage.Deleted=0 AND
		tblPageLanguage.fkLanguageBranchID=tblProperty.fkLanguageBranchID AND
		tpl.fkLanguageBranchID=tp.fkMasterLanguageBranchID
    UNION
    SELECT
		tblPageLanguage.fkLanguageBranchID AS OwnerLanguageID,
		NULL AS ReferencedLanguageID,    
        tblPageLanguage.fkPageID AS OwnerID,
        tblPageLanguage.Name As OwnerName,
        tp.pkID AS ReferencedID,
        tpl.Name AS ReferencedName,
        1 AS ReferenceType
    FROM
        tblPageLanguage
	INNER JOIN
		tblPage ON tblPage.pkID=tblPageLanguage.fkPageID
    INNER JOIN
        tblPage AS tp ON tblPageLanguage.PageLinkGUID = tp.PageGUID
    INNER JOIN
		tblPageLanguage AS tpl ON tpl.fkPageID=tp.pkID
    WHERE
        (tblPageLanguage.fkPageID NOT IN (SELECT pkID FROM #pages)) AND
        (tblPageLanguage.PageLinkGUID IN (SELECT PageGUID FROM #pages)) AND
        tblPage.Deleted=0 AND
		tpl.fkLanguageBranchID=tp.fkMasterLanguageBranchID
    UNION
    SELECT
		tblContentSoftlink.OwnerLanguageID AS OwnerLanguageID,
		tblContentSoftlink.ReferencedLanguageID AS ReferencedLanguageID,
        PLinkFrom.pkID AS OwnerID,
        PLinkFromLang.Name  As OwnerName,
        PLinkTo.pkID AS ReferencedID,
        PLinkToLang.Name AS ReferencedName,
        1 AS ReferenceType
    FROM
        tblContentSoftlink
    INNER JOIN
        tblPage AS PLinkFrom ON PLinkFrom.pkID=tblContentSoftlink.fkOwnerContentID
    INNER JOIN
		tblPageLanguage AS PLinkFromLang ON PLinkFromLang.fkPageID=PLinkFrom.pkID
    INNER JOIN
        tblPage AS PLinkTo ON PLinkTo.PageGUID=tblContentSoftlink.fkReferencedContentGUID
    INNER JOIN
		tblPageLanguage AS PLinkToLang ON PLinkToLang.fkPageID=PLinkTo.pkID
    WHERE
        (PLinkFrom.pkID NOT IN (SELECT pkID FROM #pages)) AND
        (PLinkTo.pkID IN (SELECT pkID FROM #pages)) AND
        PLinkFrom.Deleted=0 AND
		PLinkFromLang.fkLanguageBranchID=PLinkFrom.fkMasterLanguageBranchID AND
		PLinkToLang.fkLanguageBranchID=PLinkTo.fkMasterLanguageBranchID
	UNION
    SELECT
       	tblPageLanguage.fkLanguageBranchID AS OwnerLanguageID,
		NULL AS ReferencedLanguageID,
		tblPage.pkID AS OwnerID,
        tblPageLanguage.Name  As OwnerName,
        tp.pkID AS ReferencedID,
        tpl.Name AS ReferencedName,
        2 AS ReferenceType
    FROM
        tblPage
    INNER JOIN 
        tblPageLanguage ON tblPageLanguage.fkPageID=tblPage.pkID
    INNER JOIN
        tblPage AS tp ON tblPage.ArchivePageGUID=tp.PageGUID
    INNER JOIN
		tblPageLanguage AS tpl ON tpl.fkPageID=tp.pkID
    WHERE
        (tblPage.pkID NOT IN (SELECT pkID FROM #pages)) AND
        (tblPage.ArchivePageGUID IN (SELECT PageGUID FROM #pages)) AND
        tblPage.Deleted=0 AND
		tpl.fkLanguageBranchID=tp.fkMasterLanguageBranchID AND
		tblPageLanguage.fkLanguageBranchID=tblPage.fkMasterLanguageBranchID
	UNION
    SELECT 
        tblPageLanguage.fkLanguageBranchID AS OwnerLanguageID,
		NULL AS ReferencedLanguageID,
        tblPage.pkID AS OwnerID, 
        tblPageLanguage.Name  As OwnerName,
        tblPageTypeDefault.fkArchivePageID AS ReferencedID,
        tblPageType.Name AS ReferencedName,
        3 AS ReferenceType
    FROM 
        tblPageTypeDefault
    INNER JOIN
       tblPageType ON tblPageTypeDefault.fkPageTypeID=tblPageType.pkID
    INNER JOIN
        tblPage ON tblPageTypeDefault.fkArchivePageID=tblPage.pkID
	INNER JOIN 
        tblPageLanguage ON tblPageLanguage.fkPageID=tblPage.pkID
    WHERE 
        tblPageTypeDefault.fkArchivePageID IN (SELECT pkID FROM #pages) AND
        tblPageLanguage.fkLanguageBranchID=tblPage.fkMasterLanguageBranchID
    ORDER BY
       ReferenceType
    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[netContentAclChildAdd]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netContentAclChildAdd]
(
	@Name NVARCHAR(255),
	@IsRole INT,
	@ContentID	INT,
	@AccessMask INT
)
AS
BEGIN
    SET NOCOUNT ON
    /* Remove all old ACEs for @ContentID and below refering to @Name / @IsRole */
    EXEC netContentAclChildDelete @Name=@Name, @IsRole=@IsRole, @ContentID=@ContentID
    /* Create new ACEs for all childs to @ContentID */
    INSERT INTO tblContentAccess 
        (fkContentID, 
        Name,
        IsRole, 
        AccessMask) 
    SELECT 
        fkChildID, 
        @Name,
        @IsRole, 
        @AccessMask
    FROM 
        tblTree 
    WHERE 
        fkParentID=@ContentID
    /* Finally create new ACE for @ContentID */
    INSERT INTO tblContentAccess
        (fkContentID, 
        Name,
        IsRole, 
        AccessMask) 
    VALUES 
        (@ContentID, 
        @Name,
        @IsRole, 
        @AccessMask)
END
GO
/****** Object:  StoredProcedure [dbo].[netFrameDelete]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netFrameDelete]
(
	@FrameID		INT,
	@ReplaceFrameID	INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
		IF (NOT EXISTS(SELECT pkID FROM tblFrame WHERE pkID=@ReplaceFrameID))
			SET @ReplaceFrameID=NULL
		UPDATE tblWorkPage SET fkFrameID=@ReplaceFrameID WHERE fkFrameID=@FrameID
		UPDATE tblPageLanguage SET fkFrameID=@ReplaceFrameID WHERE fkFrameID=@FrameID
		DELETE FROM tblFrame WHERE pkID=@FrameID
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[netFindPageCoreDataByPageGuid]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netFindPageCoreDataByPageGuid]
	@PageGuid UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON
        --- *** use NOLOCK since this may be called during page save if debugging. The code should not be written so this happens, it's to make it work in the debugger ***
	SELECT TOP 1 P.pkID as ID, P.fkPageTypeID as PageTypeID, P.fkParentID as ParentID, P.PageGUID, PL.LinkURL, P.Deleted, P.PendingPublish, PL.Created, PL.Changed, PL.Saved, PL.StartPublish, PL.StopPublish, P.ExternalFolderID, P.fkMasterLanguageBranchID as MasterLanguageBranchID, PL.PageLinkGUID as PageLinkID, PL.AutomaticLink, PL.FetchData, P.ContentType
	FROM tblPage AS P WITH (NOLOCK)
	LEFT JOIN tblPageLanguage AS PL ON PL.fkPageID=P.pkID
	WHERE P.PageGUID = @PageGuid AND (P.fkMasterLanguageBranchID=PL.fkLanguageBranchID OR P.fkMasterLanguageBranchID IS NULL)
END
GO
/****** Object:  StoredProcedure [dbo].[netFindPageCoreDataByID]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netFindPageCoreDataByID]
	@PageID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON
        --- *** use NOLOCK since this may be called during page save if debugging. The code should not be written so this happens, it's to make it work in the debugger ***
	SELECT TOP 1 P.pkID as ID, P.fkPageTypeID as PageTypeID, P.fkParentID as ParentID, P.PageGUID, PL.LinkURL, P.Deleted, P.PendingPublish, PL.Created, PL.Changed, PL.Saved, PL.StartPublish, PL.StopPublish, P.ExternalFolderID, P.fkMasterLanguageBranchID as MasterLanguageBranchID, PL.PageLinkGUID as PageLinkID, PL.AutomaticLink, PL.FetchData, P.ContentType
	FROM tblPage AS P WITH (NOLOCK)
	LEFT JOIN tblPageLanguage AS PL ON PL.fkPageID=P.pkID
	WHERE P.pkID = @PageID AND (P.fkMasterLanguageBranchID=PL.fkLanguageBranchID OR P.fkMasterLanguageBranchID IS NULL)
END
GO
/****** Object:  StoredProcedure [dbo].[netDynamicPropertyLookup]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netDynamicPropertyLookup]
AS
BEGIN
	SET NOCOUNT ON
	SELECT
		P.fkPageID AS PageID,
		P.fkPageDefinitionID,
		PD.Name AS PropertyName,
		LanguageSpecific,
		RTRIM(LB.LanguageID) AS BranchLanguageID,
		ScopeName,
		CONVERT(INT,Boolean) AS Boolean,
		Number AS IntNumber,
		FloatNumber,
		PageType,
		PageLink AS PageLinkID,
		LinkGuid,
		Date AS DateValue,
		String,
		LongString
	FROM
		tblProperty AS P
	INNER JOIN
		tblLanguageBranch AS LB
	ON
		P.fkLanguageBranchID = LB.pkID
	INNER JOIN
		tblPageDefinition AS PD
	ON
		PD.pkID = P.fkPageDefinitionID
	WHERE   
		(LB.Enabled = 1 OR PD.LanguageSpecific < 3) AND
		(PD.fkPageTypeID IS NULL)	
END
GO
/****** Object:  StoredProcedure [dbo].[netDynamicPropertiesLoad]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netDynamicPropertiesLoad]
(
	@PageID INT
)
AS
BEGIN
	/* 
	Return dynamic properties for this page with edit-information
	*/
	SET NOCOUNT ON
	DECLARE @PropCount INT
	CREATE TABLE #tmpprop
	(
		fkPageID		INT NULL,
		fkPageDefinitionID	INT,
		fkPageDefinitionTypeID	INT,
		fkLanguageBranchID	INT NULL
	)
	/*Make sure page exists before starting*/
	IF NOT EXISTS(SELECT * FROM tblPage WHERE pkID=@PageID)
		RETURN 0
	SET @PropCount = 0
	/* Get all common dynamic properties */
	INSERT INTO #tmpprop
		(fkPageDefinitionID,
		fkPageDefinitionTypeID,
		fkLanguageBranchID)
	SELECT
		tblPageDefinition.pkID,
		fkPageDefinitionTypeID,
		1
	FROM
		tblPageDefinition
	WHERE
		fkPageTypeID IS NULL
	AND
		LanguageSpecific < 3
	/* Remember how many properties we have */
	SET @PropCount = @PropCount + @@ROWCOUNT
	/* Get all language specific dynamic properties */
	INSERT INTO #tmpprop
		(fkPageDefinitionID,
		fkPageDefinitionTypeID,
		fkLanguageBranchID)
	SELECT
		tblPageDefinition.pkID,
		fkPageDefinitionTypeID,
		tblLanguageBranch.pkID
	FROM
		tblPageDefinition
	CROSS JOIN
		tblLanguageBranch
	WHERE
		fkPageTypeID IS NULL
	AND
		LanguageSpecific > 2
	AND
		tblLanguageBranch.Enabled = 1
	ORDER BY
		tblLanguageBranch.pkID
	/* Remember how many properties we have */
	SET @PropCount = @PropCount + @@ROWCOUNT
	/* Get page references for all properties (if possible) */
	WHILE (@PropCount > 0 AND @PageID IS NOT NULL)
	BEGIN
		/* Update properties that are defined for this page */
		UPDATE #tmpprop
		SET fkPageID=@PageID
		FROM #tmpprop
		INNER JOIN tblProperty ON #tmpprop.fkPageDefinitionID=tblProperty.fkPageDefinitionID
		WHERE 				
			tblProperty.fkPageID=@PageID AND 
			#tmpprop.fkPageID IS NULL
		AND
			#tmpprop.fkLanguageBranchID = tblProperty.fkLanguageBranchID
		OR
			#tmpprop.fkLanguageBranchID IS NULL
		/* Remember how many properties we have left */
		SET @PropCount = @PropCount - @@ROWCOUNT
		/* Go up one step in the tree */
		SELECT @PageID = fkParentID FROM tblPage WHERE pkID = @PageID
	END
	/* Include all property rows */
	SELECT
		#tmpprop.fkPageDefinitionID,
		#tmpprop.fkPageID,
		PD.Name AS PropertyName,
		LanguageSpecific,
		RTRIM(LB.LanguageID) AS BranchLanguageID,
		ScopeName,
		CONVERT(INT,Boolean) AS Boolean,
		Number AS IntNumber,
		FloatNumber,
		PageType, 
		PageLink AS PageLinkID,
		LinkGuid,
		Date AS DateValue, 
		String, 
		LongString
	FROM
		#tmpprop
	LEFT JOIN
		tblLanguageBranch AS LB
	ON
		LB.pkID = #tmpprop.fkLanguageBranchID
	LEFT JOIN
		tblPageDefinition AS PD
	ON
		PD.pkID = #tmpprop.fkPageDefinitionID
	LEFT JOIN
		tblProperty AS P
	ON
		P.fkPageID = #tmpprop.fkPageID
	AND
		P.fkPageDefinitionID = #tmpprop.fkPageDefinitionID
	AND
		P.fkLanguageBranchID = #tmpprop.fkLanguageBranchID
	ORDER BY
		LanguageSpecific,
		#tmpprop.fkLanguageBranchID,
		FieldOrder
	DROP TABLE #tmpprop
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[netLongStringLoad]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netLongStringLoad]
(
	@LongStringGuid	UNIQUEIDENTIFIER
)
AS
BEGIN
	SELECT LongString FROM tblProperty WHERE guid=@LongStringGuid
END
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[netPageDefinitionDynamicCheck]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageDefinitionDynamicCheck]
(
	@PageDefinitionID	INT
)
AS
BEGIN
	SET NOCOUNT ON
	SELECT  DISTINCT
		tblProperty.fkPageID as ContentID, 
		tblLanguageBranch.Name,
		tblLanguageBranch.LanguageID AS LanguageBranch,
		tblLanguageBranch.Name AS LanguageBranchName,
		0 AS WorkID
	FROM 
		tblProperty
	INNER JOIN
		tblPage ON tblPage.pkID=tblProperty.fkPageID
	INNER JOIN
		tblLanguageBranch ON tblLanguageBranch.pkID=tblProperty.fkLanguageBranchID
	WHERE
		tblProperty.fkPageDefinitionID=@PageDefinitionID AND
		tblProperty.fkLanguageBranchID<>tblPage.fkMasterLanguageBranchID
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[netPageDataLoad]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageDataLoad]
(
	@PageID	INT, 
	@LanguageBranchID INT
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @PageTypeID INT
	DECLARE @MasterLanguageID INT
	SELECT @PageTypeID = tblPage.fkPageTypeID FROM tblPage
		WHERE tblPage.pkID=@PageID
	/*This procedure should always return a page (if exist), preferable in requested language else in master language*/
	IF (@LanguageBranchID = -1 OR NOT EXISTS (SELECT Name FROM tblPageLanguage WHERE fkPageID=@PageID AND fkLanguageBranchID = @LanguageBranchID))
		SELECT @LanguageBranchID = fkMasterLanguageBranchID  FROM tblPage
			WHERE tblPage.pkID=@PageID
	SELECT @MasterLanguageID = fkMasterLanguageBranchID FROM tblPage WHERE tblPage.pkID=@PageID
	/* Get data for page */
	SELECT
		tblPage.pkID AS PageLinkID,
		NULL AS PageLinkWorkID,
		fkParentID  AS PageParentLinkID,
		fkPageTypeID AS PageTypeID,
		NULL AS PageTypeName,
		CONVERT(INT,VisibleInMenu) AS PageVisibleInMenu,
		ChildOrderRule AS PageChildOrderRule,
		PeerOrder AS PagePeerOrder,
		CONVERT(NVARCHAR(38),tblPage.PageGUID) AS PageGUID,
		ArchivePageGUID AS PageArchiveLinkID,
		ExternalFolderID AS PageFolderID,
		CONVERT(INT,Deleted) AS PageDeleted,
		DeletedBy AS PageDeletedBy,
		DeletedDate AS PageDeletedDate,
		(SELECT ChildOrderRule FROM tblPage AS ParentPage WHERE ParentPage.pkID=tblPage.fkParentID) AS PagePeerOrderRule,
		fkMasterLanguageBranchID AS PageMasterLanguageBranchID,
		CreatorName
	FROM tblPage
	WHERE tblPage.pkID=@PageID
	IF (@@ROWCOUNT = 0)
		RETURN 0
	/* Get data for page languages */
	SELECT
		L.fkPageID AS PageID,
		CASE L.AutomaticLink
			WHEN 1 THEN
				(CASE
					WHEN L.PageLinkGUID IS NULL THEN 0	/* EPnLinkNormal */
					WHEN L.FetchData=1 THEN 4				/* EPnLinkFetchdata */
					ELSE 1								/* EPnLinkShortcut */
				END)
			ELSE
				(CASE
					WHEN L.LinkURL=N'#' THEN 3				/* EPnLinkInactive */
					ELSE 2								/* EPnLinkExternal */
				END)
		END AS PageShortcutType,
		L.ExternalURL AS PageExternalURL,
		L.PageLinkGUID AS PageShortcutLinkID,
		L.Name AS PageName,
		L.URLSegment AS PageURLSegment,
		L.LinkURL AS PageLinkURL,
		L.Created AS PageCreated,
		L.Changed AS PageChanged,
		L.Saved AS PageSaved,
		L.StartPublish AS PageStartPublish,
		L.StopPublish AS PageStopPublish,
		CONVERT(INT,L.PendingPublish) AS PagePendingPublish,
		L.CreatorName AS PageCreatedBy,
		L.ChangedByName AS PageChangedBy,
		-- RTRIM(tblPageLanguage.fkLanguageID) AS PageLanguageID,
		L.fkFrameID AS PageTargetFrame,
		0 AS PageChangedOnPublish,
		0 AS PageDelayedPublish,
		L.fkLanguageBranchID AS PageLanguageBranchID,
		CASE 
			WHEN L.PendingPublish=1 THEN 2			/* EPnWorkStatusCheckedOut */
			ELSE 4									/* EPnWorkStatusPublished */
		END AS PageWorkStatus
	FROM tblPageLanguage AS L
	WHERE L.fkPageID=@PageID
		AND L.fkLanguageBranchID=@LanguageBranchID
	/* Get the property data for the requested language */
	SELECT
		tblPageDefinition.Name AS PropertyName,
		tblPageDefinition.pkID as PropertyDefinitionID,
		ScopeName,
		CONVERT(INT, Boolean) AS Boolean,
		Number AS IntNumber,
		FloatNumber,
		PageType,
		PageLink AS PageLinkID,
		LinkGuid,
		Date AS DateValue,
		String,
		LongString,
		tblProperty.fkLanguageBranchID AS LanguageBranchID
	FROM tblProperty
	INNER JOIN tblPageDefinition ON tblPageDefinition.pkID = tblProperty.fkPageDefinitionID
	WHERE tblProperty.fkPageID=@PageID AND NOT tblPageDefinition.fkPageTypeID IS NULL
		AND (tblProperty.fkLanguageBranchID = @LanguageBranchID 
		OR (tblProperty.fkLanguageBranchID = @MasterLanguageID AND tblPageDefinition.LanguageSpecific < 3))
	/*Get category information*/
	SELECT fkPageID AS PageID,fkCategoryID,CategoryType
	FROM tblCategoryPage
	WHERE fkPageID=@PageID AND CategoryType=0
	ORDER BY fkCategoryID
	/* Get access information */
	SELECT
		fkContentID AS PageID,
		Name,
		IsRole,
		AccessMask
	FROM
		tblContentAccess
	WHERE 
	    fkContentID=@PageID
	ORDER BY
	    IsRole DESC,
		Name
	/* Get all languages for the page */
	SELECT fkLanguageBranchID as PageLanguageBranchID FROM tblPageLanguage
		WHERE tblPageLanguage.fkPageID=@PageID
RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[netPageLanguageSettingUpdate]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageLanguageSettingUpdate]
(
	@PageID				INT,
	@LanguageBranch		NCHAR(17),
	@ReplacementBranch	NCHAR(17) = NULL,
	@LanguageBranchFallback NVARCHAR(1000) = NULL,
	@Active				BIT
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @LangBranchID INT
	DECLARE @ReplacementBranchID INT
	SELECT @LangBranchID = pkID FROM tblLanguageBranch WHERE LanguageID = @LanguageBranch
	IF @LangBranchID IS NULL
	BEGIN
		RAISERROR('Language branch "%s" is not defined',16,1, @LanguageBranch)
		RETURN 0
	END
	IF NOT @ReplacementBranch IS NULL
	BEGIN
		SELECT @ReplacementBranchID = pkID FROM tblLanguageBranch WHERE LanguageID = @ReplacementBranch
		IF @ReplacementBranchID IS NULL
		BEGIN
			RAISERROR('Replacement language branch "%s" is not defined',16,1, @ReplacementBranch)
			RETURN 0
		END
	END
	IF EXISTS(SELECT * FROM tblPageLanguageSetting WHERE fkPageID=@PageID AND fkLanguageBranchID=@LangBranchID)
		UPDATE tblPageLanguageSetting SET
			fkReplacementBranchID	= @ReplacementBranchID,
			LanguageBranchFallback  = @LanguageBranchFallback,
			Active					= @Active
		WHERE fkPageID=@PageID AND fkLanguageBranchID=@LangBranchID
	ELSE
		INSERT INTO tblPageLanguageSetting(
				fkPageID,
				fkLanguageBranchID,
				fkReplacementBranchID,
				LanguageBranchFallback,
				Active)
		VALUES(
				@PageID, 
				@LangBranchID,
				@ReplacementBranchID,
				@LanguageBranchFallback,
				@Active
			)
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[netPageLanguageSettingListTree]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageLanguageSettingListTree]
AS
BEGIN
	SET NOCOUNT ON
	SELECT
	    fkPageID,
        RTRIM(Branch.LanguageID) as LanguageBranch,
        RTRIM(ReplacementBranch.LanguageID) as ReplacementBranch,
        LanguageBranchFallback,
        Active
	FROM 
	    tblPageLanguageSetting
	INNER JOIN 
	    tblLanguageBranch AS Branch 
	ON 
	    Branch.pkID = tblPageLanguageSetting.fkLanguageBranchID
	LEFT JOIN 
	    tblLanguageBranch AS ReplacementBranch 
	ON 
	    ReplacementBranch.pkID = tblPageLanguageSetting.fkReplacementBranchID
	ORDER BY 
	    fkPageID ASC
END
GO
/****** Object:  StoredProcedure [dbo].[netPageLanguageSettingList]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageLanguageSettingList]
@PageID INT
AS
BEGIN
	SET NOCOUNT ON
	SELECT	fkPageID,
			RTRIM(Branch.LanguageID) as LanguageBranch,
			RTRIM(ReplacementBranch.LanguageID) as ReplacementBranch,
			LanguageBranchFallback,
			Active
	FROM tblPageLanguageSetting
	INNER JOIN tblLanguageBranch AS Branch ON Branch.pkID = tblPageLanguageSetting.fkLanguageBranchID
	LEFT JOIN tblLanguageBranch AS ReplacementBranch ON ReplacementBranch.pkID = tblPageLanguageSetting.fkReplacementBranchID
	WHERE fkPageID=@PageID
END
GO
/****** Object:  StoredProcedure [dbo].[netPageLanguageSettingDelete]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageLanguageSettingDelete]
(
	@PageID			INT,
	@LanguageBranch	NCHAR(17)
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @LangBranchID INT
	SELECT @LangBranchID = pkID FROM tblLanguageBranch WHERE LanguageID = @LanguageBranch
	IF @LangBranchID IS NULL
	BEGIN
		RAISERROR('Language branch %s is not defined',16,1, @LanguageBranch)
		RETURN 0
	END
	DELETE FROM tblPageLanguageSetting WHERE fkPageID=@PageID AND fkLanguageBranchID=@LangBranchID
END
GO
/****** Object:  StoredProcedure [dbo].[netPageListPaged]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageListPaged]
(
	@Binary VARBINARY(8000),
	@Threshold INT = 0,
	@LanguageBranchID INT
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Pages TABLE (LocalPageID INT)
	DECLARE	@Length SMALLINT
	DECLARE @Index SMALLINT
	SET @Index = 1
	SET @Length = DATALENGTH(@Binary)
	WHILE (@Index <= @Length)
	BEGIN
		INSERT INTO @Pages VALUES(SUBSTRING(@Binary, @Index, 4))
		SET @Index = @Index + 4
	END
	/* Get all languages for all pages*/
	SELECT tblPageLanguage.fkPageID as PageID, tblPage.fkPageTypeID as PageTypeID, tblPageLanguage.fkLanguageBranchID as PageLanguageBranchID 
	FROM tblPageLanguage
	INNER JOIN @Pages on LocalPageID=tblPageLanguage.fkPageID
	INNER JOIN tblPage ON tblPage.pkID = tblPageLanguage.fkPageID
	ORDER BY tblPageLanguage.fkPageID
/* Get data for page */
	SELECT
		LocalPageID AS PageLinkID,
		NULL AS PageLinkWorkID,
		fkParentID  AS PageParentLinkID,
		fkPageTypeID AS PageTypeID,
		NULL AS PageTypeName,
		CONVERT(INT,VisibleInMenu) AS PageVisibleInMenu,
		ChildOrderRule AS PageChildOrderRule,
		0 AS PagePeerOrderRule,	-- No longer used
		PeerOrder AS PagePeerOrder,
		CONVERT(NVARCHAR(38),tblPage.PageGUID) AS PageGUID,
		ArchivePageGUID AS PageArchiveLinkID,
		ExternalFolderID AS PageFolderID,
		CONVERT(INT,Deleted) AS PageDeleted,
		DeletedBy AS PageDeletedBy,
		DeletedDate AS PageDeletedDate,
		fkMasterLanguageBranchID AS PageMasterLanguageBranchID,
		CreatorName
	FROM @Pages
	INNER JOIN tblPage ON LocalPageID=tblPage.pkID
	ORDER BY tblPage.pkID
	IF (@@ROWCOUNT = 0)
	BEGIN
		RETURN
	END
	/* Get data for page languages */
	SELECT
		CASE 
			WHEN L.PendingPublish=1 THEN 2			/* EPnWorkStatusCheckedOut */
			ELSE 4									/* EPnWorkStatusPublished */
		END AS PageWorkStatus,
		L.fkPageID AS PageID,
		CASE AutomaticLink
			WHEN 1 THEN
				(CASE
					WHEN L.PageLinkGUID IS NULL THEN 0	/* EPnLinkNormal */
					WHEN L.FetchData=1 THEN 4				/* EPnLinkFetchdata */
					ELSE 1								/* EPnLinkShortcut */
				END)
			ELSE
				(CASE
					WHEN L.LinkURL=N'#' THEN 3				/* EPnLinkInactive */
					ELSE 2								/* EPnLinkExternal */
				END)
		END AS PageShortcutType,
		L.ExternalURL AS PageExternalURL,
		L.PageLinkGUID AS PageShortcutLinkID,
		L.Name AS PageName,
		L.URLSegment AS PageURLSegment,
		L.LinkURL AS PageLinkURL,
		L.Created AS PageCreated,
		L.Changed AS PageChanged,
		L.Saved AS PageSaved,
		L.StartPublish AS PageStartPublish,
		L.StopPublish AS PageStopPublish,
		CONVERT(INT,L.PendingPublish) AS PagePendingPublish,
		L.CreatorName AS PageCreatedBy,
		L.ChangedByName AS PageChangedBy,
		L.fkFrameID AS PageTargetFrame,
		0 AS PageChangedOnPublish,
		0 AS PageDelayedPublish,
		L.fkLanguageBranchID AS PageLanguageBranchID
	FROM @Pages AS P
	INNER JOIN tblPageLanguage AS L ON LocalPageID=L.fkPageID
	WHERE 
		L.fkLanguageBranchID = @LanguageBranchID
	OR
		L.fkLanguageBranchID = (SELECT tblPage.fkMasterLanguageBranchID FROM tblPage
			WHERE tblPage.pkID=L.fkPageID)
	ORDER BY L.fkPageID
	IF (@@ROWCOUNT = 0)
	BEGIN
		RETURN
	END
	/* Get the properties */
	/* NOTE! The CASE:s for LongString and Guid uses the precomputed LongStringLength to avoid 
	referencing LongString which may slow down the query */
	SELECT
		tblProperty.fkPageID AS PageID,
		tblPageDefinition.Name AS PropertyName,
		tblPageDefinition.pkID as PropertyDefinitionID,
		ScopeName,
		CONVERT(INT, Boolean) AS Boolean,
		Number AS IntNumber,
		FloatNumber,
		PageType,
		PageLink AS PageLinkID,
		LinkGuid,	
		Date AS DateValue,
		String,
		(CASE 
			WHEN (@Threshold = 0) OR (COALESCE(LongStringLength, 2147483647) < @Threshold) THEN
				LongString
			ELSE
				NULL
		END) AS LongString,
		tblProperty.fkLanguageBranchID AS LanguageBranchID,
		(CASE 
			WHEN (@Threshold = 0) OR (COALESCE(LongStringLength, 2147483647) < @Threshold) THEN
				NULL
			ELSE
				guid
		END) AS Guid
	FROM @Pages AS P
	INNER JOIN tblPage ON tblPage.pkID=P.LocalPageID
	INNER JOIN tblProperty WITH (NOLOCK) ON tblPage.pkID=tblProperty.fkPageID --The join with tblPage ensures data integrity
	INNER JOIN tblPageDefinition ON tblPageDefinition.pkID=tblProperty.fkPageDefinitionID
	WHERE NOT tblPageDefinition.fkPageTypeID IS NULL AND
		(tblProperty.fkLanguageBranchID = @LanguageBranchID
	OR
		tblProperty.fkLanguageBranchID = tblPage.fkMasterLanguageBranchID)
	ORDER BY tblPage.pkID
	/*Get category information*/
	SELECT 
		fkPageID AS PageID,
		fkCategoryID,
		CategoryType
	FROM tblCategoryPage
	INNER JOIN @Pages ON LocalPageID=tblCategoryPage.fkPageID
	WHERE CategoryType=0
	ORDER BY fkPageID,fkCategoryID
	/* Get access information */
	SELECT
		fkContentID AS PageID,
		tblContentAccess.Name,
		IsRole,
		AccessMask
	FROM
		@Pages
	INNER JOIN 
	    tblContentAccess ON LocalPageID=tblContentAccess.fkContentID
	ORDER BY
		fkContentID
END
GO
/****** Object:  StoredProcedure [dbo].[netPageListByLanguage]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageListByLanguage]
(
	@LanguageID nchar(17),
	@PageID INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	IF @PageID IS NULL
	BEGIN
		SELECT tblPageLanguage.fkPageID as "PageID", tblPage.ContentType
		FROM tblPageLanguage
		INNER JOIN tblPage on tblPage.pkID = tblPageLanguage.fkPageID
		INNER JOIN tblLanguageBranch ON tblLanguageBranch.pkID=tblPageLanguage.fkLanguageBranchID
		WHERE tblLanguageBranch.LanguageID = @LanguageID
	END
	ELSE
	BEGIN
		SELECT tblPageLanguage.fkPageID as "PageID", tblPage.ContentType
		FROM tblPageLanguage
		INNER JOIN tblPage on tblPage.pkID = tblPageLanguage.fkPageID
		INNER JOIN tblTree ON tblTree.fkChildID=tblPageLanguage.fkPageID
		INNER JOIN tblLanguageBranch ON tblLanguageBranch.pkID=tblPageLanguage.fkLanguageBranchID
		WHERE tblTree.fkParentID=@PageID AND
		tblLanguageBranch.LanguageID = @LanguageID
		ORDER BY NestingLevel DESC
	END
END
GO
/****** Object:  StoredProcedure [dbo].[netPageTypeGetUsage]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageTypeGetUsage]
(
	@PageTypeID		INT,
	@OnlyPublished	BIT = 0
)
AS
BEGIN
	SET NOCOUNT ON
	IF (@OnlyPublished = 1)
	BEGIN
		SELECT DISTINCT
			tblPage.pkID as PageID, 
			0 AS WorkID,
			tblPageLanguage.Name,
			tblLanguageBranch.LanguageID AS LanguageBranch
		FROM 
			tblPage
		INNER JOIN 
			tblPageLanguage ON tblPage.pkID=tblPageLanguage.fkPageID
		INNER JOIN
			tblLanguageBranch ON tblLanguageBranch.pkID=tblPageLanguage.fkLanguageBranchID
		WHERE
			tblPage.fkPageTypeID = @PageTypeID
	END
	ELSE
	BEGIN
		SELECT DISTINCT
			tblPage.pkID as PageID, 
			tblWorkPage.pkID as WorkID,
			tblWorkPage.Name,
			tblLanguageBranch.LanguageID AS LanguageBranch
		FROM 
			tblWorkPage
		INNER JOIN 
			tblPage ON tblWorkPage.fkPageID = tblPage.pkID
		INNER JOIN 
			tblPageLanguage ON tblWorkPage.fkPageID=tblPageLanguage.fkPageID 
		INNER JOIN
			tblLanguageBranch ON tblLanguageBranch.pkID=tblWorkPage.fkLanguageBranchID
		WHERE
			tblPage.fkPageTypeID = @PageTypeID
	END
END
GO
/****** Object:  StoredProcedure [dbo].[netPagesChangedAfter]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPagesChangedAfter]
( 
	@RootID INT,
	@ChangedAfter DATETIME,
	@MaxHits INT
)
AS
BEGIN
	SET NOCOUNT ON
    SET @MaxHits = @MaxHits + 1 -- Return one more to determine if there are more pages to fetch (gets MaxHits + 1)
    SET ROWCOUNT @MaxHits
	SELECT 
	    tblPageLanguage.fkPageID AS PageID,
		RTRIM(tblLanguageBranch.LanguageID) AS LanguageID
	FROM
		tblPageLanguage
	INNER JOIN
		tblTree
	ON
		tblPageLanguage.fkPageID = tblTree.fkChildID AND (tblTree.fkParentID = @RootID OR (tblTree.fkChildID = @RootID AND tblTree.NestingLevel = 1))
	INNER JOIN
		tblLanguageBranch
	ON
		tblPageLanguage.fkLanguageBranchID = tblLanguageBranch.pkID
	WHERE
		(tblPageLanguage.Changed > @ChangedAfter OR tblPageLanguage.StartPublish > @ChangedAfter) AND
		(tblPageLanguage.StopPublish is NULL OR tblPageLanguage.StopPublish > GetDate()) AND
		tblPageLanguage.PendingPublish=0
	ORDER BY
		tblTree.NestingLevel,
		tblPageLanguage.fkPageID,
		tblPageLanguage.Changed DESC
	SET ROWCOUNT 0
END
GO
/****** Object:  StoredProcedure [dbo].[netURLSegmentSet]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netURLSegmentSet]
(
	@URLSegment			NCHAR(255),
	@PageID				INT,
	@LanguageBranch		NCHAR(17) = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @LangBranchID NCHAR(17);
	SELECT @LangBranchID=pkID FROM tblLanguageBranch WHERE LanguageID=@LanguageBranch
	IF @LangBranchID IS NULL 
	BEGIN 
		if @LanguageBranch IS NOT NULL
			RAISERROR('Language branch %s is not defined',16,1, @LanguageBranch)
		else
			SET @LangBranchID = -1
	END
	UPDATE tblPageLanguage
	SET URLSegment = RTRIM(@URLSegment)
	WHERE fkPageID = @PageID
	AND (@LangBranchID=-1 OR fkLanguageBranchID=@LangBranchID)
	UPDATE tblWorkPage
	SET URLSegment = RTRIM(@URLSegment)
	WHERE fkPageID = @PageID
	AND (@LangBranchID=-1 OR fkLanguageBranchID=@LangBranchID)
END
GO
/****** Object:  StoredProcedure [dbo].[netURLSegmentListPages]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netURLSegmentListPages]
(
	@URLSegment	NCHAR(255)
)
AS
BEGIN
	SET NOCOUNT ON
	IF (LEN(@URLSegment) = 0)
	BEGIN
		set @URLSegment = NULL
	END 
	SELECT DISTINCT fkPageID as "PageID"
	FROM tblPageLanguage
	WHERE URLSegment = @URLSegment
	OR (@URLSegment = NULL AND URLSegment = '' OR URLSegment IS NULL)
END
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[netUnmappedPropertyList]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netUnmappedPropertyList]
AS
BEGIN
	SET NOCOUNT ON
	SELECT 
		tblProperty.LinkGuid as GuidID,
		tblProperty.fkPageID as PageID, 
		tblProperty.fkLanguageBranchID as LanguageBranchID,
		tblPageDefinition.Name as PropertyName,
		tblPageDefinition.fkPageTypeID as PageTypeID
	FROM
		tblProperty INNER JOIN tblPageDefinition on tblProperty.fkPageDefinitionID = tblPageDefinition.pkID
	WHERE
		tblProperty.LinkGuid IS NOT NULL AND
		tblProperty.PageLink IS NULL		
END			
SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[netSubscriptionListRoots]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netSubscriptionListRoots]
AS
BEGIN
	SELECT tblPage.pkID AS PageID
	FROM tblPage
	INNER JOIN tblProperty ON tblProperty.fkPageID		= tblPage.pkID
	INNER JOIN tblPageDefinition ON tblPageDefinition.pkID	= tblProperty.fkPageDefinitionID
	WHERE tblPageDefinition.Name='EPSUBSCRIBE-ROOT' AND NOT tblProperty.PageLink IS NULL AND tblPage.Deleted=0
END
GO
/****** Object:  StoredProcedure [dbo].[netReportSimpleAddresses]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Return a list of pages in a particular branch of the tree changed between a start date and a stop date
CREATE PROCEDURE [dbo].[netReportSimpleAddresses](
	@PageID int,
	@Language int = -1,
	@PageSize int,
	@PageNumber int = 0,
	@SortColumn varchar(40) = 'ExternalURL',
	@SortDescending bit = 0
)
AS
BEGIN
	SET NOCOUNT ON;
	WITH PageCTE AS
	(
		SELECT ROW_NUMBER() OVER(ORDER BY 
			-- Page Name Sorting
			CASE WHEN @SortColumn = 'PageName' AND @SortDescending = 1 THEN tblPageLanguage.Name END DESC,
			CASE WHEN @SortColumn = 'PageName' THEN tblPageLanguage.Name END ASC,
			-- Changed By Sorting
			CASE WHEN @SortColumn = 'ChangedBy' AND @SortDescending = 1 THEN tblPageLanguage.ChangedByName END DESC,
			CASE WHEN @SortColumn = 'ChangedBy' THEN tblPageLanguage.ChangedByName END ASC,
			-- External Url Sorting
			CASE WHEN @SortColumn = 'ExternalURL' AND @SortDescending = 1 THEN tblPageLanguage.ExternalURL END DESC,
			CASE WHEN @SortColumn = 'ExternalURL' THEN tblPageLanguage.ExternalURL END ASC,
			-- Language Sorting
			CASE WHEN @SortColumn = 'Language' AND @SortDescending = 1 THEN tblLanguageBranch.LanguageID END DESC,
			CASE WHEN @SortColumn = 'Language' THEN tblLanguageBranch.LanguageID END ASC
		) AS rownum,
		tblPageLanguage.fkPageID, tblPageLanguage.PublishedVersion, count(*) over () as totcount
		FROM tblPageLanguage 
		INNER JOIN tblTree ON tblTree.fkChildID=tblPageLanguage.fkPageID 
		INNER JOIN tblPage ON tblPage.pkID=tblPageLanguage.fkPageID 
		INNER JOIN tblPageType ON tblPageType.pkID=tblPage.fkPageTypeID
		INNER JOIN tblLanguageBranch ON tblLanguageBranch.pkID=tblPageLanguage.fkLanguageBranchID 
		WHERE 
        (tblTree.fkParentID=@PageID OR (tblPageLanguage.fkPageID=@PageID AND tblTree.NestingLevel = 1 ))
        AND 
        (tblPageLanguage.ExternalURL IS NOT NULL)
        AND
        (@Language = -1 OR tblPageLanguage.fkLanguageBranchID = @Language)
	)
	SELECT PageCTE.fkPageID, PageCTE.PublishedVersion, PageCTE.rownum, totcount
	FROM PageCTE
	WHERE rownum > @PageSize * (@PageNumber)
	AND rownum <= @PageSize * (@PageNumber+1)
	ORDER BY rownum
END
GO
/****** Object:  StoredProcedure [dbo].[netReportReadyToPublish]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Return a list of pages in a particular branch of the tree published between a start date and a stop date
CREATE PROCEDURE [dbo].[netReportReadyToPublish](
	@PageID int,
	@StartDate datetime,
	@StopDate datetime,
	@Language int = -1,
	@ChangedByUserName nvarchar(256) = null,
	@PageSize int,
	@PageNumber int = 0,
	@SortColumn varchar(40) = 'PageName',
	@SortDescending bit = 0,
	@IsReadyToPublish bit = 1
)
AS
BEGIN
	SET NOCOUNT ON;
	WITH PageCTE AS
                    (
                        SELECT ROW_NUMBER() OVER(ORDER BY 
							-- Page Name Sorting
							CASE WHEN @SortColumn = 'PageName' AND @SortDescending = 1 THEN tblWorkPage.Name END DESC,
							CASE WHEN @SortColumn = 'PageName' THEN tblWorkPage.Name END ASC,
							-- Saved Sorting
							CASE WHEN @SortColumn = 'Saved' AND @SortDescending = 1 THEN tblWorkPage.Saved END DESC,
							CASE WHEN @SortColumn = 'Saved' THEN tblWorkPage.Saved END ASC,
							-- StartPublish Sorting
							CASE WHEN @SortColumn = 'StartPublish' AND @SortDescending = 1 THEN tblWorkPage.StartPublish END DESC,
							CASE WHEN @SortColumn = 'StartPublish' THEN tblWorkPage.StartPublish END ASC,
							-- Changed By Sorting
							CASE WHEN @SortColumn = 'ChangedBy' AND @SortDescending = 1 THEN tblWorkPage.ChangedByName END DESC,
							CASE WHEN @SortColumn = 'ChangedBy' THEN tblWorkPage.ChangedByName END ASC,
							-- Language Sorting
							CASE WHEN @SortColumn = 'Language' AND @SortDescending = 1 THEN tblLanguageBranch.LanguageID END DESC,
							CASE WHEN @SortColumn = 'Language' THEN tblLanguageBranch.LanguageID END ASC
							, 
							tblWorkPage.pkID ASC
                        ) AS rownum,
                        tblWorkPage.fkPageID, count(tblWorkPage.fkPageID) over () as totcount,
                        tblWorkPage.pkID as versionId
                        FROM tblWorkPage 
                        INNER JOIN tblTree ON tblTree.fkChildID=tblWorkPage.fkPageID 
                        INNER JOIN tblPage ON tblPage.pkID=tblWorkPage.fkPageID 
						INNER JOIN tblLanguageBranch ON tblLanguageBranch.pkID=tblWorkPage.fkLanguageBranchID 
                        WHERE 
							(tblTree.fkParentID=@PageID OR (tblWorkPage.fkPageID=@PageID AND tblTree.NestingLevel = 1 ))
                        AND
							(LEN(@ChangedByUserName) = 0 OR tblWorkPage.ChangedByName = @ChangedByUserName)
                        AND
							(@Language = -1 OR tblWorkPage.fkLanguageBranchID = @Language)
                        AND 
							(@StartDate IS NULL OR tblWorkPage.Saved > @StartDate)
                        AND
							(@StopDate IS NULL OR tblWorkPage.Saved < @StopDate)
                        AND
							(tblWorkPage.ReadyToPublish = @IsReadyToPublish AND tblWorkPage.HasBeenPublished = 0)
                    )
                    SELECT PageCTE.fkPageID, PageCTE.rownum, totcount, PageCTE.versionId
                    FROM PageCTE
                    WHERE rownum > @PageSize * (@PageNumber)
                    AND rownum <= @PageSize * (@PageNumber+1)
                    ORDER BY rownum
	END
GO
/****** Object:  StoredProcedure [dbo].[netQuickSearchByPath]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netQuickSearchByPath]
(
	@Path	NVARCHAR(1000),
	@PageID	INT,
	@LanguageBranch	NCHAR(17) = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Index INT
	DECLARE @LastIndex INT
	DECLARE @LinkURL NVARCHAR(255)
	DECLARE @Name NVARCHAR(255)
	DECLARE @LangBranchID NCHAR(17);
	SELECT @LangBranchID=pkID FROM tblLanguageBranch WHERE LanguageID=@LanguageBranch
	IF @LangBranchID IS NULL 
	BEGIN 
		if @LanguageBranch IS NOT NULL
			RAISERROR('Language branch %s is not defined',16,1, @LanguageBranch)
		else
			SET @LangBranchID = -1
	END
	SET @Index = CHARINDEX('/',@Path)
	SET @LastIndex = 0
	WHILE @Index > 0 OR @Index IS NULL
	BEGIN
		SET @Name = SUBSTRING(@Path,@LastIndex,@Index-@LastIndex)
		SELECT TOP 1 @PageID=pkID,@LinkURL=tblPageLanguage.LinkURL
		FROM tblPageLanguage
		LEFT JOIN tblPage AS tblPage ON tblPage.pkID=tblPageLanguage.fkPageID
		WHERE tblPageLanguage.Name LIKE @Name AND (tblPage.fkParentID=@PageID OR @PageID IS NULL)
		AND (tblPageLanguage.fkLanguageBranchID=@LangBranchID OR @LangBranchID=-1)
		IF @@ROWCOUNT=0
		BEGIN
			SET @Index=0
			SET @LinkURL = NULL
		END
		ELSE
		BEGIN
			SET @LastIndex = @Index + 1
			SET @Index = CHARINDEX('/',@Path,@LastIndex+1)
		END
	END	
	SELECT @LinkURL
END
GO
/****** Object:  StoredProcedure [dbo].[netPersonalActivityList]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPersonalActivityList]
(
    @UserName NVARCHAR(255)
)
AS
BEGIN
	SET NOCOUNT ON
	/* Not ready */
	SELECT
		W.fkPageID AS PageID,
		W.pkID AS WorkID,
		0 AS VersionStatus,
		W.ChangedByName AS UserName,
		W.Saved AS ItemCreated
	FROM
		tblWorkPage AS W
	INNER JOIN
		tblPage AS P ON P.pkID=W.fkPageID
	WHERE
	    W.ChangedByName=@UserName AND
		W.HasBeenPublished=0 AND
		W.ReadyToPublish=0 AND
		W.Rejected=0 AND
		P.Deleted=0
	UNION
	/* Ready to publish */
	SELECT DISTINCT
		W.fkPageID AS PageID,
		W.pkID AS WorkID,
		1 AS VersionStatus,
		W.ChangedByName AS UserName,
		W.Saved AS ItemCreated
	FROM
		tblWorkPage AS W
	LEFT JOIN
		tblPage AS P ON P.pkID=W.fkPageID
	WHERE
		W.HasBeenPublished=0 AND
		W.ReadyToPublish=1 AND
		W.DelayedPublish=0 AND
		W.Rejected=0 AND
		P.Deleted=0 AND
		((P.PublishedVersion<>W.pkID) OR (P.PublishedVersion IS NULL))
	UNION
	/* Rejected pages */
	SELECT 
		W.fkPageID AS PageID,
		W.pkID AS WorkID,
		2 AS VersionStatus,
		W.ChangedByName AS UserName,
		W.Saved AS ItemCreated
	FROM
		tblWorkPage AS W
	INNER JOIN
		tblPage AS P ON P.pkID=W.fkPageID
	WHERE
	    W.ChangedByName=@UserName AND
		W.HasBeenPublished=0 AND
		W.Rejected=1 AND
		P.Deleted=0
	ORDER BY VersionStatus DESC, ItemCreated DESC
END
GO
/****** Object:  StoredProcedure [dbo].[netQuickSearchByExternalUrl]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netQuickSearchByExternalUrl]
(
	@Url	NVARCHAR(255)
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @LoweredUrl NVARCHAR(255)
	SET @LoweredUrl = Lower(@Url)
	/*
		Performance notes: The subquery "Pages" must not have any more predicates or return the values used in the outer WHERE-clause, otherwise
		SQL Server falls back to a costly index scan. The performance hints LOOP on the joins are also required for the same reason, the resultset
		from "Pages" is so small that a loop join is superior in performance to index scan/hash match, a factor 1000x.
	*/
	SELECT 
		tblPageLanguage.fkPageID,
		tblLanguageBranch.LanguageID as LanguageBranch
	FROM 
		(
			SELECT fkPageID,fkLanguageBranchID
			FROM tblPageLanguage
			WHERE tblPageLanguage.ExternalURL=@LoweredUrl
		) AS Pages
	INNER LOOP JOIN 
		tblPage ON tblPage.pkID = Pages.fkPageID
	INNER LOOP JOIN
		tblPageLanguage ON tblPageLanguage.fkPageID=Pages.fkPageID AND tblPageLanguage.fkLanguageBranchID=Pages.fkLanguageBranchID
	INNER LOOP JOIN
		tblLanguageBranch ON tblLanguageBranch.pkID = Pages.fkLanguageBranchID
	WHERE 
		tblPage.Deleted=0 AND 
		tblPageLanguage.PendingPublish=0 AND
		tblPageLanguage.StartPublish <= GetDate() AND
		(tblPageLanguage.StopPublish IS NULL OR tblPageLanguage.StopPublish >= GetDate())
	ORDER BY
		tblPageLanguage.Changed DESC
END
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[netPropertySearchValue]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPropertySearchValue]
(
	@PageID			INT,
	@PropertyName 	NVARCHAR(255),
	@Equals			BIT = 0,
	@NotEquals		BIT = 0,
	@GreaterThan	BIT = 0,
	@LessThan		BIT = 0,
	@Boolean		BIT = NULL,
	@Number			INT = NULL,
	@FloatNumber	FLOAT = NULL,
	@PageType		INT = NULL,
	@PageLink		INT = NULL,
	@Date			DATETIME = NULL,
	@LanguageBranch	NCHAR(17) = NULL
)
AS
BEGIN
	DECLARE @LangBranchID NCHAR(17);
	SELECT @LangBranchID=pkID FROM tblLanguageBranch WHERE LanguageID=@LanguageBranch
	IF @LangBranchID IS NULL 
	BEGIN 
		if @LanguageBranch IS NOT NULL
			RAISERROR('Language branch %s is not defined',16,1, @LanguageBranch)
		else
			SET @LangBranchID = -1
	END
	IF NOT @Boolean IS NULL
		SELECT DISTINCT(tblProperty.fkPageID)
		FROM       tblProperty
		INNER JOIN tblPageDefinition ON tblPageDefinition.pkID=tblProperty.fkPageDefinitionID
		INNER JOIN tblPageType ON tblPageDefinition.fkPageTypeID=tblPageType.pkID
		INNER JOIN tblTree ON tblTree.fkChildID=tblProperty.fkPageID
		INNER JOIN tblPageLanguage on tblPageLanguage.fkPageID=tblProperty.fkPageID
		WHERE tblPageType.ContentType = 0
		AND tblTree.fkParentID=@PageID 
		AND tblPageDefinition.Name=@PropertyName
		AND Property = 0
		AND Boolean = @Boolean
		AND (@LangBranchID=-1 OR tblProperty.fkLanguageBranchID=@LangBranchID OR tblPageDefinition.LanguageSpecific<3)
	ELSE IF NOT @Number IS NULL
		SELECT DISTINCT(tblProperty.fkPageID)
		FROM       tblProperty
		INNER JOIN tblPageDefinition ON tblPageDefinition.pkID=tblProperty.fkPageDefinitionID
		INNER JOIN tblPageType ON tblPageDefinition.fkPageTypeID=tblPageType.pkID
		INNER JOIN tblTree ON tblTree.fkChildID=tblProperty.fkPageID
		INNER JOIN tblPageLanguage on tblPageLanguage.fkPageID=tblProperty.fkPageID
		WHERE tblPageType.ContentType = 0
		AND tblTree.fkParentID=@PageID 
		AND tblPageDefinition.Name=@PropertyName
		AND Property = 1
		AND (@LangBranchID=-1 OR tblProperty.fkLanguageBranchID=@LangBranchID OR tblPageDefinition.LanguageSpecific<3)
		AND 
		(
			(@Equals=1 		AND (Number = @Number OR (@Number IS NULL AND Number IS NULL)))
			OR
			(@GreaterThan=1 	AND Number > @Number)
			OR
			(@LessThan=1 		AND Number < @Number)
			OR
			(@NotEquals=1		AND Number <> @Number)
		)
	ELSE IF NOT @FloatNumber IS NULL
		SELECT DISTINCT(tblProperty.fkPageID)
		FROM       tblProperty
		INNER JOIN tblPageDefinition ON tblPageDefinition.pkID=tblProperty.fkPageDefinitionID
		INNER JOIN tblPageType ON tblPageDefinition.fkPageTypeID=tblPageType.pkID
		INNER JOIN tblTree ON tblTree.fkChildID=tblProperty.fkPageID
		INNER JOIN tblPageLanguage on tblPageLanguage.fkPageID=tblProperty.fkPageID
		WHERE tblPageType.ContentType = 0
		AND tblTree.fkParentID=@PageID 
		AND tblPageDefinition.Name=@PropertyName
		AND Property = 2
		AND (@LangBranchID=-1 OR tblProperty.fkLanguageBranchID=@LangBranchID OR tblPageDefinition.LanguageSpecific<3)
		AND
		(
			(@Equals=1 		AND (FloatNumber = @FloatNumber OR (@FloatNumber IS NULL AND FloatNumber IS NULL)))
			OR
			(@GreaterThan=1 	AND FloatNumber > @FloatNumber)
			OR
			(@LessThan=1 		AND FloatNumber < @FloatNumber)
			OR
			(@NotEquals=1		AND FloatNumber <> @FloatNumber)
		)
	ELSE IF NOT @PageType IS NULL
		SELECT DISTINCT(tblProperty.fkPageID)
		FROM       tblProperty
		INNER JOIN tblPageDefinition ON tblPageDefinition.pkID=tblProperty.fkPageDefinitionID
		INNER JOIN tblPageType ON tblPageDefinition.fkPageTypeID=tblPageType.pkID
		INNER JOIN tblTree ON tblTree.fkChildID=tblProperty.fkPageID
		INNER JOIN tblPageLanguage on tblPageLanguage.fkPageID=tblProperty.fkPageID
		WHERE tblPageType.ContentType = 0
		AND tblTree.fkParentID=@PageID 
		AND tblPageDefinition.Name=@PropertyName
		AND Property = 3
		AND (@LangBranchID=-1 OR tblProperty.fkLanguageBranchID=@LangBranchID OR tblPageDefinition.LanguageSpecific<3)
		AND
		(
			(@Equals=1 		AND (PageType = @PageType OR (@PageType IS NULL AND PageType IS NULL)))
			OR
			(@GreaterThan=1 	AND PageType > @PageType)
			OR
			(@LessThan=1 		AND PageType < @PageType)
			OR
			(@NotEquals=1		AND PageType <> @PageType)
		)
	ELSE IF NOT @PageLink IS NULL
		SELECT DISTINCT(tblProperty.fkPageID)
		FROM       tblProperty
		INNER JOIN tblPageDefinition ON tblPageDefinition.pkID=tblProperty.fkPageDefinitionID
		INNER JOIN tblPageType ON tblPageDefinition.fkPageTypeID=tblPageType.pkID
		INNER JOIN tblTree ON tblTree.fkChildID=tblProperty.fkPageID
		INNER JOIN tblPageLanguage on tblPageLanguage.fkPageID=tblProperty.fkPageID
		WHERE tblPageType.ContentType = 0
		AND tblTree.fkParentID=@PageID 
		AND tblPageDefinition.Name=@PropertyName
		AND Property = 4
		AND (@LangBranchID=-1 OR tblProperty.fkLanguageBranchID=@LangBranchID OR tblPageDefinition.LanguageSpecific<3)
		AND
		(
			(@Equals=1 		AND (PageLink = @PageLink OR (@PageLink IS NULL AND PageLink IS NULL)))
			OR
			(@GreaterThan=1 	AND PageLink > @PageLink)
			OR
			(@LessThan=1 		AND PageLink < @PageLink)
			OR
			(@NotEquals=1		AND PageLink <> @PageLink)
		)
	ELSE IF NOT @Date IS NULL
		SELECT DISTINCT(tblProperty.fkPageID)
		FROM       tblProperty
		INNER JOIN tblPageDefinition ON tblPageDefinition.pkID=tblProperty.fkPageDefinitionID
		INNER JOIN tblPageType ON tblPageDefinition.fkPageTypeID=tblPageType.pkID
		INNER JOIN tblTree ON tblTree.fkChildID=tblProperty.fkPageID
		INNER JOIN tblPageLanguage on tblPageLanguage.fkPageID=tblProperty.fkPageID
		WHERE tblPageType.ContentType = 0
		AND tblTree.fkParentID=@PageID 
		AND tblPageDefinition.Name=@PropertyName
		AND Property = 5
		AND (@LangBranchID=-1 OR tblProperty.fkLanguageBranchID=@LangBranchID OR tblPageDefinition.LanguageSpecific<3)
		AND
		(
			(@Equals=1 		AND ([Date] = @Date OR (@Date IS NULL AND [Date] IS NULL)))
			OR
			(@GreaterThan=1 	AND [Date] > @Date)
			OR
			(@LessThan=1 		AND [Date] < @Date)
			OR
			(@NotEquals=1		AND [Date] <> @Date)
		)
	ELSE
		SELECT DISTINCT(tblProperty.fkPageID)
		FROM tblProperty
		INNER JOIN tblPageLanguage on tblPageLanguage.fkPageID=tblProperty.fkPageID
		INNER JOIN tblPageDefinition ON tblPageDefinition.pkID=tblProperty.fkPageDefinitionID
		INNER JOIN tblPageType ON tblPageDefinition.fkPageTypeID=tblPageType.pkID
		WHERE tblPageType.ContentType = 0
		AND (@LangBranchID=-1 OR tblProperty.fkLanguageBranchID=@LangBranchID OR tblPageDefinition.LanguageSpecific<3)
END
GO
/****** Object:  StoredProcedure [dbo].[netPropertySearchString]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPropertySearchString]
(
	@PageID				INT,
	@PropertyName 		NVARCHAR(255),
	@UseWildCardsBefore	BIT = 0,
	@UseWildCardsAfter	BIT = 0,
	@String				NVARCHAR(2000) = NULL,
	@LanguageBranch		NCHAR(17) = NULL
)
AS
BEGIN
	DECLARE @LangBranchID INT
	DECLARE @Path VARCHAR(7000)
	DECLARE @SearchString NVARCHAR(2002)
	SELECT @LangBranchID=pkID FROM tblLanguageBranch WHERE LanguageID=@LanguageBranch
	IF @LangBranchID IS NULL 
	BEGIN 
		if @LanguageBranch IS NOT NULL
			RAISERROR('Language branch %s is not defined',16,1, @LanguageBranch)
		else
			SET @LangBranchID = -1
	END
	SELECT @Path=PagePath + CONVERT(VARCHAR, @PageID) + '.' FROM tblPage WHERE pkID=@PageID
	SET @SearchString=CASE    
		WHEN @UseWildCardsBefore=0 AND @UseWildCardsAfter=0 THEN @String
		WHEN @UseWildCardsBefore=1 AND @UseWildCardsAfter=0 THEN '%' + @String
		WHEN @UseWildCardsBefore=0 AND @UseWildCardsAfter=1 THEN @String + '%'
		ELSE '%' + @String + '%'
	END
	SELECT P.pkID
	FROM tblContent AS P
	INNER JOIN tblProperty ON tblProperty.fkPageID=P.pkID
	INNER JOIN tblPageLanguage ON tblPageLanguage.fkPageID=P.pkID
	INNER JOIN tblPageDefinition ON tblPageDefinition.pkID = tblProperty.fkPageDefinitionID and tblPageDefinition.Name = @PropertyName and tblPageDefinition.Property in (6,7)
	WHERE 
		P.ContentType = 0 
	AND
		CHARINDEX(@Path, P.ContentPath) = 1
	AND 
		(@LangBranchID=-1 OR tblProperty.fkLanguageBranchID=@LangBranchID OR tblPageDefinition.LanguageSpecific<3)
	AND
	(
		(@String IS NULL AND (String IS NULL AND LongString IS NULL))
				OR
		(tblPageDefinition.Property=6 AND String LIKE @SearchString)
				OR
		(tblPageDefinition.Property=7 AND LongString LIKE @SearchString)
	)
END
GO
/****** Object:  StoredProcedure [dbo].[netPropertySearchNull]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPropertySearchNull]
(
	@PageID			INT,
	@PropertyName 	NVARCHAR(255),
	@LanguageBranch	NCHAR(17) = NULL
)
AS
BEGIN
	DECLARE @LangBranchID NCHAR(17);
	SELECT @LangBranchID=pkID FROM tblLanguageBranch WHERE LanguageID=@LanguageBranch
	IF @LangBranchID IS NULL 
	BEGIN 
		if @LanguageBranch IS NOT NULL
			RAISERROR('Language branch %s is not defined',16,1, @LanguageBranch)
		else
			SET @LangBranchID = -1
	END
	SELECT DISTINCT(tblPageLanguage.fkPageID)
	FROM tblPageLanguage
	INNER JOIN tblTree ON tblTree.fkChildID=tblPageLanguage.fkPageID
	INNER JOIN tblPage ON tblPage.pkID=tblPageLanguage.fkPageID
	INNER JOIN tblPageType ON tblPageType.pkID=tblPage.fkPageTypeID
	INNER JOIN tblPageDefinition ON tblPageDefinition.fkPageTypeID=tblPageType.pkID
	WHERE tblPageType.ContentType = 0
	AND tblTree.fkParentID=@PageID  
	AND tblPageDefinition.Name=@PropertyName
	AND (@LangBranchID=-1 OR tblPageLanguage.fkLanguageBranchID=@LangBranchID OR tblPageDefinition.LanguageSpecific<3)
	AND NOT EXISTS
		(SELECT * FROM tblProperty 
		WHERE fkPageDefinitionID=tblPageDefinition.pkID 
		AND tblProperty.fkPageID=tblPage.pkID)
END
GO
/****** Object:  StoredProcedure [dbo].[netPropertySearchCategoryMeta]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPropertySearchCategoryMeta]
(
	@PageID			INT,
	@PropertyName 	NVARCHAR(255),
	@Equals			BIT = 0,
	@NotEquals		BIT = 0,
	@CategoryList	NVARCHAR(2000) = NULL,
	@LanguageBranch		NCHAR(17) = NULL
)
AS
BEGIN
	DECLARE @LangBranchID NCHAR(17);
	CREATE TABLE #category (fkCategoryID int)
	IF NOT @CategoryList IS NULL
		EXEC netCategoryStringToTable @CategoryList=@CategoryList
	SELECT fkChildID
	FROM tblTree
	INNER JOIN tblContent WITH (NOLOCK) ON tblTree.fkChildID=tblContent.pkID
	WHERE tblContent.ContentType = 0 AND tblTree.fkParentID=@PageID 
	AND
    	(
		(@CategoryList IS NULL AND 	(
							SELECT Count(tblCategoryPage.fkPageID)
							FROM tblCategoryPage
							WHERE tblCategoryPage.CategoryType=0
							AND tblCategoryPage.fkPageID=tblTree.fkChildID
						)=0
		)
		OR
		(@Equals=1 AND tblTree.fkChildID IN
						(SELECT tblCategoryPage.fkPageID 
						FROM tblCategoryPage 
						INNER JOIN #category ON tblCategoryPage.fkCategoryID=#category.fkCategoryID 
						WHERE tblCategoryPage.CategoryType=0)
		)
		OR
		(@NotEquals=1 AND tblTree.fkChildID NOT IN
						(SELECT tblCategoryPage.fkPageID 
						FROM tblCategoryPage 
						INNER JOIN #category ON tblCategoryPage.fkCategoryID=#category.fkCategoryID 
						WHERE tblCategoryPage.CategoryType=0)
		)
	)
	DROP TABLE #category
END
GO
/****** Object:  StoredProcedure [dbo].[netPropertySearchCategory]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPropertySearchCategory]
(
	@PageID			INT,
	@PropertyName 	NVARCHAR(255),
	@Equals			BIT = 0,
	@NotEquals		BIT = 0,
	@CategoryList	NVARCHAR(2000) = NULL,
	@LanguageBranch	NCHAR(17) = NULL
)
AS
BEGIN
	DECLARE @LangBranchID NCHAR(17);
	SELECT @LangBranchID=pkID FROM tblLanguageBranch WHERE LanguageID=@LanguageBranch
	IF @LangBranchID IS NULL 
	BEGIN 
		if @LanguageBranch IS NOT NULL
			RAISERROR('Language branch %s is not defined',16,1, @LanguageBranch)
		else
			SET @LangBranchID = -1
	END
	CREATE TABLE #category (fkCategoryID int)
	IF NOT @CategoryList IS NULL
		EXEC netCategoryStringToTable @CategoryList=@CategoryList
	SELECT DISTINCT(tblProperty.fkPageID)
	FROM tblProperty
	INNER JOIN tblPageDefinition ON tblPageDefinition.pkID=tblProperty.fkPageDefinitionID
	INNER JOIN tblTree ON tblTree.fkChildID=tblProperty.fkPageID
	INNER JOIN tblPageLanguage ON tblPageLanguage.fkPageID=tblProperty.fkPageID
	INNER JOIN tblPageType ON tblPageDefinition.fkPageTypeID=tblPageType.pkID
	WHERE tblPageType.ContentType = 0 
	AND tblTree.fkParentID=@PageID 
	AND tblPageDefinition.Name=@PropertyName
	AND Property = 8		
	AND (@LangBranchID=-1 OR tblProperty.fkLanguageBranchID=@LangBranchID OR tblPageDefinition.LanguageSpecific<3)
	AND (
		(@CategoryList IS NULL AND 	(
							SELECT Count(tblCategoryPage.fkPageID)
							FROM tblCategoryPage
							INNER JOIN tblPageDefinition ON tblPageDefinition.pkID = tblCategoryPage.CategoryType
							WHERE tblCategoryPage.CategoryType=tblProperty.fkPageDefinitionID
							AND tblCategoryPage.fkPageID=tblProperty.fkPageID
							AND (@LangBranchID=-1 OR tblCategoryPage.fkLanguageBranchID=@LangBranchID OR tblPageDefinition.LanguageSpecific<3)
						)=0
		)
		OR
		(@Equals=1 AND tblProperty.fkPageID IN
						(SELECT tblCategoryPage.fkPageID 
						FROM tblCategoryPage 
						INNER JOIN #category ON tblCategoryPage.fkCategoryID=#category.fkCategoryID
						INNER JOIN tblPageDefinition ON tblPageDefinition.pkID = tblCategoryPage.CategoryType
						WHERE tblCategoryPage.CategoryType=tblProperty.fkPageDefinitionID
						AND (@LangBranchID=-1 OR tblCategoryPage.fkLanguageBranchID=@LangBranchID OR tblPageDefinition.LanguageSpecific<3))
		)
		OR
		(@NotEquals=1 AND tblProperty.fkPageID NOT IN
						(SELECT tblCategoryPage.fkPageID 
						FROM tblCategoryPage 
						INNER JOIN #category ON tblCategoryPage.fkCategoryID=#category.fkCategoryID
						INNER JOIN tblPageDefinition ON tblPageDefinition.pkID = tblCategoryPage.CategoryType
						WHERE tblCategoryPage.CategoryType=tblProperty.fkPageDefinitionID
						AND (@LangBranchID=-1 OR tblCategoryPage.fkLanguageBranchID=@LangBranchID OR tblPageDefinition.LanguageSpecific<3))
		)
	  )
	DROP TABLE #category
END
GO
/****** Object:  StoredProcedure [dbo].[netPropertySearch]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPropertySearch]
(
	@PageID			INT,
	@FindProperty	NVARCHAR(255),
	@NotProperty	NVARCHAR(255),
	@LanguageBranch	NCHAR(17) = NULL
)
AS
BEGIN
	DECLARE @LangBranchID NCHAR(17);
	SELECT @LangBranchID=pkID FROM tblLanguageBranch WHERE LanguageID=@LanguageBranch
	IF @LangBranchID IS NULL 
	BEGIN 
		if @LanguageBranch IS NOT NULL
			RAISERROR('Language branch %s is not defined',16,1, @LanguageBranch)
		else
			SET @LangBranchID = -1
	END
	SET NOCOUNT ON
	/* All levels */
	SELECT
		tblPage.pkID
	FROM 
		tblPage
	INNER JOIN
		tblTree ON tblTree.fkChildID=tblPage.pkID
	INNER JOIN
		tblPageType ON tblPage.fkPageTypeID=tblPageType.pkID
	INNER JOIN
		tblPageDefinition ON tblPageType.pkID=tblPageDefinition.fkPageTypeID 
		AND tblPageDefinition.Name=@FindProperty
	INNER JOIN
		tblProperty ON tblProperty.fkPageID=tblPage.pkID 
		AND tblPageDefinition.pkID=tblProperty.fkPageDefinitionID
	INNER JOIN 
		tblPageLanguage ON tblPageLanguage.fkPageID=tblPage.pkID
	WHERE
		tblPageType.ContentType = 0 AND
		tblTree.fkParentID=@PageID AND
		tblPage.Deleted = 0 AND
		tblPageLanguage.PendingPublish = 0 AND
		(@LangBranchID=-1 OR tblPageLanguage.fkLanguageBranchID=@LangBranchID OR tblPageDefinition.LanguageSpecific<3) AND
		(@NotProperty IS NULL OR NOT EXISTS(
			SELECT * FROM tblProperty 
			INNER JOIN tblPageDefinition ON tblPageDefinition.pkID=tblProperty.fkPageDefinitionID 
			WHERE tblPageDefinition.Name=@NotProperty 
			AND tblProperty.fkPageID=tblPage.pkID))
	ORDER BY tblPageLanguage.Name ASC
END
GO
/****** Object:  StoredProcedure [dbo].[netPropertySave]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPropertySave]
(
	@PageID				INT,
	@WorkPageID			INT,
	@PageDefinitionID	INT,
	@Override			BIT,
	@LanguageBranch		NCHAR(17) = NULL,
	@ScopeName			NVARCHAR(450) = NULL,
--Per Type:
	@Number				INT = NULL,
	@Boolean			BIT = 0,
	@Date				DATETIME = NULL,
	@FloatNumber		FLOAT = NULL,
	@PageType			INT = NULL,
	@String				NVARCHAR(450) = NULL,
	@LinkGuid			uniqueidentifier = NULL,
	@PageLink			INT = NULL,
	@LongString			NVARCHAR(MAX) = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	DECLARE @LangBranchID NCHAR(17);
	IF (@WorkPageID <> 0)
		SELECT @LangBranchID = fkLanguageBranchID FROM tblWorkPage WHERE pkID = @WorkPageID
	ELSE
		SELECT @LangBranchID = pkID FROM tblLanguageBranch WHERE LanguageID = @LanguageBranch
	IF @LangBranchID IS NULL 
	BEGIN 
		if @LanguageBranch IS NOT NULL
			RAISERROR('Language branch %s is not defined',16,1, @LanguageBranch)
		else
			SET @LangBranchID = 1
	END
	DECLARE @IsLanguagePublished BIT;
	IF EXISTS(SELECT fkPageID FROM tblPageLanguage 
		WHERE fkPageID = @PageID AND fkLanguageBranchID = CAST(@LangBranchID AS INT) AND PublishedVersion IS NOT NULL)
		SET @IsLanguagePublished = 1
	ELSE
		SET @IsLanguagePublished = 0
	DECLARE @DynProp INT
	DECLARE @retval	INT
	SET @retval = 0
		SELECT
			@DynProp = pkID
		FROM
			tblPageDefinition
		WHERE
			pkID = @PageDefinitionID
		AND
			fkPageTypeID IS NULL
		IF (@WorkPageID IS NOT NULL)
		BEGIN
			/* Never store dynamic properties in work table */
			IF (@DynProp IS NOT NULL)
				GOTO cleanup
			/* Insert or update property */
			IF EXISTS(SELECT fkWorkPageID FROM tblWorkProperty 
				WHERE fkWorkPageID=@WorkPageID AND fkPageDefinitionID=@PageDefinitionID AND ((@ScopeName IS NULL AND ScopeName IS NULL) OR (@ScopeName = ScopeName)))
				UPDATE
					tblWorkProperty
				SET
					ScopeName = @ScopeName,
					Number = @Number,
					Boolean = @Boolean,
					[Date] = @Date,
					FloatNumber = @FloatNumber,
					PageType = @PageType,
					String = @String,
					LinkGuid = @LinkGuid,
					PageLink = @PageLink,
					LongString = @LongString
				WHERE
					fkWorkPageID = @WorkPageID
				AND
					fkPageDefinitionID = @PageDefinitionID
				AND 
					((@ScopeName IS NULL AND ScopeName IS NULL) OR (@ScopeName = ScopeName))
			ELSE
				INSERT INTO
					tblWorkProperty
					(fkWorkPageID,
					fkPageDefinitionID,
					ScopeName,
					Number,
					Boolean,
					[Date],
					FloatNumber,
					PageType,
					String,
					LinkGuid,
					PageLink,
					LongString)
				VALUES
					(@WorkPageID,
					@PageDefinitionID,
					@ScopeName,
					@Number,
					@Boolean,
					@Date,
					@FloatNumber,
					@PageType,
					@String,
					@LinkGuid,
					@PageLink,
					@LongString)
		END
		/* For published or languages where no version is published we save value in tblProperty as well. Reason for this is that if when page is loaded
		through tblProperty (typically netPageListPaged) the page gets populated correctly. */
		IF (@WorkPageID IS NULL OR @IsLanguagePublished = 0)
		BEGIN
			/* Insert or update property */
			IF EXISTS(SELECT fkPageID FROM tblProperty 
				WHERE fkPageID = @PageID AND fkPageDefinitionID = @PageDefinitionID  AND
					((@ScopeName IS NULL AND ScopeName IS NULL) OR (@ScopeName = ScopeName)) AND @LangBranchID = tblProperty.fkLanguageBranchID)
				UPDATE
					tblProperty
				SET
					ScopeName = @ScopeName,
					Number = @Number,
					Boolean = @Boolean,
					[Date] = @Date,
					FloatNumber = @FloatNumber,
					PageType = @PageType,
					String = @String,
					LinkGuid = @LinkGuid,
					PageLink = @PageLink,
					LongString = @LongString
				WHERE
					fkPageID = @PageID
				AND
					fkPageDefinitionID = @PageDefinitionID
				AND 
					((@ScopeName IS NULL AND ScopeName IS NULL) OR (@ScopeName = ScopeName))
				AND
					@LangBranchID = tblProperty.fkLanguageBranchID
			ELSE
				INSERT INTO
					tblProperty
					(fkPageID,
					fkPageDefinitionID,
					ScopeName,
					Number,
					Boolean,
					[Date],
					FloatNumber,
					PageType,
					String,
					LinkGuid,
					PageLink,
					LongString,
					fkLanguageBranchID)
				VALUES
					(@PageID,
					@PageDefinitionID,
					@ScopeName,
					@Number,
					@Boolean,
					@Date,
					@FloatNumber,
					@PageType,
					@String,
					@LinkGuid,
					@PageLink,
					@LongString,
					@LangBranchID)
			/* Override dynamic property definitions below the current level */
			IF (@DynProp IS NOT NULL)
			BEGIN
				IF (@Override = 1)
					DELETE FROM
						tblProperty
					WHERE
						fkPageDefinitionID = @PageDefinitionID
					AND
					(	
						@LanguageBranch IS NULL
					OR
						@LangBranchID = tblProperty.fkLanguageBranchID
					)
					AND
						fkPageID
					IN
						(SELECT fkChildID FROM tblTree WHERE fkParentID = @PageID)
				SET @retval = 1
			END
		END
cleanup:		
	RETURN @retval
END
GO
/****** Object:  StoredProcedure [dbo].[netPropertyDefinitionGetUsage]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPropertyDefinitionGetUsage]
(
	@PropertyDefinitionID	INT,
	@OnlyNoneMasterLanguage	BIT = 0,
	@OnlyPublished	BIT = 0
)
AS
BEGIN
	SET NOCOUNT ON
	--Get blocktype if property is block property
	DECLARE @ContentTypeID INT;
	SET @ContentTypeID = (SELECT tblContentType.pkID FROM 
		tblPropertyDefinition
		INNER JOIN tblPropertyDefinitionType ON tblPropertyDefinition.fkPropertyDefinitionTypeID = tblPropertyDefinitionType.pkID
		INNER JOIN tblContentType ON tblPropertyDefinitionType.fkContentTypeGUID = tblContentType.ContentTypeGUID
		WHERE tblPropertyDefinition.pkID = @PropertyDefinitionID);
	IF (@OnlyPublished = 1)
	BEGIN
		IF (@ContentTypeID IS NOT NULL)
		BEGIN
			SELECT DISTINCT
				tblContentLanguage.fkContentID as ContentID, 
				0 AS WorkID,
				tblContentLanguage.Name,
				tblLanguageBranch.LanguageID AS LanguageBranch,
				tblLanguageBranch.Name AS LanguageBranchName
			FROM 
				tblContentProperty 
			INNER JOIN 
				dbo.GetExistingScopesForDefinition(@PropertyDefinitionID) as ExistingScopes ON tblContentProperty.ScopeName = ExistingScopes.ScopeName
			INNER JOIN 
				tblContentLanguage ON tblContentProperty.fkContentID=tblContentLanguage.fkContentID
			INNER JOIN
				tblContent ON tblContent.pkID=tblContentLanguage.fkContentID
			INNER JOIN
				tblLanguageBranch ON tblLanguageBranch.pkID=tblContentLanguage.fkLanguageBranchID
			WHERE
				tblContentLanguage.fkLanguageBranchID=tblContentProperty.fkLanguageBranchID AND
				(@OnlyNoneMasterLanguage=0 OR tblContentProperty.fkLanguageBranchID<>tblContent.fkMasterLanguageBranchID)
		END
		ELSE
		BEGIN
			SELECT DISTINCT
				tblContentLanguage.fkContentID as ContentID, 
				0 AS WorkID,
				tblContentLanguage.Name,
				tblLanguageBranch.LanguageID AS LanguageBranch,
				tblLanguageBranch.Name AS LanguageBranchName
			FROM 
				tblContentProperty
			INNER JOIN 
				tblContentLanguage ON tblContentProperty.fkContentID=tblContentLanguage.fkContentID
			INNER JOIN
				tblContent ON tblContent.pkID=tblContentLanguage.fkContentID
			INNER JOIN
				tblLanguageBranch ON tblLanguageBranch.pkID=tblContentLanguage.fkLanguageBranchID
			WHERE
				tblContentLanguage.fkLanguageBranchID=tblContentProperty.fkLanguageBranchID AND
				tblContentProperty.fkPropertyDefinitionID=@PropertyDefinitionID AND
				(@OnlyNoneMasterLanguage=0 OR tblContentProperty.fkLanguageBranchID<>tblContent.fkMasterLanguageBranchID)
		END
	END
	ELSE
	BEGIN
		IF (@ContentTypeID IS NOT NULL)
		BEGIN
			SELECT DISTINCT
				tblContent.pkID as ContentID, 
				tblWorkContent.pkID AS WorkID,
				tblWorkContent.Name,
				tblLanguageBranch.LanguageID AS LanguageBranch
			FROM 
				tblWorkContentProperty
			INNER JOIN
				dbo.GetExistingScopesForDefinition(@PropertyDefinitionID) as ExistingScopes ON tblWorkContentProperty.ScopeName = ExistingScopes.ScopeName
			INNER JOIN
				tblWorkContent ON tblWorkContentProperty.fkWorkContentID=tblWorkContent.pkID
			INNER JOIN
				tblContent ON tblWorkContent.fkContentID=tblContent.pkID
			INNER JOIN 
				tblContentLanguage ON tblWorkContent.fkContentID=tblContentLanguage.fkContentID 
			INNER JOIN
				tblLanguageBranch ON tblLanguageBranch.pkID=tblContentLanguage.fkLanguageBranchID
			WHERE
				tblWorkContent.fkLanguageBranchID = tblLanguageBranch.pkID AND
				(@OnlyNoneMasterLanguage=0 OR tblWorkContent.fkLanguageBranchID<>tblContent.fkMasterLanguageBranchID)
		END
		ELSE
		BEGIN
			SELECT DISTINCT
				tblContent.pkID as ContentID, 
				tblWorkContent.pkID AS WorkID,
				tblWorkContent.Name,
				tblLanguageBranch.LanguageID AS LanguageBranch
			FROM 
				tblWorkContentProperty
			INNER JOIN
				tblWorkContent ON tblWorkContentProperty.fkWorkContentID=tblWorkContent.pkID
			INNER JOIN
				tblContent ON tblWorkContent.fkContentID=tblContent.pkID
			INNER JOIN 
				tblContentLanguage ON tblWorkContent.fkContentID=tblContentLanguage.fkContentID 
			INNER JOIN
				tblLanguageBranch ON tblLanguageBranch.pkID=tblContentLanguage.fkLanguageBranchID
			WHERE
				tblWorkContent.fkLanguageBranchID = tblLanguageBranch.pkID AND
				tblWorkContentProperty.fkPropertyDefinitionID=@PropertyDefinitionID AND
				(@OnlyNoneMasterLanguage=0 OR tblWorkContent.fkLanguageBranchID<>tblContent.fkMasterLanguageBranchID)
		END
	END
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[netPropertyDefinitionDelete]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPropertyDefinitionDelete]
(
	@PropertyDefinitionID INT
)
AS
BEGIN
	DELETE FROM tblContentProperty WHERE ScopeName IN (SELECT ScopeName FROM dbo.GetExistingScopesForDefinition(@PropertyDefinitionID)) 
	DELETE FROM tblWorkContentProperty WHERE ScopeName IN (SELECT ScopeName FROM dbo.GetExistingScopesForDefinition(@PropertyDefinitionID))
	DELETE FROM tblPropertyDefault WHERE fkPageDefinitionID=@PropertyDefinitionID
	DELETE FROM tblProperty WHERE fkPageDefinitionID=@PropertyDefinitionID
	DELETE FROM tblWorkProperty WHERE fkPageDefinitionID=@PropertyDefinitionID
	DELETE FROM tblCategoryPage WHERE CategoryType=@PropertyDefinitionID
	DELETE FROM tblWorkCategory WHERE CategoryType=@PropertyDefinitionID
	DELETE FROM tblPageDefinition WHERE pkID=@PropertyDefinitionID
END
GO
/****** Object:  StoredProcedure [dbo].[netPropertyDefinitionCheckUsage]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPropertyDefinitionCheckUsage]
(
	@PropertyDefinitionID	INT,
	@OnlyNoneMasterLanguage	BIT = 0,
	@OnlyPublished	BIT = 0
)
AS
BEGIN
	SET NOCOUNT ON
	--Get blocktype if property is block property
	DECLARE @ContentTypeID INT;
	SET @ContentTypeID = (SELECT tblContentType.pkID FROM 
		tblPropertyDefinition
		INNER JOIN tblPropertyDefinitionType ON tblPropertyDefinition.fkPropertyDefinitionTypeID = tblPropertyDefinitionType.pkID
		INNER JOIN tblContentType ON tblPropertyDefinitionType.fkContentTypeGUID = tblContentType.ContentTypeGUID
		WHERE tblPropertyDefinition.pkID = @PropertyDefinitionID);
	IF (@OnlyPublished = 1)
	BEGIN
		IF (@ContentTypeID IS NOT NULL)
		BEGIN
			SELECT TOP 1
				tblContentLanguage.fkContentID as ContentID, 
				0 AS WorkID
			FROM 
				tblContentProperty 
			INNER JOIN 
				dbo.GetExistingScopesForDefinition(@PropertyDefinitionID) as ExistingScopes ON tblContentProperty.ScopeName = ExistingScopes.ScopeName
			INNER JOIN 
				tblContentLanguage ON tblContentProperty.fkContentID=tblContentLanguage.fkContentID
		END
		ELSE
		BEGIN
			SELECT TOP 1
				tblContentLanguage.fkContentID as ContentID, 
				0 AS WorkID
			FROM 
				tblContentProperty
			INNER JOIN 
				tblContentLanguage ON tblContentProperty.fkContentID=tblContentLanguage.fkContentID
			WHERE
				tblContentLanguage.fkLanguageBranchID=tblContentProperty.fkLanguageBranchID AND
				tblContentProperty.fkPropertyDefinitionID=@PropertyDefinitionID
		END
	END
	ELSE
	BEGIN
		IF (@ContentTypeID IS NOT NULL)
		BEGIN
			SELECT TOP 1
				tblWorkContent.fkContentID as ContentID, 
				tblWorkContent.pkID AS WorkID
			FROM 
				tblWorkContentProperty
			INNER JOIN
				dbo.GetExistingScopesForDefinition(@PropertyDefinitionID) as ExistingScopes ON tblWorkContentProperty.ScopeName = ExistingScopes.ScopeName
			INNER JOIN
				tblWorkContent ON tblWorkContentProperty.fkWorkContentID=tblWorkContent.pkID
		END
		ELSE
		BEGIN
			SELECT TOP 1
				tblWorkContent.fkContentID as ContentID, 
				tblWorkContent.pkID AS WorkID
			FROM 
				tblWorkContentProperty
			INNER JOIN
				tblWorkContent ON tblWorkContentProperty.fkWorkContentID=tblWorkContent.pkID
			WHERE
				tblWorkContentProperty.fkPropertyDefinitionID=@PropertyDefinitionID
		END
	END
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[netPageTypeDelete]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageTypeDelete]
(
	@PageTypeID		INT
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	/* Do not try to delete a page type that is in use */
	SELECT pkID as PageID, Name as PageName
	FROM tblPage
	INNER JOIN tblPageLanguage ON tblPageLanguage.fkPageID=tblPage.pkID
	WHERE fkPageTypeID=@PageTypeID AND tblPageLanguage.fkLanguageBranchID=tblPage.fkMasterLanguageBranchID
	IF (@@ROWCOUNT <> 0)
		RETURN 1
	/* If the content type is used in a page definition, it can't be deleted */
	DECLARE @PageTypeGuid UNIQUEIDENTIFIER
	SET @PageTypeGuid = (SELECT PageType.PageTypeGuid FROM tblPageType as PageType WHERE PageType.pkID=@PageTypeID)
	DECLARE @PageDefinitionTypeID INT
	SET @PageDefinitionTypeID = (SELECT pkid FROM tblPageDefinitionType WHERE fkPageTypeGUID = @PageTypeGuid)
	SELECT PageType.pkID AS PageTypeID, PageType.Name AS PageTypeName FROM tblPageType AS PageType
	INNER JOIN tblPageDefinition AS PageDefinition ON PageType.pkId = PageDefinition.fkPageTypeID
	WHERE PageDefinition.fkPageDefinitionTypeID = @PageDefinitionTypeID
	IF (@@ROWCOUNT <> 0)
		RETURN 1
	/* If the content type is in use, do not delete */
	SELECT TOP 1 Property.pkID
	FROM  
	tblContentProperty as Property WITH(INDEX(IDX_tblContentProperty_ScopeName))
	INNER JOIN dbo.GetScopedBlockProperties(@PageTypeID) as ScopedProperties ON 
			Property.ScopeName LIKE (ScopedProperties.ScopeName + '%')
	IF (@@ROWCOUNT <> 0)
		RETURN 1
	DELETE FROM 
		tblPageTypeDefault
	WHERE 
		fkPageTypeID=@PageTypeID
	DELETE FROM 
		tblWorkProperty
	FROM 
		tblWorkProperty AS WP
	INNER JOIN 
		tblPageDefinition AS PD ON WP.fkPageDefinitionID=PD.pkID 
	WHERE 
		PD.Property=3 AND 
		PageType=@PageTypeID
	DELETE FROM 
		tblProperty
	FROM 
		tblProperty AS P
	INNER JOIN 
		tblPageDefinition AS PD ON P.fkPageDefinitionID=PD.pkID 
	WHERE 
		PD.Property=3 AND 
		PageType=@PageTypeID
	DELETE FROM 
		tblPageTypeToPageType 
	WHERE 
		fkPageTypeParentID=@PageTypeID OR 
		fkPageTypeChildID=@PageTypeID
	DELETE FROM 
		tblPageDefinition 
	WHERE 
		fkPageTypeID=@PageTypeID
	DELETE FROM 
		tblPageDefinitionType
	FROM 
		tblPageDefinitionType
	INNER JOIN tblPageType ON tblPageDefinitionType.fkPageTypeGUID = tblPageType.PageTypeGUID
	WHERE
		tblPageType.pkID=@PageTypeID
	DELETE FROM 
		tblPageType
	WHERE
		pkID=@PageTypeID
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[netPageDefinitionSave]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageDefinitionSave]
(
	@PageDefinitionID 		INT OUTPUT,
	@PageTypeID			INT,
	@Name				NVARCHAR(100),
	@PageDefinitionTypeID		INT,
	@Required			BIT = NULL,
	@Advanced			INT = NULL,
	@Searchable			BIT = NULL,
	@DefaultValueType		INT = NULL,
	@EditCaption			NVARCHAR(255) = NULL,
	@HelpText			NVARCHAR(2000) = NULL,
	@ObjectProgID			NVARCHAR(100) = NULL,
	@LongStringSettings		INT = NULL,
	@SettingsID		UNIQUEIDENTIFIER = NULL,
	@FieldOrder			INT = NULL,
	@Type 				INT = NULL OUTPUT,
	@OldType 			INT = NULL OUTPUT,
	@LanguageSpecific	INT = 0,
	@DisplayEditUI		BIT = NULL,
	@ExistsOnModel	BIT = 0
)
AS
BEGIN
	SELECT @OldType = tblPageDefinitionType.Property 
	FROM tblPageDefinition
	INNER JOIN tblPageDefinitionType ON tblPageDefinitionType.pkID=tblPageDefinition.fkPageDefinitionTypeID
	WHERE tblPageDefinition.pkID=@PageDefinitionID
	SELECT @Type = Property FROM tblPageDefinitionType WHERE pkID=@PageDefinitionTypeID
	IF @Type IS NULL
		RAISERROR('Cannot find data type',16,1)
	IF @PageTypeID=0
		SET @PageTypeID = NULL
	IF @PageDefinitionID = 0 AND @ExistsOnModel = 1
	BEGIN
		SET @PageDefinitionID = ISNULL((SELECT pkId FROM tblPageDefinition where Name = @Name AND fkPageTypeID = @PageTypeID), @PageDefinitionID)
	END
	IF @PageDefinitionID=0
	BEGIN	
		INSERT INTO tblPageDefinition
		(
			fkPageTypeID,
			fkPageDefinitionTypeID,
			Name,
			Property,
			Required,
			Advanced,
			Searchable,
			DefaultValueType,
			EditCaption,
			HelpText,
			ObjectProgID,
			LongStringSettings,
			SettingsID,
			FieldOrder,
			LanguageSpecific,
			DisplayEditUI,
			ExistsOnModel
		)
		VALUES
		(
			@PageTypeID,
			@PageDefinitionTypeID,
			@Name,
			@Type,
			@Required,
			@Advanced,
			@Searchable,
			@DefaultValueType,
			@EditCaption,
			@HelpText,
			@ObjectProgID,
			@LongStringSettings,
			@SettingsID,
			@FieldOrder,
			@LanguageSpecific,
			@DisplayEditUI,
			@ExistsOnModel
		)
		SET @PageDefinitionID =  SCOPE_IDENTITY() 
	END
	ELSE
	BEGIN
		UPDATE tblPageDefinition SET
			Name 		= @Name,
			fkPageDefinitionTypeID	= @PageDefinitionTypeID,
			Property 	= @Type,
			Required 	= @Required,
			Advanced 	= @Advanced,
			Searchable 	= @Searchable,
			DefaultValueType = @DefaultValueType,
			EditCaption 	= @EditCaption,
			HelpText 	= @HelpText,
			ObjectProgID 	= @ObjectProgID,
			LongStringSettings = @LongStringSettings,
			SettingsID = @SettingsID,
			LanguageSpecific = @LanguageSpecific,
			FieldOrder = @FieldOrder,
			DisplayEditUI = @DisplayEditUI,
			ExistsOnModel = @ExistsOnModel
		WHERE pkID=@PageDefinitionID
	END
	DELETE FROM tblPropertyDefault WHERE fkPageDefinitionID=@PageDefinitionID
	IF @LanguageSpecific<3
	BEGIN
		/* NOTE: Here we take into consideration that language neutral dynamic properties are always stored on language 
			with id 1 (which perhaps should be changed and in that case the special handling here could be removed). */
		IF @PageTypeID IS NULL
		BEGIN
			DELETE tblProperty
			FROM tblProperty
			INNER JOIN tblPage ON tblPage.pkID=tblProperty.fkPageID
			WHERE fkPageDefinitionID=@PageDefinitionID AND tblProperty.fkLanguageBranchID<>1
		END
		ELSE
		BEGIN
			DELETE tblProperty
			FROM tblProperty
			INNER JOIN tblPage ON tblPage.pkID=tblProperty.fkPageID
			WHERE fkPageDefinitionID=@PageDefinitionID AND tblProperty.fkLanguageBranchID<>tblPage.fkMasterLanguageBranchID
		END
		DELETE tblWorkProperty
		FROM tblWorkProperty
		INNER JOIN tblWorkPage ON tblWorkProperty.fkWorkPageID=tblWorkPage.pkID
		INNER JOIN tblPage ON tblPage.pkID=tblWorkPage.fkPageID
		WHERE fkPageDefinitionID=@PageDefinitionID AND tblWorkPage.fkLanguageBranchID<>tblPage.fkMasterLanguageBranchID
		DELETE 
			tblCategoryPage
		FROM
			tblCategoryPage
		INNER JOIN
			tblPage
		ON
			tblPage.pkID = tblCategoryPage.fkPageID
		WHERE
			CategoryType = @PageDefinitionID
		AND
			tblCategoryPage.fkLanguageBranchID <> tblPage.fkMasterLanguageBranchID
		DELETE 
			tblWorkCategory
		FROM
			tblWorkCategory
		INNER JOIN 
			tblWorkPage
		ON
			tblWorkCategory.fkWorkPageID = tblWorkPage.pkID
		INNER JOIN
			tblPage
		ON
			tblPage.pkID = tblWorkPage.fkPageID
		WHERE
			CategoryType = @PageDefinitionID
		AND
			tblWorkPage.fkLanguageBranchID <> tblPage.fkMasterLanguageBranchID
	END
END
GO
/****** Object:  StoredProcedure [dbo].[netPageDynamicBlockList]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageDynamicBlockList]
(
	@PageID INT,
	@WorkPageID INT
)
AS
BEGIN
	SET NOCOUNT ON
	IF (@WorkPageID IS NULL OR @WorkPageID=0)
		SELECT 
			ScopeName
		FROM 
			tblProperty
		WHERE 
			fkPageID=@PageID AND ScopeName LIKE '%.D:%'
	ELSE
		SELECT 
			ScopeName
		FROM 
			tblWorkProperty
		WHERE 
			fkWorkPageID=@WorkPageID AND ScopeName LIKE '%.D:%'
END
GO
/****** Object:  StoredProcedure [dbo].[netPageDynamicBlockDelete]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageDynamicBlockDelete]
(
	@PageID INT,
	@WorkPageID INT,
	@DynamicBlock NVARCHAR(450)
)
AS
BEGIN
	SET NOCOUNT ON
	IF (@WorkPageID IS NULL OR @WorkPageID=0)
		DELETE
		FROM 
			tblProperty
		WHERE 
			fkPageID=@PageID AND ScopeName LIKE '%' + @DynamicBlock + '%'
	ELSE
		DELETE
		FROM 
			tblWorkProperty
		WHERE 
			fkWorkPageID=@WorkPageID AND ScopeName LIKE '%' + @DynamicBlock + '%'
END
GO
/****** Object:  StoredProcedure [dbo].[netPageDeleteLanguage]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageDeleteLanguage]
(
	@PageID			INT,
	@LanguageBranch	NCHAR(17)
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	DECLARE @LangBranchID		INT
	SELECT @LangBranchID = pkID FROM tblLanguageBranch WHERE LanguageID=@LanguageBranch
	IF @LangBranchID IS NULL
	BEGIN
		RAISERROR (N'netPageDeleteLanguage: LanguageBranchID is null, possibly empty table tblLanguageBranch', 16, 1)
		RETURN 0
	END
	IF EXISTS( SELECT * FROM tblPage WHERE pkID=@PageID AND fkMasterLanguageBranchID=@LangBranchID )
	BEGIN
		RAISERROR (N'netPageDeleteLanguage: Cannot delete master language branch', 16, 1)
		RETURN 0
	END
	IF NOT EXISTS( SELECT * FROM tblPageLanguage WHERE fkPageID=@PageID AND fkLanguageBranchID=@LangBranchID )
	BEGIN
		RAISERROR (N'netPageDeleteLanguage: Language does not exist on page', 16, 1)
		RETURN 0
	END
	UPDATE tblWorkPage SET fkMasterVersionID=NULL WHERE pkID IN (SELECT pkID FROM tblWorkPage WHERE fkPageID=@PageID AND fkLanguageBranchID=@LangBranchID)
	DELETE FROM tblWorkProperty WHERE fkWorkPageID IN (SELECT pkID FROM tblWorkPage WHERE fkPageID=@PageID AND fkLanguageBranchID=@LangBranchID)
	DELETE FROM tblWorkCategory WHERE fkWorkPageID IN (SELECT pkID FROM tblWorkPage WHERE fkPageID=@PageID AND fkLanguageBranchID=@LangBranchID)
	DELETE FROM tblWorkPage WHERE fkPageID=@PageID AND fkLanguageBranchID=@LangBranchID
	DELETE FROM tblProperty FROM tblProperty
	INNER JOIN tblPageDefinition ON tblPageDefinition.pkID=tblProperty.fkPageDefinitionID
	WHERE fkPageID=@PageID 
	AND fkLanguageBranchID=@LangBranchID
	AND fkPageTypeID IS NOT NULL
	DELETE FROM tblCategoryPage WHERE fkPageID=@PageID AND fkLanguageBranchID=@LangBranchID
	DELETE FROM tblPageLanguage WHERE fkPageID=@PageID AND fkLanguageBranchID=@LangBranchID
	RETURN 1
END
GO
/****** Object:  StoredProcedure [dbo].[netPageDefinitionConvertSave]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageDefinitionConvertSave]
(
	@PageDefinitionID INT,
	@PageID INT = NULL,
	@WorkPageID INT = NULL,
	@LanguageBranchID INT = NULL,
	@Type INT,
	@Boolean BIT = NULL,
	@IntNumber INT = NULL,
	@FloatNumber FLOAT = NULL,
	@PageType INT = NULL,
	@LinkGuid uniqueidentifier = NULL,
	@PageReference INT = NULL,
	@DateValue DATETIME = NULL,
	@String NVARCHAR(450) = NULL,
	@LongString NVARCHAR(MAX) = NULL,
	@DeleteProperty BIT = 0
)
AS
BEGIN
	IF NOT @WorkPageID IS NULL
	BEGIN		
		IF @DeleteProperty=1 OR (@Type=0 AND @Boolean=0) OR @Type > 7
			DELETE FROM tblWorkProperty 
			WHERE fkPageDefinitionID=@PageDefinitionID AND fkWorkPageID=@WorkPageID
		ELSE
		BEGIN
			UPDATE tblWorkProperty
				SET
					Boolean=@Boolean,
					Number=@IntNumber,
					FloatNumber=@FloatNumber,
					PageType=@PageType,
					LinkGuid = @LinkGuid,
					PageLink=@PageReference,
					Date=@DateValue,
					String=@String,
					LongString=@LongString
			WHERE fkPageDefinitionID=@PageDefinitionID AND fkWorkPageID=@WorkPageID
		END
	END
	ELSE
	BEGIN
		IF @DeleteProperty=1 OR (@Type=0 AND @Boolean=0) OR @Type > 7
			DELETE FROM tblProperty 
			WHERE fkPageDefinitionID=@PageDefinitionID AND fkPageID=@PageID AND fkLanguageBranchID = @LanguageBranchID
		ELSE
		BEGIN
			UPDATE tblProperty
				SET
					Boolean=@Boolean,
					Number=@IntNumber,
					FloatNumber=@FloatNumber,
					PageType=@PageType,
					PageLink=@PageReference,
					LinkGuid = @LinkGuid,
					Date=@DateValue,
					String=@String,
					LongString=@LongString
			WHERE fkPageDefinitionID=@PageDefinitionID AND fkPageID=@PageID AND fkLanguageBranchID = @LanguageBranchID
		END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[netPageDefinitionConvertList]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageDefinitionConvertList]
(
	@PageDefinitionID INT
)
AS
BEGIN
	SELECT 
			fkPageDefinitionID,
			fkPageID,
			NULL AS fkWorkPageID,
			fkLanguageBranchID,
			NULL AS fkUserPropertyID,
			ScopeName,
			CONVERT(INT,Boolean) AS Boolean,
			Number AS IntNumber,
			FloatNumber,
			PageType,
			LinkGuid,
			PageLink,
			Date AS DateValue,
			String,
			LongString,
			CONVERT(INT,0) AS DeleteProperty
	FROM tblProperty 
	WHERE fkPageDefinitionID=@PageDefinitionID
	UNION ALL
	SELECT 
			fkPageDefinitionID,
			NULL AS fkPageID,
			fkWorkPageID,
			NULL AS fkLanguageBranchID,
			NULL AS fkUserPropertyID,
			ScopeName,
			CONVERT(INT,Boolean) AS Boolean,
			Number AS IntNumber,
			FloatNumber,
			PageType,
			LinkGuid,
			PageLink,
			Date AS DateValue,
			String,
			LongString,
			CONVERT(INT,0) AS DeleteProperty
	FROM tblWorkProperty 
	WHERE fkPageDefinitionID=@PageDefinitionID
END
GO
/****** Object:  StoredProcedure [dbo].[netPageDataLoadVersion]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageDataLoadVersion]
(
	@PageID	INT,
	@WorkID INT,
	@LangBranchID INT
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @CommonPropsWorkID INT
	DECLARE @IsMasterLanguage BIT
    DECLARE @PageTypeID INT
	IF @WorkID IS NULL
	BEGIN
		IF @LangBranchID IS NULL OR NOT EXISTS(SELECT * FROM tblWorkPage WHERE fkPageID=@PageID AND fkLanguageBranchID=@LangBranchID)
			SELECT @LangBranchID=COALESCE(fkMasterLanguageBranchID,1) FROM tblPage WHERE pkID=@PageID
		SELECT @WorkID=PublishedVersion FROM tblPageLanguage WHERE fkPageID=@PageID AND fkLanguageBranchID=@LangBranchID AND PendingPublish=0
		IF (@WorkID IS NULL OR @WorkID=0)
		BEGIN
			SELECT TOP 1 @WorkID=pkID FROM tblWorkPage WHERE fkPageID=@PageID AND fkLanguageBranchID=@LangBranchID ORDER BY Saved DESC
		END
		IF (@WorkID IS NULL OR @WorkID=0)
		BEGIN
			EXEC netPageDataLoad @PageID=@PageID, @LanguageBranchID=@LangBranchID
			RETURN 0
		END		
	END
	/*Get the page type for the requested page*/
	SELECT @PageTypeID = tblPage.fkPageTypeID FROM tblPage
		WHERE tblPage.pkID=@PageID
	/* Get Language branch from page version*/
	SELECT @LangBranchID=fkLanguageBranchID FROM tblWorkPage WHERE pkID=@WorkID
	SELECT @IsMasterLanguage = CASE WHEN EXISTS(SELECT * FROM tblPage WHERE pkID=@PageID AND fkMasterLanguageBranchID=@LangBranchID) THEN  1 ELSE 0 END
	IF (@IsMasterLanguage = 0)
	BEGIN
		IF EXISTS(SELECT * FROM tblPage WHERE PendingPublish=1 AND pkID=@PageID)
			SELECT TOP 1 @CommonPropsWorkID=tblWorkPage.pkID 
			FROM tblWorkPage 
			INNER JOIN tblPage ON tblPage.pkID=tblWorkPage.fkPageID
			WHERE tblWorkPage.fkPageID=@PageID AND tblWorkPage.fkLanguageBranchID=tblPage.fkMasterLanguageBranchID
			ORDER BY tblWorkPage.Saved DESC
		ELSE
			SELECT @CommonPropsWorkID=tblPageLanguage.PublishedVersion 
			FROM tblPageLanguage 
			INNER JOIN tblPage ON tblPage.pkID=tblPageLanguage.fkPageID
			WHERE tblPage.pkID=@PageID AND tblPageLanguage.fkLanguageBranchID=tblPage.fkMasterLanguageBranchID
		/* Get data for page for non-master language*/
		SELECT
			tblPage.pkID AS PageLinkID,
			tblWorkPage.pkID AS PageLinkWorkID,
			fkParentID  AS PageParentLinkID,
			fkPageTypeID AS PageTypeID,
			NULL AS PageTypeName,
			CONVERT(INT,tblPage.VisibleInMenu) AS PageVisibleInMenu,
			tblPage.ChildOrderRule AS PageChildOrderRule,
			tblPage.PeerOrder AS PagePeerOrder,
			CONVERT(NVARCHAR(38),tblPage.PageGUID) AS PageGUID,
			tblPage.ArchivePageGUID AS PageArchiveLinkID,
			ExternalFolderID AS PageFolderID,
			CONVERT(INT,Deleted) AS PageDeleted,
			DeletedBy AS PageDeletedBy,
			DeletedDate AS PageDeletedDate,
			(SELECT ChildOrderRule FROM tblPage AS ParentPage WHERE ParentPage.pkID=tblPage.fkParentID) AS PagePeerOrderRule,
			fkMasterLanguageBranchID AS PageMasterLanguageBranchID,
			CreatorName
		FROM
			tblWorkPage
		INNER JOIN
			tblPage
		ON
			tblPage.pkID = tblWorkPage.fkPageID
		WHERE
			tblPage.pkID = @PageID
		AND
			tblWorkPage.pkID = @WorkID	
	END
	ELSE
	BEGIN
		/* Get data for page for master language*/
		SELECT
			tblPage.pkID AS PageLinkID,
			tblWorkPage.pkID AS PageLinkWorkID,
			fkParentID  AS PageParentLinkID,
			fkPageTypeID AS PageTypeID,
			NULL AS PageTypeName,
			CONVERT(INT,tblWorkPage.VisibleInMenu) AS PageVisibleInMenu,
			tblWorkPage.ChildOrderRule AS PageChildOrderRule,
			tblWorkPage.PeerOrder AS PagePeerOrder,
			CONVERT(NVARCHAR(38),tblPage.PageGUID) AS PageGUID,
			tblWorkPage.ArchivePageGUID AS PageArchiveLinkID,
			ExternalFolderID AS PageFolderID,
			CONVERT(INT,Deleted) AS PageDeleted,
			DeletedBy AS PageDeletedBy,
			DeletedDate AS PageDeletedDate,
			(SELECT ChildOrderRule FROM tblPage AS ParentPage WHERE ParentPage.pkID=tblPage.fkParentID) AS PagePeerOrderRule,
			fkMasterLanguageBranchID AS PageMasterLanguageBranchID,
			tblPage.CreatorName
		FROM tblWorkPage
		INNER JOIN tblPage ON tblPage.pkID=tblWorkPage.fkPageID
		WHERE tblPage.pkID=@PageID AND tblWorkPage.pkID=@WorkID
	END
	IF (@@ROWCOUNT = 0)
		RETURN 0
	/* Get data for page languages */
	SELECT
		CASE
			WHEN PublishedVersion=W.pkID THEN 4							/* EPnWorkStatusPublished */
			WHEN W.HasBeenPublished=1 THEN 5							/* EPnWorkStatusPrevious */
			WHEN W.Rejected=1 THEN 1									/* EPnWorkStatusRejected */
			WHEN W.ReadyToPublish=1  AND W.DelayedPublish = 1 THEN 6	/* EPnWorkStatusDelyedPublish */
			WHEN W.ReadyToPublish=1 THEN 3								/* EPnWorkStatusCheckedIn */
			ELSE 2														/* EPnWorkStatusCheckedOut */
		END AS PageWorkStatus,
		W.fkPageID AS PageID,
		W.LinkType AS PageShortcutType,
		W.ExternalURL AS PageExternalURL,
		W.PageLinkGUID AS PageShortcutLinkID,
		W.Name AS PageName,
		W.URLSegment AS PageURLSegment,
		W.LinkURL AS PageLinkURL,
		W.Created AS PageCreated,
		tblPageLanguage.Changed AS PageChanged,
		W.Saved AS PageSaved,
		W.StartPublish AS PageStartPublish,
		W.StopPublish AS PageStopPublish,
		CONVERT(INT,PendingPublish) AS PagePendingPublish,
		tblPageLanguage.CreatorName AS PageCreatedBy,
		W.ChangedByName AS PageChangedBy,
		-- RTRIM(W.fkLanguageID) AS PageLanguageID,
		W.fkFrameID AS PageTargetFrame,
		W.ChangedOnPublish AS PageChangedOnPublish,
		W.DelayedPublish AS PageDelayedPublish,
		W.fkLanguageBranchID AS PageLanguageBranchID
	FROM tblWorkPage AS W
	INNER JOIN tblPageLanguage ON tblPageLanguage.fkPageID=W.fkPageID
	WHERE tblPageLanguage.fkLanguageBranchID=W.fkLanguageBranchID
		AND W.pkID=@WorkID
	/* Get the property data */
	SELECT
		tblPageDefinition.Name AS PropertyName,
		tblPageDefinition.pkID as PropertyDefinitionID,
		ScopeName,
		CONVERT(INT, Boolean) AS Boolean,
		Number AS IntNumber,
		FloatNumber,
		PageType,
		PageLink AS PageLinkID,
		LinkGuid,
		Date AS DateValue,
		String,
		LongString,
		tblWorkPage.fkLanguageBranchID AS LanguageBranchID
	FROM tblWorkProperty
	INNER JOIN tblWorkPage ON tblWorkPage.pkID=tblWorkProperty.fkWorkPageID
	INNER JOIN tblPageDefinition ON tblPageDefinition.pkID=tblWorkProperty.fkPageDefinitionID
	WHERE (tblWorkProperty.fkWorkPageID=@WorkID OR (tblWorkProperty.fkWorkPageID=@CommonPropsWorkID AND tblPageDefinition.LanguageSpecific<3 AND @IsMasterLanguage=0))
		   AND NOT tblPageDefinition.fkPageTypeID IS NULL
	/*Get built in category information*/
	SELECT
		fkPageID
	AS
		PageID,
		fkCategoryID,
		CategoryType,
		NULL
	FROM
		tblWorkCategory
	INNER JOIN
		tblWorkPage
	ON
		tblWorkPage.pkID = tblWorkCategory.fkWorkPageID
	WHERE
	(
		(@IsMasterLanguage = 0 AND fkWorkPageID = @CommonPropsWorkID)
		OR
		(@IsMasterLanguage = 1 AND fkWorkPageID = @WorkID)
	)
	AND
		CategoryType = 0
	ORDER BY
		fkCategoryID
	/* Get access information */
	SELECT
		fkContentID AS PageID,
		Name,
		IsRole,
		AccessMask
	FROM
		tblContentAccess
	WHERE 
	    fkContentID=@PageID
	ORDER BY
	    IsRole DESC,
		Name
	/* Get all languages for the page */
	SELECT fkLanguageBranchID as PageLanguageBranchID FROM tblPageLanguage
		WHERE tblPageLanguage.fkPageID=@PageID
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[netPageChangeMasterLanguage]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netPageChangeMasterLanguage]
(
	@PageID						INT,
	@NewMasterLanguageBranchID	INT
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @OldMasterLanguageBranchID INT;
	DECLARE @LastNewMasterLanguageVersion INT;
	DECLARE @LastOldMasterLanguageVersion INT;
	SET @OldMasterLanguageBranchID = (SELECT fkMasterLanguageBranchID FROM tblPage WHERE pkID = @PageID);
	IF(@NewMasterLanguageBranchID = @OldMasterLanguageBranchID)
		RETURN -1;
	SET @LastNewMasterLanguageVersion = (SELECT PublishedVersion FROM tblPageLanguage WHERE fkPageID = @PageID AND fkLanguageBranchID = @NewMasterLanguageBranchID AND PendingPublish = 0)
	IF (@LastNewMasterLanguageVersion IS NULL)
		RETURN -1;
	SET @LastOldMasterLanguageVersion = (SELECT PublishedVersion FROM tblPage WHERE pkID = @PageID)
	IF (@LastOldMasterLanguageVersion IS NULL)
		RETURN -1
	--Do the actual change of master language branch
	UPDATE
		tblPage
	SET
		tblPage.fkMasterLanguageBranchID = @NewMasterLanguageBranchID
	WHERE
		pkID = @PageID
	--Update tblProperty for common properties
	UPDATE
		tblProperty
	SET
		fkLanguageBranchID = @NewMasterLanguageBranchID
	FROM
		tblProperty
	INNER JOIN
		tblPageDefinition
	ON
		tblProperty.fkPageDefinitionID = tblPageDefinition.pkID
	WHERE
		LanguageSpecific < 3
	AND
		fkPageID = @PageID
	--Update tblCategoryPage for builtin and common categories
	UPDATE
		tblCategoryPage
	SET
		fkLanguageBranchID = @NewMasterLanguageBranchID
	FROM
		tblCategoryPage
	LEFT JOIN
		tblPageDefinition
	ON
		tblCategoryPage.CategoryType = tblPageDefinition.pkID
	WHERE
		(LanguageSpecific < 3
	OR
		LanguageSpecific IS NULL)
	AND
		fkPageID = @PageID
	--Move work categories and properties between the last versions of the languages
	UPDATE
		tblWorkProperty
	SET
		fkWorkPageID = @LastNewMasterLanguageVersion
	FROM
		tblWorkProperty
	INNER JOIN
		tblPageDefinition
	ON
		tblWorkProperty.fkPageDefinitionID = tblPageDefinition.pkID
	WHERE
		LanguageSpecific < 3
	AND
		fkWorkPageID = @LastOldMasterLanguageVersion
	UPDATE
		tblWorkCategory
	SET
		fkWorkPageID = @LastNewMasterLanguageVersion
	FROM
		tblWorkCategory
	LEFT JOIN
		tblPageDefinition
	ON
		tblWorkCategory.CategoryType = tblPageDefinition.pkID
	WHERE
		(LanguageSpecific < 3
	OR
		LanguageSpecific IS NULL)
	AND
		fkWorkPageID = @LastOldMasterLanguageVersion
	--Remove any remaining common properties for old master language versions
	DELETE FROM
		tblWorkProperty
	FROM
		tblWorkProperty
	INNER JOIN
		tblPageDefinition
	ON
		tblWorkProperty.fkPageDefinitionID = tblPageDefinition.pkID
	WHERE
		LanguageSpecific < 3
	AND
		fkWorkPageID IN (SELECT pkID FROM tblWorkPage WHERE fkPageID = @PageID AND fkLanguageBranchID = @OldMasterLanguageBranchID)
	--Remove any remaining common categories for old master language versions
	DELETE FROM
		tblWorkCategory
	FROM
		tblWorkCategory
	LEFT JOIN
		tblPageDefinition
	ON
		tblWorkCategory.CategoryType = tblPageDefinition.pkID
	WHERE
		(LanguageSpecific < 3
	OR
		LanguageSpecific IS NULL)
	AND
		fkWorkPageID IN (SELECT pkID FROM tblWorkPage WHERE fkPageID = @PageID AND fkLanguageBranchID = @OldMasterLanguageBranchID)
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[netConvertPropertyForPageType]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netConvertPropertyForPageType]
(
	@PageID		INT,
	@FromPageType	INT,
	@FromPropertyID 	INT,
	@ToPropertyID		INT,
	@Recursive		BIT,
	@MasterLanguageID INT,
	@IsTest			BIT
)
AS
BEGIN
	DECLARE @cnt INT;
	DECLARE @LanguageSpecific INT
	DECLARE @LanguageSpecificSource INT
	DECLARE @IsBlock BIT
	SET @LanguageSpecific = 0
	SET @LanguageSpecificSource = 0
	SET @IsBlock = 0
	CREATE TABLE  #updatepages(fkChildID int)
	INSERT INTO #updatepages(fkChildID)  
	SELECT fkChildID 
	FROM tblTree tree
	JOIN tblPage page
	ON page.pkID = tree.fkChildID 
	WHERE @Recursive = 1
	AND tree.fkParentID = @PageID
	AND page.fkPageTypeID = @FromPageType
	UNION (SELECT pkID FROM tblPage WHERE pkID = @PageID AND fkPageTypeID = @FromPageType)
	IF @IsTest = 1
	BEGIN	
		SET @cnt = (	SELECT COUNT(*)
				FROM tblProperty 
				WHERE fkPageDefinitionID = @FromPropertyID
				or ScopeName LIKE '%.' + CAST(@FromPropertyID as varchar) + '.%'
				AND  EXISTS (
					SELECT * from #updatepages WHERE fkChildID=fkPageID))
			+ (	SELECT COUNT(*)
				FROM tblWorkProperty 
				WHERE fkPageDefinitionID = @FromPropertyID
				or ScopeName LIKE '%.' + CAST(@FromPropertyID as varchar) + '.%'
				AND EXISTS (
				SELECT * 
					FROM tblWorkPage 
					WHERE pkID = fkWorkPageID
					AND  EXISTS (
						SELECT * from #updatepages WHERE fkChildID=fkPageID)
				))
		IF @ToPropertyID IS NULL OR @ToPropertyID = 0-- mark deleted rows with -
			SET @cnt = -@cnt
	END
	ELSE
	BEGIN
		IF @ToPropertyID IS NULL OR @ToPropertyID = 0-- no definition exists for the new page type for this property so remove it
		BEGIN
			DELETE
			FROM tblProperty 
			WHERE fkPageDefinitionID = @FromPropertyID
			or ScopeName LIKE '%.' + CAST(@FromPropertyID as varchar) + '.%'
			AND  EXISTS (
				SELECT * from #updatepages WHERE fkChildID=fkPageID)
			SET @cnt = -@@rowcount
			DELETE 
			FROM tblWorkProperty 
			WHERE fkPageDefinitionID = @FromPropertyID
			or ScopeName LIKE '%.' + CAST(@FromPropertyID as varchar) + '.%'
			AND EXISTS (
				SELECT * 
				FROM tblWorkPage 
				WHERE pkID = fkWorkPageID
				AND  EXISTS (
					SELECT * from #updatepages WHERE fkChildID=fkPageID)
				)
			SET @cnt = @cnt-@@rowcount 
		END 	
		ELSE IF @FromPropertyID IS NOT NULL -- from property exists and has to be replaced
		BEGIN
			-- Need to check if the property we're converting to is unique for each language or not
			SELECT @LanguageSpecific = LanguageSpecific 
			FROM tblPageDefinition 
			WHERE pkID = @ToPropertyID
			-- Need to check if the property we're converting from is unique for each language or not
			SELECT @LanguageSpecificSource = LanguageSpecific 
			FROM tblPageDefinition 
			WHERE pkID = @FromPropertyID
			-- Need to check if the property we're converting is a block (Property 12 is a block)
			SELECT @IsBlock = CAST(count(*) as bit)
			FROM tblPageDefinition 
			Where pkID = @FromPropertyID and Property = 12
			IF @IsBlock = 1
			BEGIN
				DECLARE @DefinitionTypeFrom int
				DECLARE @DefinitionTypeTo int
				SET @DefinitionTypeFrom = 
					(SELECT fkPageDefinitionTypeID FROM tblPageDefinition WHERE pkID =@FromPropertyID)
				SET @DefinitionTypeTo = 
					(SELECT fkPageDefinitionTypeID FROM tblPageDefinition WHERE pkID =@ToPropertyID)
				IF (@DefinitionTypeFrom <> @DefinitionTypeTo)
				BEGIN
					RAISERROR (N'Property definitions are not of same block type.', 16, 1)
					RETURN 0
				END
				-- Update older versions of block
				-- update scopename in tblWorkProperty
				 UPDATE tblWorkProperty 
				 SET ScopeName = dbo.ConvertScopeName(ScopeName,@FromPropertyID, @ToPropertyID)
				 FROM tblWorkProperty prop
				 INNER JOIN tblWorkPage wpa ON prop.fkWorkPageID = wpa.pkID
				 WHERE ScopeName LIKE '%.' + CAST(@FromPropertyID as varchar) + '.%'
				 AND EXISTS (SELECT * from #updatepages WHERE fkChildID=wpa.fkPageID)
				SET @cnt = @@rowcount
				-- Update current version of block
				-- update scopename in tblProperty
				 UPDATE tblProperty 
				 SET ScopeName = dbo.ConvertScopeName(ScopeName,@FromPropertyID, @ToPropertyID)
				 WHERE ScopeName LIKE '%.' + CAST(@FromPropertyID as varchar) + '.%'
				 AND  EXISTS (
					SELECT * from #updatepages WHERE fkChildID=fkPageID)
				SET @cnt = @cnt + @@rowcount
			END
			ELSE -- Not a block.
			BEGIN
				-- Update older versions
				UPDATE tblWorkProperty SET fkPageDefinitionID = @ToPropertyID
					FROM tblWorkProperty prop
					INNER JOIN tblWorkPage wpa ON prop.fkWorkPageID = wpa.pkID
					WHERE prop.fkPageDefinitionID = @FromPropertyID
					AND EXISTS (SELECT * from #updatepages WHERE fkChildID=wpa.fkPageID)
				SET @cnt = @@rowcount
				-- Update current version 
				UPDATE tblProperty SET fkPageDefinitionID = @ToPropertyID
				WHERE fkPageDefinitionID = @FromPropertyID
				AND  EXISTS (
					SELECT * from #updatepages WHERE fkChildID=fkPageID)
				SET @cnt = @cnt + @@rowcount
			END
			IF (@LanguageSpecific < 3 AND @LanguageSpecificSource > 2)
			BEGIN
				-- The destination property is not language specific which means
				-- that we need to remove all of the old properties in other
				-- languages that could not be mapped
				DELETE FROM tblWorkProperty
					FROM tblWorkProperty prop
					INNER JOIN tblWorkPage wpa ON prop.fkWorkPageID = wpa.pkID
					WHERE (prop.fkPageDefinitionID = @ToPropertyID -- already converted to new type!
					or prop.ScopeName LIKE '%.' + CAST(@ToPropertyID as varchar) + '.%')
					AND wpa.fkLanguageBranchID <> @MasterLanguageID
					AND EXISTS (SELECT * from #updatepages WHERE fkChildID=wpa.fkPageID)
				SET @cnt = @cnt - @@rowcount		
				DELETE FROM tblProperty 
				WHERE (fkPageDefinitionID = @ToPropertyID -- already converted to new type!
				or ScopeName LIKE '%.' + CAST(@ToPropertyID as varchar) + '.%')
				AND fkLanguageBranchID <> @MasterLanguageID
				AND  EXISTS (
					SELECT * from #updatepages WHERE fkChildID=fkPageID)
				SET @cnt = @cnt - @@rowcount				
			END
			ELSE IF (@LanguageSpecificSource < 3)
			BEGIN
				-- Converting from language neutral to language supporting property
				-- We must copy existing master language property to other languages for the page
				-- NOTE: Due to the way language neutral properties are loaded, that is they are always
				-- loaded from published version, see netPageDataLoadVersion it is sufficient to add property
				-- values to tblProperty (no need to update tblWorkProperty
				INSERT INTO tblProperty
					(fkPageDefinitionID,
					fkPageID,
					fkLanguageBranchID,
					ScopeName,
					Boolean,
					Number,
					FloatNumber,
					PageType,
					PageLink,
					LinkGuid,
					Date,
					String,
					LongString,
					LongStringLength)
				SELECT 
					CASE @IsBlock when 1 then Prop.fkPageDefinitionID else @ToPropertyID end, 
					Prop.fkPageID,
					Lang.fkLanguageBranchID,
					Prop.ScopeName,
					Prop.Boolean,
					Prop.Number,
					Prop.FloatNumber,
					Prop.PageType,
					Prop.PageLink,
					Prop.LinkGuid,
					Prop.Date,
					Prop.String,
					Prop.LongString,
					Prop.LongStringLength
				FROM
				tblPageLanguage Lang
				INNER JOIN
				tblProperty Prop ON Prop.fkLanguageBranchID = @MasterLanguageID
				WHERE
				Prop.fkPageID = @PageID AND
				(Prop.fkPageDefinitionID = @ToPropertyID -- already converted to new type!
				or Prop.ScopeName LIKE '%.' + CAST(@ToPropertyID as varchar) + '.%') AND
				Prop.fkLanguageBranchID = @MasterLanguageID AND
				Lang.fkLanguageBranchID <> @MasterLanguageID AND
				Lang.fkPageID = @PageID
				-- Need to add entries to tblWorkProperty for all pages not in the master language
				-- First we need to read the master language property into a temp table
				CREATE TABLE #TempWorkProperty
				(
					fkPageDefinitionID int,
					ScopeName nvarchar(450),
					Boolean bit,
					Number int,
					FloatNumber float,
					PageType int,
					PageLink int,
				    LinkGuid uniqueidentifier,
					Date datetime,
					String nvarchar(450),
					LongString nvarchar(max)
				)
				INSERT INTO #TempWorkProperty
				SELECT
					Prop.fkPageDefinitionID,
					Prop.ScopeName,
					Prop.Boolean,
					Prop.Number,
					Prop.FloatNumber,
					Prop.PageType,
					Prop.PageLink,
				    Prop.LinkGuid,
					Prop.Date,
					Prop.String,
					Prop.LongString
				FROM
					tblWorkProperty AS Prop
					INNER JOIN
					tblWorkPage AS Page ON Prop.fkWorkPageID = Page.pkID
				WHERE
					(Prop.fkPageDefinitionID = @ToPropertyID -- already converted to new type!
				or Prop.ScopeName LIKE '%.' + CAST(@ToPropertyID as varchar) + '.%') AND
					Page.fkLanguageBranchID = @MasterLanguageID AND
					Page.fkPageID = @PageID
					ORDER BY Page.pkID DESC
				-- Now add a new property for every language (except master) using the master value
				INSERT INTO	tblWorkProperty 
				SELECT
					CASE @IsBlock when 1 then TempProp.fkPageDefinitionID else @ToPropertyID end,
					Page.pkID,
					TempProp.ScopeName,
					TempProp.Boolean,
					TempProp.Number,
					TempProp.FloatNumber,
					TempProp.PageType,
					TempProp.PageLink,
					TempProp.Date,
					TempProp.String,
					TempProp.LongString,
					TempProp.LinkGuid
				FROM 
					tblWorkPage AS Page, #TempWorkProperty AS TempProp
				WHERE
					Page.fkPageID = @PageID AND
					Page.fkLanguageBranchID <> @MasterLanguageID
				DROP TABLE #TempWorkProperty
			END
		END
	END
	DROP TABLE #updatepages
	RETURN (@cnt)
END
GO
/****** Object:  StoredProcedure [dbo].[netContentEnsureVersions]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netContentEnsureVersions]
(
	@ContentID			INT
)
AS
BEGIN
	DECLARE @LangBranchID INT
	DECLARE @LanguageBranch NCHAR(17)
	DECLARE @NewWorkContentID INT
	DECLARE @UserName NVARCHAR(255)
	CREATE TABLE #ContentLangsWithoutVersion
		(fkLanguageBranchID INT)
	/* Get a list of page languages that do not have an entry in tblWorkContent for the given page */
	INSERT INTO #ContentLangsWithoutVersion
		(fkLanguageBranchID)
	SELECT 
		tblContentLanguage.fkLanguageBranchID
	FROM 
		tblContentLanguage
	WHERE	
		fkContentID=@ContentID AND
		NOT EXISTS(
			SELECT * 
			FROM 
				tblWorkContent 
			WHERE 
				tblWorkContent.fkContentID=tblContentLanguage.fkContentID AND 
				tblWorkContent.fkLanguageBranchID=tblContentLanguage.fkLanguageBranchID)
	/* Get the first language to create a page version for */
	SELECT 
		@LangBranchID=Min(fkLanguageBranchID) 
	FROM 
		#ContentLangsWithoutVersion
	WHILE NOT @LangBranchID IS NULL
	BEGIN
		/* Get language name and user name to set for page version that we are about to create */
		SELECT 
			@LanguageBranch=LanguageID 
		FROM 
			tblLanguageBranch 
		WHERE 
			pkID=@LangBranchID
		SELECT 
			@UserName=ChangedByName 
		FROM 
			tblContentLanguage 
		WHERE 
			fkContentID=@ContentID AND 
			fkLanguageBranchID=@LangBranchID
		/* Create a new page version for the given page and language */
		EXEC @NewWorkContentID = editCreateContentVersion 
			@ContentID=@ContentID, 
			@WorkContentID=NULL, 
			@UserName=@UserName,
			@LanguageBranch=@LanguageBranch
		/* TODO - check if we should mark page version as published... */
		UPDATE 
			tblWorkContent 
		SET 
			ReadyToPublish=1, 
			HasBeenPublished=1 
		WHERE 
			pkID=@NewWorkContentID
		UPDATE 
			tblContentLanguage 
		SET 
			PublishedVersion=@NewWorkContentID 
		WHERE 
			fkContentID=@ContentID AND 
			fkLanguageBranchID=@LangBranchID
		/* Get next language for the loop */
		SELECT 
			@LangBranchID=Min(fkLanguageBranchID) 
		FROM 
			#ContentLangsWithoutVersion 
		WHERE 
			fkLanguageBranchID > @LangBranchID
	END
	DROP TABLE #ContentLangsWithoutVersion
END
GO
/****** Object:  StoredProcedure [dbo].[netCategoryDelete]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[netCategoryDelete]
(
	@CategoryID			INT
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
		/* Delete any references */
		DELETE FROM tblCategoryPage WHERE fkCategoryID=@CategoryID
		DELETE FROM tblWorkCategory WHERE fkCategoryID=@CategoryID
		/* Delete the category */
		DELETE FROM tblCategory WHERE pkID=@CategoryID
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[editDeletePageCheck]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editDeletePageCheck]
(
    @PageID			INT,
	@IncludeDecendents BIT
)
AS
BEGIN
    SET NOCOUNT ON
    SET XACT_ABORT ON
    /* Get all pages to delete (= PageID and all its childs) */
	CREATE TABLE #pages
	(
		pkID	INT PRIMARY KEY,
		PageGUID UNIQUEIDENTIFIER
	)
	INSERT INTO #pages (pkID) 
	SELECT @PageID
	IF @IncludeDecendents = 1
	BEGIN
		INSERT INTO #pages (pkID) 
		SELECT 
			fkChildID 
		FROM 
			tblTree 
		WHERE fkParentID=@PageID
	END
	UPDATE #pages 
		SET PageGUID = tblPage.PageGUID
	FROM tblPage INNER JOIN #pages ON #pages.pkID=tblPage.pkID
	EXEC editDeletePageCheckInternal
    DROP TABLE #pages
END
GO
/****** Object:  StoredProcedure [dbo].[editDeleteChildsCheck]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editDeleteChildsCheck]
(
    @PageID			INT
)
AS
BEGIN
    SET NOCOUNT ON
    SET XACT_ABORT ON
    /* Get all pages to delete (all childs of PageID) */
	CREATE TABLE #pages
	(
		pkID	INT PRIMARY KEY,
		PageGUID UNIQUEIDENTIFIER
	)
	INSERT INTO #pages (pkID) 
	SELECT 
		fkChildID 
	FROM 
		tblTree 
	WHERE fkParentID=@PageID
	UPDATE #pages 
		SET PageGUID = tblPage.PageGUID
	FROM tblPage INNER JOIN #pages ON #pages.pkID=tblPage.pkID
	EXEC editDeletePageCheckInternal
    DROP TABLE #pages
    RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[editDeletePageInternal]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editDeletePageInternal]
(
    @PageID INT,
    @ForceDelete INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
-- STRUCTURE
	-- Make sure we dump structure and features like fetch data before we start off repairing links for pages that should not get deleted
	UPDATE 
	    tblPage 
	SET 
	    fkParentID = NULL,
	    ArchivePageGUID=NULL 
	WHERE 
	    pkID IN ( SELECT pkID FROM #pages )
	UPDATE 
	    tblWorkPage 
	SET 
	    fkMasterVersionID=NULL,
	    PageLinkGUID=NULL,
	    ArchivePageGUID=NULL 
	WHERE 
	    fkPageID IN ( SELECT pkID FROM #pages )
-- VERSION DATA
	-- Delete page links, archiving and fetch data pointing to us from external pages
	DELETE FROM 
	    tblWorkProperty 
	WHERE 
	    PageLink IN ( SELECT pkID FROM #pages )
	UPDATE 
	    tblWorkPage 
	SET 
	    ArchivePageGUID = NULL 
	WHERE 
	    ArchivePageGUID IN ( SELECT PageGUID FROM #pages )
	UPDATE 
	    tblWorkPage 
	SET 
	    PageLinkGUID = NULL, 
	    LinkType=0,
	    LinkURL=
		(
			SELECT TOP 1 
			      '~/link/' + CONVERT(NVARCHAR(32),REPLACE((select top 1 PageGUID FROM tblPage where tblPage.pkID = tblWorkPage.fkPageID),'-','')) + '.aspx'
			FROM 
			    tblPageType
			WHERE 
			    tblPageType.pkID=(SELECT fkPageTypeID FROM tblPage WHERE tblPage.pkID=tblWorkPage.fkPageID)
		)
	 WHERE 
	    PageLinkGUID IN ( SELECT PageGUID FROM #pages )
	-- Remove workproperties,workcategories and finally the work versions themselves
	DELETE FROM 
	    tblWorkProperty 
	WHERE 
	    fkWorkPageID IN ( SELECT pkID FROM tblWorkPage WHERE fkPageID IN ( SELECT pkID FROM #pages ) )
	DELETE FROM 
	    tblWorkCategory 
	WHERE 
	    fkWorkPageID IN ( SELECT pkID FROM tblWorkPage WHERE fkPageID IN ( SELECT pkID FROM #pages ) )
	DELETE FROM 
	    tblWorkPage 
	WHERE 
	    fkPageID IN ( SELECT pkID FROM #pages )
-- PUBLISHED PAGE DATA
	IF (@ForceDelete IS NOT NULL)
	BEGIN
		DELETE FROM 
		    tblProperty 
		WHERE 
		    PageLink IN (SELECT pkID FROM #pages)
	END
	ELSE
	BEGIN
		/* Default action: Only delete references from pages in wastebasket */
		DELETE FROM 
			tblProperty
		FROM 
		    tblProperty AS P
		INNER JOIN 
		    tblPage ON P.fkPageID=tblPage.pkID
		WHERE
			tblPage.Deleted=1 AND
			P.PageLink IN (SELECT pkID FROM #pages)
	END
	DELETE FROM 
	    tblPropertyDefault 
	WHERE 
	    PageLink IN ( SELECT pkID FROM #pages )
	UPDATE 
	    tblPage 
	SET 
	    ArchivePageGUID = NULL 
	WHERE 
	    ArchivePageGUID IN ( SELECT PageGUID FROM #pages )
	-- Remove fetch data from any external pages pointing to us
	UPDATE 
	    tblPageLanguage 
	SET     
	    PageLinkGUID = NULL, 
	    FetchData=0,
	    LinkURL=
		(
			SELECT TOP 1 
		      '~/link/' + CONVERT(NVARCHAR(32),REPLACE((select top 1 PageGUID FROM tblPage where tblPage.pkID = tblPageLanguage.fkPageID),'-','')) + '.aspx'
			FROM 
			    tblPageType
			WHERE 
			    tblPageType.pkID=(SELECT tblPage.fkPageTypeID FROM tblPage WHERE tblPage.pkID=tblPageLanguage.fkPageID)
		)
	 WHERE 
	    PageLinkGUID IN ( SELECT PageGUID FROM #pages )
	-- Remove ALC, categories and the properties
	DELETE FROM 
	    tblCategoryPage 
	WHERE 
	    fkPageID IN ( SELECT pkID FROM #pages )
	DELETE FROM 
	    tblProperty 
	WHERE 
	    fkPageID IN ( SELECT pkID FROM #pages )
	DELETE FROM 
	    tblContentAccess 
	WHERE 
	    fkContentID IN ( SELECT pkID FROM #pages )
-- KEYWORDS AND INDEXING
	DELETE FROM 
	    tblContentSoftlink
	WHERE 
	    fkOwnerContentID IN ( SELECT pkID FROM #pages )
-- PAGETYPES
	UPDATE 
	    tblPageTypeDefault 
	SET 
	    fkArchivePageID=NULL 
	WHERE fkArchivePageID IN (SELECT pkID FROM #pages)
-- PAGE/TREE
	DELETE FROM 
	    tblTree 
	WHERE 
	    fkChildID IN ( SELECT pkID FROM #pages )
	DELETE FROM 
	    tblTree 
	WHERE 
	    fkParentID IN ( SELECT pkID FROM #pages )
	DELETE FROM 
	    tblPageLanguage 
	WHERE 
	    fkPageID IN ( SELECT pkID FROM #pages )
	DELETE FROM 
	    tblPageLanguageSetting 
	WHERE 
	    fkPageID IN ( SELECT pkID FROM #pages )
	DELETE FROM
	    tblPage 
	WHERE 
	    pkID IN ( SELECT pkID FROM #pages )
END
GO
/****** Object:  StoredProcedure [dbo].[editDeleteProperty]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editDeleteProperty]
(
	@PageID			INT,
	@WorkPageID		INT,
	@PageDefinitionID	INT,
	@Override		BIT = 0,
	@LanguageBranch		NCHAR(17) = NULL,
	@ScopeName	NVARCHAR(450) = NULL
)
AS
BEGIN
	DECLARE @LangBranchID NCHAR(17);
	SELECT @LangBranchID = pkID FROM tblLanguageBranch WHERE LanguageID = @LanguageBranch
	IF @LangBranchID IS NULL 
	BEGIN 
		if @LanguageBranch IS NOT NULL
			RAISERROR('Language branch %s is not defined',16,1, @LanguageBranch)
		else
			SET @LangBranchID = -1
	END
	DECLARE @IsLanguagePublished BIT;
	IF EXISTS(SELECT fkPageID FROM tblPageLanguage 
		WHERE fkPageID = @PageID AND fkLanguageBranchID = CAST(@LangBranchID AS INT) AND PublishedVersion IS NOT NULL)
		SET @IsLanguagePublished = 1
	ELSE
		SET @IsLanguagePublished = 0
	SET NOCOUNT ON
	SET XACT_ABORT ON
	DECLARE @retval INT
	SET @retval = 0
		IF (@WorkPageID IS NOT NULL)
		BEGIN
			/* This only applies to categories, but since PageDefinitionID is unique
				between all properties it is safe to blindly delete like this */
			DELETE FROM
				tblWorkContentCategory
			WHERE
				fkWorkContentID = @WorkPageID
			AND
				CategoryType = @PageDefinitionID
			AND
				(@ScopeName IS NULL OR ScopeName = @ScopeName)
			DELETE FROM
				tblWorkProperty
			WHERE
				fkWorkPageID = @WorkPageID
			AND
				fkPageDefinitionID = @PageDefinitionID
			AND 
				((@ScopeName IS NULL AND ScopeName IS NULL) OR (@ScopeName = ScopeName))
		END
		ELSE
		BEGIN
			/* Might be dynamic properties */
			DELETE FROM
				tblContentCategory
			WHERE
				fkContentID = @PageID
			AND
				CategoryType = @PageDefinitionID
			AND
				(@ScopeName IS NULL OR ScopeName = @ScopeName)
			AND
			(
				@LanguageBranch IS NULL
			OR
				@LangBranchID = fkLanguageBranchID
			)
			IF (@Override = 1)
			BEGIN
				DELETE FROM
					tblProperty
				WHERE
					fkPageDefinitionID = @PageDefinitionID
				AND
					fkPageID IN (SELECT fkChildID FROM tblTree WHERE fkParentID = @PageID)
				AND
				(
					@LanguageBranch IS NULL
				OR
					@LangBranchID = tblProperty.fkLanguageBranchID
				)
				AND 
					((@ScopeName IS NULL AND ScopeName IS NULL) OR (@ScopeName = ScopeName))
				SET @retval = 1
			END
		END
		/* When no version is published we save value in tblProperty as well, so the property also need to be removed from tblProperty*/
		IF (@WorkPageID IS NULL OR @IsLanguagePublished = 0)
		BEGIN
			DELETE FROM
				tblProperty
			WHERE
				fkPageID = @PageID
			AND 
				fkPageDefinitionID = @PageDefinitionID  
			AND
			(
				@LanguageBranch IS NULL
			OR
				@LangBranchID = tblProperty.fkLanguageBranchID
			)
			AND
				((@ScopeName IS NULL AND ScopeName IS NULL) OR (@ScopeName = ScopeName))
		END
	RETURN @retval
END
GO
/****** Object:  StoredProcedure [dbo].[editDeletePageVersionInternal]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editDeletePageVersionInternal]
(
	@WorkPageID		INT
)
AS
BEGIN
	UPDATE tblWorkPage SET fkMasterVersionID=NULL WHERE fkMasterVersionID=@WorkPageID
	DELETE FROM tblWorkProperty WHERE fkWorkPageID=@WorkPageID
	DELETE FROM tblWorkCategory WHERE fkWorkPageID=@WorkPageID
	DELETE FROM tblWorkPage WHERE pkID=@WorkPageID
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[editDeletePageVersion]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editDeletePageVersion]
(
	@WorkPageID		INT
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	DECLARE @PageID				INT
	DECLARE @PublishedWorkID	INT
	DECLARE @LangBranchID		INT
	/* Verify that we can delete this version (i e do not allow removal of current version) */
	SELECT 
		@PageID=tblPageLanguage.fkPageID, 
		@LangBranchID=tblPageLanguage.fkLanguageBranchID,
		@PublishedWorkID=tblPageLanguage.PublishedVersion 
	FROM 
		tblWorkPage 
	INNER JOIN 
		tblPageLanguage ON tblPageLanguage.fkPageID=tblWorkPage.fkPageID AND tblPageLanguage.fkLanguageBranchID = tblWorkPage.fkLanguageBranchID
	WHERE 
		tblWorkPage.pkID=@WorkPageID
	IF (@@ROWCOUNT <> 1 OR @PublishedWorkID=@WorkPageID)
		RETURN -1
	IF ( (SELECT COUNT(pkID) FROM tblWorkPage WHERE fkPageID=@PageID AND fkLanguageBranchID=@LangBranchID ) < 2 )
		RETURN -1
	EXEC editDeletePageVersionInternal @WorkPageID=@WorkPageID
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[editDeleteChilds]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editDeleteChilds]
(
    @PageID			INT,
    @ForceDelete	INT = NULL
)
AS
BEGIN
    SET NOCOUNT ON
    SET XACT_ABORT ON
    DECLARE @retval INT
    /* Get all pages to delete (all childs of PageID) */
	CREATE TABLE #pages
	(
		pkID	INT PRIMARY KEY,
		PageGUID UNIQUEIDENTIFIER
	)
	INSERT INTO #pages (pkID) 
	SELECT 
		fkChildID 
	FROM 
		tblTree 
	WHERE fkParentID=@PageID
	UPDATE #pages 
		SET PageGUID = tblPage.PageGUID
	FROM tblPage INNER JOIN #pages ON #pages.pkID=tblPage.pkID
	EXEC @retval=editDeletePageInternal @PageID=@PageID, @ForceDelete=@ForceDelete
    DROP TABLE #pages
    RETURN @retval
END
GO
/****** Object:  StoredProcedure [dbo].[editContentVersionList]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editContentVersionList]
(
	@ContentID INT
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @ParentID		INT
	DECLARE @NewWorkContentID	INT
	/* Make sure we correct versions for page */
	EXEC netContentEnsureVersions @ContentID=@ContentID	
	/* Get info about all page versions */
	SELECT 
		W.pkID, 
		W.Name,
		W.LinkType,
		W.LinkURL,
		W.Saved, 
		W.CommonDraft,
		W.ChangedByName AS UserNameSaved,
		W.NewStatusByName As UserNameChanged,
		PT.ContentType as ContentType,
		CASE 
			WHEN P.PublishedVersion=W.pkID THEN 4	/* EPnWorkStatusPublished */
			WHEN W.HasBeenPublished=1 THEN 5		/* EPnWorkStatusPrevious */
			WHEN W.Rejected=1 THEN 1				/* EPnWorkStatusRejected */
			WHEN W.DelayedPublish=1 THEN 6			/* EPnWorkStatusDelayedPublish */
			WHEN W.ReadyToPublish=1 THEN 3			/* EPnWorkStatusCheckedIn */
			ELSE 2									/* EPnWorkStatusCheckedOut */
		END AS WorkStatus,
		W.RejectComment,
		W.fkMasterVersionID,
		RTRIM(tblLanguageBranch.LanguageID) AS LanguageBranch,
		CASE WHEN tblContent.fkMasterLanguageBranchID=P.fkLanguageBranchID THEN 1 ELSE 0 END AS IsMasterLanguageBranch
	FROM
		tblContentLanguage AS P
	INNER JOIN
		tblContent
	ON
		tblContent.pkID=P.fkContentID
	LEFT JOIN
		tblWorkContent AS W
	ON
		W.fkContentID=P.fkContentID
	LEFT JOIN
		tblContentType AS PT
	ON
		tblContent.fkContentTypeID = PT.pkID
	LEFT JOIN
		tblLanguageBranch
	ON
		tblLanguageBranch.pkID=W.fkLanguageBranchID
	WHERE
		W.fkContentID=@ContentID AND W.fkLanguageBranchID=P.fkLanguageBranchID
	ORDER BY
		W.pkID
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[editDeletePage]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editDeletePage]
(
    @PageID			INT,
    @ForceDelete	INT = NULL
)
AS
BEGIN
    SET NOCOUNT ON
    SET XACT_ABORT ON
    DECLARE @retval INT
    DECLARE @ParentID INT
    /* Get all pages to delete (= PageID and all its childs) */
	CREATE TABLE #pages
	(
		pkID	INT PRIMARY KEY,
		PageGUID UNIQUEIDENTIFIER
	)
	INSERT INTO #pages (pkID) 
	SELECT 
		fkChildID 
	FROM 
		tblTree 
	WHERE fkParentID=@PageID
	UNION
	SELECT @PageID
	UPDATE #pages 
		SET PageGUID = tblPage.PageGUID
	FROM tblPage INNER JOIN #pages ON #pages.pkID=tblPage.pkID
	SELECT @ParentID=fkParentID FROM tblPage WHERE pkID=@PageID
	EXEC @retval=editDeletePageInternal @PageID=@PageID, @ForceDelete=@ForceDelete
    DROP TABLE #pages
    RETURN @retval
END
GO
/****** Object:  StoredProcedure [dbo].[editDeleteObsoletePageVersions]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editDeleteObsoletePageVersions]
(
	@PageID		INT,
	@MaxVersions	INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	DECLARE @ObsoleteVersions	INT
	DECLARE @DeleteWorkPageID	INT
	DECLARE @retval		INT
	DECLARE @CurrentLanguage 	INT
	DECLARE @FirstLanguage	BIT
	SET @FirstLanguage = 1
	IF (@MaxVersions IS NULL OR @MaxVersions=0)
		RETURN 0
		CREATE TABLE #languages (fkLanguageBranchID INT)
		INSERT INTO #languages SELECT fkLanguageBranchID FROM tblWorkPage WHERE fkPageID = @PageID GROUP BY fkLanguageBranchID
		SET @CurrentLanguage = (SELECT MIN(fkLanguageBranchID) FROM #languages)
		WHILE (NOT @CurrentLanguage = 0)
		BEGIN
			SELECT @ObsoleteVersions=COUNT(pkID)-1 FROM tblWorkPage WHERE fkPageID=@PageID AND HasBeenPublished=1 AND fkLanguageBranchID = @CurrentLanguage
			WHILE (@ObsoleteVersions > @MaxVersions)
			BEGIN
				SELECT TOP 1 @DeleteWorkPageID=pkID FROM tblWorkPage WHERE fkPageID=@PageID AND HasBeenPublished=1  AND fkLanguageBranchID = @CurrentLanguage ORDER BY pkID ASC
				EXEC @retval=editDeletePageVersion @WorkPageID=@DeleteWorkPageID
				IF (@retval <> 0)
					BREAK
				SET @ObsoleteVersions=@ObsoleteVersions - 1
			END
			IF EXISTS(SELECT fkLanguageBranchID FROM #languages WHERE fkLanguageBranchID > @CurrentLanguage)
			    SET @CurrentLanguage = (SELECT MIN(fkLanguageBranchID) FROM #languages WHERE fkLanguageBranchID > @CurrentLanguage)
		    ELSE
		        SET @CurrentLanguage = 0
		END
		DROP TABLE #languages
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[editPublishPageVersion]    Script Date: 12/06/2012 12:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[editPublishPageVersion]
(
	@WorkPageID	INT,
	@UserName NVARCHAR(255),
	@MaxVersions INT = NULL,
	@NoWorkflow BIT = 1,
	@PublishedDate DATETIME = NULL
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	DECLARE @PageID INT
	DECLARE @retval INT
	DECLARE @FirstPublish BIT
	DECLARE @ParentID INT
	DECLARE @LangBranchID INT
	DECLARE @IsMasterLang BIT
	DECLARE @IsMasterLangPendingPublish BIT
	DECLARE @StartPublish DATETIME
	/* Verify that we have a page to publish */
	SELECT	@PageID=fkPageID,
			@LangBranchID=fkLanguageBranchID,
			@IsMasterLang = CASE WHEN tblWorkPage.fkLanguageBranchID=tblPage.fkMasterLanguageBranchID THEN 1 ELSE 0 END,
			@IsMasterLangPendingPublish = tblPage.PendingPublish
	FROM tblWorkPage WITH (ROWLOCK,XLOCK)
	INNER JOIN tblPage WITH (ROWLOCK,XLOCK) ON tblPage.pkID=tblWorkPage.fkPageID
	WHERE tblWorkPage.pkID=@WorkPageID
	IF (@@ROWCOUNT <> 1)
		RETURN 0
	IF @IsMasterLang=0 AND @IsMasterLangPendingPublish=1
		RAISERROR (N'editPublishPageVersion: Cannot publish a language before the master language has been published (WorkID=%d)', 16, 1, @WorkPageID)
	IF @PublishedDate IS NULL
		SET @PublishedDate = GetDate()
	/* Move page information from worktable to published table */
	IF @IsMasterLang=1
	BEGIN
		UPDATE 
			tblPage
		SET
			ArchivePageGUID	= W.ArchivePageGUID,
			VisibleInMenu	= W.VisibleInMenu,
			ChildOrderRule	= W.ChildOrderRule,
			PeerOrder		= W.PeerOrder,
			PendingPublish  = 0,
			@FirstPublish  = tblPage.PendingPublish
		FROM 
			tblWorkPage AS W
		WHERE 
			tblPage.pkID=W.fkPageID AND 
			W.pkID=@WorkPageID
	END
	UPDATE 
			tblPageLanguage WITH (ROWLOCK,XLOCK)
		SET
			ChangedByName = W.ChangedByName,
			PageLinkGUID	= W.PageLinkGUID,
			fkFrameID		= W.fkFrameID,
			Name			= W.Name,
			URLSegment		= W.URLSegment,
			LinkURL			= W.LinkURL,
			ExternalURL		= Lower(W.ExternalURL),
			AutomaticLink	= CASE WHEN W.LinkType = 2 OR W.LinkType = 3 THEN 0 ELSE 1 END,
			FetchData		= CASE WHEN W.LinkType = 4 THEN 1 ELSE 0 END,
			Created			= W.Created,
			Changed			= CASE WHEN W.ChangedOnPublish=0 AND tblPageLanguage.PendingPublish=0 THEN Changed ELSE @PublishedDate END,
			Saved			= @PublishedDate,
			@StartPublish = StartPublish	= COALESCE(W.StartPublish, tblPageLanguage.StartPublish, DATEADD(s, -30, @PublishedDate)),
			StopPublish		= W.StopPublish,
			PendingPublish  = 0,
			PublishedVersion= @WorkPageID,
			@FirstPublish  = tblPageLanguage.PendingPublish
		FROM 
			tblWorkPage AS W
		WHERE 
			tblPageLanguage.fkPageID=W.fkPageID AND
			W.fkLanguageBranchID=tblPageLanguage.fkLanguageBranchID AND
			W.pkID=@WorkPageID
	IF @@ROWCOUNT!=1
		RAISERROR (N'editPublishPageVersion: Cannot find correct version in tblPageLanguage for version %d', 16, 1, @WorkPageID)
	/* Remember that this version has been published, and get the publish dates back into tblWorkPage */
	UPDATE
		tblWorkPage
	SET
		HasBeenPublished=1,
		ReadyToPublish=1,
		Rejected=0,
		DelayedPublish=0,
		StartPublish=@StartPublish,
		Saved=@PublishedDate,
		NewStatusByName=@UserName,
		fkMasterVersionID = NULL
	WHERE
		pkID=@WorkPageID
	/* Remove all properties defined for this page except dynamic properties */
	DELETE FROM 
		tblProperty
	FROM 
		tblProperty
	INNER JOIN
		tblPageDefinition ON fkPageDefinitionID=tblPageDefinition.pkID
	WHERE 
		fkPageID=@PageID AND
		fkPageTypeID IS NOT NULL AND
		fkLanguageBranchID=@LangBranchID
	/* Move properties from worktable to published table */
	INSERT INTO tblProperty 
		(fkPageDefinitionID,
		fkPageID,
		fkLanguageBranchID,
		ScopeName,
		Boolean,
		Number,
		FloatNumber,
		PageType,
		PageLink,
		Date,
		String,
		LongString,
		LongStringLength,
        LinkGuid)
	SELECT
		fkPageDefinitionID,
		@PageID,
		@LangBranchID,
		ScopeName,
		Boolean,
		Number,
		FloatNumber,
		PageType,
		PageLink,
		Date,
		String,
		LongString,
		/* LongString is utf-16 - Datalength gives bytes, i e div by 2 gives characters */
		/* Include length to handle delayed loading of LongString with threshold */
		COALESCE(DATALENGTH(LongString), 0) / 2,
        LinkGuid
	FROM
		tblWorkProperty
	WHERE
		fkWorkPageID=@WorkPageID
	/* Move categories to published tables */
	DELETE 	tblCategoryPage
	FROM tblCategoryPage
	LEFT JOIN tblPageDefinition ON tblPageDefinition.pkID=tblCategoryPage.CategoryType 
	WHERE 	tblCategoryPage.fkPageID=@PageID
			AND (NOT fkPageTypeID IS NULL OR CategoryType=0)
			AND (tblPageDefinition.LanguageSpecific>2 OR @IsMasterLang=1)--Only lang specific on non-master
			AND tblCategoryPage.fkLanguageBranchID=@LangBranchID
	INSERT INTO tblContentCategory
		(fkContentID,
		fkCategoryID,
		CategoryType,
		fkLanguageBranchID,
		ScopeName)
	SELECT
		@PageID,
		fkCategoryID,
		CategoryType,
		@LangBranchID,
		ScopeName
	FROM
		tblWorkContentCategory
	WHERE
		fkWorkContentID=@WorkPageID
	/* Remove obsolete versions */
	EXEC editDeleteObsoletePageVersions @PageID=@PageID, @MaxVersions=@MaxVersions
	/* Set the Common Draft to the published page regardless if there is already common draft */
	EXEC editSetCommonDraftVersion		@WorkContentID = @WorkPageID, @Force = 1				
	RETURN 0
END
GO
/****** Object:  Default [DF_tblUserPermission_IsRole]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblUserPermission] ADD  CONSTRAINT [DF_tblUserPermission_IsRole]  DEFAULT ((1)) FOR [IsRole]
GO
/****** Object:  Default [DF__tblUniqueSequence__LastValue]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblUniqueSequence] ADD  CONSTRAINT [DF__tblUniqueSequence__LastValue]  DEFAULT ((0)) FOR [LastValue]
GO
/****** Object:  Default [DF_tblPlugIn_Accessed]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblPlugIn] ADD  CONSTRAINT [DF_tblPlugIn_Accessed]  DEFAULT (getdate()) FOR [Saved]
GO
/****** Object:  Default [DF_tblPlugIn_Created]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblPlugIn] ADD  CONSTRAINT [DF_tblPlugIn_Created]  DEFAULT (getdate()) FOR [Created]
GO
/****** Object:  Default [DF_tblPlugIn_Enabled]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblPlugIn] ADD  CONSTRAINT [DF_tblPlugIn_Enabled]  DEFAULT ((1)) FOR [Enabled]
GO
/****** Object:  Default [DF__tblSchedul__pkID__1A34DF26]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblScheduledItem] ADD  CONSTRAINT [DF__tblSchedul__pkID__1A34DF26]  DEFAULT (newid()) FOR [pkID]
GO
/****** Object:  Default [DF_tblScheduledItem_Enabled]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblScheduledItem] ADD  CONSTRAINT [DF_tblScheduledItem_Enabled]  DEFAULT ((0)) FOR [Enabled]
GO
/****** Object:  Default [DF__tblScheduledItem__IsRunnning]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblScheduledItem] ADD  CONSTRAINT [DF__tblScheduledItem__IsRunnning]  DEFAULT ((0)) FOR [IsRunning]
GO
/****** Object:  Default [DF_tblRemoteSite_IsTrusted]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblRemoteSite] ADD  CONSTRAINT [DF_tblRemoteSite_IsTrusted]  DEFAULT ((0)) FOR [IsTrusted]
GO
/****** Object:  Default [DF_tblRemoteSite_AllowUrlLookup]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblRemoteSite] ADD  CONSTRAINT [DF_tblRemoteSite_AllowUrlLookup]  DEFAULT ((0)) FOR [AllowUrlLookup]
GO
/****** Object:  Default [DF_tblPropertyDefinitionGroup_SystemGroup]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblPropertyDefinitionGroup] ADD  CONSTRAINT [DF_tblPropertyDefinitionGroup_SystemGroup]  DEFAULT ((0)) FOR [SystemGroup]
GO
/****** Object:  Default [DF_tblPropertyDefinitionGroup_Access]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblPropertyDefinitionGroup] ADD  CONSTRAINT [DF_tblPropertyDefinitionGroup_Access]  DEFAULT ((10)) FOR [Access]
GO
/****** Object:  Default [DF_tblPropertyDefinitionGroup_DefaultVisible]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblPropertyDefinitionGroup] ADD  CONSTRAINT [DF_tblPropertyDefinitionGroup_DefaultVisible]  DEFAULT ((1)) FOR [GroupVisible]
GO
/****** Object:  Default [DF_tblPropertyDefinitionGroup_GroupOrder]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblPropertyDefinitionGroup] ADD  CONSTRAINT [DF_tblPropertyDefinitionGroup_GroupOrder]  DEFAULT ((1)) FOR [GroupOrder]
GO
/****** Object:  Default [DF_tblPropertyDefault_Boolean]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblPropertyDefinitionDefault] ADD  CONSTRAINT [DF_tblPropertyDefault_Boolean]  DEFAULT ((0)) FOR [Boolean]
GO
/****** Object:  Default [DF_tblUnifiedPath_InheritAcl]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblUnifiedPath] ADD  CONSTRAINT [DF_tblUnifiedPath_InheritAcl]  DEFAULT ((1)) FOR [InheritAcl]
GO
/****** Object:  Default [DF_tblContentType_ContentTypeGUID]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblContentType] ADD  CONSTRAINT [DF_tblContentType_ContentTypeGUID]  DEFAULT (newid()) FOR [ContentTypeGUID]
GO
/****** Object:  Default [DF_tblContentType_Registered]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblContentType] ADD  CONSTRAINT [DF_tblContentType_Registered]  DEFAULT (getdate()) FOR [Created]
GO
/****** Object:  Default [DF_tblContentType_MetaDataInherit]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblContentType] ADD  CONSTRAINT [DF_tblContentType_MetaDataInherit]  DEFAULT ((0)) FOR [MetaDataInherit]
GO
/****** Object:  Default [DF_tblContentType_MetaDataDefault]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblContentType] ADD  CONSTRAINT [DF_tblContentType_MetaDataDefault]  DEFAULT ((0)) FOR [MetaDataDefault]
GO
/****** Object:  Default [DF_tblContentType_ContentType]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblContentType] ADD  CONSTRAINT [DF_tblContentType_ContentType]  DEFAULT ((0)) FOR [ContentType]
GO
/****** Object:  Default [DF__tblLanguageBranch__Enabled]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblLanguageBranch] ADD  CONSTRAINT [DF__tblLanguageBranch__Enabled]  DEFAULT ((1)) FOR [Enabled]
GO
/****** Object:  Default [DF_tblFrame_SystemFrame]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblFrame] ADD  CONSTRAINT [DF_tblFrame_SystemFrame]  DEFAULT ((0)) FOR [SystemFrame]
GO
/****** Object:  Default [DF__aspnet_Ap__Appli__145C0A3F]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[aspnet_Applications] ADD  DEFAULT (newid()) FOR [ApplicationId]
GO
/****** Object:  Default [DF_tblChangeLog_ChangeDate]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblChangeLog] ADD  CONSTRAINT [DF_tblChangeLog_ChangeDate]  DEFAULT (getdate()) FOR [ChangeDate]
GO
/****** Object:  Default [DF_tblChangeLog_Category]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblChangeLog] ADD  CONSTRAINT [DF_tblChangeLog_Category]  DEFAULT ((0)) FOR [Category]
GO
/****** Object:  Default [DF_tblChangeLog_Action]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblChangeLog] ADD  CONSTRAINT [DF_tblChangeLog_Action]  DEFAULT ((0)) FOR [Action]
GO
/****** Object:  Default [DF_tblCategory_CategoryGUID]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblCategory] ADD  CONSTRAINT [DF_tblCategory_CategoryGUID]  DEFAULT (newid()) FOR [CategoryGUID]
GO
/****** Object:  Default [DF_tblCategory_PeerOrder]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblCategory] ADD  CONSTRAINT [DF_tblCategory_PeerOrder]  DEFAULT ((100)) FOR [SortOrder]
GO
/****** Object:  Default [DF_tblCategory_Available]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblCategory] ADD  CONSTRAINT [DF_tblCategory_Available]  DEFAULT ((1)) FOR [Available]
GO
/****** Object:  Default [DF_tblCategory_Selectable]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblCategory] ADD  CONSTRAINT [DF_tblCategory_Selectable]  DEFAULT ((1)) FOR [Selectable]
GO
/****** Object:  Default [DF_tblCategory_SuperCategory]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblCategory] ADD  CONSTRAINT [DF_tblCategory_SuperCategory]  DEFAULT ((0)) FOR [SuperCategory]
GO
/****** Object:  Default [DF_tblBigTableIdentity_Guid]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblBigTableIdentity] ADD  CONSTRAINT [DF_tblBigTableIdentity_Guid]  DEFAULT (newid()) FOR [Guid]
GO
/****** Object:  Default [DF_tblBigTable_Row]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblBigTable] ADD  CONSTRAINT [DF_tblBigTable_Row]  DEFAULT ((1)) FOR [Row]
GO
/****** Object:  Default [tblBigTableReference_Index]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblBigTableReference] ADD  CONSTRAINT [tblBigTableReference_Index]  DEFAULT ((1)) FOR [Index]
GO
/****** Object:  Default [DF__aspnet_Us__UserI__1A14E395]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[aspnet_Users] ADD  DEFAULT (newid()) FOR [UserId]
GO
/****** Object:  Default [DF__aspnet_Us__Mobil__1B0907CE]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[aspnet_Users] ADD  DEFAULT (NULL) FOR [MobileAlias]
GO
/****** Object:  Default [DF__aspnet_Us__IsAno__1BFD2C07]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[aspnet_Users] ADD  DEFAULT ((0)) FOR [IsAnonymous]
GO
/****** Object:  Default [DF__aspnet_Pa__PathI__6754599E]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[aspnet_Paths] ADD  DEFAULT (newid()) FOR [PathId]
GO
/****** Object:  Default [DF__aspnet_Ro__RoleI__5070F446]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[aspnet_Roles] ADD  DEFAULT (newid()) FOR [RoleId]
GO
/****** Object:  Default [DF_tblContentTypeToContentType_Access]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContentTypeToContentType] ADD  CONSTRAINT [DF_tblContentTypeToContentType_Access]  DEFAULT ((20)) FOR [Access]
GO
/****** Object:  Default [DF_tblContentTypeToContentType_Availability]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContentTypeToContentType] ADD  CONSTRAINT [DF_tblContentTypeToContentType_Availability]  DEFAULT ((0)) FOR [Availability]
GO
/****** Object:  Default [DF_tblContentTypeDefault_VisibleInMenu]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContentTypeDefault] ADD  CONSTRAINT [DF_tblContentTypeDefault_VisibleInMenu]  DEFAULT ((1)) FOR [VisibleInMenu]
GO
/****** Object:  Default [DF_tblContentTypeDefault_ChildOrderRule]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContentTypeDefault] ADD  CONSTRAINT [DF_tblContentTypeDefault_ChildOrderRule]  DEFAULT ((1)) FOR [ChildOrderRule]
GO
/****** Object:  Default [DF_tblContentTypeDefault_PeerOrder]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContentTypeDefault] ADD  CONSTRAINT [DF_tblContentTypeDefault_PeerOrder]  DEFAULT ((100)) FOR [PeerOrder]
GO
/****** Object:  Default [DF__tblContent__ContentGUID]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContent] ADD  CONSTRAINT [DF__tblContent__ContentGUID]  DEFAULT (newid()) FOR [ContentGUID]
GO
/****** Object:  Default [DF__tblContent__Visible__2E06CDA9]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContent] ADD  CONSTRAINT [DF__tblContent__Visible__2E06CDA9]  DEFAULT ((1)) FOR [VisibleInMenu]
GO
/****** Object:  Default [DF__tblContent__Deleted__2EFAF1E2]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContent] ADD  CONSTRAINT [DF__tblContent__Deleted__2EFAF1E2]  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__tblContent__Pending__31D75E8D]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContent] ADD  CONSTRAINT [DF__tblContent__Pending__31D75E8D]  DEFAULT ((0)) FOR [PendingPublish]
GO
/****** Object:  Default [DF__tblContent__ChildOr__35A7EF71]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContent] ADD  CONSTRAINT [DF__tblContent__ChildOr__35A7EF71]  DEFAULT ((1)) FOR [ChildOrderRule]
GO
/****** Object:  Default [DF__tblContent__PeerOrd__369C13AA]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContent] ADD  CONSTRAINT [DF__tblContent__PeerOrd__369C13AA]  DEFAULT ((100)) FOR [PeerOrder]
GO
/****** Object:  Default [DF__tblContent__fkMasterLangaugeBranchID]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContent] ADD  CONSTRAINT [DF__tblContent__fkMasterLangaugeBranchID]  DEFAULT ((1)) FOR [fkMasterLanguageBranchID]
GO
/****** Object:  Default [DF_tblContent_ContentType]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContent] ADD  CONSTRAINT [DF_tblContent_ContentType]  DEFAULT ((0)) FOR [ContentType]
GO
/****** Object:  Default [[tblIndexRequestLog_Row]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblIndexRequestLog] ADD  CONSTRAINT [[tblIndexRequestLog_Row]  DEFAULT ((1)) FOR [Row]
GO
/****** Object:  Default [DF_tblTask_Status]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblTask] ADD  CONSTRAINT [DF_tblTask_Status]  DEFAULT ((0)) FOR [Status]
GO
/****** Object:  Default [DF_tblTask_Created]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblTask] ADD  CONSTRAINT [DF_tblTask_Created]  DEFAULT (getdate()) FOR [Created]
GO
/****** Object:  Default [DF_tblTask_Changed]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblTask] ADD  CONSTRAINT [DF_tblTask_Changed]  DEFAULT (getdate()) FOR [Changed]
GO
/****** Object:  Default [tblSystemBigTable_Row]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblSystemBigTable] ADD  CONSTRAINT [tblSystemBigTable_Row]  DEFAULT ((1)) FOR [Row]
GO
/****** Object:  Default [DF_tblPropertyDefinition_DefaultValueType]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblPropertyDefinition] ADD  CONSTRAINT [DF_tblPropertyDefinition_DefaultValueType]  DEFAULT ((0)) FOR [DefaultValueType]
GO
/****** Object:  Default [DF_tblPropertyDefinition_LongStringSettings]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblPropertyDefinition] ADD  CONSTRAINT [DF_tblPropertyDefinition_LongStringSettings]  DEFAULT ((-1)) FOR [LongStringSettings]
GO
/****** Object:  Default [DF_tblPropertyDefinition_CommonLang]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblPropertyDefinition] ADD  CONSTRAINT [DF_tblPropertyDefinition_CommonLang]  DEFAULT ((0)) FOR [LanguageSpecific]
GO
/****** Object:  Default [DF_tblPropertyDefinition_ExistsOnModel]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblPropertyDefinition] ADD  CONSTRAINT [DF_tblPropertyDefinition_ExistsOnModel]  DEFAULT ((0)) FOR [ExistsOnModel]
GO
/****** Object:  Default [DF_tblUnifiedPathAcl_IsRole]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblUnifiedPathAcl] ADD  CONSTRAINT [DF_tblUnifiedPathAcl_IsRole]  DEFAULT ((1)) FOR [IsRole]
GO
/****** Object:  Default [tblVisitorGroupStatistic_Row]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblVisitorGroupStatistic] ADD  CONSTRAINT [tblVisitorGroupStatistic_Row]  DEFAULT ((1)) FOR [Row]
GO
/****** Object:  Default [DF_tblXFormData_Row]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblXFormData] ADD  CONSTRAINT [DF_tblXFormData_Row]  DEFAULT ((1)) FOR [Row]
GO
/****** Object:  Default [DF__tblWorkPa__LinkT__48BAC3E5]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblWorkContent] ADD  CONSTRAINT [DF__tblWorkPa__LinkT__48BAC3E5]  DEFAULT ((0)) FOR [LinkType]
GO
/****** Object:  Default [DF__tblWorkPa__Creat__49AEE81E]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblWorkContent] ADD  CONSTRAINT [DF__tblWorkPa__Creat__49AEE81E]  DEFAULT (getdate()) FOR [Created]
GO
/****** Object:  Default [DF__tblWorkPa__Saved__4AA30C57]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblWorkContent] ADD  CONSTRAINT [DF__tblWorkPa__Saved__4AA30C57]  DEFAULT (getdate()) FOR [Saved]
GO
/****** Object:  Default [DF__tblWorkPa__Child__4B973090]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblWorkContent] ADD  CONSTRAINT [DF__tblWorkPa__Child__4B973090]  DEFAULT ((1)) FOR [ChildOrderRule]
GO
/****** Object:  Default [DF__tblWorkPa__PeerO__4C8B54C9]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblWorkContent] ADD  CONSTRAINT [DF__tblWorkPa__PeerO__4C8B54C9]  DEFAULT ((100)) FOR [PeerOrder]
GO
/****** Object:  Default [DF__tblWorkPa__Ready__4D7F7902]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblWorkContent] ADD  CONSTRAINT [DF__tblWorkPa__Ready__4D7F7902]  DEFAULT ((0)) FOR [ReadyToPublish]
GO
/****** Object:  Default [DF__tblWorkPa__Chang__4E739D3B]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblWorkContent] ADD  CONSTRAINT [DF__tblWorkPa__Chang__4E739D3B]  DEFAULT ((0)) FOR [ChangedOnPublish]
GO
/****** Object:  Default [DF__tblWorkPa__HasBe__4F67C174]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblWorkContent] ADD  CONSTRAINT [DF__tblWorkPa__HasBe__4F67C174]  DEFAULT ((0)) FOR [HasBeenPublished]
GO
/****** Object:  Default [DF__tblWorkPa__Rejec__505BE5AD]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblWorkContent] ADD  CONSTRAINT [DF__tblWorkPa__Rejec__505BE5AD]  DEFAULT ((0)) FOR [Rejected]
GO
/****** Object:  Default [DF__tblWorkPa_Delayed]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblWorkContent] ADD  CONSTRAINT [DF__tblWorkPa_Delayed]  DEFAULT ((0)) FOR [DelayedPublish]
GO
/****** Object:  Default [DF__tblWorkPa__fkLan__4258C320]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblWorkContent] ADD  CONSTRAINT [DF__tblWorkPa__fkLan__4258C320]  DEFAULT ((1)) FOR [fkLanguageBranchID]
GO
/****** Object:  Default [DF_tblWorkContent_CommonDraft]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblWorkContent] ADD  CONSTRAINT [DF_tblWorkContent_CommonDraft]  DEFAULT ((0)) FOR [CommonDraft]
GO
/****** Object:  Default [DF__tblProper__fkLan__29B609E9]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentProperty] ADD  CONSTRAINT [DF__tblProper__fkLan__29B609E9]  DEFAULT ((1)) FOR [fkLanguageBranchID]
GO
/****** Object:  Default [DF__tblPropert__guid__43F60EC8]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentProperty] ADD  CONSTRAINT [DF__tblPropert__guid__43F60EC8]  DEFAULT (newid()) FOR [guid]
GO
/****** Object:  Default [DF__tblProper__Boole__44EA3301]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentProperty] ADD  CONSTRAINT [DF__tblProper__Boole__44EA3301]  DEFAULT ((0)) FOR [Boolean]
GO
/****** Object:  Default [DF__tblConten__Activ__51300E55]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentLanguageSetting] ADD  CONSTRAINT [DF__tblConten__Activ__51300E55]  DEFAULT ((1)) FOR [Active]
GO
/****** Object:  Default [DF__tblContentLanguage__ContentGUID]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentLanguage] ADD  CONSTRAINT [DF__tblContentLanguage__ContentGUID]  DEFAULT (newid()) FOR [ContentGUID]
GO
/****** Object:  Default [DF__tblContentLanguage__Automatic]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentLanguage] ADD  CONSTRAINT [DF__tblContentLanguage__Automatic]  DEFAULT ((1)) FOR [AutomaticLink]
GO
/****** Object:  Default [DF__tblContentLanguage__FetchData]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentLanguage] ADD  CONSTRAINT [DF__tblContentLanguage__FetchData]  DEFAULT ((0)) FOR [FetchData]
GO
/****** Object:  Default [DF__tblContentLanguage__PendingPublish]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentLanguage] ADD  CONSTRAINT [DF__tblContentLanguage__PendingPublish]  DEFAULT ((0)) FOR [PendingPublish]
GO
/****** Object:  Default [DF__tblContentLanguage__Created]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentLanguage] ADD  CONSTRAINT [DF__tblContentLanguage__Created]  DEFAULT (getdate()) FOR [Created]
GO
/****** Object:  Default [DF__tblContentLanguage__Changed]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentLanguage] ADD  CONSTRAINT [DF__tblContentLanguage__Changed]  DEFAULT (getdate()) FOR [Changed]
GO
/****** Object:  Default [DF__tblContentLanguage__Saved]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentLanguage] ADD  CONSTRAINT [DF__tblContentLanguage__Saved]  DEFAULT (getdate()) FOR [Saved]
GO
/****** Object:  Default [DF_tblContentCategory_CategoryType]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentCategory] ADD  CONSTRAINT [DF_tblContentCategory_CategoryType]  DEFAULT ((0)) FOR [CategoryType]
GO
/****** Object:  Default [DF_tblContentCategory_LanguageBranchID]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentCategory] ADD  CONSTRAINT [DF_tblContentCategory_LanguageBranchID]  DEFAULT ((1)) FOR [fkLanguageBranchID]
GO
/****** Object:  Default [DF_tblAccess_IsRole]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentAccess] ADD  CONSTRAINT [DF_tblAccess_IsRole]  DEFAULT ((1)) FOR [IsRole]
GO
/****** Object:  Default [DF__aspnet_Perso__Id__72C60C4A]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] ADD  DEFAULT (newid()) FOR [Id]
GO
/****** Object:  Default [DF__aspnet_Me__Passw__2F10007B]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[aspnet_Membership] ADD  DEFAULT ((0)) FOR [PasswordFormat]
GO
/****** Object:  Default [DF__tblWorkPr__Boole__55209ACA]    Script Date: 12/06/2012 12:39:08 ******/
ALTER TABLE [dbo].[tblWorkContentProperty] ADD  CONSTRAINT [DF__tblWorkPr__Boole__55209ACA]  DEFAULT ((0)) FOR [Boolean]
GO
/****** Object:  Default [DF_tblWorkContentCategory_CategoryType]    Script Date: 12/06/2012 12:39:08 ******/
ALTER TABLE [dbo].[tblWorkContentCategory] ADD  CONSTRAINT [DF_tblWorkContentCategory_CategoryType]  DEFAULT ((0)) FOR [CategoryType]
GO
/****** Object:  Check [CK_tblScheduledItem]    Script Date: 12/06/2012 12:39:05 ******/
ALTER TABLE [dbo].[tblScheduledItem]  WITH CHECK ADD  CONSTRAINT [CK_tblScheduledItem] CHECK  (([DatePart]='yy' OR ([DatePart]='mm' OR ([DatePart]='wk' OR ([DatePart]='dd' OR ([DatePart]='hh' OR ([DatePart]='mi' OR ([DatePart]='ss' OR [DatePart]='ms'))))))))
GO
ALTER TABLE [dbo].[tblScheduledItem] CHECK CONSTRAINT [CK_tblScheduledItem]
GO
/****** Object:  Check [CH_tblBigTable]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblBigTable]  WITH CHECK ADD  CONSTRAINT [CH_tblBigTable] CHECK  (([Row]>=(1)))
GO
ALTER TABLE [dbo].[tblBigTable] CHECK CONSTRAINT [CH_tblBigTable]
GO
/****** Object:  Check [CH_tblBigTableReference_Index]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblBigTableReference]  WITH CHECK ADD  CONSTRAINT [CH_tblBigTableReference_Index] CHECK  (([Index]>=(-1)))
GO
ALTER TABLE [dbo].[tblBigTableReference] CHECK CONSTRAINT [CH_tblBigTableReference_Index]
GO
/****** Object:  Check [CH_tblIndexRequestLog]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblIndexRequestLog]  WITH CHECK ADD  CONSTRAINT [CH_tblIndexRequestLog] CHECK  (([Row]>=(1)))
GO
ALTER TABLE [dbo].[tblIndexRequestLog] CHECK CONSTRAINT [CH_tblIndexRequestLog]
GO
/****** Object:  Check [CH_tblSystemBigTable]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblSystemBigTable]  WITH CHECK ADD  CONSTRAINT [CH_tblSystemBigTable] CHECK  (([Row]>=(1)))
GO
ALTER TABLE [dbo].[tblSystemBigTable] CHECK CONSTRAINT [CH_tblSystemBigTable]
GO
/****** Object:  Check [CH_tblVisitorGroupStatistic]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblVisitorGroupStatistic]  WITH CHECK ADD  CONSTRAINT [CH_tblVisitorGroupStatistic] CHECK  (([Row]>=(1)))
GO
ALTER TABLE [dbo].[tblVisitorGroupStatistic] CHECK CONSTRAINT [CH_tblVisitorGroupStatistic]
GO
/****** Object:  Check [CH_tblXFormData]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblXFormData]  WITH CHECK ADD  CONSTRAINT [CH_tblXFormData] CHECK  (([Row]>=(1)))
GO
ALTER TABLE [dbo].[tblXFormData] CHECK CONSTRAINT [CH_tblXFormData]
GO
/****** Object:  ForeignKey [FK_tblCategory_tblCategory]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblCategory]  WITH CHECK ADD  CONSTRAINT [FK_tblCategory_tblCategory] FOREIGN KEY([fkParentID])
REFERENCES [dbo].[tblCategory] ([pkID])
GO
ALTER TABLE [dbo].[tblCategory] CHECK CONSTRAINT [FK_tblCategory_tblCategory]
GO
/****** Object:  ForeignKey [FK_tblBigTable_tblBigTableIdentity]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblBigTable]  WITH CHECK ADD  CONSTRAINT [FK_tblBigTable_tblBigTableIdentity] FOREIGN KEY([pkId])
REFERENCES [dbo].[tblBigTableIdentity] ([pkId])
GO
ALTER TABLE [dbo].[tblBigTable] CHECK CONSTRAINT [FK_tblBigTable_tblBigTableIdentity]
GO
/****** Object:  ForeignKey [FK_tblBigTableStoreInfo_tblBigTableStoreConfig]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblBigTableStoreInfo]  WITH CHECK ADD  CONSTRAINT [FK_tblBigTableStoreInfo_tblBigTableStoreConfig] FOREIGN KEY([fkStoreId])
REFERENCES [dbo].[tblBigTableStoreConfig] ([pkId])
GO
ALTER TABLE [dbo].[tblBigTableStoreInfo] CHECK CONSTRAINT [FK_tblBigTableStoreInfo_tblBigTableStoreConfig]
GO
/****** Object:  ForeignKey [FK_tblBigTableReference_RefId_tblBigTableIdentity]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblBigTableReference]  WITH CHECK ADD  CONSTRAINT [FK_tblBigTableReference_RefId_tblBigTableIdentity] FOREIGN KEY([RefIdValue])
REFERENCES [dbo].[tblBigTableIdentity] ([pkId])
GO
ALTER TABLE [dbo].[tblBigTableReference] CHECK CONSTRAINT [FK_tblBigTableReference_RefId_tblBigTableIdentity]
GO
/****** Object:  ForeignKey [FK_tblBigTableReference_tblBigTableIdentity]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblBigTableReference]  WITH CHECK ADD  CONSTRAINT [FK_tblBigTableReference_tblBigTableIdentity] FOREIGN KEY([pkId])
REFERENCES [dbo].[tblBigTableIdentity] ([pkId])
GO
ALTER TABLE [dbo].[tblBigTableReference] CHECK CONSTRAINT [FK_tblBigTableReference_tblBigTableIdentity]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__Appli__1920BF5C]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[aspnet_Users]  WITH CHECK ADD FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
/****** Object:  ForeignKey [FK__aspnet_Pa__Appli__66603565]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[aspnet_Paths]  WITH CHECK ADD FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
/****** Object:  ForeignKey [FK__aspnet_Ro__Appli__4F7CD00D]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[aspnet_Roles]  WITH CHECK ADD FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
/****** Object:  ForeignKey [FK_tblContentTypeToContentType_tblContentType]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContentTypeToContentType]  WITH CHECK ADD  CONSTRAINT [FK_tblContentTypeToContentType_tblContentType] FOREIGN KEY([fkContentTypeParentID])
REFERENCES [dbo].[tblContentType] ([pkID])
GO
ALTER TABLE [dbo].[tblContentTypeToContentType] CHECK CONSTRAINT [FK_tblContentTypeToContentType_tblContentType]
GO
/****** Object:  ForeignKey [FK_tblContentTypeToContentType_tblContentType1]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContentTypeToContentType]  WITH CHECK ADD  CONSTRAINT [FK_tblContentTypeToContentType_tblContentType1] FOREIGN KEY([fkContentTypeChildID])
REFERENCES [dbo].[tblContentType] ([pkID])
GO
ALTER TABLE [dbo].[tblContentTypeToContentType] CHECK CONSTRAINT [FK_tblContentTypeToContentType_tblContentType1]
GO
/****** Object:  ForeignKey [FK_tblContentTypeDefault_tblContentType]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContentTypeDefault]  WITH CHECK ADD  CONSTRAINT [FK_tblContentTypeDefault_tblContentType] FOREIGN KEY([fkContentTypeID])
REFERENCES [dbo].[tblContentType] ([pkID])
GO
ALTER TABLE [dbo].[tblContentTypeDefault] CHECK CONSTRAINT [FK_tblContentTypeDefault_tblContentType]
GO
/****** Object:  ForeignKey [FK_tblContent_tblContent]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContent]  WITH CHECK ADD  CONSTRAINT [FK_tblContent_tblContent] FOREIGN KEY([fkParentID])
REFERENCES [dbo].[tblContent] ([pkID])
GO
ALTER TABLE [dbo].[tblContent] CHECK CONSTRAINT [FK_tblContent_tblContent]
GO
/****** Object:  ForeignKey [FK_tblContent_tblContentType]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContent]  WITH CHECK ADD  CONSTRAINT [FK_tblContent_tblContentType] FOREIGN KEY([fkContentTypeID])
REFERENCES [dbo].[tblContentType] ([pkID])
GO
ALTER TABLE [dbo].[tblContent] CHECK CONSTRAINT [FK_tblContent_tblContentType]
GO
/****** Object:  ForeignKey [FK_tblContent_tblLanguageBranch]    Script Date: 12/06/2012 12:39:06 ******/
ALTER TABLE [dbo].[tblContent]  WITH CHECK ADD  CONSTRAINT [FK_tblContent_tblLanguageBranch] FOREIGN KEY([fkMasterLanguageBranchID])
REFERENCES [dbo].[tblLanguageBranch] ([pkID])
GO
ALTER TABLE [dbo].[tblContent] CHECK CONSTRAINT [FK_tblContent_tblLanguageBranch]
GO
/****** Object:  ForeignKey [FK_tblIndexRequestLog_tblBigTableIdentity]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblIndexRequestLog]  WITH CHECK ADD  CONSTRAINT [FK_tblIndexRequestLog_tblBigTableIdentity] FOREIGN KEY([pkId])
REFERENCES [dbo].[tblBigTableIdentity] ([pkId])
GO
ALTER TABLE [dbo].[tblIndexRequestLog] CHECK CONSTRAINT [FK_tblIndexRequestLog_tblBigTableIdentity]
GO
/****** Object:  ForeignKey [FK_tblTask_tblPlugIn]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblTask]  WITH CHECK ADD  CONSTRAINT [FK_tblTask_tblPlugIn] FOREIGN KEY([fkPlugInID])
REFERENCES [dbo].[tblPlugIn] ([pkID])
GO
ALTER TABLE [dbo].[tblTask] CHECK CONSTRAINT [FK_tblTask_tblPlugIn]
GO
/****** Object:  ForeignKey [FK_tblSystemBigTable_tblBigTableIdentity]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblSystemBigTable]  WITH CHECK ADD  CONSTRAINT [FK_tblSystemBigTable_tblBigTableIdentity] FOREIGN KEY([pkId])
REFERENCES [dbo].[tblBigTableIdentity] ([pkId])
GO
ALTER TABLE [dbo].[tblSystemBigTable] CHECK CONSTRAINT [FK_tblSystemBigTable_tblBigTableIdentity]
GO
/****** Object:  ForeignKey [FK_tblPropertyDefinition_tblContentType]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblPropertyDefinition]  WITH CHECK ADD  CONSTRAINT [FK_tblPropertyDefinition_tblContentType] FOREIGN KEY([fkContentTypeID])
REFERENCES [dbo].[tblContentType] ([pkID])
GO
ALTER TABLE [dbo].[tblPropertyDefinition] CHECK CONSTRAINT [FK_tblPropertyDefinition_tblContentType]
GO
/****** Object:  ForeignKey [FK_tblRelation_tblItem_FromId]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblRelation]  WITH CHECK ADD  CONSTRAINT [FK_tblRelation_tblItem_FromId] FOREIGN KEY([FromId])
REFERENCES [dbo].[tblItem] ([pkID])
GO
ALTER TABLE [dbo].[tblRelation] CHECK CONSTRAINT [FK_tblRelation_tblItem_FromId]
GO
/****** Object:  ForeignKey [FK_tblRelation_tblItem_ToId]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblRelation]  WITH CHECK ADD  CONSTRAINT [FK_tblRelation_tblItem_ToId] FOREIGN KEY([ToId])
REFERENCES [dbo].[tblItem] ([pkID])
GO
ALTER TABLE [dbo].[tblRelation] CHECK CONSTRAINT [FK_tblRelation_tblItem_ToId]
GO
/****** Object:  ForeignKey [fk_tblScheduledItemLog_tblScheduledItem]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblScheduledItemLog]  WITH CHECK ADD  CONSTRAINT [fk_tblScheduledItemLog_tblScheduledItem] FOREIGN KEY([fkScheduledItemId])
REFERENCES [dbo].[tblScheduledItem] ([pkID])
GO
ALTER TABLE [dbo].[tblScheduledItemLog] CHECK CONSTRAINT [fk_tblScheduledItemLog_tblScheduledItem]
GO
/****** Object:  ForeignKey [FK_tblUnifiedPathProperty_tblUnifiedPath]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblUnifiedPathProperty]  WITH CHECK ADD  CONSTRAINT [FK_tblUnifiedPathProperty_tblUnifiedPath] FOREIGN KEY([fkUnifiedPathID])
REFERENCES [dbo].[tblUnifiedPath] ([pkID])
GO
ALTER TABLE [dbo].[tblUnifiedPathProperty] CHECK CONSTRAINT [FK_tblUnifiedPathProperty_tblUnifiedPath]
GO
/****** Object:  ForeignKey [FK_tblUnifiedPathAcl_tblUnifiedPath]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblUnifiedPathAcl]  WITH CHECK ADD  CONSTRAINT [FK_tblUnifiedPathAcl_tblUnifiedPath] FOREIGN KEY([fkUnifiedPathID])
REFERENCES [dbo].[tblUnifiedPath] ([pkID])
GO
ALTER TABLE [dbo].[tblUnifiedPathAcl] CHECK CONSTRAINT [FK_tblUnifiedPathAcl_tblUnifiedPath]
GO
/****** Object:  ForeignKey [FK_tblVisitorGroupStatistic_tblBigTableIdentity]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblVisitorGroupStatistic]  WITH CHECK ADD  CONSTRAINT [FK_tblVisitorGroupStatistic_tblBigTableIdentity] FOREIGN KEY([pkId])
REFERENCES [dbo].[tblBigTableIdentity] ([pkId])
GO
ALTER TABLE [dbo].[tblVisitorGroupStatistic] CHECK CONSTRAINT [FK_tblVisitorGroupStatistic_tblBigTableIdentity]
GO
/****** Object:  ForeignKey [FK_tblSchemaItem_tblSchema]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblSchemaItem]  WITH CHECK ADD  CONSTRAINT [FK_tblSchemaItem_tblSchema] FOREIGN KEY([fkSchemaId])
REFERENCES [dbo].[tblSchema] ([pkID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[tblSchemaItem] CHECK CONSTRAINT [FK_tblSchemaItem_tblSchema]
GO
/****** Object:  ForeignKey [FK_tblWindowsRelations_Group]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblWindowsRelations]  WITH CHECK ADD  CONSTRAINT [FK_tblWindowsRelations_Group] FOREIGN KEY([fkWindowsGroup])
REFERENCES [dbo].[tblWindowsGroup] ([pkID])
GO
ALTER TABLE [dbo].[tblWindowsRelations] CHECK CONSTRAINT [FK_tblWindowsRelations_Group]
GO
/****** Object:  ForeignKey [FK_tblWindowsRelations_User]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblWindowsRelations]  WITH CHECK ADD  CONSTRAINT [FK_tblWindowsRelations_User] FOREIGN KEY([fkWindowsUser])
REFERENCES [dbo].[tblWindowsUser] ([pkID])
GO
ALTER TABLE [dbo].[tblWindowsRelations] CHECK CONSTRAINT [FK_tblWindowsRelations_User]
GO
/****** Object:  ForeignKey [FK_tblXFormData_tblBigTableIdentity]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblXFormData]  WITH CHECK ADD  CONSTRAINT [FK_tblXFormData_tblBigTableIdentity] FOREIGN KEY([pkId])
REFERENCES [dbo].[tblBigTableIdentity] ([pkId])
GO
ALTER TABLE [dbo].[tblXFormData] CHECK CONSTRAINT [FK_tblXFormData_tblBigTableIdentity]
GO
/****** Object:  ForeignKey [FK_tblWorkContent_tblContent]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblWorkContent]  WITH CHECK ADD  CONSTRAINT [FK_tblWorkContent_tblContent] FOREIGN KEY([fkContentID])
REFERENCES [dbo].[tblContent] ([pkID])
GO
ALTER TABLE [dbo].[tblWorkContent] CHECK CONSTRAINT [FK_tblWorkContent_tblContent]
GO
/****** Object:  ForeignKey [FK_tblWorkContent_tblFrame]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblWorkContent]  WITH CHECK ADD  CONSTRAINT [FK_tblWorkContent_tblFrame] FOREIGN KEY([fkFrameID])
REFERENCES [dbo].[tblFrame] ([pkID])
GO
ALTER TABLE [dbo].[tblWorkContent] CHECK CONSTRAINT [FK_tblWorkContent_tblFrame]
GO
/****** Object:  ForeignKey [FK_tblWorkContent_tblLanguageBranch]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblWorkContent]  WITH CHECK ADD  CONSTRAINT [FK_tblWorkContent_tblLanguageBranch] FOREIGN KEY([fkLanguageBranchID])
REFERENCES [dbo].[tblLanguageBranch] ([pkID])
GO
ALTER TABLE [dbo].[tblWorkContent] CHECK CONSTRAINT [FK_tblWorkContent_tblLanguageBranch]
GO
/****** Object:  ForeignKey [FK_tblWorkContent_tblWorkContent2]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblWorkContent]  WITH CHECK ADD  CONSTRAINT [FK_tblWorkContent_tblWorkContent2] FOREIGN KEY([fkMasterVersionID])
REFERENCES [dbo].[tblWorkContent] ([pkID])
GO
ALTER TABLE [dbo].[tblWorkContent] CHECK CONSTRAINT [FK_tblWorkContent_tblWorkContent2]
GO
/****** Object:  ForeignKey [FK_tblContentSoftlink_tblContent]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentSoftlink]  WITH CHECK ADD  CONSTRAINT [FK_tblContentSoftlink_tblContent] FOREIGN KEY([fkOwnerContentID])
REFERENCES [dbo].[tblContent] ([pkID])
GO
ALTER TABLE [dbo].[tblContentSoftlink] CHECK CONSTRAINT [FK_tblContentSoftlink_tblContent]
GO
/****** Object:  ForeignKey [FK_tblContentProperty_tblContent]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentProperty]  WITH CHECK ADD  CONSTRAINT [FK_tblContentProperty_tblContent] FOREIGN KEY([fkContentID])
REFERENCES [dbo].[tblContent] ([pkID])
GO
ALTER TABLE [dbo].[tblContentProperty] CHECK CONSTRAINT [FK_tblContentProperty_tblContent]
GO
/****** Object:  ForeignKey [FK_tblContentProperty_tblContent2]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentProperty]  WITH CHECK ADD  CONSTRAINT [FK_tblContentProperty_tblContent2] FOREIGN KEY([ContentLink])
REFERENCES [dbo].[tblContent] ([pkID])
GO
ALTER TABLE [dbo].[tblContentProperty] CHECK CONSTRAINT [FK_tblContentProperty_tblContent2]
GO
/****** Object:  ForeignKey [FK_tblContentProperty_tblLanguageBranch]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentProperty]  WITH CHECK ADD  CONSTRAINT [FK_tblContentProperty_tblLanguageBranch] FOREIGN KEY([fkLanguageBranchID])
REFERENCES [dbo].[tblLanguageBranch] ([pkID])
GO
ALTER TABLE [dbo].[tblContentProperty] CHECK CONSTRAINT [FK_tblContentProperty_tblLanguageBranch]
GO
/****** Object:  ForeignKey [FK_tblContentProperty_tblPropertyDefinition]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentProperty]  WITH CHECK ADD  CONSTRAINT [FK_tblContentProperty_tblPropertyDefinition] FOREIGN KEY([fkPropertyDefinitionID])
REFERENCES [dbo].[tblPropertyDefinition] ([pkID])
GO
ALTER TABLE [dbo].[tblContentProperty] CHECK CONSTRAINT [FK_tblContentProperty_tblPropertyDefinition]
GO
/****** Object:  ForeignKey [FK_tblContentLanguageSetting_tblContent]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentLanguageSetting]  WITH CHECK ADD  CONSTRAINT [FK_tblContentLanguageSetting_tblContent] FOREIGN KEY([fkContentID])
REFERENCES [dbo].[tblContent] ([pkID])
GO
ALTER TABLE [dbo].[tblContentLanguageSetting] CHECK CONSTRAINT [FK_tblContentLanguageSetting_tblContent]
GO
/****** Object:  ForeignKey [FK_tblContentLanguageSetting_tblLanguageBranch1]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentLanguageSetting]  WITH CHECK ADD  CONSTRAINT [FK_tblContentLanguageSetting_tblLanguageBranch1] FOREIGN KEY([fkLanguageBranchID])
REFERENCES [dbo].[tblLanguageBranch] ([pkID])
GO
ALTER TABLE [dbo].[tblContentLanguageSetting] CHECK CONSTRAINT [FK_tblContentLanguageSetting_tblLanguageBranch1]
GO
/****** Object:  ForeignKey [FK_tblContentLanguageSetting_tblLanguageBranch2]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentLanguageSetting]  WITH CHECK ADD  CONSTRAINT [FK_tblContentLanguageSetting_tblLanguageBranch2] FOREIGN KEY([fkReplacementBranchID])
REFERENCES [dbo].[tblLanguageBranch] ([pkID])
GO
ALTER TABLE [dbo].[tblContentLanguageSetting] CHECK CONSTRAINT [FK_tblContentLanguageSetting_tblLanguageBranch2]
GO
/****** Object:  ForeignKey [FK_tblContentLanguage_tblContent2]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentLanguage]  WITH CHECK ADD  CONSTRAINT [FK_tblContentLanguage_tblContent2] FOREIGN KEY([fkContentID])
REFERENCES [dbo].[tblContent] ([pkID])
GO
ALTER TABLE [dbo].[tblContentLanguage] CHECK CONSTRAINT [FK_tblContentLanguage_tblContent2]
GO
/****** Object:  ForeignKey [FK_tblContentLanguage_tblFrame]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentLanguage]  WITH CHECK ADD  CONSTRAINT [FK_tblContentLanguage_tblFrame] FOREIGN KEY([fkFrameID])
REFERENCES [dbo].[tblFrame] ([pkID])
GO
ALTER TABLE [dbo].[tblContentLanguage] CHECK CONSTRAINT [FK_tblContentLanguage_tblFrame]
GO
/****** Object:  ForeignKey [FK_tblContentLanguage_tblLanguageBranch]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentLanguage]  WITH CHECK ADD  CONSTRAINT [FK_tblContentLanguage_tblLanguageBranch] FOREIGN KEY([fkLanguageBranchID])
REFERENCES [dbo].[tblLanguageBranch] ([pkID])
GO
ALTER TABLE [dbo].[tblContentLanguage] CHECK CONSTRAINT [FK_tblContentLanguage_tblLanguageBranch]
GO
/****** Object:  ForeignKey [FK_tblContentCategory_tblCategory]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentCategory]  WITH CHECK ADD  CONSTRAINT [FK_tblContentCategory_tblCategory] FOREIGN KEY([fkCategoryID])
REFERENCES [dbo].[tblCategory] ([pkID])
GO
ALTER TABLE [dbo].[tblContentCategory] CHECK CONSTRAINT [FK_tblContentCategory_tblCategory]
GO
/****** Object:  ForeignKey [FK_tblContentCategory_tblContent]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentCategory]  WITH CHECK ADD  CONSTRAINT [FK_tblContentCategory_tblContent] FOREIGN KEY([fkContentID])
REFERENCES [dbo].[tblContent] ([pkID])
GO
ALTER TABLE [dbo].[tblContentCategory] CHECK CONSTRAINT [FK_tblContentCategory_tblContent]
GO
/****** Object:  ForeignKey [FK_tblContentAccess_tblContent]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[tblContentAccess]  WITH CHECK ADD  CONSTRAINT [FK_tblContentAccess_tblContent] FOREIGN KEY([fkContentID])
REFERENCES [dbo].[tblContent] ([pkID])
GO
ALTER TABLE [dbo].[tblContentAccess] CHECK CONSTRAINT [FK_tblContentAccess_tblContent]
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__PathI__73BA3083]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser]  WITH CHECK ADD FOREIGN KEY([PathId])
REFERENCES [dbo].[aspnet_Paths] ([PathId])
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__UserI__74AE54BC]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
/****** Object:  ForeignKey [FK__aspnet_Pr__UserI__440B1D61]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[aspnet_Profile]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
/****** Object:  ForeignKey [FK__aspnet_Me__Appli__2D27B809]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[aspnet_Membership]  WITH CHECK ADD FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
/****** Object:  ForeignKey [FK__aspnet_Me__UserI__2E1BDC42]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[aspnet_Membership]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__PathI__6E01572D]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[aspnet_PersonalizationAllUsers]  WITH CHECK ADD FOREIGN KEY([PathId])
REFERENCES [dbo].[aspnet_Paths] ([PathId])
GO
/****** Object:  ForeignKey [FK__aspnet_Us__RoleI__5629CD9C]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[aspnet_UsersInRoles]  WITH CHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[aspnet_Roles] ([RoleId])
GO
/****** Object:  ForeignKey [FK__aspnet_Us__UserI__5535A963]    Script Date: 12/06/2012 12:39:07 ******/
ALTER TABLE [dbo].[aspnet_UsersInRoles]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
/****** Object:  ForeignKey [FK_tblWorkContentProperty_tblContent]    Script Date: 12/06/2012 12:39:08 ******/
ALTER TABLE [dbo].[tblWorkContentProperty]  WITH CHECK ADD  CONSTRAINT [FK_tblWorkContentProperty_tblContent] FOREIGN KEY([ContentLink])
REFERENCES [dbo].[tblContent] ([pkID])
GO
ALTER TABLE [dbo].[tblWorkContentProperty] CHECK CONSTRAINT [FK_tblWorkContentProperty_tblContent]
GO
/****** Object:  ForeignKey [FK_tblWorkContentProperty_tblContentType]    Script Date: 12/06/2012 12:39:08 ******/
ALTER TABLE [dbo].[tblWorkContentProperty]  WITH CHECK ADD  CONSTRAINT [FK_tblWorkContentProperty_tblContentType] FOREIGN KEY([ContentType])
REFERENCES [dbo].[tblContentType] ([pkID])
GO
ALTER TABLE [dbo].[tblWorkContentProperty] CHECK CONSTRAINT [FK_tblWorkContentProperty_tblContentType]
GO
/****** Object:  ForeignKey [FK_tblWorkContentProperty_tblPropertyDefinition]    Script Date: 12/06/2012 12:39:08 ******/
ALTER TABLE [dbo].[tblWorkContentProperty]  WITH CHECK ADD  CONSTRAINT [FK_tblWorkContentProperty_tblPropertyDefinition] FOREIGN KEY([fkPropertyDefinitionID])
REFERENCES [dbo].[tblPropertyDefinition] ([pkID])
GO
ALTER TABLE [dbo].[tblWorkContentProperty] CHECK CONSTRAINT [FK_tblWorkContentProperty_tblPropertyDefinition]
GO
/****** Object:  ForeignKey [FK_tblWorkContentProperty_tblWorkContent]    Script Date: 12/06/2012 12:39:08 ******/
ALTER TABLE [dbo].[tblWorkContentProperty]  WITH CHECK ADD  CONSTRAINT [FK_tblWorkContentProperty_tblWorkContent] FOREIGN KEY([fkWorkContentID])
REFERENCES [dbo].[tblWorkContent] ([pkID])
GO
ALTER TABLE [dbo].[tblWorkContentProperty] CHECK CONSTRAINT [FK_tblWorkContentProperty_tblWorkContent]
GO
/****** Object:  ForeignKey [FK_tblWorkContentCategory_tblCategory]    Script Date: 12/06/2012 12:39:08 ******/
ALTER TABLE [dbo].[tblWorkContentCategory]  WITH CHECK ADD  CONSTRAINT [FK_tblWorkContentCategory_tblCategory] FOREIGN KEY([fkCategoryID])
REFERENCES [dbo].[tblCategory] ([pkID])
GO
ALTER TABLE [dbo].[tblWorkContentCategory] CHECK CONSTRAINT [FK_tblWorkContentCategory_tblCategory]
GO
/****** Object:  ForeignKey [FK_tblWorkContentCategory_tblWorkContent]    Script Date: 12/06/2012 12:39:08 ******/
ALTER TABLE [dbo].[tblWorkContentCategory]  WITH CHECK ADD  CONSTRAINT [FK_tblWorkContentCategory_tblWorkContent] FOREIGN KEY([fkWorkContentID])
REFERENCES [dbo].[tblWorkContent] ([pkID])
GO
ALTER TABLE [dbo].[tblWorkContentCategory] CHECK CONSTRAINT [FK_tblWorkContentCategory_tblWorkContent]
GO
