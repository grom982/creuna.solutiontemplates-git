﻿using NUnit.Framework;

namespace $safeprojectname$
{
    [TestFixture]
    public abstract class TestBase
    {
        [TestFixtureSetUp]
        public void SetUp()
        {
            Arrange();
        }

        protected abstract void Arrange();

        [TestFixtureTearDown]
        public void TearDown()
        {
            Cleanup();
        }

        protected virtual void Cleanup(){}
    }
}