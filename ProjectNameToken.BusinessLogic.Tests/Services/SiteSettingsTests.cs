﻿using EPiServer;
using EPiServer.Core;
using ProjectNameToken.BusinessLogic.Components;
using ProjectNameToken.Common.Contracts;
using NUnit.Framework;
using Rhino.Mocks;

namespace $safeprojectname$.Services
{
    class SiteSettingsTests : TestBase
    {
        private ISiteSettings _instance;
        private IContentRepository _contentRepository;

        protected override void Arrange()
        {
            _contentRepository = MockRepository.GenerateStub<IContentRepository>();
        }

        //[Test]
        //public void It_Should_Return_Start_Page()
        //{
        //    var startPage = MockRepository.GenerateStub<PageData>();
        //    _contentRepository.Stub(x => x.Get<PageData>(ContentReference.StartPage)).Return(startPage);

        //    _instance = new SiteSettings(_contentRepository, MockRepository.GenerateStub<IConfigurationManagerWrapper>());
        //    var actualStartPage = _instance.FrontPage;

        //    Assert.AreSame(startPage, actualStartPage);
        //}

        [Test]
        public void It_Should_Call_Configuration_Wrapper()
        {
            var mocks = new MockRepository();
            var configWrapper = mocks.StrictMock<IConfigurationManagerWrapper>();
            Expect.Call(configWrapper.IsDebugEnabled()).Return(false);
            mocks.ReplayAll();

            _instance = new SiteSettings(_contentRepository, configWrapper);
            Assert.AreEqual(false, _instance.MinificationEnabled);
            mocks.VerifyAll();
        }

    }
}