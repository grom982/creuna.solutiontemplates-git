﻿using Creuna.IoC;

namespace ProjectNameToken.Common
{
    public static class New
    {
        public static TContract InstanceOf<TContract>()
        {
            var result = Give.Me.InstanceOf<TContract>();
            return result;
        }

        public static TContract NamedInstanceOf<TContract>(string name)
        {
            var result = Give.Me.NamedInstanceOf<TContract>(name);
            return result;
        }
    }
}
