DECLARE @dbname nvarchar(128)
SET @dbname = N'__databaseName'

IF (EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE ('[' + name + ']' = @dbname OR name = @dbname)))
	
BEGIN
select '__databaseExistsMessage' as Result
END
ELSE BEGIN
select '__databaseNotExistsMessage' as Result
END