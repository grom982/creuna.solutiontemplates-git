﻿function Update-AllKits()
{	
	$kits = Get-Kit
	
	foreach ($kit in $kits)
	{
		Update-Kit -id $kit.id
	}
}

function Update-Kit()
{
	param ([string]$id = $(Read-Host -Prompt "Id"))
	
	$kit = EnsureKit -Id $id
	
	if ($kit -ne $null)
	{
		$kitIsInstalled = IsKitInstalled -Id $kit.Id -Version $kit.Version
		
		if ($kitIsInstalled -eq $true)
		{
			foreach ($package in $kit.Packages)
			{
				UpdatePackage -Target $package.Target -PackageId $package.Id -InstallPackage $false
			}
		}
		else
		{
			Write-Host 'Kit is not installed'
		}
	}
}

function New-Database()
{
	param ([string]$initialCatalog = '',
	[string]$dataSource = '',
	[string]$userId = '',
	[string]$password = '',
	[string]$adminLogin = '',
	[string]$adminPassword = ''
	)
	
	$defaultParameters = GetDefaultConnectionParameters
	
	$parameters = GetConnectionParameters -adminLogin $adminLogin -adminPassword $adminPassword -dataSource $dataSource -defaultParameters $defaultParameters -initialCatalog $initialCatalog -password $password -userId $userId
	
	$databaseExists = CheckDatabaseExists -parameters $parameters
	
	if ($databaseExists -eq $false)
	{	
		$scriptsRoot = Get-SqlScriptsRoot 
		$scriptPath = $scriptsRoot + 'CreateDatabase.sql'
	    
		$initialCatalog = $parameters.InitialCatalog
		
		if (UseIntegratedSecurity -parameters $parameters)
		{
			sqlcmd -E -t 10 -S $parameters.DataSource -i $scriptPath -v Db=$initialCatalog
		}
		else
		{
			sqlcmd -U $parameters.AdminLogin -P $parameters.AdminPassword -t 10 -S $parameters.DataSource -i $scriptPath -v Db=$initialCatalog
		}
		
		Write-Host 'Database created'
		
		SetDatabaseOwner -parameters $parameters
		ApplyData -parameters $parameters
	}
}

function Get-Kit()
{	
	param ([string]$id, [switch]$Installed)
			
	$kitsPath = Get-KitsRoot
	
	$kits = @()
	
	$Dir = get-childitem $kitsPath -Force -recurse

	$kitConfigs = $Dir | where {$_.extension -eq ".kitspec"}

	foreach ($kitConfig in $kitConfigs)
	{
		if ($kitConfig -ne $null)
		{
			$kit = ParseKitConfig -kitConfig $kitConfig.FullName
			
			if ($kit -eq $null)
			{
				Write-Host 'kit is null'
			}
			else
			{
				$kits += $kit
			}
		}
		else
		{
			Write-Host 'kitConfig is null'
		}
	}
	
	if ($Installed -eq $true)
	{
		$result = @()
		
		foreach ($kit in $kits)
		{
			$kitIsInstalled = IsKitInstalled -Id $kit.Id -Version $kit.Version
			
			if ($kitIsInstalled -eq $true)
			{
				$result += $kit
			}
		}
		
		return $result
	}
	
	if ($id -ne $null -and $id -ne '')
	{
		$kits = ($kits | Where-Object {$_.Id -eq $id}) | Sort-Object Version -Descending
	}
	
	return $kits
}

function Install-Kit
{
    param ([string]$Id = $(Read-Host -Prompt "Id"), [string]$Version)
			
	$kit = EnsureKit -id $Id -Version $Version
	
	if ($kit -ne $null)
	{
		$kitIsInstalled = IsKitInstalled -Id $kit.Id -Version $kit.Version
		
		if ($kitIsInstalled -eq $true)
		{
			Write-Host 'Kits is already installed'
		}
		else
		{
			foreach ($package in $kit.Packages)
			{
				InstallUninstallPackage -Target $package.Target -PackageId $package.Id -InstallPackage $true
			}
			
			AddKitToKitsConfig -kitId $kit.Id -kitVersion $kit.Version
		}
	}
}

function Uninstall-Kit()
{
	param ([string]$Id = $(Read-Host -Prompt "Id"), [string]$Version)
		
	$kit = EnsureKit -id $Id -Version $Version
	
	if ($kit -ne $null)
	{
		$kitIsInstalled = IsKitInstalled -Id $kit.Id -Version $kit.Version
		
		if ($kitIsInstalled -eq $true)
		{
			foreach ($package in $kit.Packages)
			{
				InstallUninstallPackage -Target $package.Target -PackageId $package.Id -InstallPackage $false
			}
			
			RemoveKitFromKitsConfig -kitId $kit.Id -kitVersion $kit.Version
		}
		else
		{
			Write-Host 'Kit is not installed'
		}
	}
}

function SetDatabaseOwner()
{
	param ([Object]$parameters = $null)
	
	if ($parameters -eq $null)
	{
		Write-Host 'Connection parameters not defined'
		return $null
	}
	
	$databaseExists = CheckDatabaseExists -parameters $parameters
	
	if ($databaseExists -eq $true)
	{
		$scriptsRoot = Get-SqlScriptsRoot 
		$scriptPath = $scriptsRoot + 'SetDatabaseOwner.sql'
		
		$initialCatalog = $parameters.InitialCatalog
		$userId = $parameters.UserId
		
		if (UseIntegratedSecurity -parameters $parameters)
		{
			sqlcmd -E -t 10 -S $parameters.DataSource -i $scriptPath -v Db=$initialCatalog -v Login=$userId
		}
		else
		{
			sqlcmd -U $parameters.AdminLogin -P $parameters.AdminPassword -t 10 -S $parameters.DataSource -i $scriptPath -v Db=$initialCatalog -v Login=$userId
		}
		
		
		Write-Host 'Database owner set'
	}
}

function ApplyData()
{
	param ([Object]$parameters = $null)
	
	if ($parameters -eq $null)
	{
		Write-Host 'Connection parameters not defined'
		return $null
	}
	
	$databaseExists = CheckDatabaseExists -parameters $parameters
	
	if ($databaseExists -eq $true)
	{
		$scriptsRoot = Get-SqlScriptsRoot 
		$scriptPath = $scriptsRoot + 'ApplyData.sql'
	    		
		$initialCatalog = $parameters.InitialCatalog
		
		if (UseIntegratedSecurity -parameters $parameters)
		{
			sqlcmd -E -t 10 -S $parameters.DataSource -i $scriptPath  -v Db=$initialCatalog
		}
		else
		{
			sqlcmd -U $parameters.AdminLogin -P $parameters.AdminPassword -t 10 -S $parameters.DataSource -i $scriptPath  -v Db=$initialCatalog
		}
		
		
		Write-Host 'Data is applied'
	}
}

function CheckDatabaseExists()
{
	param ([Object]$parameters = $null)
	
	if ($parameters -eq $null)
	{
		Write-Host 'Connection parameters not defined'
		return $null
	}
	
	$databaseExistsMessage = 'DATABASE_EXISTS'
	$databaseNotExistsMessage = 'DATABASE_NOT_EXISTS'
	
	$connectionString = GetConnectionStringByParameters -parameters $parameters
		
	$scriptsRoot = Get-SqlScriptsRoot 
	$scriptPath = $scriptsRoot + 'CheckDatabaseExists.sql'
		
	$checkDatabaseExistScript = Get-Content $scriptPath
	
	$checkDatabaseExistScript = $checkDatabaseExistScript -replace '__databaseName', $initialCatalog `
	-replace '__databaseExistsMessage', $databaseExistsMessage `
	-replace '__databaseNotExistsMessage', $databaseNotExistsMessage
	
	$connection = new-object System.Data.SqlClient.SqlConnection ($connectionString)

    $connection.Open()
	
	$checkDatabaseExistsCmd = new-object "System.Data.SqlClient.SqlCommand" ($checkDatabaseExistScript, $connection)
	
	$result = $checkDatabaseExistsCmd.ExecuteScalar()
	
	if ($result -eq $databaseExistsMessage)
	{
		Write-Host 'Database already exists'
		return $true
	}
	
	if ($result -eq $databaseNotExistsMessage)
	{
		Write-Host 'Database does not exist'
		return $false
	}
	
	return $null
}

function EnsureKit()
{
	param ([string]$Id = $(Read-Host -Prompt "Id"), [string]$Version)
	
	$kit = Get-SingleKit -Id $Id -Version $Version
	
	if ($kit -eq $null)
	{
		Write-Host 'Kit was not found'
	}
	
	return $kit
}

function Creuna-Get-Projects()
{
	$debug = IsDebug
	if ($debug -eq $false)
	{
		$result = @(get-project -all)
		return $result
	}
	else
	{
		$proj1 = New-Object System.Object
		
		$proj1 | Add-Member -type NoteProperty -Name Id -Value 1
		$proj1 | Add-Member -type NoteProperty -Name Name -Value "ProjectNameToken.BusinessLogic"

		$proj2 = New-Object System.Object
			
		$proj2 | Add-Member -type NoteProperty -Name Id -Value 2
		$proj2 | Add-Member -type NoteProperty -Name Name -Value "ProjectNameToken.Common"
		
		$proj3 = New-Object System.Object
			
		$proj3 | Add-Member -type NoteProperty -Name Id -Value 3
		$proj3 | Add-Member -type NoteProperty -Name Name -Value "ProjectNameToken.ContentModels"
		
		$proj4 = New-Object System.Object
			
		$proj4 | Add-Member -type NoteProperty -Name Id -Value 4
		$proj4 | Add-Member -type NoteProperty -Name Name -Value "ProjectNameToken.DomainModels"

		$proj5 = New-Object System.Object
			
		$proj5 | Add-Member -type NoteProperty -Name Id -Value 5
		$proj5 | Add-Member -type NoteProperty -Name Name -Value "ProjectNameToken.Orchestrators"
		
		$proj6 = New-Object System.Object
			
		$proj6 | Add-Member -type NoteProperty -Name Id -Value 6
		$proj6 | Add-Member -type NoteProperty -Name Name -Value "ProjectNameToken.Presentation"
		$proj6 | Add-Member -type NoteProperty -Name FullName -Value "C:\Projects\SolutionTemplate\ProjectNameToken.Presentation\ProjectNameToken.Presentation.csproj"
		
		$proj7 = New-Object System.Object
			
		$proj7 | Add-Member -type NoteProperty -Name Id -Value 7
		$proj7 | Add-Member -type NoteProperty -Name Name -Value "ProjectNameToken.ServiceAgents"
		
		$proj8 = New-Object System.Object
			
		$proj8 | Add-Member -type NoteProperty -Name Id -Value 8
		$proj8 | Add-Member -type NoteProperty -Name Name -Value "ProjectNameToken.ViewModels"
		
		$result = @($proj1, $proj2, $proj3, $proj4, $proj5, $proj6, $proj7, $proj8)
		
		return $result
	}
}

function GetCommandsPackagePath()
{
	param ([string]$packageId, [string]$packageVersion)
	$packageName = $packageId + '.' + $packageVersion
	
	return 'packages\packageName' -replace 'packageName', $packageName
}

function GetCommandsPackageConfig
{
	param ([string]$packageId, [string]$packageVersion)
	$packagePath = GetCommandsPackagePath -packageId $packageId -packageVersion $packageVersion
	
	$result = '<commands path="packagePath" id="packageId" version="packageVersion"/>' -replace "packagePath", $packagePath -replace "packageId", $PackageId -replace "packageVersion", $packageVersion
	
	return $result
}

function Confirm-KitsConfig()
{
	param ([string]$PackageId = $(Read-Host -Prompt "PackageId"), [string]$PackageVersion = $(Read-Host -Prompt "PackageVersion"))
	
	$filePath = Get-KitsConfigPath
	
    if (-not(test-path $filePath)) 
    {
		$commandsPackageConfig = GetCommandsPackageConfig -packageId $packageId -packageVersion $packageVersion
		$fileContent = 
'<kits>
    commandsPackageConfig
    <installed/>
</kits>' -replace 'commandsPackageConfig', $commandsPackageConfig
	
        New-Item $filePath -type file -Value $fileContent
    }
    else
    {
        $config = Get-KitsConfig
		
		$currentPackage = $config.kits.SelectSingleNode("commands")
		
		if ($currentPackage -ne $null)
		{
			$currentPackageId = $currentPackage.GetAttribute("id")
			$currentPackageVersion = $currentPackage.GetAttribute("version")
			
			if ($packageVersion -gt $currentPackageVersion)
			{
				$packagePath = GetCommandsPackagePath -packageId $packageId -packageVersion $packageVersion
				
				$currentPackage.SetAttribute("path", $packagePath)
				$currentPackage.SetAttribute("version", $packageVersion)
				
				$config.Save($filePath)
			}
		}
    }
}

function IsKitInstalled()
{
	param ([string]$Id = $(Read-Host -Prompt "Id"), [string]$Version = $(Read-Host -Prompt "Version"))
	
	$kit = Get-InstalledKitConfigElement -Id $Id -Version $Version
	
	if ($kit -eq $null)
	{
		return $false
	}
	else
	{
		return $true
	}
}

function Get-InstalledKitConfigElement()
{
	param ([string]$Id = $(Read-Host -Prompt "Id"), [string]$Version = $(Read-Host -Prompt "Version"), [Xml]$config)
	
	if ($config -eq $null)
	{
		$config = Get-KitsConfig
	}
	
	$expression = "/kits/installed/kit[@id='$id' and @version='$version']" -replace '$id', $Id -replace '$version', $Version
	
	$kit = $config.SelectSingleNode($expression)
	
	return $kit
}

function Get-SingleKit
{
	param ([string]$Id = $(Read-Host -Prompt "Id"), [string]$Version)
	
	$kit = get-kit $Id
	
	if ($kit -ne $null)
	{
		if ($Version -eq $null -or $Version -eq "")
		{
			if ($kit.Count -gt 1)
			{
				$kit = ($kit | Sort-Object Version -Descending)[0] # latest version
			}
		}
		else
		{
			$kit = ($kit | Where-Object {$_.Version -eq $Version})
		}
	}
	
	return $kit
}

function UpdatePackage()
{
	param (
		[string]$Target = $(Read-Host -Prompt "Target"),
		[string]$PackageId = $(Read-Host -Prompt "PackageId"),
		[bool]$InstallPackage = $(Read-Host -Prompt "InstallPackage")
	)
	
	$projectToInstall = GetProjectToInstall -target $Target

	if ($projectToInstall -eq $null)
	{
		Write-Error 'Project was not found for target ' $Target
	}
	else
	{
		$debug = IsDebug
		if ($debug -eq $false)
		{
			$existedPackages = Get-Package -ListAvailable -Filter $PackageId
			
			if ($existedPackages -eq $null)
			{
				Write-Host 'Package cannot be installed because it cannot be found: ' $packageId
			}
			else
			{
				Update-Package $PackageId -Project $projectToInstall.Name
			}
		}
		else
		{
			Write-Host 'PACKAGE SHOULD BE UPDATED'
		}
	}
}

function InstallUninstallPackage()
{
	param (
		[string]$Target = $(Read-Host -Prompt "Target"),
		[string]$PackageId = $(Read-Host -Prompt "PackageId"),
		[bool]$InstallPackage = $(Read-Host -Prompt "InstallPackage")
	)
	
	$projectToInstall = GetProjectToInstall -target $Target

	if ($projectToInstall -eq $null)
	{
		Write-Error 'Project was not found for target ' $Target
	}
	else
	{
		$debug = IsDebug
		if ($InstallPackage -eq $true)
		{
			if ($debug -eq $false)
			{
				$existedPackages = Get-Package -ListAvailable -Filter $PackageId
				
				if ($existedPackages -eq $null)
				{
					Write-Host 'Package cannot be installed because it cannot be found: ' $packageId
				}
				else
				{
					Install-Package $PackageId -Project $projectToInstall.Name
				}
			}
			else
			{
				Write-Host 'PACKAGE SHOULD BE INSTALLED'
			}
		}
		else
		{
			if ($debug -eq $false)
			{
				$existedPackages = Get-Package -ListAvailable -Filter $PackageId
				
				if ($existedPackages -eq $null)
				{
					Write-Host 'Package cannot be uninstalled because it cannot be found: ' $packageId
				}
				else
				{
					Uninstall-Package $PackageId -Project $projectToInstall.Name
				}
			}
			else
			{
				Write-Host 'PACKAGE SHOULD BE UNINSTALLED'
			}
		}		
	}
}

function GetProjectToInstall()
{
	param ([string]$Target = $(Read-Host -Prompt "Target"))
	
	$projects = Creuna-Get-Projects
	
	$projectToInstall = ($projects | Where-Object {$_.Name -cmatch $Target})
	
	return $projectToInstall
}

function ParseKitConfig
{
    param ([string]$kitConfig)    
	
    $xml = $null;
    $xml = New-Object XML
    $xml.Load($kitConfig)
    
	$objKit = New-Object System.Object
		
	$objKit | Add-Member -type NoteProperty -Name Id -Value $xml.Kit.metadata.SelectSingleNode("id").get_InnerXml()
	$objKit | Add-Member -type NoteProperty -Name Version -Value $xml.Kit.metadata.SelectSingleNode("version").get_InnerXml()
	$objKit | Add-Member -type NoteProperty -Name Authors -Value $xml.Kit.metadata.SelectSingleNode("authors").get_InnerXml()
	$objKit | Add-Member -type NoteProperty -Name Owners -Value $xml.Kit.metadata.SelectSingleNode("owners").get_InnerXml()
	$objKit | Add-Member -type NoteProperty -Name Description -Value $xml.Kit.metadata.SelectSingleNode("description").get_InnerXml()
	
	$packages = $xml.SelectNodes("/kit/packages/package")
	
	$objKit | Add-Member -type NoteProperty -Name Packages -Value @()
	
	foreach ($package in $packages)
	{
		$objPackage = New-Object System.Object
		
		$objPackage | Add-Member -type NoteProperty -Name Id -Value $package.GetAttribute("id")
		$objPackage | Add-Member -type NoteProperty -Name Version -Value $package.GetAttribute("version")
		$objPackage | Add-Member -type NoteProperty -Name Target -Value $package.GetAttribute("target")
		
		$objKit.Packages += $objPackage
	}
	
	return $objKit
}

function AddKitToKitsConfig()
{
	param ([string]$kitId = $(Read-Host -Prompt "kitId"), [string]$kitVersion)
	
	$kitsConfig = Get-KitsConfig
			
	$kitElement = $kitsConfig.CreateElement('kit')
	$kitElement.SetAttribute('id', $kitId)
	$kitElement.SetAttribute('version', $kitVersion)
	
	$installedElement = $kitsConfig.kits.SelectSingleNode("installed")
	
	if ($installedElement -eq $null)
	{
		$installedElement = $kitsConfig.CreateElement('installed')
		$installedElement.AppendChild($kitElement)

		$kitsConfig.kits.AppendChild($installedElement)
	}
	else
	{				
		$installedElement.AppendChild($kitElement)
	}
	
	$kitsPath = Get-KitsConfigPath
	
	$kitsConfig.Save($kitsPath)
}

function RemoveKitFromKitsConfig()
{
	param ([string]$kitId = $(Read-Host -Prompt "Id"), [string]$kitVersion)
	
	$kitsConfig = Get-KitsConfig
	$installedKitElement = Get-InstalledKitConfigElement -Id $kitId -Version $kitVersion -config $kitsConfig
	
	$installedKitElement.ParentNode.RemoveChild($installedKitElement)
	
	$kitsPath = Get-KitsConfigPath
	
	$kitsConfig.Save($kitsPath)
}

function Get-KitsConfig()
{
	$kitsPath = Get-KitsConfigPath
	
	$xml = New-Object XML;
    
    $xml.Load($kitsPath)
	
	return $xml
}

function Get-KitsConfigPath()
{
	$solutionPath = Get-SolutionPath
	$kitsPath = $solutionPath + '.kits'
	
	return $kitsPath
}

function Get-KitsRoot()
{
	$config = Get-KitsConfig
	$solutionPath = Get-SolutionPath
    
	$result = $config.kits.commands.GetAttribute("path")
	
	$result = $solutionPath + $result + '\tools\kits\'
	
	return $result
}

function Get-SqlScriptsRoot()
{
	$config = Get-KitsConfig
	$solutionPath = Get-SolutionPath
    
	$result = $config.kits.commands.GetAttribute("path")
	
	$result = $solutionPath + $result + '\tools\SQL scripts\'
	
	return $result
}

function UseIntegratedSecurity()
{
	param ([Object]$parameters = $null) # throw exception if not defined
	
	if ($parameters.AdminLogin -eq 'ADMIN' -and $parameters.AdminPassword -eq 'ADMIN')
	{
		return $true
	}
	else
	{
		return $false
	}
}

function GetConnectionStringByParameters()
{
	param ([Object]$parameters = $null) # throw exception if not defined
	
	$login = $parameters.AdminLogin
	$password = $parameters.AdminPassword
	$connectionString = ''
	
	if (UseIntegratedSecurity -parameters $parameters)
	{
		$connectionString = "Persist Security Info=False;Integrated Security=true;Data Source={0};Initial Catalog={1};" -f $parameters.DataSource, 'master'
	}
	else
	{
		$connectionString = "Data Source={0};Initial Catalog={1};User ID={2};Password={3}" -f $parameters.DataSource, 'master', $parameters.UserId, $parameters.Password
	}
	
	return $connectionString
}


function GetConnectionString()
{
	$presentationProject = Creuna-Get-Projects | Where-Object {$_.Name -cmatch 'Presentation'}
	$presentationProjectFile = New-Object System.IO.FileInfo($presentationProject.FullName)
	$presentationProjectFolder = $presentationProjectFile.Directory.FullName
	$connectionStringsPath = $presentationProjectFolder + '\configs\connectionStrings.config'
	
	$connectionStrings = New-Object XML;
    
    $connectionStrings.Load($connectionStringsPath)
	
	$connectionStringName = 'EPiServerDB'
	$expression = "/connectionStrings/add[@name='connectionStringName']" -replace 'connectionStringName', $connectionStringName
	
	$connectionString = $connectionStrings.SelectSingleNode($expression)
	
	$result = ''
	
	if ($connectionString -ne $null)
	{
		$result = $connectionString.GetAttribute("connectionString")
	}
	
	return $result
}

function GetDefaultConnectionParameters()
{
	$connectionString = GetConnectionString
	#Data Source=.\SQLEXPRESS;Initial Catalog=ProjectNameToken.local;Integrated Security=False;User ID=epiuser;Password=Newuser12;MultipleActiveResultSets=True;Connect Timeout=10
	
	$splittedConnectionString = $connectionString.Split(';')
	
	$dataSource = ''
	$initialCatalog = ''
	$userId = ''
	$password = ''
	$adminLogin = 'ADMIN'
	$adminPassword = 'ADMIN'
	
	foreach ($keyValuePair in $splittedConnectionString)
	{
		$parameter = $keyValuePair.Split('=')
		
		if ($parameter.Count -eq 2)
		{
			$parameterName = $parameter[0]
			$parameterValue = $parameter[1]
			
			switch ($parameterName)
			{
				{$_ -eq 'data source'}{$dataSource = $parameterValue}
				{$_ -eq 'initial catalog'}{$initialCatalog = $parameterValue}
				{$_ -eq 'user id'}{$userId = $parameterValue}
				{$_ -eq 'password'}{$password = $parameterValue}
			}
		}
	}
	
	$obj = New-Object System.Object
	
	$obj | Add-Member -type NoteProperty -Name DataSource -Value $dataSource
	$obj | Add-Member -type NoteProperty -Name InitialCatalog -Value $initialCatalog
	$obj | Add-Member -type NoteProperty -Name UserId -Value $userId
	$obj | Add-Member -type NoteProperty -Name Password -Value $password
	$obj | Add-Member -type NoteProperty -Name AdminLogin -Value 'ADMIN'
	$obj | Add-Member -type NoteProperty -Name AdminPassword -Value 'ADMIN'
	
	return $obj
}

function GetConnectionParameters()
{
	param ([string]$dataSource = '',
		[string]$initialCatalog = '',
		[string]$userId = '',
		[string]$password = '',
		[string]$adminLogin = '',
		[string]$adminPassword = '',
		[Object]$defaultParameters = $null
		)
		
	if ($defaultParameters -eq $null)
	{
		# should we return user defined parameters instead?
		return $null
	}
	
	if ($dataSource -ne '')
	{
		$defaultParameters.DataSource = $dataSource
	}
	
	if ($initialCatalog -ne '')
	{
		$defaultParameters.InitialCatalog = $initialCatalog
	}
	
	if ($userId -ne '')
	{
		$defaultParameters.UserId = $userId
	}
	
	if ($password -ne '')
	{
		$defaultParameters.Password = $password
	}
	
	if ($adminLogin -ne '')
	{
		$defaultParameters.AdminLogin = $adminLogin
	}
	
	if ($adminPassword -ne '')
	{
		$defaultParameters.AdminPassword = $adminPassword
	}
	
	return $defaultParameters
}

function Get-SolutionPath()
{
	$solutionFile = New-Object System.IO.FileInfo($dte.Solution.FullName)
	$solutionPath =  $solutionFile.Directory.FullName + '\'
	
	return $solutionPath
}

function IsDebug()
{
	return $false;
}

Export-ModuleMember -function Update-AllKits
Export-ModuleMember -function Update-Kit
Export-ModuleMember -function New-Database
Export-ModuleMember -function Get-Kit
Export-ModuleMember -function Install-Kit
Export-ModuleMember -function Uninstall-Kit
Export-ModuleMember -Function Confirm-KitsConfig
