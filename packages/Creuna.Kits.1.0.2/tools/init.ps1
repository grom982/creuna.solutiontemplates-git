﻿param($installPath, $toolsPath, $package)

Import-Module (Join-Path $toolsPath Commands.psm1)

Confirm-KitsConfig -packageId $package.Id -packageVersion $package.Version