﻿using System;
using Creuna.Common.EPiServer.Extensions;
using EPiServer.Core;

namespace ProjectNameToken.ContentModels.Pages.Base
{
    public static class BasePageExtensions
    {
        public static string GetPageIdentifierString(PageData pageData)
        {
            if (pageData == null)
            {
                return "<no page>";
            }

            var result = string.Format("'{0}' (Page ID: {1}, Page Type: {2})",
                pageData.PageName,
                pageData.PageLink.ID,
                pageData.PageTypeName);

            return result;
        }

        public static TReferencedPage GetRequiredSetting<TReferencedPage>(this PageBase currentPage,
            PageReference pageLink, string propertyName)
            where TReferencedPage : class, IContentData
        {
            TReferencedPage result;

            string pageName = GetPageIdentifierString(currentPage);

            if (PageReference.IsNullOrEmpty(pageLink) || (result = pageLink.TryGetContent<TReferencedPage>()) == null)
            {
                throw new InvalidOperationException(string.Format(
                    "Configuration parameter '{0}' is not properly set on the {1}. ",
                    propertyName, pageName));
            }

            return result;
        }

        public static PageReference GetRequiredSetting(this PageBase currentPage, PageReference pageLink, string propertyName)
        {
            var result = pageLink;

            string pageName = GetPageIdentifierString(currentPage);

            if (PageReference.IsNullOrEmpty(result))
            {
                throw new InvalidOperationException(string.Format(
                    "Configuration parameter '{0}' is not properly set on the {1}. ",
                    propertyName, pageName));
            }

            return result;
        }
    }
}