﻿using EPiServer.DataAnnotations;

namespace ProjectNameToken.ContentModels.Pages
{
    [ContentType(DisplayName = "[Container] Folder", GUID = "{528F664C-852F-4999-87B9-B1251A6A5376}")]
    public partial class FolderPage : Base.PageBase
    {}
}