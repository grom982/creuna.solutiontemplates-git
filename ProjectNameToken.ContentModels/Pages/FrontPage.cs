﻿using EPiServer.DataAnnotations;

namespace ProjectNameToken.ContentModels.Pages
{
    [ContentType(DisplayName = "[Front] Site Root", GUID = "a4b10781-e954-4b2c-b94e-15379553acb5", Description = "")]
    public partial class FrontPage : Base.PageBase
    {

    }
}