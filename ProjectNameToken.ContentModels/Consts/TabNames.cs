﻿namespace ProjectNameToken.ContentModels.Consts
{
    /// <summary>
    /// Admin mode tab names to use with DisplayAttribute. 
    /// </summary>
    public class TabNames
    {
        public const string SiteSettings = "Site Settings";
    }
}
