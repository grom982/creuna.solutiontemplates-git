@echo off

rem set version=%~1

rem if "%version%"=="" (
rem 	echo Please pass version to build, i.e.
rem 	echo build 1.0.0.0
rem 	exit -1
rem )

set zip="C:\Program Files\7-Zip\7z.exe"
set outputFile=%~dp0output\Creuna.Templates.EPi7.zip
set includeList=.buildfiles

del /F/Q "%outputFile%">nul

call %zip% a -r -tzip "%outputFile%" @"%includeList%"

rem echo call %zip% a -r -tzip "%outputFile%" @"%includeList%"