﻿using NUnit.Framework;

namespace ProjectNameToken.UnitTests
{
    public class TestBase
    {
        [SetUp]
        public virtual void Setup()
        {}

        [TearDown]
        public virtual void Cleanup()
        {}
    }
}
